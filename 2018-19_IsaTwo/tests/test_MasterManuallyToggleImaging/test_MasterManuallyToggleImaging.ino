
#include "IsaTwoConfig.h"
#include "Wire.h"

void setup() {
  Serial.begin(115200);
  while (!Serial) ;
  Serial << ENDL;
  Serial << "=============================================" << ENDL;
  Serial << "=    Utility to manually toggle imager      =" << ENDL;
  Serial << "=============================================" << ENDL<<ENDL;
   Serial << "Configuring as output and setting to HIGH" << ENDL;

  Wire.begin();
  pinMode(ImagerCtrl_MasterDigitalPinNbr, OUTPUT);
  digitalWrite(ImagerCtrl_MasterDigitalPinNbr, HIGH);

  Serial << "Enter any character followed by ENTER to toggle the imager" << ENDL;
}

void toggleImager() {
  if (digitalRead(ImagerCtrl_MasterDigitalPinNbr) == LOW) {
    Serial << "Deactivating Imager..." << ENDL;
    digitalWrite(ImagerCtrl_MasterDigitalPinNbr, HIGH);
  } else {
    Serial << "Activating Imager at " << millis() << "..." << ENDL;
    digitalWrite(ImagerCtrl_MasterDigitalPinNbr, LOW);
  }
}

void loop() {
  if (Serial.available()) {
    toggleImager();
    while (Serial.available()) {
      Serial.read();
    }
  }
}
