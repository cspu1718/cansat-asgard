/*
   TestBeacon.ino
   An emmitter providing regular emission of a constant message according to the configuration parameters below.

*/

/**************configuration***************/
constexpr uint16_t DataSize = 50;
uint8_t data[DataSize] = "I'm your test beacon!";
constexpr uint32_t EmissionPeriod = 500; // ms.
constexpr float RF69_Frequency = 434.2; // Mhz
constexpr bool UseEncryption = true;
constexpr bool UseAddressedMessages = false;
constexpr int8_t RF_Power = 20;  // For RF69_HWC, valid range is 14 to 20 ? to be confirmed.

/**** End of configuration ************************

*/
#include <SPI.h>
#include <RH_RF69.h>
#include "elapsedMillis.h"
#include "DebugCSPU.h"

/************ Radio Setup ***************/

#if (defined ARDUINO_AVR_UNO) // Carte Uno
#  define RFM69_CS      10
#  define RFM69_INT     9
#  define RFM69_RST     6
#  define LED           13
#elif (defined ARDUINO_SAMD_FEATHER_M0_EXPRESS)
#  define RFM69_CS      10
#  define RFM69_INT     9
#  define RFM69_RST     6
#  define LED           13
#else
#  error "Unknown board type"
#endif

// Singleton instance of the radio driver
RH_RF69 rf69(RFM69_CS, RFM69_INT);
elapsedMillis elapsed(0);  // Time elapsed since last emission

void setup()
{
  DINIT(115200);
  pinMode(LED, OUTPUT);
  pinMode(RFM69_RST, OUTPUT);
  digitalWrite(RFM69_RST, LOW);

  Serial << "TEST BEACON PROGRAM" << ENDL;
  Serial << "-------------------" << ENDL;

  // manual reset
  digitalWrite(RFM69_RST, HIGH);
  delay(10);
  digitalWrite(RFM69_RST, LOW);
  delay(10);

  if (!rf69.init()) {
    Serial.println("RFM69 radio init failed. Aborted.");
    while (1);
  }
  Serial.println("RFM69 radio init OK!");

  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dBm (for low power module)
  // No encryption
  if (!rf69.setFrequency(RF69_Frequency)) {
    Serial.println("setFrequency failed. Aborted.");
    while (true);
  }

  // If you are using a high power RF69 eg RFM69HW, you *must* set a Tx power with the
  // ishighpowermodule flag set like this:
  rf69.setTxPower(RF_Power, true);  // range from 14-20 for high power version, 2nd arg must be true for 69HW
  // range from -18-13 for low power version, 2nd arg must be false for 69HCW

  // The encryption key has to be the same as the one in the server
  uint8_t key[16] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08
                  }; // does it work without a known encryption key ?
  if (UseEncryption) {
    rf69.setEncryptionKey(key);
    Serial.print("encryption key is");
    Serial.print("key [16]");
  }
 
  Serial << "RFM69 radio @" << RF69_Frequency << " MHz" << ENDL;
  Serial << "  Power level: " << RF_Power << ", encryption: " << (UseEncryption ? "Yes" : "No") << ", addressed: " << (UseAddressedMessages ? "Yes" : "No") << ENDL;
  Serial << "  Emission period: " << EmissionPeriod << " msec." << ENDL;
}
void sendPacket() {
  if (elapsed > EmissionPeriod) {
    rf69.send(data, strlen((char *) data)+1);
    rf69.waitPacketSent();
    Serial << millis() << ": Sent a packet" << ENDL;
    digitalWrite(LED, !digitalRead(LED));
    elapsed = 0;
  }
}

void loop() {
  sendPacket();
}
