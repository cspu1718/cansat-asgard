/**
    Utility program for calibrating a RF source.
    Allows to select interactively one of the beacons or one of the tracked cans and:
    - measures RSSI every DelayBetweenMeasures msec
    - provides average on NumSamplesToAverage sample collected with
      an interruption of more than MaxDelayBetweenAveragedSamples msec.
    - provides the interval in which all averagd measures have been received.
*/

#include <RH_RF69.h>
#include "EtheraConfig.h"
#include "CSPU_Debug.h"
#include <math.h>
#include "RSSI_Listener.h"
#include "CSPU_Test.h"

constexpr uint32_t DelayBetweenMeasures = 600; // msec
constexpr unsigned int NumSamplesToAverage=10;
constexpr uint32_t MaxDelayBetweenAveragedSamples=3000; // msec

RSSI_Listener listener;
float frequency;
elapsedMillis idleDuration;

// 2 constant needed
// set the beacon to 3 meter
// need to tell to enter manually data in
#ifndef ADAFRUIT_FEATHER_M0_EXPRESS // FEATHER_M0_EXPRESS w/ radio
#  error "This program only works for FeatherM0 Express and Arduino Uno"
#endif

RH_RF69 rf69(RFM69_ChipSelectPin, RFM69_InterruptPin);



constexpr float RF69_Frequency = BeaconFrequency[0]; // Mhz

const int maxIrrelevantValue = 0;
const int minIrrelevantValue = -100;
float zeroConstants;
float rssiZero = 0;
int distanceZero;// meter
const int NumSamples = 5;


void zeroConstanstsConverter() {

  Serial << " enter the distance between the receiver and the beacon an integer in meter (distanceZero= " << ENDL;
  while (distanceZero <= 0) {
    if (Serial.available()) {
      distanceZero = Serial.parseInt();
    }
  }
  Serial << " x0 = " << distanceZero << "m" << ENDL;
  if (!listener.measureRSSI0(distanceZero, 1000)) {
    Serial << "ERROR COMPUTING RSSI0" << ENDL;
    exit(-1);
  }
}

void setup() {
  DINIT(115200);
  Serial << ENDL << ENDL;
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(RFM69_ResetPin, OUTPUT);
  digitalWrite(RFM69_ResetPin, LOW);

  Serial.println("Ethera calibration utility");
  Serial.println("--------------------------");
  Serial.println();

  // manual reset
  digitalWrite(RFM69_ResetPin, HIGH);
  delay(10);
  digitalWrite(RFM69_ResetPin, LOW);
  delay(10);

  if (!rf69.init()) {
    Serial << "RFM69 radio init failed. Aborted." << ENDL;
    while (1);
  }
  Serial.println("RFM69 radio init OK!");

  bool calibrateBeacon = CSPU_Test::inputSingleDigitInteger("Do you want to calibrate a Beacon (1) or a Can (2)", 1, 2) == 1 ? true : false;
  uint8_t maxIdx = calibrateBeacon ? NumBeacons - 1 : NumConfiguredCans - 1;
  uint8_t sourceIdx;
  if (calibrateBeacon) {
    sourceIdx = CSPU_Test::inputSingleDigitInteger("Input the ID of the Beacon to be calibrated", 0, maxIdx);
  } else {
    sourceIdx = CSPU_Test::inputSingleDigitInteger("Input the ID of the Can to be calibrated", 0, maxIdx);
  }

  frequency = calibrateBeacon ?  BeaconFrequency[sourceIdx] : CanFrequency[sourceIdx];
  bool encrypt = calibrateBeacon ?  BeaconUseEncryption[sourceIdx] : CanUseEncryption[sourceIdx];
  unsigned char key[CanEncryptionKeySize];
  if (calibrateBeacon) {
    memcpy(key, BeaconEncryptionKey[sourceIdx], BeaconEncryptionKeySize);
  } else {
    memcpy(key, CanEncryptionKey[sourceIdx], BeaconEncryptionKeySize);
  }
  if (calibrateBeacon) {
    listener.begin(rf69, frequency, encrypt, key, RSSI_MeasurementTimeout, sourceIdx);
  }
  else {
     listener.begin(rf69, frequency, encrypt, key, RSSI_MeasurementTimeout, -1);
  }
  Serial << "Listener initialized for ";
  if (calibrateBeacon) Serial << "BEACON";
  else {
    Serial << "CAN '" << CanName[sourceIdx] << "'";
  }
  Serial << " #" << sourceIdx << ENDL;
  Serial << "Frequency: " << frequency << " MHz, ";
  if (encrypt) Serial << "encrypted";
  else Serial << "NOT encrypted";
  Serial << ENDL << "-----------------------------" << ENDL;
  

  Serial << "Place source at known x0 distance, and press any key when ready" << ENDL;
  CSPU_Test::pressAnyKey();
  Serial << "Starting measurements (averaging every "<< NumSamplesToAverage << " measures)..." << ENDL; 
  idleDuration=0;
}

void performAveraging(float rssi) {
	static float sum=0;
	static int count = 0;
  static int minRSSI=100, maxRSSI=-10000; 
	// If too large delay since previous sample, reset sum
    if (idleDuration > MaxDelayBetweenAveragedSamples) {
    	sum = 0;
    	count = 0;
      minRSSI=100;
      maxRSSI=-10000;
    }
	sum += rssi;
	count++;
  if (rssi < minRSSI) minRSSI=rssi;
  if (rssi > maxRSSI) maxRSSI=rssi;
	if (count == NumSamplesToAverage) {
		Serial << "Average on last " << NumSamplesToAverage << " samples: " ;
		Serial.print(sum/NumSamplesToAverage, 1);
		Serial << " dBm. Measures in [" << minRSSI << ";" << maxRSSI << "]" << ENDL << ENDL;
		sum = 0;
		count = 0;
    minRSSI=100;
    maxRSSI=-10000;
	}
}

void loop() {
  float rssi;
  if (listener.getRSSI(rssi)) {
    Serial << frequency << " MHz: RSSI =  ";
    Serial.println(rssi, 1);

    performAveraging(rssi);

    digitalWrite(LED_BUILTIN, true);
    delay(DelayBetweenMeasures/2);
    digitalWrite(LED_BUILTIN, false);
    delay(DelayBetweenMeasures/2);
    idleDuration = 0;
  }
  if (idleDuration > 5000) {
    Serial << "Not receiving: is the source on ? " << ENDL;
    idleDuration = 0;
  }

}
