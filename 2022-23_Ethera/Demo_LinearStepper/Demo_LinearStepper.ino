/*
  Utility program which just moves the linear steppers to demonstrate
  the air brakes. 
  In interactive mode (constant Interactive = True), it can be used with a USB port connected to a serial monitor.
  When Interactive = False it can be used in a stand-alone can. 
*/


#include "LinearStepper.h"
#include "CSPU_Debug.h"
#include "EtheraConfig.h"
#include "CSPU_Test.h"

//---------------- CONFIGURATION ---------------------------------------------------
constexpr bool Interactive = false; // Set to true to select parameters manually, 
                                    // False to use in a stand-alone can. 

constexpr uint32_t DelayInMsec = 2000;
constexpr uint8_t MaxStepperPosition = 55;
constexpr uint16_t NumStepsToForce= 200; // The number of steps used to force to zéro. 

// -----------------------------------------------------------------------------------

LinearStepper myStepper;

void forceToZero() {
    // Just force to 0 a couple of times
    Serial << "  Forcing to zéro by " << NumStepsToForce << " steps..." << ENDL;
    for (int i =0; i < 10;i++) {
      myStepper.forceToZero(NumStepsToForce);
    }
}

void setup() {
  if (Interactive) {
    DINIT(115200);
  } else {
    DINIT_WITH_TIMEOUT(USB_SerialBaudRate);
  }
  
  Serial << "Demo Linear Stepper: it will be actuators from 0 to " << MaxStepperPosition << " and back, continuously): " << ENDL;
  Serial << "When reaching 0, it will force to 0 10 times, by " << NumStepsToForce << " steps." << ENDL;
  Serial << "  Pins: dir=" << LinearStepperDirectionPin << ", step=" << LinearStepperStepPin << ", sleep=" << LinearStepperSleepPin << ENDL;
  Serial << "  Total number of steps:" << LinearStepperNumSteps << ENDL;

  uint8_t slowDownFactor = LinearStepperSlowDownFactor;
  if (Interactive) {
    Serial << "The demo can run at maximum speed or be slowed down by a factor 1 (max speed) to 9 (min speed)." << ENDL;
    Serial << "Slow down factor from EtheraConfig.h: " << LinearStepperSlowDownFactor << ENDL;
    slowDownFactor = CSPU_Test::inputSingleDigitInteger("How many times do you to slow down ?", 1, 9);
  }
  Serial << "  Slow-down factor: " << slowDownFactor << ENDL;
  myStepper.begin( LinearStepperDirectionPin, LinearStepperStepPin, LinearStepperSleepPin, LinearStepperNumSteps);
  myStepper.setSlowDownFactor(slowDownFactor);
  Serial << "Init ok" << ENDL;
  // We assume original position is max. open. 
  myStepper.setCurrentPosition(MaxStepperPosition);

  if (Interactive) {
    myStepper.resetPosition();
    while (!CSPU_Test::askYN("Are the stepper motor at position 0 ?")) {
      Serial << "  Forcing " << NumStepsToForce << " steps beyond 0..." << ENDL;
      myStepper.forceToZero(NumStepsToForce);
    }
  } else {
    forceToZero();
  }
  Serial << "Launching demo..." << ENDL;
}

void loop() {
  delay(DelayInMsec);
  if (myStepper.getPosition() == 0) {
    myStepper.setPosition(MaxStepperPosition);
  }
  else {
    forceToZero();
  }
}
