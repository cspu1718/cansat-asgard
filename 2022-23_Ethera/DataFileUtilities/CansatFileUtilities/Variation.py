from math import fabs


class Variation:
    """ A class for managing a variaotion a simulated data. It maintains a current value, which is
        increase or decreased, once or repetitively, whenever method get_increment() is called.
        Usage:
           myValue = 5
           myVariation = Variation(10, 5, True)
           (when ever the value must change)
           myValue += myVariation.get_increment()"""
    def __init__(self, max_delta, num_steps, repeat=False):
        """Initialise the requested variation
        Parameters
        ----------
        max_delta: integer or float, positive or negative
            The maximum variation to be applied.
        num_steps: integer
            The number of steps to use to vary from 0 to max_delta
        repeat: boolean
            If true, the variation is applied repetitively, alternately increasing and decreasing the value.
            If false, the variation stops after the max_delta is reached.
        """
        assert max_delta != 0
        assert num_steps > 0
        self.repeat = repeat
        self.current = 0
        self.maxDelta = max_delta
        self.increment = fabs(max_delta / num_steps)
        self.increasing = (max_delta > 0)
        self.done = False

    def __str__(self):
        return "max_delta={}, increment={}, increasing={}, repeat={}, done={}".format(self.maxDelta,
                                                                                      self.increment,
                                                                                      self.increasing,
                                                                                      self.repeat,
                                                                                      self.done)

    def get_increment(self):
        """Obtain the variation to apply in the current record"""
        if self.done:
            return 0

        if self.increasing:
            self.current += self.increment
            if ((self.maxDelta > 0) and (self.current > self.maxDelta)) or ((self.maxDelta < 0) and (self.current > 0)):
                if self.repeat:
                    self.current -= 2 * self.increment
                    self.increasing = False
                else:
                    self.done = True
        else:
            self.current -= self.increment
            if ((self.maxDelta < 0) and (self.current < self.maxDelta)) or ((self.maxDelta > 0) and (self.current < 0)):
                if self.repeat:
                    self.current += 2 * self.increment
                    self.increasing = True
                else:
                    self.done = True
        if self.done:
            return 0
        else:
            if self.increasing:
                return self.increment
            else:
                return -self.increment
