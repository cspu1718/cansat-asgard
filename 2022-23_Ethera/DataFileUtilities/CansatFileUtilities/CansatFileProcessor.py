from os import path
from datetime import datetime
from csv import reader as csv_reader

from . import CansatProcessorParamFile
from .constants import M_PER_DEGREE_LONGITUDE, M_PER_DEGREE_LATITUDE, \
                        NUM_PRIMARY_MISSION_FIELDS, \
                        TIMESTAMP_FIELD_IDX, \
                        NEW_GPS_DATA_FIELD_IDX, \
                        GPS_LONGITUDE_FIELD_IDX, GPS_LATITUDE_FIELD_IDX, \
                        ALTITUDE_FIELD_IDX, REF_ALTITUDE_FIELD_IDX, \
                        THERM1_TEMP_FIELD_IDX
from .GooglePathCreator import *


class CansatFileProcessor:
    """ A class implementing a processor to handle Cansat data files. It provides:
    - the generic organisation of the operations
    - the processing of all primary-mission data
    - a number of methods to be implemented in mission-specific subclass to process the
      secondary mission data.

    The processing includes:
    - The production of a completed version of the input file
    - The generation of a Google-path file (.kml), for 3D visualisation of the trajectory.
    - The generation of a summary file describing the applied processing and providing
      a number of statistics on the data.
    The processing uses data from file processRecordFile.input.txt found in the working
    directory. If not found, a template is provided: complete it and launch the processor again.
    """
    def __init__(self, processor_name: str, team_name: str, force_output_file_overwrite: bool):
        """Constructor
        :param str processor_name: The name of this processor.
        :param str team_name: The name of the cansat team (=cansat project)
        :param bool force_output_file_overwrite: if True, output files are overwritten if existing. If
                    False, the processing fails is any output file already exists."""
        self.processor_name = processor_name
        self.team_name = team_name
        self.force_output_file_overwrite = force_output_file_overwrite
        self._param_file = self._get_parameters_file_object(self.processor_name + ".input.txt")
        self._summary_file = None
        self.__outputFile = None
        self.__outputFile = None
        self.__inputFile = None
        self.__google_path_file = None
        self.__hadAnyRelevantData = False

        ## @name Data collected from the current record
        # @{
        self._current_ts = 0
        self._current_ts_relative = 0
        self._current_temp_therm1 = 0
        self._current_temp_therm1_corr = 0
        self._current_longitude = 0
        self._current_latitude = 0
        self._current_new_gps_data = False
        self._current_can_x_gps = 0
        self._current_can_y_gps = 0
        self._current_altitude = 0
        self._current_ref_altitude = 0
        self._current_relative_altitude = 0
        self._withinRelevantData = False
        ## @}

        ## @name Global data collected during processing.
        # @{
        self.numSkippedRows = 0
        self.numRecords = 0
        self.numRecordsWritten = 0
        self.minTS = 1E20
        self.maxTS = 0
        self.minTS_Corrected = 1E20
        self.maxTS_Corrected = 0
        self.outputMinTS = 1E20
        self.outputMaxTS = 0
        self.outputMinTS_Relative = 1E20
        self.outputMaxTS_Relative = -1E20
        self.maxRelativeAltitude = -1000.0
        self.maxRelativeAltitudeTS = -1000
        self.maxRelativeAltitudeTS_Relative = 0
        self.maxTemperature = -1000.0
        self.maxTemperatureTS = -1000.0
        self.maxTemperatureTS_Relative = -1000.0
        self.minTemperature = 1000.0
        self.minTemperatureTS = 0
        self.minTemperatureTS_Relative = 0
        self.maxRelevantRelativeAltitude = -1000
        self.maxRelevantRelativeAltitudeTS = 0
        self.maxRelevantRelativeAltitudeTS_Relative = 0
        self.maxRelevantTemperature = -1000.0
        self.maxRelevantTemperatureTS = 0
        self.maxRelevantTemperatureTS_Relative = 0
        self.minRelevantTemperature = 1000
        self.minRelevantTemperatureTS = 0
        self.minRelevantTemperatureTS_Relative = 0
        ## @}

    def run(self) -> bool:
        """ Perform the processing.
        :return: True if everything was successful, False otherwise. """
        try:
            if not self._check_files_preconditions():
                return False
            if not self._open_files():
                return False
            self._init_output_files()
            print(f"Processing file '{self._param_file.inputFileName}'...")
            self._process_input_file()
            self._write_other_output_files()
            self._close_files()
            self._print_conclusion_message()
            return True
        except StopIteration:
            print(f"ERROR: Exception during processor execution.")
            return False

    def _check_files_preconditions(self) -> bool:
        """
         Check pre-condition to ensure a successful opening of input and output files (but do not actually open
         them) to avoid creating some, while others cannot be opened.
         :return: True if everything is ok, and object is ready for processing.
        """
        if not self._param_file.read():
            return False
        # Check input file can be found.
        if not path.exists(self._param_file.inputFileName):
            print(f"ERROR: Cannot find input file '{self._param_file.inputFileName}'.")
            return False
        # Check summary file can be created.
        if (not self.force_output_file_overwrite) and path.exists(self._param_file.summaryFileName):
            print(f"ERROR: Output summary file '{self._param_file.summaryFileName}' exists. "
                  f"Remove it and launch again.")
            return False
        # check output files can be generated.
        if (not self.force_output_file_overwrite) and path.exists(self._param_file.outputFileName):
            print(f"ERROR: Output file '{self._param_file.outputFileName}' exists.")
            return False
        # Check google path file can be created.
        if (not self.force_output_file_overwrite) and path.exists(self._param_file.googlePathFileName):
            print(f"ERROR: Google path file '{self._param_file.googlePathFileName}' exists. "
                  f"Remove it and launch again.")
            return False
        if not self._check_secondary_mission_files_preconditions(self.force_output_file_overwrite):
            return False
        return True

    def _open_files(self) -> bool:
        """ Actually open all input and output files. This method is called after all checks have been
            performed to ensure successful creation.
            :return: True if everything is ok, and object is ready for processing.
        """
        self._summary_file = open(self._param_file.summaryFileName, 'w')
        if self._summary_file is None:
            print(f"ERROR: Cannot open file {self._param_file.summaryFileName}.")
            return False
        self.__outputFile = open(self._param_file.outputFileName, 'w')
        if self._summary_file is None:
            print(f"ERROR: Cannot open fine {self._param_file.outputFileName}.")
            return False
        self.__google_path_file = open(self._param_file.googlePathFileName, 'w')
        if self.__google_path_file is None:
            print(f"ERROR: Cannot open fine {self._param_file.googlePathFileName}.")
            return False
        self.__inputFile = open(self._param_file.inputFileName, 'r')
        if self.__inputFile is None:
            print(f"ERROR: Cannot open fine {self._param_file.inputFileName}.")
            return False
        if not self._open_other_secondary_mission_files():
            return False
        return True

    def _init_output_files(self):
        """
        Write initial content in output files.
        """
        prm = self._param_file
        summary = self._summary_file
        summary.write(f"{self.processor_name}, {datetime.now()}\n\n")
        summary.write(f"Input parameters:\n")
        summary.write(f"  Parameters file: {prm.inputFileName}\n")
        summary.write(f"  Number of fields in file (primary + secondary): "
                      f"{NUM_PRIMARY_MISSION_FIELDS}+{self.get_num_secondary_mission_fields()}\n")
        summary.write(f"  Relevant data starts at TS = {prm.relevantDataStartTS}\n")
        summary.write(f"  Relevant data stops at TS  = {prm.relevantDataStopTS}\n")
        summary.write(
            f"  Duration of relevant data = {(prm.relevantDataStopTS - prm.relevantDataStartTS) / 1000.0} "
            f"sec = {((prm.relevantDataStopTS - prm.relevantDataStartTS) / 1000.0 / 60.0):.1f} min.\n")
        summary.write(f"  Offset applied to temp. thermistor 1  = {prm.offsetTherm1}°C \n")
        summary.write(
            f"  Origin of XY map: X=0 at long. {prm.XY_Origin_Longitude:.6f}, Y=0 at lat. {prm.XY_Origin_Latitude:.6f}"
            f" \n")
        summary.write(
            f"  Conversion lat/long vers XY: {M_PER_DEGREE_LONGITUDE}m/°long., {M_PER_DEGREE_LATITUDE}m/°lat.\n")
        summary.write(
            f"  Google path in '{self._param_file.googlePathFileName}'\n"
        )
        self.__outputFile.write(self._get_csv_header() + '\n')
        GooglePathCreator.init_google_path_file(self.__google_path_file, self.team_name)
        GooglePathCreator.start_path_until_takeoff(self.__google_path_file)
        self._init_summary_file_for_secondary_mission(summary)
        self._init_other_secondary_mission_files()

    def _close_files(self):
        """ Close all input and output files.  """
        self._summary_file.close()
        GooglePathCreator.terminate_path(self.__google_path_file)
        self.__google_path_file.close()
        self.__outputFile.close()
        self.__inputFile.close()
        self._close_other_secondary_mission_files()

    def _process_input_file(self):
        """ Read the data file from the original records, process each line, and generate (i) a completed
            version of the file and (ii) the GooglePath file."""
        reader = csv_reader(self.__inputFile, delimiter=',')
        num_record_fields = NUM_PRIMARY_MISSION_FIELDS + self.get_num_secondary_mission_fields()
        for row in reader:
            if (len(row) != num_record_fields) or (not row[0].isdigit()):
                self.numSkippedRows += 1
                continue
            else:
                self.numRecords += 1
                ts = int(row[0])
                self._current_ts = ts
                self._current_ts_relative = ts - self._param_file.originTS
                if ts > self.maxTS:
                    self.maxTS = ts
                if ts < self.minTS:
                    self.minTS = ts
                if row[THERM1_TEMP_FIELD_IDX] == 'ovf':
                    self._current_temp_therm1 = 0.0
                else:
                    self._current_temp_therm1 = float(row[THERM1_TEMP_FIELD_IDX])
                self._current_temp_therm1_corr = self._current_temp_therm1 + self._param_file.offsetTherm1
                if self._current_temp_therm1_corr > self.maxTemperature:
                    self.maxTemperature = self._current_temp_therm1_corr
                    self.maxTemperatureTS = ts
                    self.maxTemperatureTS_Relative = self._current_ts_relative
                if self._current_temp_therm1_corr < self.minTemperature:
                    self.minTemperature = self._current_temp_therm1_corr
                    self.minTemperatureTS = ts
                    self.minTemperatureTS_Relative = self._current_ts_relative

                # calculate BEFORE adding column (changes indexes!)
                self._current_altitude = float(row[ALTITUDE_FIELD_IDX])
                self._current_ref_altitude = float(row[REF_ALTITUDE_FIELD_IDX])
                self._current_relative_altitude = self._current_altitude - self._current_ref_altitude

                if self._current_relative_altitude > self.maxRelativeAltitude:
                    self.maxRelativeAltitude = self._current_relative_altitude
                    self.maxRelativeAltitudeTS = ts
                    self.maxRelativeAltitudeTS_Relative = self._current_ts_relative

                self._withinRelevantData = \
                    (ts >= self._param_file.relevantDataStartTS) and (ts <= self._param_file.relevantDataStopTS)
                if not self.__hadAnyRelevantData and self._withinRelevantData:
                    # first relevant data
                    GooglePathCreator.start_path_from_takeoff(self.__google_path_file)
                    self.__hadAnyRelevantData = True
                elif self.__hadAnyRelevantData and not self._withinRelevantData:
                    # first non relevant data after relevant data
                    GooglePathCreator.start_path_from_touch_down(self.__google_path_file)
                    self.__hadAnyRelevantData = False

                # for any data, add GPS position if any in google path.
                self._current_latitude = float(row[GPS_LATITUDE_FIELD_IDX])
                self._current_longitude = float(row[GPS_LONGITUDE_FIELD_IDX])
                self._current_new_gps_data = int(row[NEW_GPS_DATA_FIELD_IDX]) == 1
                if self._current_new_gps_data:
                    GooglePathCreator.insert_position(self.__google_path_file, self._current_latitude,
                                                      self._current_longitude, self._current_altitude)

                if self._withinRelevantData:
                    self._process_primary_mission_info(row)
                    self._process_secondary_mission_info(row)

                    # insert additional columns and output.
                    # A. From secondary mission, at end or row.
                    self._append_secondary_mission_columns(row)
                    # B. From primary mission, within the row.
                    self.numRecordsWritten += 1
                    row.insert(TIMESTAMP_FIELD_IDX + 1, f"{self._current_ts_relative}")
                    row.insert(TIMESTAMP_FIELD_IDX + 2, f"{(self._current_ts_relative / 1000.0):.1f}")
                    row.insert(GPS_LONGITUDE_FIELD_IDX + 3, f"{self._current_can_x_gps:.2f}")
                    row.insert(GPS_LONGITUDE_FIELD_IDX + 4, f"{self._current_can_y_gps:.2f}")
                    row.insert(REF_ALTITUDE_FIELD_IDX + 5, f"{self._current_relative_altitude:.1f}")
                    row.insert(THERM1_TEMP_FIELD_IDX + 6, f"{self._current_temp_therm1_corr:.1f}")
                    line = ','.join(str(x) for x in row)
                    # print(line)
                    self.__outputFile.write(line + '\n')

    def _process_primary_mission_info(self, row: []):
        """ Perform processing of primary mission information found in the record file. """
        if self._current_temp_therm1_corr > self.maxRelevantTemperature:
            self.maxRelevantTemperature = self._current_temp_therm1_corr
            self.maxRelevantTemperatureTS = self._current_ts
            self.maxRelevantTemperatureTS_Relative = self._current_ts_relative
        if self._current_temp_therm1_corr < self.minRelevantTemperature:
            self.minRelevantTemperature = self._current_temp_therm1_corr
            self.minRelevantTemperatureTS = self._current_ts
            self.minRelevantTemperatureTS_Relative = self._current_ts_relative
        if self._current_relative_altitude > self.maxRelevantRelativeAltitude:
            self.maxRelevantRelativeAltitude = self._current_relative_altitude
            self.maxRelevantRelativeAltitudeTS = self._current_ts
            self.maxRelevantRelativeAltitudeTS_Relative = self._current_ts_relative
        # insert and fill new columns if relevant
        if self._current_ts_relative > self.outputMaxTS_Relative:
            self.outputMaxTS_Relative = self._current_ts_relative
            self.outputMaxTS = self._current_ts
        if self._current_ts_relative < self.outputMinTS_Relative:
            self.outputMinTS_Relative = self._current_ts_relative
            self.outputMinTS = self._current_ts

        # Compute XY coordinates from GPS
        if self._current_longitude != 0:
            self._current_can_x_gps, self._current_can_y_gps = self._param_file.convert_to_xy(self._current_latitude,
                                                                                              self._current_longitude)
        else:
            # print(f"no gps at {ts}")
            self._current_can_x_gps = self._current_can_y_gps = 0.0

    def _write_other_output_files(self):
        """ Write the content of any output file (other than the completed record file and Google path file). """
        summary = self._summary_file
        summary.write("\nStatistics\n")
        summary.write("  Number of records processed          : {}\n".format(self.numRecords))
        summary.write("  Number of non-record lines (skipped) : {}\n".format(self.numSkippedRows))

        # 1. General data
        summary.write("  Processed {} record (ts from {} to {})\n".format(
            self.numRecords,
            self.minTS,
            self.maxTS))
        summary.write("    Max. relative altitude: {:.1f}m at timestamp {} (relative {})\n".
                      format(self.maxRelativeAltitude,
                             self.maxRelativeAltitudeTS,
                             self.maxRelativeAltitudeTS_Relative))
        summary.write("    Max. temperature: {:.1f}°C at timestamp {} (rel. {})\n".format(
            self.maxTemperature,
            self.maxTemperatureTS,
            self.maxTemperatureTS_Relative))
        summary.write("    Min. temperature: {:.1f}°C at timestamp {} (rel. {})\n".format(
            self.minTemperature,
            self.minTemperatureTS,
            self.minTemperatureTS_Relative))
        summary.write(f"  Output {self.numRecordsWritten} relevant records in file '{self._param_file.outputFileName}'"
                      f" from {self.outputMinTS} (rel. {self.outputMinTS_Relative}) to "
                      f"{self.outputMaxTS} (rel. {self.outputMaxTS_Relative})\n")
        # 2. relevant data
        summary.write("    Max. relevant relative altitude: {:.1f}m at timestamp {} (relative {})\n".
                      format(self.maxRelevantRelativeAltitude,
                             self.maxRelevantRelativeAltitudeTS,
                             self.maxRelevantRelativeAltitudeTS_Relative))
        summary.write("    Max. relevant temperature: {:.1f}°C at timestamp {} (rel. {})\n".format(
            self.maxRelevantTemperature,
            self.maxRelevantTemperatureTS,
            self.maxRelevantTemperatureTS_Relative))
        summary.write("    Min. relevant temperature: {:.1f}°C at timestamp {} (rel. {})\n".format(
            self.minRelevantTemperature,
            self.minRelevantTemperatureTS,
            self.minRelevantTemperatureTS_Relative))

        self._write_other_secondary_mission_files()

    ## @name Methods to be overridden in subclass to support secondary mission
    #  ------------------------------------------------------------------------
    # @{
    @staticmethod
    def get_num_secondary_mission_fields():
        """
        Overwrite in subclass.
        :return: The number of data fields in the input file for the secondary mission.
        """
        return 0

    def _process_secondary_mission_info(self, row: []) -> None:
        """
            Collect relevant statistics and calculate derived information of a relevant row from the input file
            using secondary mission data. When this method is called, primary mission information is available
            as attributes self.current_***.
        """
        pass

    def _append_secondary_mission_columns(self, row: []) -> None:
        """
        Append secondary mission-related data at the end of the provided row (the row contains _all_ data
        from the original data file, and will be output in the output file). This must be performed based
        on values obtained from the incoming row in method _process_secondary_mission_info().
        This method is to be implemented in the subclass.
        Warning: do not insert values anywhere else than at the end of the list (using row.append()).
        :param row: The row currently being processed.
        """
        pass

    def _check_secondary_mission_files_preconditions(self, do_overwrite: bool) -> bool:
        """
        Check pre-condition to ensure a successful opening of input and output files (but do not actually open
         them) to avoid creating some, while others cannot be opened.
        :param do_overwrite: if True existing file may be overwritten, else output files may not preexist.
        :return: True if everything is ok, and object is ready for processing.
        """
        return True

    def _init_summary_file_for_secondary_mission(self, summary_file) -> None:
        """  Write initial information regarding secondary mission in summary file.
             Summary_file is open and ready. End with backslash-n, do not close file.
        :param summary_file: the summary file object, ready for writing.
        """
        pass

    def _open_other_secondary_mission_files(self) -> bool:
        """
        Overwrite in subclass to open additional files. Their content will be initialised in
        _init_other_secondary_mission_files(), content written in write_other_secondary_files()
        and they will be closed in _close_other_secondary_mission_files().
        This method is called after the parameters file is read.
        :return: True if files have been successfully open, False otherwise.
        """
        return True

    def _init_other_secondary_mission_files(self) -> None:
        """
        Overwrite in subclass to initialize the content of additional output files, which have been open in
        _open_other_secondary_mission_files(), will be written to in write_other_secondary_files()
        and will be closed in _close_other_secondary_mission_files.
        This method is called before the input file is processed, and after the parameters file is read.
        """
        pass

    def _write_other_secondary_mission_files(self) -> None:
        """
        Overwrite in subclass to write the content of additional output files, which have been open in
        _open_other_secondary_mission_files(), initialized in _init_other_secondary_mission_files()
        and will be closed in _close_other_secondary_mission_files.
        This method is called after the input file is processed.
        """
        pass

    def _close_other_secondary_mission_files(self) -> None:
        """
        Overwrite in subclass to close the additional files, opened in
        _open_other_secondary_mission_files(), initialized in _init_other_secondary_mission_files()
        and written to in _write_other_secondary_mission_files.
        """
        pass

    def _print_conclusion_message(self) -> None:
        """
        Print final message on the command line. Overwrite in subclass to print additional messages
        Be sure to call super().print_conclusion_message() before printing.
        """
        print(f"Done (skipped {self.numSkippedRows} rows).")
        print(f"  - Results in file '{self._param_file.outputFileName}' ({self.numRecordsWritten}/{self.numRecords}"
              f" rows).")
        print(f"  - Google path file in '{self._param_file.googlePathFileName}'")
        print(f"See details in file '{self._param_file.summaryFileName}'")

    def _get_csv_header(self) -> str:
        """ Return the CSV header for the processed file. To add secondary mission columns, overwrite in subclass
            and return super()._get_csv_header() + ',field1, field2 etc.'.
        :return: The complete CSV header.
        """
        return 'TS, relTS, relTS (s),' \
               'GPS_Measures,GPS_LatDegrees,GPS_LongDegrees,GPS_pos_X,GPS_pos_Y,GPS_Alt,' \
               'tempBMP,pressure,alt.,ref.alt.,rel.alt.,descVelocity,' \
               'therm1,therm1corr'

    # ------ END OF METHODS TO OVERRIDE IN SUBCLASS --------------
    # ## @}

    @staticmethod
    def _get_parameters_file_object(param_file_name) -> CansatProcessorParamFile:
        """ Obtain the instance of the ParameterFile object (override in subclass to return
            a subclass of CansatProcessorParamFile in case additional input parameters are
            required for the secondary mission). """
        return CansatProcessorParamFile(param_file_name)
