#!/usr/bin/env python3

""" This is the script to launch to generate a simulated can data file in a Cansat project.

To support a new xxxx project:
   - Create a new module  xxxxFileUtilities
   - In this module create
          - a xxxxRecord class in xxxxRecord.py, inheriting CansatRecord
            (see EtheraRecord for an example).
          - a xxxxTestDataGenerator class and xxxxTestDataGeneratorConfig.py file
            (see EtheraTestDataGenerator for an example)
   - In this script, use the xxxxTestDataGenerator
"""

# import sys for argv
import os.path
import sys
from EtheraFileUtilities import EtheraTestDataGenerator

MYSELF = os.path.basename(sys.argv[0])
TITLE = '\nCanSat data record file generator\n' \
          '---------------------------------'
USAGE = 'Usage: ' + MYSELF + ' (no option, no argument)'
print(TITLE)
print()
if len(sys.argv) != 1:
    print(MYSELF, ': Missing or extraneous argument (Aborted)')
    print(USAGE)
    sys.exit(1)

tdg = EtheraTestDataGenerator()     # Change this line for a new project.
tdg.generate()
tdg._terminate()

exit(0)
