#
# This file contains all constants used to configure the generation of the Ethera-specific part of the test data by
# class EtheraTestDataGenerator.
#
from .EtheraTypes import Beacon, MonitoredCan
from CansatFileUtilities import Variation
from CansatFileUtilities import START_LATITUDE, START_LONGITUDE, M_PER_DEGREE_LATITUDE, M_PER_DEGREE_LONGITUDE

BEACONS = [ Beacon(430.2, START_LATITUDE+100/M_PER_DEGREE_LATITUDE, START_LONGITUDE-100/M_PER_DEGREE_LONGITUDE, -12.3, 100),
            Beacon(430.8, START_LATITUDE-100/M_PER_DEGREE_LATITUDE, START_LONGITUDE+100/M_PER_DEGREE_LONGITUDE, -12.3, 100)]
MONITORED_CANS = [ MonitoredCan(433.1, 1, -45, Variation(500, 500, True) ),
                   MonitoredCan(433.9, 1, -54, Variation(600, 700, True) ) ]