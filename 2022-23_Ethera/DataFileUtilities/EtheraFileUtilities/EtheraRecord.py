from CansatFileUtilities import CansatRecord
from random import randint
from math import floor

class EtheraRecord(CansatRecord):
    def __init__(self, start_ts, beacons: [], monitored_cans:[]):
        super().__init__(start_ts)
        self.brakePosition = 0
        self.flightPhase = 0
        self.frequency = 0.0
        self.rssi = 0
        self.distance = 0.0
        self.flightControlCode = 0
        self.beacons = beacons
        self.monitored_cans = monitored_cans
        self.currentBeaconOrCan = 0


    def modify_record(self):
        super().modify_record()
        num_beacons = len(self.beacons)
        num_cans = len(self.monitored_cans)
        for b in self.monitored_cans:
            if b.distanceVariation is not None:
                b.distance += b.distanceVariation.get_increment()

        #print(f"num_cans={num_cans}, num_beacons={num_beacons}, self.currentBeaconOrCan={self.currentBeaconOrCan}")
        self.frequency = 0
        self.distance = 0
        self.rssi = 0
        # No reception in half the records
        if randint(1, 2) != 1:
            if self.currentBeaconOrCan < num_beacons:
                if self.gpsLatitude != 0 and self.gpsLongitude != 0:
                    self.frequency = self.beacons[self.currentBeaconOrCan].frequency
                    self.distance, self.rssi = self.beacons[self.currentBeaconOrCan].\
                        get_distance_and_rssi(self.gpsLatitude, self.gpsLongitude, self.altitude-self.refAltitude)
            elif self.currentBeaconOrCan < ( num_beacons + num_cans):
                can_idx = self.currentBeaconOrCan - num_beacons
                #print(f"processing can {can_idx}")
                self.frequency = self.monitored_cans[can_idx].frequency
                self.distance = self.monitored_cans[can_idx].distance
                self.rssi = self.monitored_cans[can_idx].rssi

        self.currentBeaconOrCan += 1
        if self.currentBeaconOrCan >= ( num_beacons + num_cans):
            self.currentBeaconOrCan = 0

    def csv_line(self):
        return super().csv_line()  \
            + str(self.flightControlCode) + ',' \
            + str(self.brakePosition)+ ','  \
            + str(self.flightPhase)+ ',' \
            + f'{self.frequency:.1f},{floor(self.rssi):.0f},' \
            + str(floor(self.distance))