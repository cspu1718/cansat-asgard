# This file contains the utility functions to process RF measurements:
#  - Calibrate from known distance
#  - Compute distance with provided calibration data
#  Etc.
from math import sqrt, log10, floor

from EtheraFileUtilities import EtheraProcessorParamFile


def get_distance_to_beacon_from_delta(delta_x, delta_y, rel_altitude) -> float:
    """ Obtain the x0 and RSSI0 values """
    return sqrt(delta_x*delta_x + delta_y*delta_y + rel_altitude*rel_altitude)


def get_distance_to_beacon(can_x, can_y, rel_altitude, beacon_x, beacon_y) -> float:
    return get_distance_to_beacon_from_delta(can_x - beacon_x, can_y - beacon_y, rel_altitude)


def get_distance(rssi: float, x0: float, rssi0: float) -> float:
    k0 = pow(10, (rssi0 + 20 * log10(x0)) / 20)
    return floor(k0 / pow(10, rssi / 20))


def compute_location(param_file: EtheraProcessorParamFile, gps_position_x: float, gps_position_y: float,
                     beacon0_distance: float, beacon1_distance: float,
                     relative_altitude: float):
    """
    Calculate the position of the can, based on distance from 2 beacons and altitude. This gives 2 possible
    positions. The closest to the GPS position is returned.
    In case measurement errors result in negative distances, those distances are forced to 0
    In case the sum of the computed to the beacons < distance_between_beacons, the position is assumed to be on
    the segment joining the beacons, at X0/(X0+X1)*dist_between_beacons m from beacon 0.
    :param param_file: the object providing access to the various parameters required for calculation.
    :param gps_position_x:  X position of the can (m)
    :param gps_position_y:  Y position of the can (m)
    :param beacon0_distance:  distance to beacon 0 (m)
    :param beacon1_distance:  distance to beacon 1 (m)
    :param relative_altitude: altitude from ground (m)
    :return: can X position (m)
    :return: can Y position (m)
    :return: A string documenting the computation intermediate results
    :return: A boolean: was the data fixed in any way.
    :return: The error on the position compared to the GPS avg position (positive number).
             -1 if no position could be calculated.
    """
    fixed_error = False
    distance_error = -1
    position_x = 1E10
    position_y = 1E10
    tmp = beacon0_distance ** 2 - relative_altitude ** 2
    the_info = f"INPUT: rel_alt={relative_altitude} gpsXY: ({gps_position_x};{gps_position_y}) " \
               f"b0dist: {beacon0_distance} b1dist: {beacon1_distance}) "
    if tmp < 0:
        the_info += "Cannot " \
                    "calculate X0: beacon0_distance**2 - relative_altitude**2 < 0 "\
                   f"(b0_dist={beacon0_distance} & rel_alt={relative_altitude}. Set X0 to 0)"
        fixed_error = True
        tmp = 0
    x0 = sqrt(tmp)
    tmp = beacon1_distance ** 2 - relative_altitude ** 2
    if tmp < 0:
        the_info += f"cannot calculate X1: beacon1_distance**2 - relative_altitude**2 < 0 " \
                   f"(b1_dist={beacon1_distance} & rel_alt={relative_altitude})"
        tmp = 0
        fixed_error = True
    x1 = sqrt(tmp)
    d = param_file.distance_between_beacons
    if d < (x0 + x1):
        # the ground circles have 2 intersections.
        a = (x0 ** 2 - x1 ** 2 + d ** 2) / (2 * d)
        b = (x1 ** 2 - x0 ** 2 + d ** 2) / (2 * d)
        tmp = x0 ** 2 - a ** 2
        if tmp < 0:
            the_info += f"cannot calculate h: x0**2 - a**2 < 0 (X0={x0:.2f} x1={x1:.2f} & a={a:.2f}). Set h to 0. "
            tmp = 0
            fixed_error = True
        h = sqrt(tmp)
        x5 = param_file.beacon0_X + (a / d) * (param_file.beacon1_X - param_file.beacon0_X)
        y5 = param_file.beacon0_Y + (a / d) * (param_file.beacon1_Y - param_file.beacon0_Y)
        x3 = x5 - (h * (param_file.beacon1_Y - param_file.beacon0_Y) / d)
        y3 = y5 + (h * (param_file.beacon1_X - param_file.beacon0_X) / d)
        x4 = x5 + (h * (param_file.beacon1_Y - param_file.beacon0_Y) / d)
        y4 = y5 - (h * (param_file.beacon1_X - param_file.beacon0_X) / d)

        # select P3 or P4
        distance_to_p3 = sqrt((x3 - gps_position_x) ** 2 + (y3 - gps_position_y) ** 2)
        distance_to_p4 = sqrt((x4 - gps_position_x) ** 2 + (y4 - gps_position_y) ** 2)
        if distance_to_p3 < distance_to_p4:
            position_x = x3
            position_y = y3
        else:
            position_x = x4
            position_y = y4
        the_info += f"X0={x0:.4f} X1={x1:.4f} a={a:.4f} b={b:.4f} h={h:.4f} x5={x5:.4f} y5={y5:.4f} " \
                    f"x3={x3:.4f} y3={y3:.4f} x4={x4:.4f} y4={y4:.4f} " \
                    f"distP3={distance_to_p3:.2f} distP4={distance_to_p4:.2f}"
    elif x0 + x1 == 0:
        the_info += f"X1 + X0 = 0. No calculation"
        return position_x, position_y, the_info, fixed_error, distance_error
    else:
        # All other cases: tangent circles, disjoint circles, etc.
        # This error is most likely due to the can being on top of the inter-beacon segment, so circles are
        # close to tangent, and measurement error made them exterior or tangent.  Let's assume we are
        # on top of the inter-beacon segment at distance d * X0(X0+X1) from beacon 0
        k = x0/(x0+x1)
        the_info += f"X0={x0:.4f} X1={x1:.4f}  x0+x1 <= distance between beacons ({d}). "\
                    f"Forced position on inter-beacons segments at {k:.2f} of inter-beacon distance from beacon 0."
        position_x = param_file.beacon0_X + (param_file.beacon1_X - param_file.beacon0_X) * k
        position_y = param_file.beacon0_X + (param_file.beacon1_Y + param_file.beacon0_Y) * k
        fixed_error = True
    distance_error = sqrt((position_x - gps_position_x)**2 + (position_y - gps_position_y)**2)
    return position_x, position_y, the_info, fixed_error, distance_error


def define_rf_position_quality(param_file: EtheraProcessorParamFile, delay_between_beacons: int,
                               x: float, y: float, fixed: bool) -> str:
    """ Define the quality of the computed position."""
    if delay_between_beacons > param_file.max_delay_between_beacons_for_POOR or x > 10E5 or y > 10E5:
        quality = "BAD"
    elif delay_between_beacons > param_file.max_delay_between_beacons_for_GOOD:
        if fixed:
            quality = "BAD"
        else:
            quality = "POOR"
    else:
        if fixed:
            quality = "POOR"
        else:
            quality = "GOOD"
    return quality
