# The class managing the specific Ethera aspects of the processing input file.

from CansatFileUtilities import CansatProcessorParamFile
import csv
from math import sqrt


class EtheraProcessorParamFile(CansatProcessorParamFile):
    """ The class in charge of the input parameter's file for project Ethera. It extends CansatProcessorParamFile
     for the secondary mission."""
    def __init__(self, input_file_name: str):
        super().__init__(input_file_name)
        self.rfFileName = ""
        self.rfFileName_GOOD = ""
        self.rfFileName_GOOD_POOR = ""
        ## key = frequency in MHz, value = file name
        self.frequency_file_names = {}
        self.can0_frequency = 0
        self.can1_frequency = 0
        self.beacon0_frequency = 0
        self.beacon0_latitude = 0
        self.beacon0_longitude = 0
        self.beacon1_frequency = 0
        self.beacon1_latitude = 0
        self.beacon1_longitude = 0
        self.beacon0_X = self.beacon0_Y = 0
        self.beacon1_X = self.beacon1_Y = 0
        self.distance_between_beacons = 0
        self.max_delay_between_beacons_for_GOOD = 0
        self.max_delay_between_beacons_for_POOR = 0

    def _write_secondary_mission_fields(self):
        self._paramFile.write("# Leave XY origin to (0.000, 0.0000) to use the mid-point between the beacons.\n")
        self._paramFile.write("# Frequency of beacon 0 in MHz \n000.0\n")
        self._paramFile.write("# Latitude of beacon 0 (in degrees) \n000.0\n")
        self._paramFile.write("# Longitude of beacon 0 (in degrees) \n000.0\n")
        self._paramFile.write("# Frequency of beacon 1 in MHz \n000.0\n")
        self._paramFile.write("# Latitude of beacon 1 (in degrees) \n000.0\n")
        self._paramFile.write("# Longitude of beacon 1 (in degrees) \n000.0\n")
        self._paramFile.write("# Frequency of tracked can 0 (in MHz) \n000.0\n")
        self._paramFile.write("# Frequency of tracked can 1 (in MHz) \n000.0\n")
        self._paramFile.write("# Maximum delay between beacons measures for GOOD position (in msec) \n3000\n")
        self._paramFile.write("# Maximum delay between beacons measures for POOR position (in msec) \n5000\n")

    def _read_secondary_mission_fields(self, reader: csv.reader):
        self.beacon0_frequency = self._read_float_input_param(reader)
        if self.beacon0_frequency < 0.0001:
            print("*** ERROR: reading null value as beacon 0 frequency!")
        self.beacon0_latitude = self._read_float_input_param(reader)
        self.beacon0_longitude = self._read_float_input_param(reader)
        self.beacon1_frequency = self._read_float_input_param(reader)
        if self.beacon1_frequency < 0.0001:
            print("*** ERROR: reading null value as beacon 1 frequency!")
        self.beacon1_latitude = self._read_float_input_param(reader)
        self.beacon1_longitude = self._read_float_input_param(reader)
        self.can0_frequency = self._read_float_input_param(reader)
        self.can1_frequency = self._read_float_input_param(reader)
        self.rfFileName = self.inputFileNameNoExt + '.rf.all.csv'
        self.rfFileName_GOOD = self.inputFileNameNoExt + '.rf.good.csv'
        self.rfFileName_GOOD_POOR = self.inputFileNameNoExt + '.rf.good-poor.csv'
        self.frequency_file_names[self.beacon0_frequency] = \
            self.inputFileNameNoExt + '.b.' + str(self.beacon0_frequency) + '.csv'
        self.frequency_file_names[self.beacon1_frequency] = \
            self.inputFileNameNoExt + '.b.' + str(self.beacon1_frequency) + '.csv'
        self.frequency_file_names[self.can0_frequency] = \
            self.inputFileNameNoExt + '.c.' + str(self.can0_frequency) + '.csv'
        self.frequency_file_names[self.can1_frequency] = \
            self.inputFileNameNoExt + '.c.' + str(self.can1_frequency) + '.csv'
        self.max_delay_between_beacons_for_GOOD = self._read_integer_input_param(reader)
        self.max_delay_between_beacons_for_POOR = self._read_integer_input_param(reader)
        if self.max_delay_between_beacons_for_GOOD > self.max_delay_between_beacons_for_POOR:
            print(f"**** ERROR in file '{self.paramFileName}: \n"
                  f"     max_delay_between_beacons_for_GOOD "
                  f"({self.max_delay_between_beacons_for_GOOD}) > max_delay_between_beacons_for_POOR "
                  f"({self.max_delay_between_beacons_for_POOR})")
            print(f"Aborted.")
            exit(1)
        if abs(self.XY_Origin_Longitude) < 0.001:
            print(f"Origin longitude is null: using average of beacons longitudes")
            self.XY_Origin_Longitude = (self.beacon0_longitude + self.beacon1_longitude) / 2
        if abs(self.XY_Origin_Latitude) < 0.001:
            print(f"Origin latitude is null: using average of beacons latitudes")
            self.XY_Origin_Latitude = (self.beacon0_latitude + self.beacon1_latitude) / 2
        # Conversion to xy cannot be performed before the map origin is set.
        self.beacon0_X, self.beacon0_Y = self.convert_to_xy(self.beacon0_latitude, self.beacon0_longitude)
        self.beacon1_X, self.beacon1_Y = self.convert_to_xy(self.beacon1_latitude, self.beacon1_longitude)
        self.distance_between_beacons = \
            sqrt((self.beacon0_X - self.beacon1_X) ** 2 + (self.beacon0_Y - self.beacon1_Y) ** 2)
