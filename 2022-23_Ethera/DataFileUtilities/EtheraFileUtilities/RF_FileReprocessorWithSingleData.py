from .RF_FileReprocessor import RfFileReprocessor


class RfFileReprocessorWithSingleData(RfFileReprocessor):
    """ A simple implementation of the reprocessor,
        using a single x0/rssi0 pair for each beacon."""
    def __init__(self, x0_0: float, rssi0_0: float, x0_1: float, rssi0_1: float):
        super().__init__()
        self.x0 = [x0_0, x0_1]
        self.rssi0 = [rssi0_0, rssi0_1]

    def get_output_tag(self) -> str:
        return f".({self.x0[0]:.1f}_{self.rssi0[0]:.1f})({self.x0[1]:.1f}_{self.rssi0[1]:.1f})"

    def print_new_calibration_data_description(self, summary_file):
        summary_file.write(f"  New calibration data used:\n")
        for i in (0, 1):
            summary_file.write(f"    beacon {i}: x0 = {self.x0[i]:.1f} m, rssi0 = {self.rssi0[i]:.1f} dBm\n")

    def get_beacon_calibration_data(self, beacon_id: int, avg_rel_altitude: float):
        return self.x0[beacon_id], self.rssi0[beacon_id]

