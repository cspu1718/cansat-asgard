from CansatFileUtilities import CansatTestDataGenerator
from CansatFileUtilities import START_ALTITUDE, START_LATITUDE, START_LONGITUDE, \
    DURATION_BEFORE_HANDOVER_IN_HOURS, \
    M_PER_DEGREE_LONGITUDE, M_PER_DEGREE_LATITUDE, \
    START_TS, \
    REC_PER_SECOND, DURATION_BEFORE_RETRIEVAL_IN_HOURS, \
    Variation
from .EtheraRecord import EtheraRecord
from .EtheraTestDataGeneratorConfig import BEACONS, MONITORED_CANS


class EtheraTestDataGenerator(CansatTestDataGenerator):
    """ A class that generates simulated data for the Ethera project.
    """
    def __init__(self):
        super().__init__(EtheraRecord(START_TS, BEACONS, MONITORED_CANS))
        for b in BEACONS:
            print(f"Beacon {b.frequency}MHz at lat. {b.latitude}, long {b.longitude})")
        for c in MONITORED_CANS:
            print(f"Monitored Can frequency: {c.frequency} MHz ")

    def _output_file_name(self):
        return "EtheraTestData.csv"

    def generate(self):
        # 1. Generate records before launch
        print('  Can startup...')
        # 1.1 Without GPS data
        self.record.flightControlCode = 1  # STARTING UP
        self._generate_records(3 * 60 * 5)

        # 1.2 With stable GPS data
        print(f'  From startup to hand-over from TS {self.record.timestamp}...')
        self.record.flightControlCode = 2  # Ground condition
        self.record.gpsAltitude = START_ALTITUDE
        self.record.gpsLatitude = START_LATITUDE
        self.record.gpsLongitude = START_LONGITUDE
        self._generate_records(DURATION_BEFORE_HANDOVER_IN_HOURS * 60 * 60 * 5)

        # 1.3 With varying GPS data (transport to launch site)
        print(f'  Transport to launch site from TS {self.record.timestamp}...')
        self.record.latitudeVariation = Variation(300 / M_PER_DEGREE_LATITUDE, 40, False)
        self.record.longitudeVariation = Variation(400 / M_PER_DEGREE_LONGITUDE, 50, False)
        self._generate_records(300)

        # 2. Take-off and ascent
        print(f'  Take-off and ascent from TS {self.record.timestamp}...')
        #    Ascent on 120 record
        #    Accelerate on 50 records, ascent on 90 more, decelerate on 20 last.
        self.record.flightControlCode = 3  # Not descending
        self.record.latitudeVariation = Variation(800 / M_PER_DEGREE_LATITUDE, 40, repeat=True)
        self.record.longitudeVariation = Variation(700 / M_PER_DEGREE_LONGITUDE, 60, repeat=True)
        self.record.altitudeVariation = Variation(995, 120, repeat=False)
        self.record.verticalVelocityVariation = Variation(-15, 10, repeat=False)
        self._generate_records(50)

        self.record.countSinceNewGPS = 2
        self.record.latitudeVariation = Variation(500 / M_PER_DEGREE_LATITUDE, 40, repeat=True)
        self.record.longitudeVariation = Variation(700 / M_PER_DEGREE_LONGITUDE, 50, repeat=True)
        self._generate_records(50)

        self.record.verticalVelocityVariation = Variation(15, 20, False)
        self._generate_records(20)

        # We are at the apex. Assume ejection from rocket
        # 3. Descent: acceleration to 10 m/s in 3 sec = 15 rec then stable velocity
        #             variation of lat/long until the end
        #             temperature drops by 15°C in 1 second = 5 rec then comes back in 120 sec = 600 rec.
        #             After 4 seconds = 20 records start modulating descent velocity with airbrake.
        print(f"  Descent... from TS {self.record.timestamp}")

        self.record.thermistorVariation = Variation(-15, 1 * REC_PER_SECOND, False)
        self.record.bmpTempVariation = Variation(-13, 50 * REC_PER_SECOND, False)
        self.record.verticalVelocityVariation = Variation(10, 2 * REC_PER_SECOND, False)
        self.record.altitudeVariation = Variation(-2 * 5, 2 * REC_PER_SECOND, False)  # = 5 m/s

        self._generate_records(1 * REC_PER_SECOND)

        # After 1 second descent, nominal descent is detected and thermistor starts going up.
        self.record.flightControlCode = 4  # NOMINAL DESCENT
        self.record.thermistorVariation = Variation(15, 119 * REC_PER_SECOND, False)
        self._generate_records(1 * REC_PER_SECOND)
        # After 2 seconds velocity is  10 m/s, increase altitude variation.
        self.record.altitudeVariation = Variation(-2 * 10, 2 * REC_PER_SECOND, False)  # = 10 m/s
        self._generate_records(2 * REC_PER_SECOND)
        # After 4 seconds, we assume ejection and start air-brakes.
        print(f"  ts={self.record.timestamp}: Assuming we're out of the rocket")
        print(f"  Starting air-brakes... from TS {self.record.timestamp}, altitude={self.record.altitude:.1f}")
        # Phase 1: open 55, vertical velocity = 8, down to 750 m.
        self.record.flightPhase = 1
        self.record.brakePosition = 55
        self.record.set_descent_velocity(8)
        print(f"  Phase {self.record.flightPhase}: ts={self.record.timestamp}, airbrake: {self.record.brakePosition}%,"
              f" alt. = {self.record.altitude:.1f}m, "
              f"velocity={self.record.verticalVelocity:.1f}m/s, "
              f"brakes:{self.record.brakePosition}")
        while self.record.altitude > 750:
            self._generate_records(REC_PER_SECOND)

        self.record.flightPhase = 2
        self.record.brakePosition = 40
        self.record.set_descent_velocity(9)
        print(
            f"  Phase {self.record.flightPhase}: ts={self.record.timestamp}, airbrake: {self.record.brakePosition}%,"
            f" alt. = {self.record.altitude:.1f}m, "
            f"velocity={self.record.verticalVelocity:.1f}m/s, "
            f"brakes:{self.record.brakePosition}")
        while self.record.altitude > 600:
            self._generate_records(REC_PER_SECOND)

        # Phase 3: open 20, vertical velocity = 9.5, down to 400.
        self.record.flightPhase = 3
        self.record.brakePosition = 20
        self.record.set_descent_velocity(9.5)
        # After about 50 s descent, BMP temp. reached bottom, move up again during the remaining 70 seconds.
        self.record.bmpTempVariation = Variation(13, 70 * REC_PER_SECOND, False)
        print(
            f"  Phase {self.record.flightPhase}: ts={self.record.timestamp}, airbrake: {self.record.brakePosition}%,"
            f" alt. = {self.record.altitude:.1f}m, "
            f"velocity={self.record.verticalVelocity:.1f}m/s, "
            f"brakes:{self.record.brakePosition}")
        while self.record.altitude > 400:
            self._generate_records(REC_PER_SECOND)

        # Phase 4: open 0, vertical velocity = 10.7, down to ground.
        self.record.flightPhase = 4
        self.record.brakePosition = 0
        self.record.set_descent_velocity(10.7)
        print(
            f"  Phase {self.record.flightPhase}: ts={self.record.timestamp}, airbrake: {self.record.brakePosition}%,"
            f" alt. = {self.record.altitude:.1f}m, "
            f"velocity={self.record.verticalVelocity:.1f}m/s, "
            f"brakes:{self.record.brakePosition}")

        print(f'  Descent  down to 95 m... from TS {self.record.timestamp}')
        # When altitude reaches 95 m cans stop
        self._generate_records_down_to_ground(95, DURATION_BEFORE_RETRIEVAL_IN_HOURS * 60 * 60 * 5)
        print(f"  Can turned off at TS {self.record.timestamp}")
