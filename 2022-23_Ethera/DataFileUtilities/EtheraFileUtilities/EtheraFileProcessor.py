
from CansatFileUtilities import CansatFileProcessor
from . import EtheraProcessorParamFile
from os import path

from .RF_Calculations import get_distance_to_beacon, get_distance, compute_location, define_rf_position_quality

FLIGHT_CONTROL_CODE_IDX = 11
AIR_BRAKES_POSITION_IDX = 12
FLIGHT_PHASE_IDX = 13
FREQUENCY_FIELD_IDX = 14
RSSI_FIELD_IDX = 15
DISTANCE_FIELD_IDX = 16
NUMBER_OF_SECONDARY_MISSION_FIELDS = 6


class EtheraFileProcessor(CansatFileProcessor):
    """ The processor used to handle data files from the Ethera cansat. It adds the required features to process
        the secondary mission data: RF measurements and air-brakes positions.
    """
    def __init__(self, processor_name, force_output_file_overwrite: bool):
        super().__init__(processor_name, "Ethera", force_output_file_overwrite)
        self._rfFile = None
        self._rfFile_GOOD = None
        self._rfFile_GOOD_POOR = None
        self._frequency_files = []       # file objects for frequency files (idx see _frequency_files_mapping).

        ## @name Data collected during processing.
        # @{
        self.beacon0_distance = 0.0
        self.beacon0_rssi = 0.0
        self.beacon0_ts = 0
        self.beacon0_can_x = 0
        self.beacon0_can_y = 0
        self.beacon0_rel_altitude = 0
        self.beacon0_latitude = 0
        self.beacon0_longitude = 0
        self.beacon0_can_x = 0
        self.beacon0_can_y = 0
        self.beacon1_distance = 0.0
        self.beacon1_rssi = 0.0
        self.beacon1_ts = 0
        self.beacon1_can_x = 0
        self.beacon1_can_y = 0
        self.beacon1_rel_altitude = 0
        self.beacon1_latitude = 0
        self.beacon1_longitude = 0
        self.beacon1_can_x = 0
        self.beacon1_can_y = 0
        self.oldest_beacon_measure = -1
        ## @}

        ## @name Global data & stats collected during processing.
        # @{
        self.frequency_files_mapping = {}  # key = frequency, value = idx in _frequency_files and num_distance_rows.
        self.num_distance_rows = []  # number of rows in each freq. file (idx see _frequency_files_mapping).
        self.beacon_max_error = []  # (idx see _frequency_files_mapping).
        self.beacon_total_error = []  # (idx see _frequency_files_mapping).
        self.num_rf_positions = 0
        self.num_bad_rf_positions = 0
        self.num_poor_rf_positions = 0
        self.num_good_rf_positions = 0
        self.rf_max_error_good = 0
        self.rf_max_error_poor = 0
        self.rf_max_error_bad = 0
        self.rf_total_error_good = 0
        self.rf_total_error_poor = 0
        self.rf_total_error_bad = 0
        ## @}

        # Secondary mission data from current row
        self.current_frequency = 0
        self.current_distance = 0
        self.current_rssi = 0
        # secondary mission data from previous row
        self.previous_flight_phase = -10
        self.previous_flight_control_code = -10
        self.previous_brake_position = -10
        self.air_brakes_transitions = []  # list of tuples (ctrl_code, phase, brakes position, timestamp, rel timestamp)

    @staticmethod
    def _get_parameters_file_object(param_file_name):
        return EtheraProcessorParamFile(param_file_name)

    def _check_secondary_mission_files_preconditions(self, do_overwrite: bool) -> bool:
        if (not self.force_output_file_overwrite) and path.exists(self._param_file.rfFileName):
            print(f"ERROR: RF output file '{self._param_file.rfFileName}' exists.")
            return False
        # Check frequency files
        for fn in self._param_file.frequency_file_names:
            if (not self.force_output_file_overwrite) and path.exists(fn):
                print(f"ERROR: RF output file '{fn}' exists.")
                return False
        return True

    def _open_other_secondary_mission_files(self) -> bool:
        # Open rf file
        self._rfFile = open(self._param_file.rfFileName, 'w')
        self._rfFile_GOOD = open(self._param_file.rfFileName_GOOD, 'w')
        self._rfFile_GOOD_POOR = open(self._param_file.rfFileName_GOOD_POOR, 'w')
        # Open frequency files
        for freq, fn in self._param_file.frequency_file_names.items():
            f = open(fn, 'w')
            if f is None:
                print(f"ERROR: Cannot open file '{fn}'.")
                return False
            self._frequency_files.append(f)
            self.num_distance_rows.append(0)
            self.beacon_max_error.append(-1)
            self.beacon_total_error.append(0)
            self.frequency_files_mapping[freq] = len(self._frequency_files) - 1
        return True

    def _init_other_secondary_mission_files(self):
        for f in (self._rfFile, self._rfFile_GOOD, self._rfFile_GOOD_POOR):
            f.write(
                f'avg ts, avg ts rel, avg_gps_lat, avg_gps_long, avg_gps_x_pos, avg_gps_y_pos, '
                f'beacon0_ts, beacon0_latitude, beacon0_longitude, beacon0_freq, beacon0_rssi, beacon0_dist, '
                f'beacon1_ts, beacon1_latitude, beacon1_longitude, beacon1_freq, beacon1_rssi, beacon1_dist, '
                f'deltaT (sec.), avg_rel_alt,delay (msec), quality,'
                f'fixed error,x,y, error (m vs. gps), intermediate values\n')
        for f in self._frequency_files:
            f.write(f"ts,rel.ts (msec),rel.ts (sec), new GSP, lat.,long.,canX (m),canY (m),"
                    f"altitude,rel. altitude (m),frequency (MHz),rssi (dBm),distance (m from RF),"
                    f"distance (m from GPS),error (m)\n")

    def _write_other_secondary_mission_files(self):
        self._summary_file.write(
            f"  Generated {self.num_rf_positions} beacon measurements used to define position in: \n"
            f"    '{self._param_file.rfFileName}': {self.num_rf_positions} rows including {self.num_bad_rf_positions} "
            f"BAD row(s) (delay > {self._param_file.max_delay_between_beacons_for_POOR} or calculation issue)\n"
            f"    '{self._param_file.rfFileName_GOOD}': {self.num_good_rf_positions} rows (delay <= "
            f"{self._param_file.max_delay_between_beacons_for_GOOD}) \n"
            f"    '{self._param_file.rfFileName_GOOD_POOR}': {self.num_good_rf_positions + self.num_poor_rf_positions} "
            f"rows ({self._param_file.max_delay_between_beacons_for_GOOD} "
            f"< delay <= {self._param_file.max_delay_between_beacons_for_POOR}) \n")
        if self.num_good_rf_positions > 0:
            self._summary_file.write(
                f"    Error on good rows: avg.: {self.rf_total_error_good / self.num_good_rf_positions:.1f} m, "
                f"max.: {self.rf_max_error_good:.1f} m\n")
        if self.num_poor_rf_positions > 0:
            self._summary_file.write(
                f"    Error on poor rows: avg.: {self.rf_total_error_poor / self.num_poor_rf_positions:.1f} m, "
                f"max.: {self.rf_max_error_poor:.1f} m\n")
        if self.num_bad_rf_positions > 0:
            self._summary_file.write(
                f"    Error on bad rows: avg.: {self.rf_total_error_bad / self.num_bad_rf_positions:.1f} m, "
                f"max.: {self.rf_max_error_bad:.1f} m\n")
        self._summary_file.write(
            f"    NB: Quality is forced to BAD when no circle intersection could be computed (x=y=1E10). \n"
            f"        BAD quality calculations should not be used. \n"
            f"        Quality is degraded from GOOD to POOR, or from POOR to BAD when intermediate calculation failed\n"
            f"        and the error was 'corrected' by forcing ground distance to a beacon to 0.\n"
        )
        self._summary_file.write(
            f"  Generated distance information per frequency in separate files:\n")
        for freq, fn in self._param_file.frequency_file_names.items():
            idx = self.frequency_files_mapping[freq]
            self._summary_file.write(
                f"    {fn} ({self.num_distance_rows[idx]} rows")
            if self.beacon_max_error[idx] >= 0 and self.num_distance_rows[idx] > 0:
                self._summary_file.write(f", error: max={self.beacon_max_error[idx]:.1f}m, "
                                         f"avg={self.beacon_total_error[idx]/self.num_distance_rows[idx]:.1f} m")
            self._summary_file.write(")\n")
        self._summary_file.write(f"  Air-brakes information:\n")
        for ctrl_code, phase, air_brakes_position, ts, ts_rel in self.air_brakes_transitions:
            self._summary_file.write(f"    {ts} (rel. {ts_rel}): "
                                     f"Ctrl code={ctrl_code}({self.get_ctrl_code_legend(ctrl_code)}), phase={phase}, "
                                     f"position={air_brakes_position}\n")

    @staticmethod
    def get_ctrl_code_legend(code) -> str:
        """ Return a string as human-readable legend to a flight control code. """
        if code == 0:
            return 'No data'
        elif code == 1:
            return "Starting up"
        elif code == 2:
            return "Ground conditions"
        elif code == 3:
            return "Not descending"
        elif code == 4:
            return "Nominal descent"
        elif code == 10:
            return "Low Descent Velocity Alert"
        elif code == 11:
            return "High Descent Velocity Alert"
        else:
            return "UNEXPECTED CODE"

    def _close_other_secondary_mission_files(self):
        self._rfFile.close()
        self._rfFile_GOOD.close()
        self._rfFile_GOOD_POOR.close()
        for f in self._frequency_files:
            f.close()

    def _init_summary_file_for_secondary_mission(self, summary):
        prm = self._param_file
        summary.write(f"  Maximum delays between measurement from beacons:\n")
        summary.write(f"     GOOD <= {prm.max_delay_between_beacons_for_GOOD} msec\n")
        summary.write(f"     {prm.max_delay_between_beacons_for_GOOD} < POOR <= "
                      f"{prm.max_delay_between_beacons_for_POOR} msec\n")
        summary.write(f"      BAD > {prm.max_delay_between_beacons_for_POOR} msec\n")
        summary.write(f"  Beacon 0: {prm.beacon0_frequency:.1f} MHz \n")
        summary.write(f"    lat. {prm.beacon0_latitude:.6f}, long. {prm.beacon0_longitude:.6f} \n")
        summary.write(f"    X = {prm.beacon0_X:.2f} m, Y =  {prm.beacon0_Y:.2f} m \n")
        summary.write(f"  Beacon 1: {prm.beacon1_frequency:.1f} MHz \n")
        summary.write(f"    lat. {prm.beacon1_latitude:.6f}, long. {prm.beacon1_longitude:.6f} \n")
        summary.write(f"    X = {prm.beacon1_X:.2f} m, Y =  {prm.beacon1_Y:.2f} m \n")
        summary.write(f"  Distance between beacons: {prm.distance_between_beacons:.2f} m \n")
        summary.write(f"  Monitored can 0: {prm.can0_frequency:.1f} MHz \n")
        summary.write(f"  Monitored can 1: {prm.can1_frequency:.1f} MHz \n")

    def _get_csv_header(self):
        return super()._get_csv_header() + ',flightCtrlCode,brakePos,phase,freq.,rssi,dist.'

    def _process_air_brakes_mission_info(self, row: []) -> None:
        """ Collect statistics about air brakes mission.
        :param row: The current row from the file.
        """
        flight_ctrl_code = int(row[FLIGHT_CONTROL_CODE_IDX])
        flight_phase = int(row[FLIGHT_PHASE_IDX])
        air_brakes_position = int(row[AIR_BRAKES_POSITION_IDX])
        if flight_ctrl_code != self.previous_flight_control_code \
                or flight_phase != self.previous_flight_phase \
                or air_brakes_position != self.previous_brake_position:
            self.air_brakes_transitions.append((flight_ctrl_code, flight_phase, air_brakes_position, self._current_ts,
                                                self._current_ts_relative))
            self.previous_flight_control_code = flight_ctrl_code
            self.previous_flight_phase = flight_phase
            self.previous_brake_position = air_brakes_position

    def _process_secondary_mission_info(self, row: []):
        # 1. Collect RF info
        self.current_frequency = float(row[FREQUENCY_FIELD_IDX])
        change = False
        is_beacon = False
        beacon_id = -1
        prm = self._param_file
        if self.current_frequency > 0.1:
            # We have an RF measurement.
            # We need to (i) output it to the dedicated file after identifying whether the data
            #                is about a beacon (and in this case, about which one).
            #            (ii) for beacon frequencies, pair measurements to estimate position.
            # (a) Collect measured data.
            self.current_distance = int(row[DISTANCE_FIELD_IDX])
            self.current_rssi = int(float(row[RSSI_FIELD_IDX]))

            # (b) Detect whether this is about a beacon frequency, and collect useful data.
            if abs(self.current_frequency - self._param_file.beacon0_frequency) < 0.1:
                self.beacon0_distance = self.current_distance
                self.beacon0_rssi = self.current_rssi
                self.beacon0_ts = self._current_ts
                self.beacon0_can_x = self._current_can_x_gps
                self.beacon0_can_y = self._current_can_y_gps
                self.beacon0_rel_altitude = self._current_relative_altitude
                self.beacon0_latitude = self._current_latitude
                self.beacon0_longitude = self._current_longitude
                self.oldest_beacon_measure = 1
                is_beacon = True
                beacon_id = 0
                change = True
            if abs(self.current_frequency - self._param_file.beacon1_frequency) < 0.1:
                self.beacon1_distance = self.current_distance
                self.beacon1_rssi = self.current_rssi
                self.beacon1_ts = self._current_ts
                self.beacon1_can_x = self._current_can_x_gps
                self.beacon1_can_y = self._current_can_y_gps
                self.beacon1_rel_altitude = self._current_relative_altitude
                self.beacon1_latitude = self._current_latitude
                self.beacon1_longitude = self._current_longitude
                self.oldest_beacon_measure = 0
                is_beacon = True
                beacon_id = 1
                change = True

            # (c) Pair beacon measurements.
            if change and self.beacon0_distance != 0 and self.beacon1_distance != 0:
                # Add line in rf file and reset
                self.num_rf_positions += 1
                avg_ts = int((self.beacon0_ts + self.beacon1_ts) / 2)
                avg_rel_ts = avg_ts - self._param_file.originTS
                avg_rel_altitude = (self.beacon0_rel_altitude + self.beacon1_rel_altitude)/2
                avg_can_longitude = (self.beacon0_longitude + self.beacon1_longitude)/2
                avg_can_latitude = (self.beacon0_latitude + self.beacon1_latitude)/2
                avg_can_x = (self.beacon0_can_x + self.beacon1_can_x)/2
                avg_can_y = (self.beacon0_can_y + self.beacon1_can_y)/2
                delay_between_beacons = abs(self.beacon0_ts - self.beacon1_ts)
                x, y, info, fixed, error = compute_location(self._param_file, avg_can_x, avg_can_y,
                                                            self.beacon0_distance, self.beacon1_distance,
                                                            avg_rel_altitude)
                quality = define_rf_position_quality(prm, delay_between_beacons, x, y, fixed)
                if quality == "BAD":
                    self.num_bad_rf_positions += 1
                    self.rf_total_error_bad += error
                    if error > self.rf_max_error_bad:
                        self.rf_max_error_bad = error
                elif quality == "POOR":
                    self.num_poor_rf_positions += 1
                    self.rf_total_error_poor += error
                    if error > self.rf_max_error_poor:
                        self.rf_max_error_poor = error
                elif quality == "GOOD":
                    self.num_good_rf_positions += 1
                    self.rf_total_error_good += error
                    if error > self.rf_max_error_good:
                        self.rf_max_error_good = error
                else:
                    print(f"*** ERROR: unexpected quality indicator '{quality}'.")

                str_row = f"{avg_ts}, {avg_rel_ts},{avg_can_latitude},{avg_can_longitude},"\
                          f"{avg_can_x:.1f},{avg_can_y:.1f},"\
                          f"{self.beacon0_ts},{self.beacon0_latitude},{self.beacon0_longitude},"\
                          f"{self._param_file.beacon0_frequency}," \
                          f"{self.beacon0_rssi},{self.beacon0_distance}, "\
                          f"{self.beacon1_ts},{self.beacon1_latitude},{self.beacon1_longitude}," \
                          f"{self._param_file.beacon1_frequency}," \
                          f"{self.beacon1_rssi},{self.beacon1_distance}, "\
                          f"{abs(self.beacon0_ts - self.beacon1_ts)/1000.0}, "\
                          f"{avg_rel_altitude:.2f},{delay_between_beacons},{quality},{fixed},"\
                          f"{x:.2f},{y:.2f},{error:.1f}, {info}"\
                          f"\n"
                self._rfFile.write(str_row)
                if quality == "GOOD":
                    self._rfFile_GOOD.write(str_row)
                if quality != "BAD":
                    self._rfFile_GOOD_POOR.write(str_row)

                if self.oldest_beacon_measure == 0:
                    self.beacon0_distance = 0
                else:
                    self.beacon1_distance = 0

            # (d) Output a row in the right frequency file.
            self._output_frequency_file_row(is_beacon, beacon_id)

        # 2. Then proceed with the other secondary mission.
        self._process_air_brakes_mission_info(row)

    def _output_frequency_file_row(self, is_beacon: bool, beacon_id: int):
        """ Output one row in one of the frequency files, based on the self.current_xxxx values.
        :param: is_beacon True if the frequency a beacon's one.
        :param: beacon_id The id of the beacon (only valid if is_beacon is True).
        """
        gps_dist_from_beacon = 0
        error = 0
        idx = self.frequency_files_mapping[self.current_frequency]
        if is_beacon:
            if beacon_id == 0:
                gps_dist_from_beacon = get_distance_to_beacon(self._current_can_x_gps, self._current_can_y_gps,
                                                              self._current_relative_altitude,
                                                              self._param_file.beacon0_X,
                                                              self._param_file.beacon0_Y)
                recomputed_distance = get_distance(self.current_rssi, gps_dist_from_beacon, self.current_rssi)
                if abs(recomputed_distance - gps_dist_from_beacon) > 1:
                    raise Exception(f"*** Error: recomputing distance from RSSI fails for beacon {beacon_id}:\n"
                                    f"           gps_distance = {gps_dist_from_beacon:.1f} m\n"
                                    f"           computed distance from RSSI = {recomputed_distance:.1f}\n")
                error = abs(self.current_distance - gps_dist_from_beacon)
                if error > self.beacon_max_error[idx]:
                    self.beacon_max_error[idx] = error
                self.beacon_total_error[idx] += error
            elif beacon_id == 1:
                gps_dist_from_beacon = get_distance_to_beacon(self._current_can_x_gps, self._current_can_y_gps,
                                                              self._current_relative_altitude,
                                                              self._param_file.beacon1_X,
                                                              self._param_file.beacon1_Y)
                recomputed_distance = get_distance(self.current_rssi, gps_dist_from_beacon, self.current_rssi)
                if abs(recomputed_distance - gps_dist_from_beacon) > 1:
                    print(f"*** Error: recomputing distance from RSSI fails for beacon {beacon_id}:\n"
                          f"           gps_distance = {gps_dist_from_beacon:.1f} m\n"
                          f"           computed distance from RSSI = {recomputed_distance:.1f}\n")
                error = abs(self.current_distance - gps_dist_from_beacon)
                if error > self.beacon_max_error[idx]:
                    self.beacon_max_error[idx] = error
                self.beacon_total_error[idx] += error
            else:
                print(f"**** ERROR: got unexpected beacon_id {beacon_id} "
                      f"in EtheraFileProcessor._output_frequency_file_row.")

        idx = self.frequency_files_mapping[self.current_frequency]
        f = self._frequency_files[idx]
        f.write(f"{self._current_ts},{self._current_ts_relative},{self._current_ts_relative / 1000.0},"
                f"{int(self._current_new_gps_data)},"
                f"{self._current_latitude}, {self._current_longitude},"
                f"{self._current_can_x_gps:.1f}, {self._current_can_y_gps:.1f}, "
                f"{self._current_altitude:.1f},{self._current_relative_altitude:.1f},"
                f"{self.current_frequency:.1f},{self.current_rssi},{self.current_distance},"
                f"{gps_dist_from_beacon:.1f},{error:.1f}\n")
        self.num_distance_rows[idx] += 1

    def _append_secondary_mission_columns(self, row: []) -> None:
        # We do not append anything to the processed file.
        pass

    def _print_conclusion_message(self):
        super()._print_conclusion_message()
        print(f"RF positions in files:")
        print(f" - '{self._param_file.rfFileName}' ({self.num_rf_positions} rows).")
        print(f" - '{self._param_file.rfFileName_GOOD}' ({self.num_good_rf_positions} rows).")
        print(f" - '{self._param_file.rfFileName_GOOD_POOR}' ({self.num_good_rf_positions + self.num_poor_rf_positions}"
              f" rows).")
        print(f"RF rssi and distances in files:")
        for freq, fn in self._param_file.frequency_file_names.items():
            print(f" - '{fn}'")

    @staticmethod
    def get_num_secondary_mission_fields():
        """
        :return: The number of data fields in the input file for the secondary mission.
        """
        return NUMBER_OF_SECONDARY_MISSION_FIELDS
