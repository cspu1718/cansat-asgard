from os import path

from .RF_Calculations import get_distance_to_beacon
from .RF_FileReprocessor import RfFileReprocessor
from csv import reader as csv_reader

from .constants import FREQ_NUM_FIELDS, FREQ_GPS_DISTANCE_FIELD_IDX, FREQ_RSSI_FIELD_IDX, FREQ_REL_ALTITUDE_FIELD_IDX, \
    FREQ_CAN_X_FIELD_IDX, FREQ_CAN_Y_FIELD_IDX


class RfFileReprocessorUsingAltitude(RfFileReprocessor):
    """ An implementation of the reprocessor using multiple x0/rssi0 pairs, and selecting
        one according to the relative altitude."""

    USE_INTERPOLATION = False
    # If True, linear interpolation is used to evaluation x0 and rssi0 b
    # between the two closest altitude values. If False, the closest match is used.
    # Experience show that interpolation introduces additional errors: DO NOT USE IT!
    DBG = False
    # If true, debugging info is printed on stdout

    def __init__(self, calib_file_tag: str):
        super().__init__()
        self.calib_file_tag = calib_file_tag
        self.calibration_data = [[], []]  # a list of (x0, rssi0) pairs for each beacon.
        self.calibration_file_names = [f"{self.param_file.inputFileNameNoExt}.{calib_file_tag}"
                                       f".calibration.b.{self.param_file.beacon0_frequency}.csv",
                                       f"{self.param_file.inputFileNameNoExt}.{calib_file_tag}"
                                       f".calibration.b.{self.param_file.beacon1_frequency}.csv"]

    def check_files_preconditions(self) -> bool:
        for i in (0, 1):
            if not self.init_calibration_data(i):
                return False
        return super().check_files_preconditions()

    def init_calibration_data(self, beacon_id: int) -> bool:
        calibration_file_name = self.calibration_file_names[beacon_id]
        if not path.exists(calibration_file_name):
            print(f"ERROR: Cannot find input file '{calibration_file_name}'.")
            return False
        print(f"Opening calibration file '{calibration_file_name}...")
        f = open(calibration_file_name, 'r')
        if not f:
            print(f"**** Error opening calibration file '{calibration_file_name}")
            raise Exception(f"**** Error opening calibration file '{calibration_file_name}")
        print(f"Processing beacon #{beacon_id} frequency file ({calibration_file_name}).")
        reader = csv_reader(f, delimiter=',')

        # First row is the header.
        reader.__next__()

        for row in reader:
            if (len(row) != FREQ_NUM_FIELDS) or (not row[0].isdigit()):
                raise Exception(f"**** Invalid row detected in calibration file: {row[:5]} (...)")
            else:
                print(".", end='')
                # 1. Collect rssi/distance info.
                rssi = float(row[FREQ_RSSI_FIELD_IDX])
                rel_altitude = float(row[FREQ_REL_ALTITUDE_FIELD_IDX])
                can_x = float(row[FREQ_CAN_X_FIELD_IDX])
                can_y = float(row[FREQ_CAN_Y_FIELD_IDX])
                if beacon_id == 0:
                    beacon_x = self.param_file.beacon0_X
                    beacon_y = self.param_file.beacon0_Y
                elif beacon_id == 1:
                    beacon_x = self.param_file.beacon1_X
                    beacon_y = self.param_file.beacon1_Y
                else:
                    print(f"*** ERROR: Unexpected beacon id {beacon_id}. Recalibration aborted.")
                    return False
                gps_distance = get_distance_to_beacon(can_x, can_y, rel_altitude, beacon_x, beacon_y)
                gps_distance_in_file = float(row[FREQ_GPS_DISTANCE_FIELD_IDX])
                if abs(gps_distance - gps_distance_in_file) > 0.1:
                    print(f"While processing '{','.join(str(x) for x in row)}'")
                    raise Exception(f"*** Error: GPS distance inconsistent with the one found in file:\n"
                                    f" Found {gps_distance_in_file:.1f} m, calculated {gps_distance:.1f} m"
                                    f" collecting calibration data for beacon #{beacon_id}. row[0]={row[0]}")
                # 2. Store calibration info
                self.calibration_data[beacon_id].append((rel_altitude, gps_distance, rssi))
        print(f" ({len(self.calibration_data[beacon_id])} rows).")
        f.close()
        if len(self.calibration_data[beacon_id]) == 0:
            raise Exception(f"*** Error: did not find any data in the file")
        self.calibration_data[beacon_id].sort()
        return True

    def get_output_tag(self) -> str:
        return self.calib_file_tag

    def print_new_calibration_data_description(self, summary_file):
        summary_file.write(f"  New calibration data used")
        if self.USE_INTERPOLATION:
            summary_file.write(f" (using interpolation):\n")
        else:
            summary_file.write(f" (using closest match):\n")
        for i in (0, 1):
            summary_file.write(f"    {self.calibration_file_names[i]} ({len(self.calibration_data[i])} rows)\n")

    def get_beacon_calibration_data(self, beacon_id: int, rel_altitude: float):
        if self.DBG:
            print(f"Retrieving calibration data for beacon # {beacon_id}, alt. = {rel_altitude}")
        # Find enclosing altitudes
        cal_data = self.calibration_data[beacon_id]
        upper = 0
        while upper < (len(cal_data)-1) and cal_data[upper][0] < rel_altitude:
            upper += 1
        # Now either
        #    (i)  cal_data[upper].altitude is > than rel_altitude
        #         i.a: if not the first element, we should interpolate or choose between element upper-1 and upper
        #         i.b: if the first element, we should use it.
        #    (ii) cal_Data[upper] is the last element and its altitude is not > than the requested one:
        #         we should use it as such.
        if upper == 0:
            if self.DBG:
                print(f"First element. Returning {cal_data[upper]}")
            return cal_data[upper][1], cal_data[upper][2]
        elif upper == len(cal_data):
            if self.DBG:
                print(f"Last element. Returning {cal_data[upper]}")
            return cal_data[upper][1], cal_data[upper][2]
        else:
            if self.DBG:
                print(f"Target is between #{upper-1} ({cal_data[upper-1]}) and  #{upper} ({cal_data[upper]}m) ")
            ratio = (rel_altitude-cal_data[upper-1][0])/(cal_data[upper][0] - cal_data[upper-1][0])
            if self.DBG:
                print(f"Target ({rel_altitude}) is at {ratio} of interval")
            if self.USE_INTERPOLATION:
                x0 = cal_data[upper-1][1] + ratio*(cal_data[upper][1]-cal_data[upper-1][1])
                rssi0 = cal_data[upper-1][2] + ratio*(cal_data[upper][2]-cal_data[upper-1][2])
                if self.DBG:
                    print(f"Returning: x0={x0:.1f}, rssi0 = {rssi0}")
            else:
                if ratio < .5:
                    x0 = cal_data[upper - 1][1]
                    rssi0 = cal_data[upper - 1][2]
                    if self.DBG:
                        print(f"Returning lowest element: x0={x0:.1f}, rssi0 = {rssi0}")
                else:
                    x0 = cal_data[upper][1]
                    rssi0 = cal_data[upper][2]
                    if self.DBG:
                        print(f"Returning upper element: x0={x0:.1f}, rssi0 = {rssi0}")
            return x0, rssi0
