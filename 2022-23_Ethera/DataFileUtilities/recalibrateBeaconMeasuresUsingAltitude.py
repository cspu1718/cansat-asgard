#!/usr/bin/env python3
"""
This utility launches an instance of RfFileReprocessorUsingAltitude to reprocess data.
See the documentation of RfFileReprocessorUsingAltitude for details about the processing.
"""

from os import path
import sys

from EtheraFileUtilities.RF_FileReprocessorUsingAltitude import RfFileReprocessorUsingAltitude

MYSELF = path.basename(sys.argv[0])
MYSELF_NO_EXT = path.splitext(MYSELF)[0]
USAGE = f"Usage : {MYSELF}  aaa \n" \
         "        where aaa is a tag identifying the calibration data which is assumed to be in " \
        "         files 'xxxx.aaa.calibration.b.fff.f.csv'  with:" \
        "           xxxx identifies the original file (from processRecordFile.input.txt)" \
        "           fff.f is the frequency of a beacon (from processRecordFile.input.txt) " \
        "         Other data is retrieved from 'processRecordFile.input.txt'."
TITLE = "--------------------------------------------------------------------\n" \
        "-- Recalibration of beacon distance measurements (using altitude) --\n" \
        "--------------------------------------------------------------------"

print(TITLE)
if len(sys.argv) != 2:
    print(MYSELF_NO_EXT, ': Wrong number of parameters (Aborted)')
    print(USAGE)
    sys.exit(1)

try:
    tag = str(sys.argv[1])

except ValueError as e:
    print(f"{MYSELF}: Error:{e}.")
    print(USAGE)
    sys.exit(1)

print(f"Processing file with calibration data '{tag}'...")

processor = RfFileReprocessorUsingAltitude(tag)
if not processor.run():
    print(f"{MYSELF}: Error running processor. Aborted.")
    exit(2)
print(f"-- {MYSELF}: End of job. --")
exit(0)
