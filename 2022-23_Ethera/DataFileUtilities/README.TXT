This directory contains 2 general-purpose Python scripts:
   generateSimulatedCanData.py generates an almost likely test files to validate the data postprocessing.
   processRecordFile.py processes a data file retrieved from the can or the Ground Station, and:
      Add additional columns in a xxx.process.csv file, with derived data useful for producing charts
      Extracts the RF data in a number of additional files (see xxxx.summary.txt) for details
      Generates file xxxx.summary.txt with a bunch of statistics collected during the processing, and a
      detailed account of produced files.
      
It constitutes a PyCharm project which is dependent on the local environment and should be restructured to allow
for different users to use it in their working copy.

The logic is implemented in
   - a couple of generic classes in package CansatFileUtilities
   - a couple of project-specific subclasses and additional classes in EtheraFileUtilities.
Next projects should be able to reuse the CansatFileUtilities part unchanged.

Currently:
  - The project is intended to be used on MacOS, with a very specific version of the interpreter located in
    /usr/local/Cellar (although any interpreter v3.7+ should do).
  - The project is consequently not expected to run on any machine configured otherwise.

For a future version, it would be advisable to:
  - restructure the python sources (see @todo tag in DataFileUtilities.__init__.py
  - include the python scripts only in the GIT repository
  - Have every developer manage its PyCharm project (or other IDE project) locally and include the source directory
    as a content root of its project.

Ethera specificities:
Ethera includes a couple of additional scripts to recalibrate RF-measurements:
   - recalibrateBeaconMeasuresUsingAltitude.py
   - recalibrateBeaconMeasuresWithSingleData.py