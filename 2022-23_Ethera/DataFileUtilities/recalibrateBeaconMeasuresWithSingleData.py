#!/usr/bin/env python3
"""
This utility launches an instance of RfFileReprocessorWithSingleData to reprocess data.
See the documentation of RfFileReprocessorWithSingleData for details about the processing.
"""

from os import path
import sys

from EtheraFileUtilities.RF_FileReprocessorWithSingleData import RfFileReprocessorWithSingleData

MYSELF = path.basename(sys.argv[0])
MYSELF_NO_EXT = path.splitext(MYSELF)[0]
USAGE = f"Usage : {MYSELF}  x0_0 rssi0_0 x0_1 rssi0_1\n" \
         "        where x0_0 (float) is the value of the reference distance (x0) for beacon 0\n" \
         "              rssi0_0 (float) is the corresponding value of the RSSI (rssi0) for beacon 0\n" \
         "              x0_1 (float) is the value of the reference distance (x0) for beacon 1\n" \
         "              rssi0_1 (float) is the corresponding value of the RSSI (rssi0) for beacon 1\n" \
         "        Other data is retrieved from 'processRecordFile.input.txt'."
TITLE = "---------------------------------------------------\n" \
        "-- Recalibration of beacon distance measurements --\n" \
        "---------------------------------------------------"

print(TITLE)
if len(sys.argv) != 5:
    print(MYSELF_NO_EXT, ': Wrong number of parameters (Aborted)')
    print(USAGE)
    sys.exit(1)

try:
    x0_0 = float(sys.argv[1])
    rssi0_0 = float(sys.argv[2])
    x0_1 = float(sys.argv[3])
    rssi0_1 = float(sys.argv[4])
except ValueError as e:
    print(f"{MYSELF}: Error:{e}.")
    print(USAGE)
    sys.exit(1)

print(f"Processing file with {x0_0 }m/{rssi0_0} dBm (beacon 0) "
      f"and {x0_1 }m/{rssi0_1} dBm (beacon 1)...")

processor = RfFileReprocessorWithSingleData(x0_0, rssi0_0, x0_1, rssi0_1)
if not processor.run():
    print(f"{MYSELF}: Error running processor. Aborted.")
    exit(2)
print(f"-- {MYSELF}: End of job. --")
exit(0)
