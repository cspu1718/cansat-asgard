/**
    This is the main for the Osiris can.

    More info in the documentation of class CanSetupAndMain.
*/

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CanSetupAndLoop.h"
#include "OsirisAcquisitionProcess.h"
#include "OsirisCanCommander.h"

class Can : public CanSetupAndLoop<OsirisAcquisitionProcess, OsirisCanCommander> {
    virtual const char* getProjectName() const {
      return "OSIRIS";
    };
    virtual void printBanner()  const{
      Serial << " #####    " << ENDL;
      Serial << "#     #   ######  #####   ####   #####   ###### "  << ENDL;
      Serial << "#     #  #          #    #    #    #    #       "       << ENDL;
      Serial << "#     #  #          #    #    #    #    #       "       << ENDL;
      Serial << "#     #   #####     #    #####     #     #####  " << ENDL;
      Serial << "#     #        #    #    #  #      #          # "   << ENDL;
      Serial << "#     #        #    #    #   #     #          # "  << ENDL;
      Serial << " #####   ######   #####  #    #  #####  ######  "  << ENDL;
    };
};

Can can;

void setup() {
  can.setup();
}

void loop() {
  can.loop();
}
