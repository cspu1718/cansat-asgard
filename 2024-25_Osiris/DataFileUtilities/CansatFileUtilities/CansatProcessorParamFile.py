import csv
from os import path
from CansatFileUtilities import M_PER_DEGREE_LATITUDE, M_PER_DEGREE_LONGITUDE


class CansatProcessorParamFile:
    """
    The class managing the input file containing all parameters for the file processing
    (as far as the primary mission is concerned). Once the file is read, all parameters are
    available as public data members.
    This class is to be subclassed to handle additional information required by secondary mission (see
    methods to be overridden by subclass, below).
    """
    def __init__(self, param_file_name: str):
        """ Constructor
        :param str param_file_name: The name of the parameter file. It is expected to be found in the
        current directory. """
        self.paramFileName = param_file_name
        self._paramFile = None
        ## @name data parameters read from file
        # @{
        self.inputFileName = ''
        self.inputFileNameNoExt = ''
        self.outputFileName = ''
        self.summaryFileName = ''
        self.googlePathFileName = ''
        self.relevantDataStartTS = 0
        self.relevantDataStopTS = 0
        self.originTS = 0
        self.offsetTherm1 = 0.0
        self.XY_Origin_Latitude = 0.0
        self.XY_Origin_Longitude = 0.0
        ## @}

    def read(self) -> bool:
        """ Read the content of the input file
        Returns True if the file was successfully read, False otherwise.
        If the file cannot be found, a default
        file is created, to be completed by the user.
        """
        from . import comment_stripper
        if path.exists(self.paramFileName):
            self._paramFile = open(self.paramFileName, "r")
            reader = csv.reader(comment_stripper(self._paramFile))

            self._read_primary_mission_fields(reader)
            self._read_secondary_mission_fields(reader)
            self._paramFile.close()

            if not (self.relevantDataStartTS and self.relevantDataStopTS):
                print(f"Incomplete input file '{self.paramFileName}")
                return False
        else:
            print(f"Could not find parameters file '{self.paramFileName}' in current directory")
            self._write_default_input_file()
            print("Created a template in current directory, please complete it and run the script again.")
            return False
        return True

    def convert_to_xy(self, latitude, longitude) -> (float, float):
        """ Convert geographic coordinates (lat/long in decimal degrees) in cartesian coordinates.
        :param latitude: The latitude to convert
        :param longitude: The longitude to convert.
        :return: The X coordinate (O-E, positive to E)
        :return: The Y coordinate (N-S, positive to N)
        """
        if self.XY_Origin_Latitude == 0:
            print(f"*** Suspicious value for XY_Origin_Latitude ({self.XY_Origin_Latitude})."
                  f"    Did you call convert_to_xy() before setting the origin? ")
        return (longitude - self.XY_Origin_Longitude) * M_PER_DEGREE_LONGITUDE, \
            (latitude - self.XY_Origin_Latitude) * M_PER_DEGREE_LATITUDE

    def _write_default_input_file(self):
        """ Create a default parameters file with name self.paramFileName, in the current directory."""
        self._paramFile = open(self.paramFileName, "w")
        self._paramFile.write(f"##########################################\n")
        self._paramFile.write(f"# Parameter file for data file processing utility (Comma-separated values, CSV). \n"
                              f"# Complete nnnnn and xxxxx values, do not add or remove any line or column.\n")
        self._paramFile.write(f"##########################################\n")
        self._paramFile.write(f"#\n")
        self._paramFile.write(f"#\n")
        self._paramFile.write(
            "# Input file name (plain file from can or RF-Transceiver, must be in the same directory\n"
            "# as this file)\nxxxxx.csv\n")
        self._write_primary_mission_fields()
        self._write_secondary_mission_fields()
        self._paramFile.close()

    def _write_primary_mission_fields(self):
        """ Write the primary mission fields required in the input file. This method is called
            when a default, empty parameter file is generated. """
        self._paramFile.write("# Relevant data starts from (timestamp)\n00000\n")
        self._paramFile.write("# Relevant data stops at (timestamp)\n00000\n")
        self._paramFile.write("# Relative timestamps start at (timestamp)\n00000\n")
        self._paramFile.write("# Offset for temperature thermistor 1 \n0.0\n")
        self._paramFile.write("# Latitude of XY map origin (in degrees) \n000.0\n")
        self._paramFile.write("# Longitude of XY map origin (in degrees) \n000.0\n")

    def _read_integer_input_param(self, the_reader: csv.reader):
        """ Read an integer parameter from file.
        :param the_reader: The CSV reader to use to access the file.
        """
        try:
            local_row = the_reader.__next__()
            if len(local_row) == 1:
                value = int(local_row[0])
                if value == 0:
                    print("Error: found null value in parameters file (line {}).".format(the_reader.line_num))
                return value
            else:
                print("Error reading '{self.paramFileName}': line {the_reader.line_num} should contain 1 value.")
                print("      Got '{}'".format(",".join(local_row)))
                return 0
        except StopIteration:
            print(f"ERROR: Premature end of parameter file '{self.paramFileName}' while reading an integer.")
            raise

    def _read_float_input_param(self, the_reader: csv.reader):
        """ Read a float parameter from file.
        :param the_reader: The CSV reader to use to access the file.
        """
        try:
            local_row = the_reader.__next__()
            if len(local_row) == 1:
                value = float(local_row[0])
                return value
            else:
                print(f"Error reading '{self.paramFileName}': line {the_reader.line_num} should contain 1 value.")
                print("      Got '{}'".format(",".join(local_row)))
                return 0.0
        except StopIteration:
            print(f"ERROR: Premature end of parameter file '{self.paramFileName}' while reading a float.")
            raise

    def _read_primary_mission_fields(self, reader: csv.reader) -> None:
        """ Read all primary mission-related fields from the reader. """
        self.inputFileName = reader.__next__()[0]
        self.inputFileNameNoExt = path.splitext(self.inputFileName)[0]
        self.outputFileName = self.inputFileNameNoExt + '.processed.csv'
        self.summaryFileName = self.inputFileNameNoExt + ".summary.txt"
        self.googlePathFileName = self.inputFileNameNoExt + ".google_path.kml"
        self.relevantDataStartTS = self._read_integer_input_param(reader)
        self.relevantDataStopTS = self._read_integer_input_param(reader)
        self.originTS = self._read_integer_input_param(reader)
        self.offsetTherm1 = self._read_float_input_param(reader)
        self.XY_Origin_Latitude = self._read_float_input_param(reader)
        self.XY_Origin_Longitude = self._read_float_input_param(reader)

    ## @name Methods to be overridden in subclass
    #  ------------------------------------------
    # @{

    def _write_secondary_mission_fields(self) -> None:
        """ Append the parameters file with content related to secondary mission. `
            self._param_file is open and ready for writing."""
        print("No secondary mission parameters written in parameter file")

    def _read_secondary_mission_fields(self, reader: csv.reader) -> None:
        """ Read secondary mission parameters into public data members
        :param csv.reader reader: The reader object to use to call methoes _read_xxxx_infput_param()
                                  to access the parameters. """
        print("No secondary mission parameters read from parameter file")
    ## @}
