from random import Random
import copy
from .CansatTestDataGeneratorConfig import ACQUISITION_PERIOD, REC_PER_SECOND
from .Variation import Variation


class CansatRecord:
    """ A utility record class to generate simulated data. It contains all data present in an on-board
        CansatRecord, and provides a number of features to have this data modified in a consistent way,
        possibly with random noise added."""
    myRandom = Random()

    def __init__(self, start_ts):
        self.timestamp = start_ts
        self.newGPS_Data = 0
        self.gpsLatitude = 0.0
        self.gpsLongitude = 0.0
        self.gpsAltitude = 0.0
        self.pressure = 0.0
        self.altitude = 0.0
        self.refAltitude = 0.0
        self.verticalVelocity = 0.0
        self.bmpTemperature = 0.0
        self.therm1_Temperature = 0.0
        self.therm2_Temperature = 0.0

        self.countSinceNewGPS = 0
        self.latitudeVariation = None
        self.longitudeVariation = None
        self.altitudeVariation = None
        self.verticalVelocityVariation = None
        self.thermistorVariation = None
        self.bmpTempVariation = None

    def csv_line(self) -> str:
        """ Obtain a csv representation of the record.
        :return: the CSV string (with no final carriage return)."""
        return str(self.timestamp) + ',' \
            + str(self.newGPS_Data) + ',' \
            + '{:.5f},{:.5f},{:.1f},'.format(self.gpsLatitude, self.gpsLongitude, self.gpsAltitude) \
            + '{:.1f},{:.1f},{:.1f},{:.1f},'.format(self.bmpTemperature, self.pressure, self.altitude,
                                                    self.refAltitude) \
            + '{:.2f},{:.1f},'.format(self.verticalVelocity, self.therm1_Temperature)
            #+ '{:.1f},{:.1f},'.format(self.motor1_position, self.motor2_position)

    def noisy_version(self):
        """ Obtain a deep copy of this object, with noise added. GPS data is updated in the
            original object, to preserve noise when no new data arrives."""
        noisy = copy.deepcopy(self)
        noisy.add_noise()
        # update GPS data in original record
        self.gpsLatitude = noisy.gpsLatitude
        self.gpsLongitude = noisy.gpsLongitude
        self.gpsAltitude = noisy.gpsAltitude
        return noisy

    def set_descent_velocity(self, velocity: float):
        """ Configure the record with the provided descent velocity.
        This means:
            1. set vertical_velocity
            2. set corresponding altitude variation ( max variation = -current altitude),
               num records = (current altitude) / (velocity) * REC_PER_SECOND .
        :param float velocity: The new descent velocity."""
        self.verticalVelocity = velocity
        self.altitudeVariation = Variation(-self.altitude, self.altitude/velocity * REC_PER_SECOND)

    def add_noise(self):
        """ Add random noise on all data """
        if self.newGPS_Data == 1:
            if self.gpsLatitude != 0:
                self.gpsLatitude += self.myRandom.normalvariate(0, 0.00005)
            if self.gpsLongitude != 0:
                self.gpsLongitude += self.myRandom.normalvariate(0, 0.00005)
            if self.gpsAltitude != 0:
                self.gpsAltitude += self.myRandom.normalvariate(0, 0.6)
        self.bmpTemperature += self.myRandom.normalvariate(0, 0.1)
        self.pressure += self.myRandom.normalvariate(0, 0.5)
        self.altitude += self.myRandom.normalvariate(0, 0.5)
        self.verticalVelocity += self.myRandom.normalvariate(0, 0.3)
        self.therm1_Temperature += self.myRandom.normalvariate(0, 0.3)
        self.therm2_Temperature += self.myRandom.normalvariate(0, 0.4)
        #self.motor1_position = self.myRandom.randint(-10, 10)
        #self.motor2_position = self.myRandom.randint(-10, 10)

    def modify_record(self):
        """Modify the record according to the rules defined with the Variation objects."""
        self.timestamp += ACQUISITION_PERIOD
        self.countSinceNewGPS = (self.countSinceNewGPS + 1) % 3
        if self.countSinceNewGPS == 0 and self.gpsLatitude != 0:
            self.newGPS_Data = 1
            if self.latitudeVariation is not None:
                self.gpsLatitude += self.latitudeVariation.get_increment()
            if self.longitudeVariation is not None:
                self.gpsLongitude += self.longitudeVariation.get_increment()
                if self.gpsLongitude > 180:
                    print("ERROR: longitude={} > 180°".format(self.gpsLongitude))
                    exit(3)
        else:
            self.newGPS_Data = 0
        if self.altitudeVariation is not None:
            altitude_increment = self.altitudeVariation.get_increment()
            self.gpsAltitude += altitude_increment
            self.altitude += altitude_increment
            self.pressure -= altitude_increment / 10.0
        if self.verticalVelocityVariation is not None:
            self.verticalVelocity += self.verticalVelocityVariation.get_increment()
        if self.thermistorVariation is not None:
            temp_increment = self.thermistorVariation.get_increment()
            self.therm1_Temperature += temp_increment

        if self.bmpTempVariation is not None:
            self.bmpTemperature += self.bmpTempVariation.get_increment()
