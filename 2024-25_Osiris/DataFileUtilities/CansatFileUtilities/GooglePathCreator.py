# This file content a number of utility functions to create the GooglePath .KML file
# It is used from the CansatFileProcessor class.

class GooglePathCreator:
    """ A class with methods to create a Google path file (.klm).
    Usage:
        file=open("my_file.kml", 'w')
        GooglePathCreator.init_google_path_file(file, "my team")
        GooglePathCreator.start_path_until_take_off()
          (repetitively call GooglePathCreator.insert_position() here for each position
           before takeoff).
        GooglePathCreator.start_path_from_takeoff(file)
           (repetitively call GooglePathCreator.insert_position() here for each position
           from takeoff to touchdown).
        GooglePathCreator.start_path_from_touch_down(file)
          (repetitively call GooglePathCreator.insert_position() here for each position
           from touchdown).
        GooglePathCreator.terminate_path(file)
    """
    @classmethod
    def init_google_path_file(cls, file, team_name: str):
        """ Write initial content into .kml file
        :param file: The file to write to (already open)
        :param str team_name: The name of the team (or project)."""
        file.write('<?xml version="1.0" encoding="UTF-8"?>\n'
                   '<kml xmlns="http://www.opengis.net/kml/2.2">\n'
                   f'  <Document><name>Path of the {team_name} can</name>\n'
                   '    <Style id="flight">\n'
                   '      <LineStyle><color>7f00ffff</color><width>4</width></LineStyle>\n'
                   '      <PolyStyle><color>7f00ff00</color></PolyStyle>\n'
                   '    </Style>\n'
                   '    <Style id="before">\n')
        file.write('      <LineStyle><color>7fff0000</color><width>4</width></LineStyle>\n '
                   '      <PolyStyle><color>7f00ff00</color></PolyStyle>\n'
                   '    </Style>\n'
                   '    <Style id="after">\n'
                   '	   <LineStyle><color>7f0000ff</color><width>4</width></LineStyle>\n'
                   '      <PolyStyle><color>7f00ff00</color></PolyStyle>\n'
                   '    </Style>\n')

    @classmethod
    def start_path_until_takeoff(cls, file):
        """
        Write content of file to start the part of the path until take-off.
        This must be called after init_google_path_file().
        :param file: The file to write to (already open)
        """
        file.write('    <Placemark>\n'
                   '      <styleUrl>#before</styleUrl>\n'
                   '      <LineString>\n'
                   '          <extrude>1</extrude>'
                   '          <tessellate>1</tessellate><altitudeMode>clampToGround</altitudeMode>\n'
                   '          <coordinates>\n')

    @classmethod
    def insert_position(cls, file, latitude: float, longitude: float, altitude: float):
        """
        Insert a particular position in the current path.
        :param file: The file to write to (already open).
        :param float latitude: The latitude in decimal degrees.
        :param float longitude: The longitude in decimal degrees.
        :param float altitude: The latitude relative to the ground in m.
        """
        if abs(longitude) < 0.001 or abs(latitude) < 0.001:
            # discard invalid positions
            return
        file.write(f"\t\t{longitude},{latitude},{altitude}\n")

    @classmethod
    def start_path_from_takeoff(cls, file):
        """
        Close previous path and start path from take-off.
        :param file: The file to write to (already open).
        """
        file.write('         </coordinates>\n'
                   '      </LineString>\n'
                   '    </Placemark>\n'
                   '    <Placemark>\n'
                   '       <styleUrl>#flight</styleUrl>\n'
                   '       <LineString><extrude>1</extrude><tessellate>1</tessellate>'
                   '            <altitudeMode>absolute</altitudeMode>\n'
                   '       <coordinates>\n')

    @classmethod
    def start_path_from_touch_down(cls, file):
        """
        Close previous path and start path from touch-down.
        :param file: The file to write to (already open).
        """
        file.write('         </coordinates>\n'
                   '      </LineString>\n'
                   '    </Placemark>\n'
                   '    <Placemark>\n'
                   '       <styleUrl>#after</styleUrl>\n'
                   '       <LineString><extrude>1</extrude><tessellate>1</tessellate>'
                   '            <altitudeMode>clampToGround</altitudeMode>\n'
                   '       <coordinates>\n')

    @classmethod
    def terminate_path(cls, file):
        """
        Close previous last path and google path file.
        :param file: The file to write to (already open).
        """
        file.write('         </coordinates>\n'
                   '       </LineString>\n'
                   '    </Placemark>\n'
                   '  </Document>\n'
                   '</kml>')
