#
# This file contains all constants used to configure the generation of the Osiris-specific part of the test data by
# class OsirisTestDataGenerator.
#
from CansatFileUtilities import Variation
from CansatFileUtilities import START_LATITUDE, START_LONGITUDE, M_PER_DEGREE_LATITUDE, M_PER_DEGREE_LONGITUDE
