from CansatFileUtilities import CansatRecord
from random import randint
from math import floor


class OsirisRecord(CansatRecord):
    def __init__(self, start_ts):
        super().__init__(start_ts)
        self.flightControlCode = 0
        self.flightPhase = 0
        self.messageReceived = False
        self.messageSent = False


    def modify_record(self):
        super().modify_record()

    def csv_line(self) -> str:
        """ Obtain a csv representation of the record.
        :return: the CSV string (with no final carriage return)."""
        return str(self.timestamp) + ',' \
            + str(self.newGPS_Data) + ',' \
            + '{:.5f},{:.5f},{:.1f},'.format(self.gpsLatitude, self.gpsLongitude, self.gpsAltitude) \
            + '{:.1f},{:.1f},{:.1f},{:.1f},'.format(self.bmpTemperature, self.pressure, self.altitude,
                                                    self.refAltitude) \
            + '{:.2f},{:.1f},'.format(self.verticalVelocity, self.therm1_Temperature) \
            + '{:.1f},{:.1f},'.format(self.messageReceived, self.messageSent)
    def add_noise(self):
        """ Add random noise on all data """
        if self.newGPS_Data == 1:
            if self.gpsLatitude != 0:
                self.gpsLatitude += self.myRandom.normalvariate(0, 0.00005)
            if self.gpsLongitude != 0:
                self.gpsLongitude += self.myRandom.normalvariate(0, 0.00005)
            if self.gpsAltitude != 0:
                self.gpsAltitude += self.myRandom.normalvariate(0, 0.6)
        self.bmpTemperature += self.myRandom.normalvariate(0, 0.1)
        self.pressure += self.myRandom.normalvariate(0, 0.5)
        self.altitude += self.myRandom.normalvariate(0, 0.5)
        self.verticalVelocity += self.myRandom.normalvariate(0, 0.3)
        self.therm1_Temperature += self.myRandom.normalvariate(0, 0.3)
        self.messageReceived = self.myRandom.choice([True, False])
        self.messageSent = self.myRandom.choice([True, False])


    def csv_line(self):
        return super().csv_line()  \
            + str(self.flightControlCode) + ',' \
            + str(self.flightPhase)+ ','  \
            + str(self.messageReceived)+ ',' \
            + str(self.messageSent)