
from CansatFileUtilities import CansatFileProcessor
from . import OsirisProcessorParamFile
from os import path


FLIGHT_CONTROL_CODE_IDX = 11
AIR_BRAKES_POSITION_IDX = 12
FLIGHT_PHASE_IDX = 13
FREQUENCY_FIELD_IDX = 14
RSSI_FIELD_IDX = 15
DISTANCE_FIELD_IDX = 16
NUMBER_OF_SECONDARY_MISSION_FIELDS = 4


class OsirisFileProcessor(CansatFileProcessor):
    """ The processor used to handle data files from the Osiris cansat. It adds the required features to process
        the secondary mission data: RF relay.
    """
    def __init__(self, processor_name: str, force_output_file_overwrite: bool):
        """Constructor
               :param str processor_name: The name of this processor.
               :param bool force_output_file_overwrite: if True, output files are overwritten if existing. If
                           False, the processing fails is any output file already exists."""
        super().__init__(processor_name, "Osiris", force_output_file_overwrite)


    @staticmethod
    def _get_parameters_file_object(param_file_name):
        return OsirisProcessorParamFile(param_file_name)

    def _check_secondary_mission_files_preconditions(self, do_overwrite: bool) -> bool:
        return True

    def _open_other_secondary_mission_files(self) -> bool:
        return True

    def _init_other_secondary_mission_files(self):
        pass

    def _write_other_secondary_mission_files(self):
        self._summary_file.write(" No secondary mission info")

    @staticmethod
    def get_ctrl_code_legend(code) -> str:
        """ Return a string as human-readable legend to a flight control code. """
        if code == 0:
            return 'No data'
        elif code == 1:
            return "Starting up"
        elif code == 2:
            return "Ground conditions"
        elif code == 3:
            return "Not descending"
        elif code == 4:
            return "Nominal descent"
        elif code == 10:
            return "Low Descent Velocity Alert"
        elif code == 11:
            return "High Descent Velocity Alert"
        else:
            return "UNEXPECTED CODE"

    def _close_other_secondary_mission_files(self):
        pass

    def _init_summary_file_for_secondary_mission(self, summary):
        prm = self._param_file


    def _get_csv_header(self):
        return super()._get_csv_header() + ',flightCtrlCode,phase,motor1,motor2'


    def _process_secondary_mission_info(self, row: []):
        pass


    def _append_secondary_mission_columns(self, row: []) -> None:
        # We do not append anything to the processed file.
        pass

    def _print_conclusion_message(self):
        super()._print_conclusion_message()


    @staticmethod
    def get_num_secondary_mission_fields():
        """
        :return: The number of data fields in the input file for the secondary mission.
        """
        return NUMBER_OF_SECONDARY_MISSION_FIELDS
