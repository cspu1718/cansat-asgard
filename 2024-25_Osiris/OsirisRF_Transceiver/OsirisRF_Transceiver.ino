/**
    This is the main for the RF Transceiver of the Osiris projet, using the CanSetupAndLoop template.
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "OsirisRF_Transceiver.h"

OsirisRF_Transceiver receiver;

void setup() {
  if (!receiver.begin()) {
    exit(1);
  }
}

void loop() {
  receiver.run();
}
