#include "Arduino.h"
#include "OsirisConfig.h"
#include "OsirisXBeeClient.h"
#include "CSPU_Test.h"

#ifndef RF_ACTIVATE_API_MODE
#error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

// Variables de configuration
constexpr uint32_t  DefaultDestinationSH = OS_Client2_XBeeAddressSH;
constexpr uint32_t  DefaultDestinationSL = OS_Client2_XBeeAddressSL;
constexpr unsigned int StorageDelay = 20;

// ----------------- Variables Globales -------------------
CansatCanID MyCanID;             // Detected from XBee module.
HardwareSerial &RF = Serial1;
OsirisXBeeClient xbc(OS_Client2_XBeeAddressSH, OS_Client2_XBeeAddressSL);
CansatRecord myRecord;
char myString[xbc.MaxStringSize];
CansatFrameType stringType;
char myself[25];

// Identifiants des stations
constexpr uint8_t GroundStationID = 1;
constexpr uint8_t TargetStationID = 2;

unsigned long numRecordsToGroundOK = 0;

// ----------------- Fonctions ----------------------------

// Cette fonction traite la commande reçue et la renvoie à la station de destination
void processCmdRequest(char* cmd) {
  Serial << "Commande reçue : " << cmd << ENDL;

  // Ouverture du message pour l'envoyer à la station de destination
  xbc.openStringMessage(CansatFrameType::CmdResponse);
  xbc << "Commande reçue et envoyée à la station cible : " << cmd;
  xbc.closeStringMessage();

  // Affichage du résultat
  Serial << "Commande envoyée à la station cible : " << cmd << ENDL;
}

void sendStatusMsg() {
  Serial << "Envoi d'un message d'état." << ENDL;
  xbc.openStringMessage(CansatFrameType::StatusMsg);
  xbc << "Message d'état de la canette principale.";
  xbc.closeStringMessage();
}

// Fonction de vérification des commandes et d'envoi de réponses
void checkRF() {
  bool gotRecord;
  char incomingString[xbc.MaxStringSize + 1];
  uint8_t seqNbr;

  // Recevoir un message
  while (xbc.receive(myRecord, incomingString, stringType, seqNbr, gotRecord)) {
    if (gotRecord) {
      Serial << "Record reçu " << ENDL;
      // Ouverture du message pour l'envoyer à la station de destination
      xbc.openStringMessage(CansatFrameType::CmdResponse);
      xbc << "record reçu et envyé à la station cible";
      xbc.closeStringMessage();

      // Affichage du résultat
      Serial << "Commande envoyée à la station cible " << ENDL;

    }
    else {
      Serial << "String received: '" << incomingString << "'" << ENDL;
      // Ouverture du message pour l'envoyer à la station de destination
      xbc.openStringMessage(CansatFrameType::CmdResponse);
      xbc << incomingString << " RELAYED";
      xbc.closeStringMessage();
      // Affichage du résultat
      Serial << "string sent to target " << ENDL;

    }
  }
}


// Envoi de la commande à la station cible
void sendCmdToTargetStation(uint8_t targetStationID, const char* cmd) {
  Serial << "Envoi de la commande à la station cible..." << ENDL;
  xbc.openStringMessage(CansatFrameType::CmdRequest);
  xbc << cmd;  // Envoie de la commande à la station cible
  xbc.closeStringMessage();
}

// Fonction principale de setup
void setup() {
  DINIT(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  Serial << ENDL << ENDL;
  Serial << "***** Canette principale *****" << ENDL;
  Serial << "Initialisation terminée. Canette principale prête." << ENDL;
  RF.begin(115200);
  xbc.begin(RF); // no return value
  xbc.printConfigurationSummary(Serial);

}

// Fonction principale de loop
void loop() {
  // Vérifier et recevoir des messages (commandes) en provenance de la station au sol
  checkRF();
  // Petit délai pour éviter que la boucle ne soit trop rapide
  delay(400);
}
