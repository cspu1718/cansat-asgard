/* This program simulates the receive ground client: receive message and display on screen */

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#ifndef ARDUINO_ARCH_SAMD
#error "This program only works on SAMD boards (Feather MO_Express, ItsyBitsy M4 Express, etc.)"
#endif

#include "OsirisXBeeClient.h"
#include "CSPU_Test.h"
 
#ifndef RF_ACTIVATE_API_MODE
#error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

// --------------------- Globals --------------------
CansatCanID MyCanID;             // Detected from XBee module.
HardwareSerial &RF = Serial1;
OsirisXBeeClient xbc(XBeeAddressSH_01, XBeeAddressSL_01);
CansatRecord myRecord;
char myString[xbc.MaxStringSize];
CansatFrameType stringType;
char myself[25];

// --------------------------------------------------


// Fonction de réception de message via XBee et affichage
void checkRF() {
  bool gotRecord;
  char incomingString[xbc.MaxStringSize + 1];
  uint8_t seqNbr;

  // Recevoir un message
  while (xbc.receive(myRecord, incomingString, stringType, seqNbr, gotRecord)) {
    if (gotRecord) {
      Serial << "Record receive : " << ENDL;
    }
    else {
      Serial << "String receive: '" << incomingString << "'" << ENDL;
    }
  } // while
} // checkRF

// Fonction principale de setup
void setup() {
  DINIT(115200);

  pinMode(LED_BUILTIN, OUTPUT);
  Serial << ENDL << ENDL;
  Serial << "***** CLIENT AU SOL SKETCH *****" << ENDL;
  Serial << "Initializing Serials and communications..." << ENDL;
  RF.begin(115200);
  xbc.begin(RF); // Initialisation de la communication avec XBee

  xbc.printConfigurationSummary(Serial);

  // Initialisation du module XBee
  auto component = xbc.getXBeeSystemComponent();
  strcpy(myself, xbc.getLabel(component));

  // Vérifier si le module XBee est correctement configuré
  if (!xbc.checkXBeeModuleConfiguration()) {
    Serial << "*** Error: fix module configuration with XBee_ConfigureOsirisModule.ino before running the test!" << ENDL;
    Serial << "Aborted." << ENDL;
    Serial.flush();
    exit(-1);
  }

  // Indiquer que c'est une station au sol
  Serial << "Client au sol prêt à recevoir des messages..." << ENDL;
  Serial << "---------------------------------------" << ENDL;
}

void loop() {
  // Vérifier les commandes reçues
  checkRF();
}
