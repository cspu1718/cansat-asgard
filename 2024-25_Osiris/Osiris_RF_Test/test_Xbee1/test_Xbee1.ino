// Just regularly emit a message

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#ifndef ARDUINO_ARCH_SAMD
#error "This program only works on SAMD boards (Feather MO_Express, ItsyBitsy M4 Express, etc.)"
#endif

#include "OsirisConfig.h"
#include "OsirisXBeeClient.h"
#include "CSPU_Test.h"

#ifndef RF_ACTIVATE_API_MODE
#error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

constexpr uint32_t DefaultDestinationSH = OS_Can_XBeeAddressSH;
constexpr uint32_t DefaultDestinationSL = OS_Can_XBeeAddressSL;
constexpr unsigned int CommandEmissionPeriod = 3000; // ms. Period for sending a pseudo-command to the can.

HardwareSerial &RF = Serial1;
OsirisXBeeClient xbc(DefaultDestinationSH, DefaultDestinationSL);
unsigned long numMsgToCanOK = 0; // Count OK records to ground
unsigned long numMsgToCanKO = 0; // Count KO records to ground
elapsedMillis elapsed = 1000;

void sendCommand() {
  char msg[50] ;
  sprintf(msg, "%ld dummy command message", millis());
  xbc.openStringMessage(CansatFrameType::CmdRequest);
  xbc << msg;
  bool ok = xbc.closeStringMessage();
  Serial << "Sent: '" << msg << "'" << ENDL;
  if (ok) {
    numMsgToCanOK++;
  } else {
    numMsgToCanKO++;
  }
}

void setup() {
  DINIT_WITH_TIMEOUT(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  Serial << ENDL << ENDL;
  Serial << "***** GROUND STATION Simulation *****" << ENDL;
  Serial << "Initializing Serials and communications..." << ENDL;
  RF.begin(115200);
  xbc.begin(RF); // no return value
  xbc.printConfigurationSummary(Serial);

  // Check the module is properly configured
  if (!xbc.checkXBeeModuleConfiguration()) {
    Serial << "*** Error: fix module configuration with XBee_ConfigureOsirisModule.ino before running the test!" << ENDL;
    Serial << "Aborted." << ENDL;
    Serial.flush();
    exit(-1);
  }

  Serial << "Initialisation over." << ENDL << ENDL;

  Serial << "Sending commands to Can (ID=" << xbc.getXBeeModuleID(DefaultDestinationSH, DefaultDestinationSL) << ", ";
  PrintHexWithLeadingZeroes(Serial, DefaultDestinationSH);
  Serial << " - ";
  PrintHexWithLeadingZeroes(Serial, DefaultDestinationSL);
  Serial << ") every " << CommandEmissionPeriod / 1000 << " sec" << ENDL;
  Serial << "---------------------------------------" << ENDL;
}

void loop() {
  if (elapsed > CommandEmissionPeriod) {
    elapsed = 0;
    sendCommand();
    // Optionally print stats about command sends (if needed)
    Serial << "Commands sent: " << numMsgToCanOK << " OK, " << numMsgToCanKO << " KO" << ENDL;
  }

  CSPU_Test::heartBeat(); // Keeps the test heart beating, if needed
}
