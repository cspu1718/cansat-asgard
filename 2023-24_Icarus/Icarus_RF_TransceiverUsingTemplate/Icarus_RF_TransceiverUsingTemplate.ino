/**
    This is the main for the Icarus projet, using the CanSetupAndLoop template (was not used during the
    project, but used to test CanSetupAndLoop template.
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatRF_Transceiver.h"
#include "IcarusRecord.h"

CansatRF_Transceiver<IcarusRecord> receiver;

void setup() {
  if (!receiver.begin()){
    exit(1);
  }
}

void loop() {
  receiver.run();
}
