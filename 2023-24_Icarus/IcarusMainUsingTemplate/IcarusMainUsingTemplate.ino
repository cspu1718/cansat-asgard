/**
    This is the main for the Icarus projet, using the CanSetupAndLoop template (was not used during the
    project, but used to test CanSetupAndLoop template.
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CanSetupAndLoop.h"
#include "IcarusAcquisitionProcess.h"
#include "IcarusCanCommander.h"

class Can : public CanSetupAndLoop<IcarusAcquisitionProcess, IcarusCanCommander> {
    virtual const char* getProjectName() const {
      return "ICARUS";
    };
    virtual void printBanner()  const{
      Serial << "########    " << ENDL;
      Serial << "   #      ######    #     ######   #      #  ###### "  << ENDL;
      Serial << "   #      #       #   #   #     #  #      #  # "       << ENDL;
      Serial << "   #      #      #     #  #    #   #      #  # "       << ENDL;
      Serial << "   #      #      #######  #####    #      #   #####  " << ENDL;
      Serial << "   #      #      #     #  #    #   #      #       #"   << ENDL;
      Serial << "   #      #      #     #  #     #  #      #       # "  << ENDL;
      Serial << "########  ###### #     #  #      #  ######   ###### "  << ENDL;
    };
};

Can can;

void setup() {
  can.setup();
}

void loop() {
  can.loop();
}
