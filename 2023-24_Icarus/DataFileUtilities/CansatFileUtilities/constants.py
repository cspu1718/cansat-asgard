# All constant definitions for processing Cansat data files
# Those constants are assumed to be valid for any Cansat project.
# NB: This should be a docstring, but for some reasons, when using a module
#     docstring, it is used by doxygen as a docstring for the first next element.
#     using doxygen @namespace or @file does not help.

NUM_PRIMARY_MISSION_FIELDS = 11
## @name Index of the various primary mission fields in the can's CSV file to be processed
# @{
TIMESTAMP_FIELD_IDX = 0
NEW_GPS_DATA_FIELD_IDX = 1
GPS_LATITUDE_FIELD_IDX = 2
GPS_LONGITUDE_FIELD_IDX = 3
GPS_ALTITUDE_FIELD_IDX = 4
ALTITUDE_FIELD_IDX = 7
REF_ALTITUDE_FIELD_IDX = 8
THERM1_TEMP_FIELD_IDX = 10
## @}

## @name Conversion from geographical to cartesian coordinates
# ref.: https://www.omnicalculator.com/other/latitude-longitude-distance
# @{
M_PER_DEGREE_LONGITUDE = 70270
M_PER_DEGREE_LATITUDE = 111200
## @}
