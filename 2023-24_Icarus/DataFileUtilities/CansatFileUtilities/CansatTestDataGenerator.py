from itertools import repeat
from os import path

from CansatFileUtilities import CansatRecord
from .CansatTestDataGeneratorConfig import *


class CansatTestDataGenerator:
    """ A class that generates simulated data for a Cansat project, taking into account the primary data only.
        Subclass for each specific project.
    """
    def __init__(self, record: CansatRecord):
        self.totalNumberOfRecords = 0
        if path.exists(self._output_file_name()):
            if FORCE_OUTPUT_FILE_OVERWRITE:
                print('Overwriting data file \'' + self._output_file_name() + '\'...')
            else:
                print('*** ERROR: Output file \'' + self._output_file_name() + '\' exists (Aborted)')
                exit(3)
        else:
            print('Generating data file \'' + self._output_file_name() + '\'...')
        self.outputFile = open(self._output_file_name(), 'w')
        self.record = record
        self.record.altitude = START_ALTITUDE
        self.record.pressure = START_PRESSURE
        self.record.refAltitude = START_ALTITUDE
        self.record.bmpTemperature = START_TEMP + 5.1
        self.record.therm1_Temperature = START_TEMP

    def _generate_records_down_to_ground(self, ground_altitude, number_of_records) -> None:
        """
        Generate records until the ground altitude is reached. If it is not reached within max_number_of_records,
        an error message is output. When the ground is reached, the variation of vertical velocity, latitude, longitude,
        altitude, pressure and temperature are cancelled. Vertical velocity is set to 0.
        :param ground_altitude:  The altitude of the ground (absolute)
        :param number_of_records: The number of records to generate.
        """
        ground = False
        while number_of_records > 0:
            self._generate_records(1)
            if not ground and self.record.altitude < ground_altitude:
                self.record.verticalVelocityVariation = None
                self.record.verticalVelocity = 0
                self.record.longitudeVariation = None
                self.record.latitudeVariation = None
                self.record.altitudeVariation = None
                self.record.thermistorVariation = None
                self.record.bmpTempVariation = None
                print(f"  Ground reached at TS {self.record.timestamp}")
                ground = True
            number_of_records -= 1
        if not ground:
            print(f"**** Error: number of record exhausted ({number_of_records}), "
                  f"altitude not reached ({ground_altitude}) "
                  f"current altitude: {self.record.altitude:.1f} m")

    def _generate_records_down_to_altitude(self, target_altitude: float, max_number_of_records: int = 1000) -> None:
        """
        Generate records until the target altitude is reached. If it is not reached within number_of_records,
        an error message is output.
        :param target_altitude:  The altitude of the ground (absolute)
        :param max_number_of_records: The number of records to generate.
        """
        records_left = max_number_of_records
        target_reached = False
        while not target_reached and records_left > 0:
            self._generate_records(1)

            records_left -= 1
        if target_reached:
            print(f"  {target_altitude}m reached at TS {self.record.timestamp}")
        else:
            print(f"*** Error: number of record exhausted ({max_number_of_records}), "
                  f"target altitude not reached ({target_altitude}) "
                  f"current altitude: {self.record.altitude:.1f} m")

    def _generate_records(self, number) -> None:
        """
        Generate a number of records, applying noise if ADD_NOISE is true.
        :param number: The number of records to generate.
        """
        for _ in repeat(None, int(number)):
            self.record.modify_record()
            if ADD_NOISE:
                self.outputFile.write(self.record.noisy_version().csv_line() + '\n')
            else:
                self.outputFile.write(self.record.csv_line() + '\n')
        self.totalNumberOfRecords += number

    ## @name Methods to be overwritten in subclass.
    # @{
    def _output_file_name(self):
        """ Possibly override this method in subclass."""
        return "CansatTestData.csv"

    def generate(self):
        """ This is the main generation method. Since generation cannot possibly be performed separately for
            primary and secondary data, it has to be implemented in the subclass."""
        print("*** Implement method 'generate()' in subclass of CansatTestDataGenerator")

    def _close_files(self):
        """ Close all files. Be sure to call super()._close_files() if this method is overridden."""
        self.outputFile.close()

    def _terminate(self):
        """ Terminate the generation. Be sure to call super()._terminate() if this method is overriden. """
        self._close_files()
        print('Generated', self.totalNumberOfRecords, 'records. End of job.')
    ## @}
