""" @package DataFileUtilities.CommentStripper
A small utility to strip CSV files from comments.
"""


def comment_stripper(iterator):
    """
    Comment stripper for csv files (ignores lines starting with '#')
    Usage:
    reader = csv.reader (comment_stripper(open (csv_file)))
    """
    for line in iterator:
        if line[:1] == '#':
            continue
        if not line.strip():
            continue
        yield line
