""" @package DataFileUtilities
The python utilities used to manage data files on the ground.
@todo After completion of Icarus project:
        1. move scripts into IcarusFileUtilities and change their names
           to make them specific to Icarus.
        2. move all python code in a new root (cansat_asgard/python? cansat_asgard/cspu_python
           package?)
        3. Have the data, configuration file in the working directory, which should be _outside_
           the python sources.
"""