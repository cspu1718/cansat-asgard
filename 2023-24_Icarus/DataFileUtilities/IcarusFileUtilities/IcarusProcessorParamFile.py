# The class managing the specific Icarus aspects of the processing input file.

from CansatFileUtilities import CansatProcessorParamFile
import csv
from math import sqrt


class IcarusProcessorParamFile(CansatProcessorParamFile):
    """ The class in charge of the input parameter's file for project Icarus. It extends CansatProcessorParamFile
     for the secondary mission."""
    def __init__(self, input_file_name: str):
        super().__init__(input_file_name)

    def _write_secondary_mission_fields(self):
        #self._paramFile.write("# Leave XY origin to (0.000, 0.0000) to use the mid-point between the beacons.\n")
        pass

    def _read_secondary_mission_fields(self, reader: csv.reader):
        pass