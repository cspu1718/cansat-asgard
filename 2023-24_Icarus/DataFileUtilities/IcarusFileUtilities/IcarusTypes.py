from math import log10, sqrt, floor
from CansatFileUtilities import M_PER_DEGREE_LATITUDE, M_PER_DEGREE_LONGITUDE


class Beacon:
    """ An auxiliary object simulating the behaviour of a beacon, for generating test data for Icarus RF mission."""
    def __init__(self, frequency, latitude, longitude, rssi0, x0):
        self.frequency = frequency
        self.latitude = latitude
        self.longitude = longitude
        self.rssi0 = rssi0
        self.x0 = x0
        self.k0 = pow(10, (self.rssi0 + 20*log10(self.x0))/20)

    def distance_from_rssi(self, rssi):
        """ Calculate the distance from a rssi value, using the calibration provided in the constructor.
            (assuming a theoretical spherical propagation."""
        return floor(self.k0/pow(10, rssi/20))

    def get_distance_and_rssi(self, latitude, longitude, rel_altitude):
        """ Compute distance and RSSI measured from beacon, when can is at gps latitude/longitude and
            provided relative altitude.
        """
        delta_latitude = latitude - self.latitude
        delta_longitude = longitude - self.longitude
        delta_x = delta_longitude*M_PER_DEGREE_LONGITUDE
        delta_y = delta_latitude * M_PER_DEGREE_LATITUDE
        ground_distance = sqrt(delta_x*delta_x + delta_y*delta_y)
        distance = sqrt(ground_distance**2 + rel_altitude**2)
        rssi = self.rssi0 + 20*log10(self.x0) - 20*log10(distance)
        rssi = floor(rssi)
        # print(f"beacon lat={self.latitude}, long={self.longitude}")
        # print(f"delta lat={delta_latitude}, delta long={delta_longitude}")
        # print(f"delta x={delta_x}, y={delta_y}")
        # print(f"received lat={latitude}, long={longitude}, x0={self.x0}, dist={distance}")
        # print(f"rssi = {rssi}, dist from rssi: {self.distance_from_rssi(rssi)}")
        return self.distance_from_rssi(rssi), rssi


class MonitoredCan:
    """ An auxiliary object simulating the behaviour of a monitored can, for generating test data for Icarus
    RF mission."""
    def __init__(self, frequency, distance, rssi, distance_variation):
        self.frequency = frequency
        self.distance = distance
        self.rssi = rssi
        self.distanceVariation = distance_variation
