from datetime import datetime
from os import path
from csv import reader as csv_reader

from CansatFileUtilities import M_PER_DEGREE_LONGITUDE, M_PER_DEGREE_LATITUDE
from IcarusFileUtilities import IcarusProcessorParamFile
from IcarusFileUtilities.RF_Calculations import get_distance, compute_location, define_rf_position_quality, \
    get_distance_to_beacon
from IcarusFileUtilities.constants import RF_NUM_FIELDS, RF_B0_RSSI_FIELD_IDX, RF_B1_RSSI_FIELD_IDX, \
    RF_AVG_CAN_X_FIELD_IDX, RF_AVG_CAN_Y_FIELD_IDX, RF_AVG_REL_ALT_FIELD_IDX, RF_DELTA_T_FIELD_IDX, \
    FREQ_REL_ALTITUDE_FIELD_IDX, FREQ_GPS_DISTANCE_FIELD_IDX, FREQ_CAN_X_FIELD_IDX, FREQ_CAN_Y_FIELD_IDX, \
    FREQ_RSSI_FIELD_IDX, FREQ_NUM_FIELDS, FREQ_ERROR_FIELD_IDX, RF_AVG_TS_FIELD_INDEX, FREQ_TS_FIELD_IDX


class RfFileReprocessor:
    """
    This utility processes the xxxx.rf.all.csv file, produced by the IcarusFileProcessor,
    and produces new files, recalculating the distances to the beacons and the can positions using different x0/rssi0
    couples.

    Specialized subclasses implement different strategies, to define the x0,rssi0 pair to use for the
    recalculations.

    It generates the following files:
      - xxx.tag.rf.recalib.all.csv
      - xxx.tag.rf.recalib.good.csv
      - xxx.tag.rf.recalib.poor.csv
      - xxx.tag.b.fff.f.csv
      - xxx.tag.recalib.summary.txt
     where 'xxx' is the name of the original file
           'tag' identifies the recalibration data used
           'fff.f' is the frequency of a beacon.
    """

    def __init__(self):
        self.param_file = IcarusProcessorParamFile("processRecordFile.input.txt")
        if not self.param_file.read():
            return
        self.rf_input_file_name = "UNDEFINED"

        self.rf_output_file_name_all = "UNDEFINED"
        self.rf_output_file_name_good = "UNDEFINED"
        self.rf_output_file_name_good_poor = "UNDEFINED"
        self.summary_file_name = "UNDEFINED"
        self.rf_input_file = None
        self.rf_output_file_all = None
        self.rf_output_file_good = None
        self.rf_output_file_good_poor = None
        self.summary_file = None
        self.input_file_header_row = "UNDEFINED HEADER"
        self.beacon_frequencies = [0.0, 0.0]
        self.num_distance_rows = [0, 0]
        self.in_frequency_files_names = [None, None]  # key = beacon ID
        self.in_frequency_files = [None, None]
        self.out_frequency_files_names = [None, None]
        self.out_frequency_files = [None, None]
        self.out_frequency_files_max_error = [0, 0]
        self.out_frequency_files_max_error_ts = [0, 0]
        self.out_frequency_files_total_error = [0, 0]
        self.out_frequency_files_previous_max_error = [0, 0]
        self.out_frequency_files_previous_total_error = [0, 0]

        # statistics
        self.numSkippedRows = 0
        self.numRows = 0
        self.numGoodRows = 0
        self.numPoorRows = 0
        self.rf_max_error_good = 0
        self.rf_max_error_good_ts = 0
        self.rf_max_error_poor = 0
        self.rf_max_error_poor_ts = 0
        self.rf_max_error_bad = 0
        self.rf_max_error_bad_ts = 0
        self.rf_total_error_good = 0
        self.rf_total_error_poor = 0
        self.rf_total_error_bad = 0

    def get_output_tag(self) -> str:
        """ Return a string used to tag files containing newly calibrated data."""
        return "Undefined output tag (implement in subclass!)"

    def print_new_calibration_data_description(self, summary_file):
        """ Write some text in summary file to describe the data used to recalibrated. """
        summary_file.write("Undefined calibration data description (implement in subclass)!\n")

    def get_beacon_calibration_data(self, beacon_id: int, rel_altitude: float):
        """ Return the calibration info to use to the provided beacon and altitude
        :param beacon_id: 0 or 1: the id of the beacon
        :param rel_altitude: The altitude for which calibration is requested.
        :return: x0 value.
        :return: rssi0 value.
        """
        print(f"*** Error: method get_beacon_calibration_data() must be implemented in subclass.")
        return 0, 0

    def check_files_preconditions(self) -> bool:
        """
         Check pre-condition to ensure a successful opening of input and output files (but do not actually open
         them) to avoid creating some, while others cannot be opened.
         :return: True if everything is ok, and object is ready for processing.
        """
        self.rf_input_file_name = self.param_file.rfFileName
        file_id = self.param_file.inputFileNameNoExt
        output_tag = self.get_output_tag()
        self.rf_output_file_name_all = file_id + '.' + output_tag + ".rf.all.csv"
        self.rf_output_file_name_good = file_id + '.' + output_tag + ".rf.good.csv"
        self.rf_output_file_name_good_poor = file_id + '.' + output_tag + ".rf.good-poor.csv"
        self.summary_file_name = file_id + '.' + output_tag + ".summary.txt"
        self.beacon_frequencies[0] = self.param_file.beacon0_frequency
        self.beacon_frequencies[1] = self.param_file.beacon1_frequency
        for idx in (0, 1):
            self.in_frequency_files_names[idx] = \
                self.param_file.frequency_file_names[self.beacon_frequencies[idx]]
            self.out_frequency_files_names[idx] = file_id + '.' + output_tag + ".b." + \
                str(self.beacon_frequencies[idx]) + ".csv"

        # Check input file can be found.
        if not path.exists(self.rf_input_file_name):
            print(f"ERROR: Cannot find input file '{self.rf_input_file_name}'.")
            return False
        for idx in (0, 1):
            if not path.exists(self.in_frequency_files_names[idx]):
                print(f"ERROR: Cannot find input file '{self.in_frequency_files_names[idx]}'.")
                return False
        return True

    @staticmethod
    def open_one_file(file_name: str, mode: str):
        f = open(file_name, mode)
        if f is None:
            print(f"ERROR: Cannot open fine {file_name}.")
            return False
        else:
            print(f"  Opening '{file_name}'...")
            return f

    def open_files(self) -> bool:
        """ Actually open all input and output files. This method is called after all checks have been
            performed to ensure successful creation.
            :return: True if everything is ok, and object is ready for processing.
        """
        self.rf_input_file = open(self.rf_input_file_name, 'r')
        if not self.rf_input_file:
            print(f"Error: cannot open input file '{self.rf_input_file_name}' (Aborted).")
            return False
        self.rf_output_file_all = self.open_one_file(self.rf_output_file_name_all, 'w')
        self.rf_output_file_good = self.open_one_file(self.rf_output_file_name_good, 'w')
        self.rf_output_file_good_poor = self.open_one_file(self.rf_output_file_name_good_poor, 'w')
        self.summary_file = self.open_one_file(self.summary_file_name, 'w')
        for idx in (0, 1):
            self.in_frequency_files[idx] = self.open_one_file(self.in_frequency_files_names[idx], 'r')
            if not self.in_frequency_files[idx]:
                return False
            self.out_frequency_files[idx] = self.open_one_file(self.out_frequency_files_names[idx], 'w')
            if not self.out_frequency_files[idx]:
                return False
        return self.rf_output_file_all and self.rf_output_file_good and self.rf_output_file_good_poor \
            and self.summary_file

    def init_output_files(self):
        """
        Write initial content in output files.
        :return: N/A.
        """
        prm = self.param_file
        summary = self.summary_file
        summary.write(f"RF File reprocessor, {datetime.now()}\n\n")
        summary.write(f"Input parameters:\n")
        summary.write(f"  Reprocessed file: {self.rf_input_file_name}\n")
        summary.write(
            f"  Origin of XY map: X=0 at long. {prm.XY_Origin_Longitude:.6f}, Y=0 at lat. {prm.XY_Origin_Latitude:.6f}"
            f" \n")
        summary.write(
            f"  Conversion lat/long vers XY: {M_PER_DEGREE_LONGITUDE}m/°long., {M_PER_DEGREE_LATITUDE}m/°lat.\n")
        summary.write(f"  Maximum delays between measurement from beacons:\n")
        summary.write(f"     GOOD <= {prm.max_delay_between_beacons_for_GOOD} msec\n")
        summary.write(f"     {prm.max_delay_between_beacons_for_GOOD} < POOR <= "
                      f"{prm.max_delay_between_beacons_for_POOR} msec\n")
        summary.write(f"      BAD > {prm.max_delay_between_beacons_for_POOR} msec\n")
        summary.write(f"  Beacon 0: {prm.beacon0_frequency:.1f} MHz \n")
        summary.write(f"    lat. {prm.beacon0_latitude:.6f}, long. {prm.beacon0_longitude:.6f} \n")
        summary.write(f"    X = {prm.beacon0_X:.2f} m, Y =  {prm.beacon0_Y:.2f} m \n")
        summary.write(f"  Beacon 1: {prm.beacon1_frequency:.1f} MHz \n")
        summary.write(f"    lat. {prm.beacon1_latitude:.6f}, long. {prm.beacon1_longitude:.6f} \n")
        summary.write(f"    X = {prm.beacon1_X:.2f} m, Y =  {prm.beacon1_Y:.2f} m \n")
        summary.write(f"  Distance between beacons: {prm.distance_between_beacons:.2f} m \n")
        self.print_new_calibration_data_description(summary)

    def close_files(self):
        """ close all input and output files.
        """
        summ = self.summary_file

        summ.write(f"Beacon distances:\n")
        for idx in (0, 1):
            summ.write(f"  #{idx} {self.beacon_frequencies[idx]} MHz: "
                       f"{self.num_distance_rows[idx]} rows")
            if self.out_frequency_files_max_error[idx] >= 0 and self.num_distance_rows[idx] > 0:
                summ.write(f", error: max={self.out_frequency_files_max_error[idx]:.1f}m "
                           f"(at ts {self.out_frequency_files_max_error_ts[idx]}), "
                           f"avg={self.out_frequency_files_total_error[idx]/self.num_distance_rows[idx]:.1f}m")
                summ.write(f" (previously: max={self.out_frequency_files_previous_max_error[idx]:.1f}m, "
                           f"avg={self.out_frequency_files_previous_total_error[idx]/self.num_distance_rows[idx]:.1f}"
                           f"m)")
            summ.write("\n")

        summ.write(f"RF position files:\n")
        summ.write(f"  Skipped {self.numSkippedRows} rows in input file\n")
        summ.write(f"  Processed {self.numRows} rows from input file into: \n")
        summ.write(f"    {self.numGoodRows} good rows. ")
        if self.numGoodRows > 0:
            summ.write(f"Error: avg.: {self.rf_total_error_good/self.numGoodRows:.1f} m, "
                       f"max.: {self.rf_max_error_good:.1f} m at avg TS {self.rf_max_error_good_ts}\n")
        summ.write(f"    {self.numPoorRows} poor rows. ")
        if self.numPoorRows > 0:
            summ.write(f"Error: avg.: {self.rf_total_error_poor/self.numPoorRows:.1f} m, "
                       f"max.: {self.rf_max_error_poor:.1f} m at avg TS {self.rf_max_error_poor_ts}\n")
        num_bad = self.numRows-self.numPoorRows-self.numGoodRows
        summ.write(f"    {num_bad} bad rows. ")
        if num_bad > 0:
            summ.write(f"Error: avg.: {self.rf_total_error_bad/num_bad:.1f} m, "
                       f"max.: {self.rf_max_error_bad:.1f} m at avg TS {self.rf_max_error_bad_ts}\n")
        summ.close()
        self.rf_output_file_all.close()
        self.rf_output_file_good.close()
        self.rf_output_file_good_poor.close()
        for idx in (0, 1):
            self.in_frequency_files[idx].close()
            self.out_frequency_files[idx].close()

    def process_position_files(self):
        reader = csv_reader(self.rf_input_file, delimiter=',')

        # First row is the header.
        row = reader.__next__()
        row.append("beacon0_new_distance")
        row.append("beacon1_new_distance")
        row.append("new_x")
        row.append("new_y")
        row.append("new_quality")
        row.append("new_error")
        row.append("new_info")
        header = ','.join(str(x) for x in row) + "\n"
        self.rf_output_file_all.write(header)
        self.rf_output_file_good.write(header)
        self.rf_output_file_good_poor.write(header)

        for row in reader:
            if (len(row) != RF_NUM_FIELDS) or (not row[0].isdigit()):
                print(f"Skipped row (length={len(row)}): {row[:5]} (...)")
                self.numSkippedRows += 1
                continue
            else:
                print(row)
                self.numRows += 1
                avg_ts = int(row[RF_AVG_TS_FIELD_INDEX])
                print(".", end='')
                # 1. Recompute distances
                avg_rel_altitude = float(row[RF_AVG_REL_ALT_FIELD_IDX])
                beacon0_rssi = float(row[RF_B0_RSSI_FIELD_IDX])
                x0_0, rssi0_0 = self.get_beacon_calibration_data(0, avg_rel_altitude)
                beacon0_new_distance = get_distance(beacon0_rssi, x0_0, rssi0_0)
                beacon1_rssi = float(row[RF_B1_RSSI_FIELD_IDX])
                x0_1, rssi0_1 = self.get_beacon_calibration_data(1, avg_rel_altitude)
                beacon1_new_distance = get_distance(beacon1_rssi, x0_1, rssi0_1)
                # 2. Recompute positions
                avg_can_x = float(row[RF_AVG_CAN_X_FIELD_IDX])
                avg_can_y = float(row[RF_AVG_CAN_Y_FIELD_IDX])
                x, y, info, fixed, error = compute_location(self.param_file, avg_can_x, avg_can_y,
                                                            beacon0_new_distance, beacon1_new_distance,
                                                            avg_rel_altitude)
                # 4. Update quality indicator
                delay_between_beacons = int(float(row[RF_DELTA_T_FIELD_IDX]) * 1000)
                quality = define_rf_position_quality(self.param_file, delay_between_beacons, x, y, fixed)
                # 5. Complete row with new information
                row.append(f"{beacon0_new_distance:.1f}")
                row.append(f"{beacon1_new_distance:.1f}")
                row.append(f"{x:.1f}")
                row.append(f"{y:.1f}")
                row.append(quality)
                row.append(f"{error:.1f}")
                row.append(info)

                # 6. Accumulate error info and output in appropriate file
                line = ','.join(str(x) for x in row)+"\n"
                self.rf_output_file_all.write(line)
                if quality == "BAD":
                    print('BAD')
                    self.rf_total_error_bad += error
                    if error > self.rf_max_error_bad:
                        self.rf_max_error_bad = error
                        self.rf_max_error_bad_ts = avg_ts
                elif quality == "POOR":
                    print('POOR')
                    self.numPoorRows += 1
                    self.rf_total_error_poor += error
                    if error > self.rf_max_error_poor:
                        self.rf_max_error_poor = error
                        self.rf_max_error_poor_ts = avg_ts
                    self.rf_output_file_good_poor.write(line)
                elif quality == "GOOD":
                    print('GOOD')
                    self.numGoodRows += 1
                    self.rf_total_error_good += error
                    if error > self.rf_max_error_good:
                        self.rf_max_error_good = error
                        self.rf_max_error_good_ts = avg_ts
                    self.rf_output_file_good.write(line)
                    self.rf_output_file_good_poor.write(line)
                else:
                    print(f"*** ERROR: unexpected quality indicator '{quality}'.")
        print()

    def print_conclusion_message(self):
        print(f"End of job. See details in '{self.summary_file_name}'.")

    def process_beacon_frequency_file(self, b_id: int):
        print(f"Processing beacon #{b_id} frequency file ({self.in_frequency_files_names[b_id]}).")
        reader = csv_reader(self.in_frequency_files[b_id], delimiter=',')

        # First row is the header.
        row = reader.__next__()
        row.append("new_distance")
        row.append("new_error")
        # row.append("new_info")
        header = ','.join(str(x) for x in row) + "\n"
        self.out_frequency_files[b_id].write(header)

        for row in reader:
            if (len(row) != FREQ_NUM_FIELDS) or (not row[0].isdigit()):
                print(f"Skipped row (length={len(row)}): {row[:5]} (...)")
                self.numSkippedRows += 1
                continue
            else:
                self.numRows += 1
                print(".", end='')
                # 1. Recompute distances
                beacon_rssi = float(row[FREQ_RSSI_FIELD_IDX])
                rel_altitude = float(row[FREQ_REL_ALTITUDE_FIELD_IDX])
                x0, rssi0 = self.get_beacon_calibration_data(b_id, rel_altitude)
                beacon_new_distance = get_distance(beacon_rssi, x0, rssi0)
                can_x = float(row[FREQ_CAN_X_FIELD_IDX])
                can_y = float(row[FREQ_CAN_Y_FIELD_IDX])
                if b_id == 0:
                    beacon_x = self.param_file.beacon0_X
                    beacon_y = self.param_file.beacon0_Y
                elif b_id == 1:
                    beacon_x = self.param_file.beacon1_X
                    beacon_y = self.param_file.beacon1_Y
                else:
                    print(f"*** ERROR: Unexpected beacon id {b_id}. Recalibration aborted.")
                    return
                gps_distance = get_distance_to_beacon(can_x, can_y, rel_altitude, beacon_x, beacon_y)
                gps_distance_in_file = float(row[FREQ_GPS_DISTANCE_FIELD_IDX])
                if abs(gps_distance - gps_distance_in_file) > 0.1:
                    print(f"*** Error: GPS distance inconsistent with the one found in file:")
                    print(f"           Found {gps_distance_in_file:.1f} m, calculated {gps_distance:.1f} m")
                error = abs(gps_distance - beacon_new_distance)
                previous_error = float(row[FREQ_ERROR_FIELD_IDX])
                # 2. Complete row with new information
                row.append(f"{beacon_new_distance:.1f}")
                row.append(f"{error:.1f}")
                # row.append(info)

                # 3. Accumulate error info and output in appropriate file
                line = ','.join(str(x) for x in row) + "\n"
                self.out_frequency_files[b_id].write(line)
                self.num_distance_rows[b_id] += 1
                self.out_frequency_files_total_error[b_id] += error
                if error > self.out_frequency_files_max_error[b_id]:
                    self.out_frequency_files_max_error[b_id] = error
                    self.out_frequency_files_max_error_ts[b_id] = int(row[FREQ_TS_FIELD_IDX])
                self.out_frequency_files_previous_total_error[b_id] += previous_error
                if previous_error > self.out_frequency_files_previous_max_error[b_id]:
                    self.out_frequency_files_previous_max_error[b_id] = previous_error
        print()

    def run(self) -> bool:
        try:
            if not self.check_files_preconditions():
                return False
            if not self.open_files():
                return False
            self.init_output_files()
            print(f"Processing file '{self.rf_input_file_name}'...")
            self.process_position_files()
            for i in (0, 1):
                self.process_beacon_frequency_file(i)
            self.close_files()
            self.print_conclusion_message()
            return True
        except StopIteration:
            print(f"ERROR: Exception during processor execution.")
            return False
