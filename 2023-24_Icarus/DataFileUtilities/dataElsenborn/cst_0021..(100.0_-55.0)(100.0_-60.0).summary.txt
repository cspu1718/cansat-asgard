RF File reprocessor, 2023-05-02 23:01:20.542695

Input parameters:
  Reprocessed file: cst_0021.rf.all.csv
  Origin of XY map: X=0 at long. 6.181910, Y=0 at lat. 50.483900 
  Conversion lat/long vers XY: 70270m/°long., 111200m/°lat.
  Maximum delays between measurement from beacons:
     GOOD <= 3000 msec
     3000 < POOR <= 5000 msec
      BAD > 5000 msec
  Beacon 0: 433.5 MHz 
    lat. 50.486980, long. 6.185660 
    X = 342.50 m, Y =  263.51 m 
  Beacon 1: 434.5 MHz 
    lat. 50.482395, long. 6.185230 
    X = -167.36 m, Y =  233.30 m 
  Distance between beacons: 510.75 m 
  New calibration data used:
    beacon 0: x0 = 100.0 m, rssi0 = -55.0 dBm
    beacon 1: x0 = 100.0 m, rssi0 = -60.0 dBm
Beacon distances:
  #0 433.5 MHz: 32 rows, error: max=2210.0m, avg=322.6m (previously: max=896.5m, avg=416.1m)
  #1 434.5 MHz: 35 rows, error: max=921.9m, avg=220.4m (previously: max=891.2m, avg=592.5m)
RF position files:
  Skipped 0 rows in input file
  Processed 115 rows from input file into: 
    30 good rows. Error: avg.: 364.5 m, max.: 849.0 m
    17 poor rows. Error: avg.: 1329.9 m, max.: 6199.5 m
    68 bad rows. Error: avg.: 8.3 m, max.: 562.6 m
