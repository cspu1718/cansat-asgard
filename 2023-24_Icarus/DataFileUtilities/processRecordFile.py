#!/usr/bin/env python3

""" Script to process Icarus data file.

    It modifies the original file into an output file as follows, in all files:
        - Add header line
        - Add and complete column relativeTS
        - Add and complete column relativeAltitude
        - Add and complete columns tempTherm1corr
    It generates a separate file with distance information, with columns
        TS, relative TS, dist for freq1, dist for freq2, dist for freq3 etc.
        (frequencies are identifies in _summary_file file below).
    Save key information in file MYSELF._summary_file.txt in the current directory. If the file exists
    the program stops.
    Read required information for processing from file MYSELF.input.txt in current directory. If the
    file does not exist, an empty version of it, properly formatted is generated.
    To run the script at command prompt:
        make sure the directory containing the script is in PATH and PYTHON_PATH
        cd <directory containing data file to process>
        processRecordFile.py
"""
# import sys  # for argv
from os import path
import sys
from IcarusFileUtilities import IcarusFileProcessor

FORCE_OUTPUT_FILE_OVERWRITE = True  # for development only.

MYSELF = path.basename(sys.argv[0])
MYSELF_NO_EXT = path.splitext(MYSELF)[0]
TITLE = '-------------------------------------------\n' \
        '-- Icarus record file processing utility --\n' \
        '-------------------------------------------'
USAGE = 'Usage: ' + MYSELF + ' (file)'

print(TITLE)
if len(sys.argv) != 1:
    print(MYSELF_NO_EXT, ': Extraneous argument(s) detected (Aborted)')
    print(USAGE)
    sys.exit(1)

processor = IcarusFileProcessor(MYSELF_NO_EXT,FORCE_OUTPUT_FILE_OVERWRITE)  # Select here the right processor.
if not processor.run():
    print(f"{MYSELF}: Error running processor. Aborted.")
    exit(2)
print(f"-- {MYSELF}: End of job. --")
exit(0)

