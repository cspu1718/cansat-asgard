#define DEBUG_CSPU
#include "DebugCSPU.h"

int switchState;
int lastSwitchState = HIGH;  // Variable to store the previous switch state, high is set as initial value for the microswitch used
int counter;

void setup() {
  Serial.begin(115200);
  while (!Serial);
  Serial.println("ok");
  pinMode(13, INPUT);
  counter = 0;
}

void loop() {
  switchState = digitalRead(13);
  if (switchState == HIGH) {
    // Serial.println("switch open");
  }
  else {
    // Serial.println("switch closed");
    if (switchState != lastSwitchState) { //compare actual switchState with the previous one, increments the counter if needed
      counter = counter + 1;
      Serial.println(counter);
    }
  }
  lastSwitchState = switchState; // Update the last switch state
  delay(50);
}
