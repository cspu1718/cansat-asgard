/*
   HBridgeMotor.h
*/

#pragma once

#include "Arduino.h"

/** @ingroup CSPU_Motors
    @brief Interface class to control a DC motor power using a H-bridge.
    - Motor TO BE COMPLETED
    - H-Bridge.
*/
class HBridgeMotor {

  public :
    /** Run the motor in forward direction.
      @param power defines the power to control the velocity of the motor
              (between 0 and 255).
    */
    virtual void SetForward(const uint8_t power) = 0;

    /** Stop the motor.
    */
    virtual void Brake() = 0;

    /** Run the motor in backwards direction.
      @param power defines the power to control the velocity of the motor
              (between 0 and 255).
    */
    virtual void SetBackwards(const uint8_t power) = 0;

    /** Run the motor in forward direction for a predefined duration and
        speed. This method is synchronous and return after the motor
        is stopped again.
        @param power defines the power to control the velocity of the motor
              (between 0 and 255).
        @param duration The duration of the movement, in msec.
    */
    virtual void TurnForward(const uint32_t duration, const uint8_t power);

    /** Run the motor in backwards direction for a predefined duration and
        speed. This method is synchronous and return after the motor
        is stopped again.
        @param power defines the power to control the velocity of the motor
              (between 0 and 255).
        @param duration The duration of the movement, in msec.
    */
    virtual void TurnBackwards (const uint32_t duration, uint8_t power);
};
