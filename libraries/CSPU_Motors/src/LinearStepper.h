/*
   LinearStepper.h
*/
#pragma once
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

/**   @ingroup CSPU_Motors
      @brief Interface class to control a LinearStepper motor.
    The class has been tested during project Ethera with:
    - LinearStepper SM0804.
    - DRV8834.
*/
class LinearStepper {

  public:
    /** Method to call before any other method. Initializes whatever must be.
     *  @param theDirectionPin Pin number of the pin used as step pin .The Step Pin is used to give a direction to the steps.
        @param theStepPin Pin number of the pin used as direction pin.The Direction Pin is used to make steps forward and backward with the DirectionPin.
        @param theSleepPin Pin number of the pin used sleep pin. The Sleep Pin is used to turn off et on the pin.
        @param totalNumOfSteps The total number of motor steps which separate position 0 from position 100.
        @return True if initialisation was successful, false otherwise.
    */
    bool begin(uint8_t theDirectionPin, uint8_t theStepPin, uint8_t theSleepPin,  uint32_t totalNumOfSteps = 4823);
    
    /** Obtain the current position of the linear stepper..
        @return The current position, between 0 & 100 defining.
    */
	uint8_t getPosition (){
	  return round((currentStepPosition*100.0f)/totalNumberOfSteps);
	};

    /** Move the linear stepper to the requested position.
        @param position A number between 0 & 100 defining the requested position.
    */
    void setPosition (uint8_t position);
    
    /** Reset the position to 0. */
    void resetPosition();
    
    /** Set to position 0 and force additional steps towards 0.
      @param extraSteps The number of extra steps to force.
    */
    void forceToZero(uint32_t extraSteps = 100);
    
    /** Configure the number of steps between position 0 and 100. */
    void setTotalNumberOfSteps(uint32_t newValue) {
      totalNumberOfSteps = newValue;
    };
    
    /** Obtain the total number of steps between position 0 and 100. */
    uint32_t getTotalNumberOfSteps() const {
      return totalNumberOfSteps;
    };

    /** Configure a slow-down factor. It multiplies the minimum delay during which
     *  the step pin is hold HIGH and LOW. If this method is not called, the
     *  slow-down factor is 1 (i.e. the stepper used the maximum speed).
     *  @param theSlowDownFactor The coefficient used to multiply the minimum delay.
     */
    void setSlowDownFactor(uint8_t theSlowDownFactor) {
    	durationHigh = theSlowDownFactor*MinDurationHigh;
    	durationLow = theSlowDownFactor*MinDurationLow;
    }

    /** Update the current position assumed by the stepper to the provide position.
     *  (use this method to inform the object about the current position of the stepper.
     *  @param newPosition The position to assume from now on (Integer from 0 to 100).
     */
    void setCurrentPosition(uint8_t newPosition) {
    	currentStepPosition = newPosition;
    }

  private:
    /** Method to configure the current direction. 
     *  @param positive True = forward direction, False = backward direction.
     */
    void setDirection(bool positive);
    
    /** Method for move the linear stepper by numSteps steps in the current direction. 
     *  @param numSteps The number of steps to use to move the linear stepper. 
     */
    void moveN_Steps(long int numSteps); 

    uint8_t pinDir; /**< Number of the pin connected to pin #12 of the DRV8834 */
    uint8_t pinStep; /**< Number of the pin connected to pin #6 of the DRV8834 */
    uint8_t pinSleep; /**< Number of the pin connected to pin #9 of the DRV8834 */
    uint32_t currentStepPosition; /**< The current position of the stepper */
    uint32_t totalNumberOfSteps; /**< The total number of steps between position 0 and 100 */

    uint16_t durationHigh = MinDurationHigh; /**< The duration used for holding step pin HIGH, in µs. */
    uint16_t durationLow = MinDurationLow; /**< The duration used for holding step pin LOW, in µs. */
    const uint16_t MinDurationHigh = 150; /**< The minimum duration for holding step pin HIGH, in µs, minimum 150 µs */
    const uint16_t MinDurationLow = 100; /**< The minimum duration for holding step pin LOW, in µs, minimum 100 µs */

};
