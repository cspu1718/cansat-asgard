#include "CansatConfig.h"
#define DBG_OPEN 0
#define DBG_CLOSED 0
#define DBG_COUNTER 0
#define DBG_TIME 0
#include "DebugCSPU.h"
#include "ClickCounter.h"

bool ClickCounter::begin(uint8_t thePinNumber) {
  lastSwitchState = HIGH;
  pinMode(pinNumber, INPUT);
  pinNumber = thePinNumber;
  counter = 0;
  return true;
}

bool ClickCounter::count(uint16_t target, uint16_t maxDelayBetweenClicksInMsec) {
  int switchState;
  uint16_t localCount = 0;
  elapsedMillis elapsed = 0;
  while (localCount < target) {
    if (elapsed > maxDelayBetweenClicksInMsec) { /**< Maximum delay between clicks. If too much time passed between clicks: return false.*/
      DPRINTS (DBG_TIME, "Clickcounter timeout: ");
      DPRINTLN(DBG_TIME, elapsed);
      return false;
    }

    switchState = digitalRead(pinNumber);
    if (switchState == HIGH) {
      DPRINTSLN(DBG_OPEN, "switch open");
    }
    else {
      DPRINTSLN(DBG_CLOSED, "switch closed");
      if (switchState != lastSwitchState) { /**< Compare actual switchState with the previous one, increments the counter if needed. */
        counter++;
        localCount++;
        elapsed = 0;
        DPRINTS (DBG_COUNTER, "Click detected. Total counter: ");
        DPRINTLN(DBG_COUNTER, counter);
        DPRINTS (DBG_COUNTER, "Counter: ");
        DPRINTLN(DBG_COUNTER, localCount);
      }
    }
    lastSwitchState = switchState; /**< Update the last switch state. */
    delay(1);
  }
  return true;
}
