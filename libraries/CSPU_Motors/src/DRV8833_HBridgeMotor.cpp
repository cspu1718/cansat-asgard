/*
   DRV8833_HBridgeMotor.cpp
*/

#include "CansatConfig.h"
#define DBG_BACKWARDS 0
#define DBG_FORWARD 0
#define DBG_BEGIN 0
#define DBG_BRAKE 0
#include "DebugCSPU.h"
#include "DRV8833_HBridgeMotor.h"

bool DRV8833_HBridgeMotor::begin(uint8_t forwardPin, uint8_t reversePin) {
  DRV8833_HBridgeMotorForward_Pin = forwardPin;
  DRV8833_HBridgeMotorReverse_Pin = reversePin;
  DPRINTS(DBG_BEGIN, "DRV8833_HBridgeMotorForward_Pin=");
  DPRINTLN(DBG_BEGIN, DRV8833_HBridgeMotorForward_Pin );
  DPRINTS(DBG_BEGIN, "DRV8833_HBridgeMotorReverse_Pin=" );
  DPRINTLN(DBG_BEGIN, DRV8833_HBridgeMotorReverse_Pin );
  pinMode(DRV8833_HBridgeMotorForward_Pin, OUTPUT);
  pinMode(DRV8833_HBridgeMotorReverse_Pin, OUTPUT);
  Brake();
  return true;
}


void DRV8833_HBridgeMotor::SetForward(const uint8_t power) {
  DPRINTSLN(DBG_FORWARD, "DRV8833_H-bridge: turning forward");
  analogWrite(DRV8833_HBridgeMotorReverse_Pin, 0);
  analogWrite(DRV8833_HBridgeMotorForward_Pin, power);
}

void DRV8833_HBridgeMotor::Brake() {
  DPRINTSLN(DBG_BRAKE, "DRV8833 braking");
  analogWrite(DRV8833_HBridgeMotorForward_Pin, 0);
  analogWrite(DRV8833_HBridgeMotorReverse_Pin, 0);
}

void DRV8833_HBridgeMotor::SetBackwards(const uint8_t power) {
  DPRINTSLN(DBG_FORWARD, "DRV8833_H-bridge: turning backwards");
  analogWrite(DRV8833_HBridgeMotorForward_Pin, 0);
  analogWrite(DRV8833_HBridgeMotorReverse_Pin, power);
}
