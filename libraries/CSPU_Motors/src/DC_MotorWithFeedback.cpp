/*
   DC_MotorWithFeedback.cpp
*/

#include "CansatConfig.h"
#define DEBUG_CSPU
#define DBG_BEGIN 0
#define DBG_BACKWARDS 0
#define DBG_FORWARD 0
#include "DebugCSPU.h"
#include "DC_MotorWithFeedback.h"
constexpr bool AssumeMoveWhenTimeout = true;

bool DC_MotorWithFeedback::begin(HBridgeMotor& theMotor, uint8_t clickPinNumber, uint16_t maxDelayBetweenClicksInMsec) {
  DPRINTS(DBG_BEGIN, "Click pin number=");
  DPRINTLN(DBG_BEGIN, clickPinNumber );
  motor = &theMotor;
  cc.begin(clickPinNumber);
  maxDelayBetweenClicks = maxDelayBetweenClicksInMsec;
  position = 0;
  return true;
}

bool DC_MotorWithFeedback::TurnForward (const uint32_t numberOfClicks, const uint8_t power) {
  DPRINTS(DBG_FORWARD, "Turn forward, ");
  DPRINT(DBG_FORWARD, numberOfClicks);
  DPRINTSLN(DBG_FORWARD, " clicks");
  motor->SetForward(power);
  bool result = cc.count(numberOfClicks, maxDelayBetweenClicks);
  if (result) {
    DPRINTSLN(DBG_FORWARD, "Counting ok");
  }
  if (AssumeMoveWhenTimeout || result) {
    position += numberOfClicks;
  }
  DPRINTSLN(DBG_FORWARD, "Deactivation of the motor");
  motor->Brake();
  return result;
}

bool DC_MotorWithFeedback::TurnBackwards (const uint32_t numberOfClicks, const uint8_t power) {
  DPRINTS(DBG_BACKWARDS, "Turn backwards, ");
  DPRINT(DBG_BACKWARDS, numberOfClicks);
  DPRINTSLN(DBG_BACKWARDS, " clicks");
  motor->SetBackwards(power);
  bool result = cc.count(numberOfClicks, maxDelayBetweenClicks);
  if (result) {
    DPRINTSLN(DBG_BACKWARDS, "Counting ok");
  }
   if (AssumeMoveWhenTimeout || result) {
    position -= numberOfClicks;
  }
  DPRINTSLN(DBG_BACKWARDS, "Deactivation of the motor");
  motor->Brake();
  return result;
}
