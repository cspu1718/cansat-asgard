#pragma once

class ClickCounter {

  public :
    /** Method to call before any other method. Initializes whatever must be.
       @param thePinNumber the pin number for the switch.
       @return true if initialization succeeded.
    */
    bool begin (uint8_t thePinNumber);

    /**Method to count the number of clicks.
       @param target the number of clicks to count.
       @param maxDelayBetweenClicksInMsec the maximum delay in milliseconds between clicks.
       @return true if the target has been reached.
       @return false if too much time passed between clicks.
    */
    bool count (uint16_t target,  uint16_t maxDelayBetweenClicksInMsec);

  private :
    int lastSwitchState;
    int counter;
    uint8_t pinNumber; /**< Define the pin number.*/
};
