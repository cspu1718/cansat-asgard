/*
   HBridgeMotor.cpp
*/

#include "CansatConfig.h"
#define DBG_BACKWARDS 0
#define DBG_FORWARD 0
#define DBG_BRAKE 0
#include "DebugCSPU.h"
#include "HBridgeMotor.h"

void HBridgeMotor::TurnForward(const uint32_t duration, const uint8_t power) {
  DPRINTSLN(DBG_FORWARD, "Activation of the H-bridge");
  SetForward(power);
  DPRINTS(DBG_FORWARD, "Motor turns forward during ");
  DPRINT(DBG_FORWARD, duration );
  DPRINTSLN(DBG_FORWARD, " milliseconds.");
  delay(duration);
  Brake();
  DPRINTSLN(DBG_FORWARD, "Deactivation of the H-bridge");
}

void HBridgeMotor::TurnBackwards(const uint32_t duration, const uint8_t power) {
  DPRINTSLN(DBG_BACKWARDS, "Activation of the H-bridge");
  SetBackwards(power);
  DPRINTS(DBG_BACKWARDS, "Motor turns backwards during ");
  DPRINT(DBG_BACKWARDS, duration );
  DPRINTSLN(DBG_BACKWARDS, " milliseconds.");
  delay(duration);
  Brake();
  DPRINTSLN(DBG_BACKWARDS, "Deactivation of the H-bridge");
}
