/*
   DC_MotorWithFeedback.h
*/

#pragma once
#include "HBridgeMotor.h"
#include "ClickCounter.h"


class DC_MotorWithFeedback {
  public:

    /** Method to call before any other method. Initializes whatever must be.
       @param theMotor the motor to initialize.
       @param clickPinNumber the pin number for the switch.
       @param maxDelayBetweenClicksInMsec the maximum delay in milliseconds between clicks.
       @return true if initialization succeeded
    */
    bool begin(HBridgeMotor& theMotor, uint8_t clickPinNumber,  uint16_t maxDelayBetweenClicksInMsec = 500);
    /** Method to turn forward the motor during a number of clicks.
      @param numberOfClicks the number of clicks to count.
      @param power defines the power to control the velocity of the motor
             (between 0 and 255).
      @return True if the motor actually turned as required, false if the clicks
              were not detected and the motor was stopped on timeout.
    */
    bool TurnForward (const uint32_t numberOfClicks, const uint8_t power);
    /** Method to turn forward the motor during a number of clicks.
      @param numberOfClicks the number of clicks to count.
      @param power defines the power to control the velocity of the motor
              (between 0 and 255).
      @return True if the motor actually turned as required, false if the clicks
              were not detected and the motor was stopped on timeout.
    */
    bool TurnBackwards (const uint32_t numberOfClicks, const uint8_t power);
    /** Method to get the position of the motor.
    */
    int16_t getPosition() {
      return position;
    };
    /** Method to set the position of the motor to zero.
    */
    void ZeroPosition () {
      position = 0;
    };

  private:
    HBridgeMotor* motor;
    ClickCounter cc;
    uint16_t maxDelayBetweenClicks;
    int16_t position;
};
