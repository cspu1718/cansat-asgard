/*
   L293_HBridgeMotor.h
*/

#pragma once
#include "HBRidgeMotor.h"
#include "Arduino.h"

/** @ingroup CSPU_Motors
    @brief Interface class to control a DC motor power using a H-bridge.
    The class has been tested during project G-Mini with:
    - Motor TO BE COMPLETED
    - H-Bridge L923D.
*/
class L293_HBridgeMotor: public HBridgeMotor {

  public :
    /** Method to call before any other method. Initializes whatever must be.
        @param pwmPin the PWM Pin number actives the H-Bridge.
        @param forwardPin the Forward Pin is used to turn forward.
        @param reversePin the Reverse Pin is used to turn backwards.
        @return true if initialization succeeded
    */
    bool begin(uint8_t pwmPin, uint8_t forwardPin, uint8_t reversePin);
    
    // Overriden methods from base class
    virtual void Brake() override;
    virtual void SetForward (const uint8_t power)override;
    virtual void SetBackwards (const uint8_t power)override;

  private :
  
    uint8_t L293_HBridgeMotorPWM_Pin; /**< Number of the PWM (Enable1 pin of H-Bridge) */
    uint8_t L293_HBridgeMotorForward_Pin; /**< Number of the pin connected to pin #2 (input1) of the H-bridge */
    uint8_t L293_HBridgeMotorReverse_Pin; /**< Number of the pin connected to pin #7 (input2) of the H-bridge */

};
