/*
   L293_HBridgeMotor.cpp
*/

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_BACKWARDS 0
#define DBG_FORWARD 0
#define DBG_BEGIN 0
#define DBG_BRAKE 0
#include "L293_HBridgeMotor.h"

bool L293_HBridgeMotor::begin(uint8_t pwmPin, uint8_t forwardPin, uint8_t reversePin) {
  L293_HBridgeMotorPWM_Pin = pwmPin;
  L293_HBridgeMotorForward_Pin = forwardPin;
  L293_HBridgeMotorReverse_Pin = reversePin;
  DPRINTS(DBG_BEGIN, "L293_HBridgeMotorPWM_Pin=");
  DPRINTLN(DBG_BEGIN, L293_HBridgeMotorPWM_Pin );
  DPRINTS(DBG_BEGIN, "L293_HBridgeMotorForward_Pin=");
  DPRINTLN(DBG_BEGIN, L293_HBridgeMotorForward_Pin );
  DPRINTS(DBG_BEGIN, "L293_HBridgeMotorReverse_Pin=" );
  DPRINTLN(DBG_BEGIN, L293_HBridgeMotorReverse_Pin );
  pinMode(L293_HBridgeMotorForward_Pin, OUTPUT);
  pinMode(L293_HBridgeMotorReverse_Pin, OUTPUT);
  pinMode(L293_HBridgeMotorPWM_Pin, OUTPUT);
  return true;
}


void L293_HBridgeMotor::SetForward(const uint8_t power) {
  DPRINTSLN(DBG_FORWARD, "Forward activation of the L293_H-bridge");
  digitalWrite(L293_HBridgeMotorReverse_Pin, LOW);
  digitalWrite(L293_HBridgeMotorForward_Pin, HIGH);
  analogWrite(L293_HBridgeMotorPWM_Pin, power);
}
void L293_HBridgeMotor::Brake() {
  DPRINTSLN(DBG_BRAKE, "L293 braking");
  analogWrite(L293_HBridgeMotorPWM_Pin, 0);
  digitalWrite(L293_HBridgeMotorForward_Pin, LOW);
  digitalWrite(L293_HBridgeMotorReverse_Pin, LOW);
}

void L293_HBridgeMotor::SetBackwards(uint8_t power) {
  DPRINTSLN(DBG_BACKWARDS, "Backwards activation of the L293_H-bridge");
  digitalWrite(L293_HBridgeMotorForward_Pin, LOW);
  digitalWrite(L293_HBridgeMotorReverse_Pin, HIGH);
  analogWrite(L293_HBridgeMotorPWM_Pin, power);
}
