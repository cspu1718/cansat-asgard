/*
   DRV8833_HBridgeMotor.h
*/

#pragma once
#include "HBRidgeMotor.h"
#include "Arduino.h"

/** @ingroup CSPU_Motors
    @brief Interface class to control a low power DC motor power using a H-bridge.
    The class has been tested during project Icarus with:
    - Motor TO BE COMPLETED
    - H-Bridge DRV8833.

    WARNING: this class currently does not use the power parameters and always run motors at full speed;
*/
class DRV8833_HBridgeMotor: public HBridgeMotor {

  public :
    /** Method to call before any other method. Initializes whatever must be.
        @param forwardPin the Forward Pin is used to turn forward.
        @param reversePin the Reverse Pin is used to turn backwards.
        @return true if initialization succeeded
    */
    bool begin(uint8_t forwardPin, uint8_t reversePin);

    /** Run the motor in forward direction.
        @param power defines the power to control the velocity of the motor
              (between 0 and 255)..
    */
    virtual void SetForward(const uint8_t power) override;

    /**Stop the motor.
    */
    virtual void Brake() override;

    /** Run the motor in reverse direction.
        @param power defines the power to control the velocity of the motor
              (between 0 and 255).
    */
    virtual void SetBackwards (const uint8_t power) override;

  private :
    uint8_t DRV8833_HBridgeMotorForward_Pin; /**< Number of the pin connected to pin #2 (input1) of the H-bridge */
    uint8_t DRV8833_HBridgeMotorReverse_Pin; /**< Number of the pin connected to pin #7 (input2) of the H-bridge */

};
