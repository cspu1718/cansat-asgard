#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "DC_MotorWithFeedback.h"

#define USE_DRV8833 // define to use DRV8833, instead of L293 circuit
#ifdef USE_DRV8833
#  include "DRV8833_HBridgeMotor.h"
constexpr uint8_t Forward_Pin[2] = {5, A1}; // DRV can use 2 motors so there are 2 differents pin.
constexpr uint8_t Reverse_Pin[2] = {6, A2};
constexpr uint8_t Power = 255;
constexpr uint16_t MaxDelayBetweenClicks = 2000; // msec
constexpr uint16_t MaxClicksForTest = 10;

#define BACKWARDS     // Define to perform backwards movement tests. 
#define FORWARDS      // Define to perform forward movement tests. 
#  define HBRIDGE_CLASS DRV8833_HBridgeMotor
#else
#  include "L293_HBridgeMotor.h"
constexpr uint8_t PWM_Pin = 5;
constexpr uint8_t Forward_Pin = 9;
constexpr uint8_t Reverse_Pin = 11;
constexpr uint8_t Power = 255;
#  define HBRIDGE_CLASS L293_HBridgeMotor
#endif

constexpr uint8_t clickPinNumber[2] = {12, 9}; //use {9, 12} when working on the Icarus reference circuit
constexpr uint8_t motorID = 1; // 0 or 1 
HBRIDGE_CLASS actualMotor ;
DC_MotorWithFeedback myMotor ;

void setup() {
  bool result;
  DINIT(115200);
#ifdef USE_DRV8833
  result = actualMotor.begin(Forward_Pin[motorID], Reverse_Pin[motorID]);
#else
  result = actualMotor.begin(PWM_Pin, Forward_Pin[motorID], Reverse_Pin[motorID]);
#endif
  if (!result) {
    Serial << "Error during actual motor init (aborted)" << ENDL;
    exit(1);
  }
  result = myMotor.begin(actualMotor, clickPinNumber[motorID], MaxDelayBetweenClicks);
  if (!result) {
    Serial << "Error during my motor init (aborted)" << ENDL;
    exit(1);
  }

  Serial << "Init OK " << ENDL;
}

void loop() {
  static uint16_t numClicks = 1;
#ifdef FORWARDS
  Serial << "Turning forward " << numClicks << " clicks" << ENDL;
  Serial << "Click " << numClicks << " times...." << ENDL;
  if (!myMotor.TurnForward(numClicks, Power)) {
    Serial << "Timeout detected" << ENDL;
  }
  Serial << "Motor stop" << ENDL;
  delay(4000);
#endif

#ifdef BACKWARDS
  Serial << "Turning backwards " << numClicks << " clicks" << ENDL;
  Serial << "Click " << numClicks << " times...." << ENDL;
  if (!myMotor.TurnBackwards(numClicks, Power)){
   Serial << "Timeout detected" << ENDL;
  }
  Serial << "Motor stop" << ENDL;
  delay(4000);
#endif

  numClicks++;
  numClicks = numClicks % MaxClicksForTest; 
}
