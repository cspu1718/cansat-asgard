/*
  Utility program which just moves the linear steppers to demonstrate
  the air brakes. 
  
*/


#include "LinearStepper.h"
#include "CSPU_Debug.h"
#include "EtheraConfig.h"
#include "CSPU_Test.h"

constexpr uint32_t DelayInMsec = 2000;
constexpr uint8_t MaxStepperPosition = 55;

LinearStepper myStepper;


void setup() {
  DINIT(115200);
  Serial << "Demo Linear Stepper: it will be actuators from 0 to " << MaxStepperPosition << " and back, continuously): " << ENDL;
  Serial << "  Pins: dir=" << LinearStepperDirectionPin << ", step=" << LinearStepperStepPin << ", sleep=" << LinearStepperSleepPin << ENDL;
  Serial << "  Total number of steps:" << LinearStepperNumSteps << ENDL;
  Serial << "The demo can run at maximum speed or be slowed down by a factor 1 (max speed) to 9 (min speed)." << ENDL;
  uint8_t slowDownFactor = CSPU_Test::inputSingleDigitInteger("How many times do you to slow down?", 1, 9);
                   

  myStepper.begin( LinearStepperDirectionPin, LinearStepperStepPin, LinearStepperSleepPin, LinearStepperNumSteps);
  myStepper.setSlowDownFactor(slowDownFactor);
  Serial << "Init ok" << ENDL;
  myStepper.setPosition(0);

  
  while (!CSPU_Test::askYN("Are the stepper motor at position 0 ?")) {
    Serial << "  Force 50 steps beyond 0.." << ENDL;
    myStepper.forceToZero(50);
  }
  Serial << "Launching demo..." << ENDL;
}

void loop() {
  delay(DelayInMsec);
  if (myStepper.getPosition() == 0) {
    myStepper.setPosition(MaxStepperPosition);
  }
  else {
    myStepper.resetPosition();
  }
}
