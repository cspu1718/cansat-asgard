/**
 * Test program for DRV8833
 * 
 * Connections: (board to DRV8833)
 * digital pin DRV_A1_PIN  to A1in 
 * digital pin DRV_A2_PIN  to A2in 
 * Connect the motor to A1out pin and A2out pin.
 * Connect all the ground to the card's ground.
 * Connect the 3V pin to the 
 */

// Define the control inputs 
#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DRV_A1_PIN 6  // Board pin to connect to DRV8833's A1in 
#define DRV_A2_PIN 5
#define TIME_STEP 5000

void setup() {   
  DINIT(115880);
  // Initialize the control pins as outputs   
  pinMode(DRV_A1_PIN, OUTPUT);   
  pinMode(DRV_A2_PIN, OUTPUT);
  digitalWrite(DRV_A1_PIN, LOW);
  digitalWrite(DRV_A2_PIN, LOW);
  delay(300);
  Serial <<" Init OK" << ENDL;  
  }
  
  void loop() {   
    Serial << "Rotating clockwise, full speed during " << TIME_STEP << " msec "<< ENDL;
    digitalWrite(DRV_A1_PIN, HIGH);  
    digitalWrite(DRV_A2_PIN, LOW);  
    delay(TIME_STEP);   
    digitalWrite(DRV_A1_PIN, LOW);  // stop
    delay(TIME_STEP);
    
   Serial << "Rotating counterclockwise, full speed during " << TIME_STEP << " msec "<< ENDL;
    digitalWrite(DRV_A1_PIN, LOW);  
    digitalWrite(DRV_A2_PIN, HIGH);  
    delay(TIME_STEP);   
    digitalWrite(DRV_A2_PIN, LOW);  // stop
    delay(TIME_STEP);
    }
