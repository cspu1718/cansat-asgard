/*
 Test ClickCounterClass
 */
#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "ClickCounter.h"

ClickCounter cc;

void setup() {
  // Initialize the click counter.
  Serial.begin(115200);
  while (!Serial);
  Serial.println("ok");
  cc.begin(12);
}

void loop() {
  // Test if when you click it is countd.
  Serial << "Click three times...." << ENDL;
  cc.count(3, 5000);
  Serial << "Counted 3 clicks" << ENDL;
  delay(2000);

  Serial << "Click five times...." << ENDL;
  cc.count(5, 5000);
  Serial << "Counted 5 clicks" << ENDL;
  delay(2000);

  Serial << "Click ten times...." << ENDL;
  cc.count(10, 5000);
  Serial << "Counted 10 clicks" << ENDL;
  delay(2000);

  Serial << "Click twenty times...." << ENDL;
  cc.count(20, 5000);
  Serial << "Counted 20 clicks" << ENDL;
  delay(2000);
}
