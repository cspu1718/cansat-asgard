/*
    Test program for class HBridge_Motor.
*/

#define DEBUG_CSPU
#define USE_ASSERTIONS
#include "DebugCSPU.h"

#define USE_DRV8833 // define to use DRV8833, instead of L293 circuit
#ifdef USE_DRV8833
#  include "DRV8833_HBridgeMotor.h"
constexpr uint8_t Forward_Pin = 9;
constexpr uint8_t Reverse_Pin = 5;
constexpr uint8_t Power = 255;

#  define HBRIDGE_CLASS DRV8833_HBridgeMotor
#else
#  include "L293_HBridgeMotor.h"
constexpr uint8_t PWM_Pin = 5;
constexpr uint8_t Forward_Pin = 9;
constexpr uint8_t Reverse_Pin = 11;
constexpr uint8_t Power = 255;
#  define HBRIDGE_CLASS L293_HBridgeMotor
#endif

HBRIDGE_CLASS myMotor ;

//This test is used to test any motors between DRV and L293.
void setup() {
  bool result;
  DINIT(115200);
#ifdef USE_DRV8833
  result = myMotor.begin(Forward_Pin, Reverse_Pin);
#else
  result = myMotor.begin(PWM_Pin, Forward_Pin, Reverse_Pin);
#endif
  if (!result) {
    Serial << "Error during motor init (aborted)" << ENDL;
    exit(1);
  }

}

void loop() {
  Serial << "Turning forward" << ENDL;
  myMotor.TurnForward(500, Power);
  delay(1000);
  Serial << "Turning backwards" << ENDL;
  myMotor.TurnBackwards(500, Power);
  delay(1000);
}
