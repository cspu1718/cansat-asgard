/*
  Test for class LinearStepper.
  - LinearStepper description: When holding the motor from below, green is the leftmost cable and red is the rightmost cable. They are connected in the following order: green, black, blue and red.

    Wiring LinearStepper(SM1018) to DRV8834:
    B2    Black
    B1    Red
    A1    Green
    A2    Blue

    Wiring DRV8834 to Feather MO Express:
    SLP    D09
    STEP   D06
    DIR    D12
    VMOT   3V
    GND    GND

    A COMPLETER
    Description de l'objet du programme, de la manière de l'utiliser
    et des CONNEXIONS

    ENCORE A FAIRE:
      1. Remplacer les délays par une question à l'utilisateur pour qu'il vérifie que le mvt est correct

         Utiliser CSPU_Test::CSPU_Test::requestCheck("Est-ce correct?", numErrors); à la place de chaque delay()
      2. Tester avec les nouveaux actuateurs, en utilisant les connecteurs soudés, et en en mettant 2
         2 en parallèle.
      3. Après que tout fonctionne bien:
         Voir si on peut réduire les delais à moins d'une 1ms.
            - changer le commentaire dans le .h: stocker les délays en microsecondes, pas en millisecondes
            - changer le code partout où on utilise DelayHigh et DelayLow: utiliser la fonction delayMicroseconds()
              plutôt que la fonction delay().
            - Voir si on peut descendre à 500 µs ? 200 µs ? 100 µs ?
*/


#include "LinearStepper.h"
#include "CSPU_Debug.h"
#include "EtheraConfig.h"
#include "CSPU_Test.h"

uint16_t numErrors = 0;

LinearStepper myStepper;

void setup() {
  DINIT(115200);
  Serial << "Testing Linear Stepper: " << ENDL;
  Serial << "  Pins: dir=" << LinearStepperDirectionPin << ", step=" << LinearStepperStepPin << ", sleep=" << LinearStepperSleepPin << ENDL;
  Serial << "  Total number of steps:" << LinearStepperNumSteps << ENDL;
  myStepper.begin( LinearStepperDirectionPin, LinearStepperStepPin, LinearStepperSleepPin, LinearStepperNumSteps);
  Serial.println("Moving to 100");
  myStepper.setPosition(100);
  CSPU_Test::CSPU_Test::requestCheck("Est-ce correct?", numErrors);
  Serial.println("Moving to 50");
  myStepper.setPosition(50);
  CSPU_Test::requestCheck("Est-ce correct?", numErrors);
  Serial.println("Moving to 75");
  myStepper.setPosition(75);
  CSPU_Test::requestCheck("Est-ce correct?", numErrors);
  Serial.println("Moving to 30");
  myStepper.setPosition(30);
  CSPU_Test::requestCheck("Est-ce correct?", numErrors);
  Serial.println("reset position");
  myStepper.resetPosition();
  CSPU_Test::requestCheck("Est-ce correct?", numErrors);
  Serial.println("Forcing to 0 with 10 extra steps");
  myStepper.forceToZero(10);

  CSPU_Test::requestCheck("Est-ce correct?", numErrors);
  Serial << "Setting number of steps to 500, and moving to position 100. Move should be restricted" << ENDL;
  myStepper.setTotalNumberOfSteps(500);
  myStepper.setPosition(100);
  CSPU_Test::requestCheck("Est-ce correct?", numErrors);
  
  Serial << "resetting..." << ENDL;
  myStepper.resetPosition();

  Serial.println("End of job");


}

void loop() {

}
