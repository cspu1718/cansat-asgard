/*
 * TestUtilityFunctions.h
 *
 */

#pragma once
#include "CansatFlightAnalyser.h" 
#include "CansatInterface.h"

extern uint32_t numErrors;
extern uint32_t recordCounter;
extern CansatFlightAnalyser<> controller;
extern bool stopAfterFailedTest;
extern bool stopAfterFirstFailedCheck;

/** process the record, updating altitude, increasing timestamp by CansatAcquisitionPeriod
 *  and superimposing +-0.75m of noise on the altitude.
 */
/*void RunA_RecordForRefAltitudeTest(CansatRecord& record,float altitude); */

/** Print secondary mission -related information from record */
void printFlightInfo(CansatRecord&rec);

/** Check controller info in record against expected values. Phase is not checked if 255 */
void checkCtrlInfo(CansatRecord& record,FlightControlCode ctrlCode, const char* ctrlCodeMsg, uint8_t phase=255) ;

/** Check whether flight phase is either phase1 or phase2 (used to support phase detectiong delay) */
void checkPhase(CansatRecord& record, uint8_t phase1, uint8_t phase2) ;

/** Check target rope len is the target value.
 *  This function keeps track of the previous target, and does not fail if the target is still
 *  the previous one during TorusMinDelayBetweenTargetUpdate msec.  */
// TMP REMOVE void checkTarget(CansatRecord& record, uint16_t target) ;

/** Alternately add an substract 1.1*NoAltitudeChangeTolerance to altitude, in order to avoid
 *  the can is considered static.
 */
void wobbleAltitude(CansatRecord& record);

/** Just wait until the board is running since at least TorusMinDelayBeforeFirstUpdate msec. */
void waitEndOfStartup(CansatFlightAnalyser<>& ctrl, CansatRecord& record) ;

/** Process enough times the record (increaing timestamp) for the reference altitude to be set to the current altitude */
// TMP REMOVE void setReferenceAltitude(CansatRecord& record);

/** Print short summary of errors so far. Stop program if StopAfterFailedTest is true */
void printTestSummary();
