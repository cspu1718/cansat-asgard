/*
   TestUtilityFunctions.cpp

*/

#include "./TestUtilityFunctions.h"

#include "CansatConfig.h"

void RunA_RecordForRefAltitudeTest(CansatRecord& record, float altitude) {
  record.altitude = altitude + (random(0, 150) - 75.0f) / 100.0f;
  record.timestamp += CansatAcquisitionPeriod;
  controller.run(record);
  recordCounter++;
  if ((recordCounter % 100) == 0) {
    Serial << ".";
  }
}

const char * ctrlCodeString(FlightControlCode code) {
  switch ((int) code) {
    case (int) FlightControlCode::NoData:
      return "NoData";
    case  (int) FlightControlCode::StartingUp:
      return "StartingUp";
    case  (int) FlightControlCode::GroundConditions:
      return "GroundCond";
    case  (int) FlightControlCode::NotDescending:
      return "NotDesc";
    case  (int) FlightControlCode::NominalDescent:
      return "NominalDesc";
    case  (int) FlightControlCode::LowDescentVelocityAlert:
      return "LowVelocityAlert";
    case  (int) FlightControlCode::HighDescentVelocityAlert:
      return "HighVelocityAlert";
    default:
      return "Unknown";
  }
}
void printFlightInfo(CansatRecord& rec) {
  float sec = (rec.timestamp % 60000) / 1000.0f;
  uint16_t min = rec.timestamp / 60000;
  uint8_t  hour = min / 60;
  min = min % 60;

  Serial << hour << ":" << min << ":" << sec << ", ts=" << rec.timestamp << ", rel.alt.=" << rec.altitude - rec.refAltitude
         << ", descVeloc.=" << rec.descentVelocity << ", ";
  Serial << ",ctrlInfo=" << ctrlCodeString(rec.flightControlCode) << ", ph=" << rec.flightPhase << ENDL;
}

/** Check controller info in record against expected values. Phase is not checked if 255 */
void checkCtrlInfo(CansatRecord& record, FlightControlCode ctrlCode, const char* ctrlCodeMsg, uint8_t phase) {
  if (record.flightControlCode != ctrlCode) {
    printFlightInfo(record);
    Serial << "*** Error: controller code is NOT " << ctrlCodeMsg << ENDL;
    numErrors++;
    if (stopAfterFirstFailedCheck) {
      Serial << "Stopped because of 'stopAfterFirstFailedCheck' setting" << ENDL;
      delay(500);
      exit(-1);
    }
  }
  if ((phase != 255) && (record.flightPhase != phase)) {
    printFlightInfo(record);
    Serial << "*** Error:  flightPhase is NOT " << phase << ENDL;
    numErrors++;
    if (stopAfterFirstFailedCheck) {
      Serial << "Stopped because of 'stopAfterFirstFailedCheck' setting" << ENDL;
      delay(500);
      exit(-1);
    }
  }
}

/** Check whether flight phase is either phase1 or phase2 (used to support phase detectiong delay) */
void checkPhase(CansatRecord& record, uint8_t phase1, uint8_t phase2) {
  if ((record.flightPhase != phase1) && (record.flightPhase != phase2)) {
    printFlightInfo(record);
    Serial << "*** Error:  flightPhase is NOT " << phase1  << " nor " << phase2 << ENDL;
    numErrors++;
    if (stopAfterFirstFailedCheck) {
      Serial << "Stopped because of 'stopAfterFirstFailedCheck' setting" << ENDL;
      delay(500);
      exit(-1);
    }
  }
}

/* TMP: REMOVE!
  void checkTarget(CansatRecord& record, uint16_t target) {
	static uint16_t previousTarget=60000;
	static uint32_t lastChangeTS=0;
	if (record.parachuteTargetRopeLen!=target){
		if ((record.parachuteTargetRopeLen!=previousTarget) ||
				((lastChangeTS -record.timestamp) <= CansatMinDelayBetweenPhaseChange)) {
			// In this case, the delay in target update is abnormal

			Serial << "*** Error: target=" << record.parachuteTargetRopeLen << ", expected " << target
					<< ", TS=" << record.timestamp << ", lastChange=" <<lastChangeTS << ENDL;
			numErrors++;
			if (stopAfterFirstFailedCheck) {
				Serial << "Stopped because of 'stopAfterFirstFailedCheck' setting" << ENDL;
				delay(500);
				exit(-1);
			}
		}
	} // if != target
	if (record.parachuteTargetRopeLen != previousTarget) {
		previousTarget=record.parachuteTargetRopeLen;
		lastChangeTS=record.timestamp;
	}
  }
*/

void wobbleAltitude(CansatRecord& record) {
  constexpr float altitudeWobbling = 1.1 * NoAltitudeChangeTolerance;
  static bool add = false;
  if (add) record.altitude += altitudeWobbling;
  else record.altitude -= altitudeWobbling;
  add = !add;
}

void waitEndOfStartup(CansatFlightAnalyser<>& ctrl, CansatRecord& record) {
  int numRecords = CansatMinDelayBeforeFlightControl / CansatAcquisitionPeriod + 1;
  Serial << "Waiting until start up is over (" << CansatMinDelayBeforeFlightControl / 1000.0
         << " sec. since startup  = " << numRecords << " records)...";
  while (numRecords > 0) {
    record.timestamp += CansatAcquisitionPeriod;
    ctrl.run(record);
    numRecords--;
  }
  Serial << " OK" << ENDL;
}

#ifdef TMP_REMOVE
void setReferenceAltitude(CansatRecord& record) {
  Serial << "Setting ref. altitude to " << record.altitude << "m..." ;
  record.refAltitude = record.altitude;
  /* TMP REMOVE while (fabs(record.refAltitude-record.altitude) > 0.1) {
  	controller.run(record);
  	record.timestamp+=CansatAcquisitionPeriod;
  	recordCounter++;
  	if ((recordCounter % 100)==0) Serial << "." ;
    }
    Serial << " OK" << ENDL;
  */
}
#endif

void printTestSummary() {
  Serial << ENDL << "===== Test over (processed " << recordCounter << " records). Errors so far: "
         << numErrors  << " =====" << ENDL;
  if (numErrors && stopAfterFailedTest) {
    Serial << "Stopping because of 'stopAfterFailedTest' setting in test program" << ENDL;
    delay(500);
    exit(-1);
  }
}
