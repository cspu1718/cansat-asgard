Test of CansatAcquisitionProcess only is impossible without simulating
many complex classes, which is time-consuming for little benefit.

Actual testing is performed by running the can software limited to
primary mission, i.e. by
running  the CansatMain sketch from folder 0000-00_Templates.