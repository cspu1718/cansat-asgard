/*
 * CansatRF_TransceiverWithScreen.cpp
 */

#include "CansatRF_TransceiverWithScreen.h"

#define DBG_RTSCV_INIT 1

template<class RECORD>
bool CansatRF_TransceiverWithScreen<RECORD>::begin() {
	DPRINTSLN(DBG_RTSCV_INIT, "Initialisation RF_TransceiverWithScreen...");
	if (screen.begin(this->getProjectName())) {
		screen.displayMessage("Initializing...");
		DPRINTSLN(DBG_RTSCV_INIT,"Init of Screen OK");
	} else {
		DPRINTSLN(DBG_DIAGNOSTIC,"Init of Screen FAILED");
	}

	if (CansatRF_Transceiver<RECORD>::begin()) {
		DPRINTSLN(DBG_RTSCV_INIT,"Init of CansatRF_Transceiver OK");
	} else {
		DPRINTSLN(DBG_DIAGNOSTIC,"Init of CansatRF_Transceiver failed.");
	while (true) ;
	}
	screen.displayMessage(" Running...");
	screen.displayTemporaryMessage("Init OK.",3);
	return true;
}; // begin()
    
template<class RECORD>
void CansatRF_TransceiverWithScreen<RECORD>::run() {
	CansatRF_Transceiver<RECORD>::run();
	// Once a second, update display GPS position
	if (GPS_PositionChanged && (elapsedForGPS > 1000)) {
		screen.setGPS_Position(lastGPS_Latitude, lastGPS_Longitude);
		GPS_PositionChanged = false;
		elapsedForGPS = 0;
	}
	screen.run(); // Update screen as often as possible.
	int clicks=0;
	clicks = screen.readButton(screen.ButtonId::A);
	if (clicks==1) handleSingleClickButtonA();
	if (clicks==2) handleDoubleClickButtonA();

	clicks = screen.readButton(screen.ButtonId::B);
	if (clicks==1) handleSingleClickButtonB();
	if (clicks==2) handleDoubleClickButtonB();

	clicks = screen.readButton(screen.ButtonId::C);
	if (clicks==1) handleSingleClickButtonC();
	if (clicks==2) handleDoubleClickButtonC();
} // Run

template<class RECORD>
void CansatRF_TransceiverWithScreen<RECORD>::handleSingleClickButtonA()
{
	this->setUplinkActive(true);
	displayTemporaryMessage("-> InitiateCmdMode",1);
	CansatXBeeClient* xb = this->getXBeeClient();
	RF_OPEN_CMD_REQUEST(xb);
	*xb << (int)CansatCmdRequestType::InitiateCmdMode;
	RF_CLOSE_CMD_REQUEST(xb);
	this->setUplinkActive(false);
};

template<class RECORD>
void CansatRF_TransceiverWithScreen<RECORD>::handleSingleClickButtonB() {
	displayTemporaryMessage("Single click B",1);
};
	 
template<class RECORD>
void CansatRF_TransceiverWithScreen<RECORD>::handleSingleClickButtonC() {
	this->setUplinkActive(true);
	displayTemporaryMessage("-> StartBuzzer",1);
	CansatXBeeClient* xb = this->getXBeeClient();
	RF_OPEN_CMD_REQUEST(xb);
	*xb << (int)CansatCmdRequestType::StartBuzzer << ", 5";
	RF_CLOSE_CMD_REQUEST(xb);
	this->setUplinkActive(false);
}
template<class RECORD>
void CansatRF_TransceiverWithScreen<RECORD>::handleDoubleClickButtonA() {
	this->setUplinkActive(true);
	displayTemporaryMessage("-> TerminateCmdMode",1);
	CansatXBeeClient* xb = this->getXBeeClient();
	RF_OPEN_CMD_REQUEST(xb);
	*xb << (int)CansatCmdRequestType::TerminateCmdMode;
	RF_CLOSE_CMD_REQUEST(xb);
	this->setUplinkActive(false);
};
	
template<class RECORD>
void CansatRF_TransceiverWithScreen<RECORD>::handleDoubleClickButtonB() {
	displayTemporaryMessage("Double click B",1);
};
	 
template<class RECORD>
void CansatRF_TransceiverWithScreen<RECORD>::handleDoubleClickButtonC() {
	displayTemporaryMessage("Double click C",1);
};
    
template<class RECORD>
bool CansatRF_TransceiverWithScreen<RECORD>::receiveFrame(bool& receivedA_String, bool& receivedA_Record){
	bool result = CansatRF_Transceiver<RECORD>::receiveFrame(receivedA_String, receivedA_Record);
	if (result) {
		if (receivedA_String) {
			screen.displayMessage(this->getStringReceived());
		}
		if (receivedA_Record) {
			const RECORD& rec = this->getRecordReceived();
			if (rec.newGPS_Measures) {
				lastGPS_Latitude = rec.GPS_LatitudeDegrees;
				lastGPS_Longitude = rec.GPS_LongitudeDegrees;
				GPS_PositionChanged = true;
			}
		}
	}
	return result;
}

