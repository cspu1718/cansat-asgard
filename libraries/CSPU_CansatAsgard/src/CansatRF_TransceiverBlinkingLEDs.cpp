/*
 * CansatRF_TransceiverBlinkingLEDs.cpp
 */

#include "CansatRF_TransceiverBlinkingLEDs.h"

constexpr byte CansatRF_TransceiverBlinkingLEDs::activityLED_Pin[
							CansatRF_TransceiverBlinkingLEDs::NumActivityLEDS] ;

bool CansatRF_TransceiverBlinkingLEDs::begin() {
	//Define LED's on OUTPUT
	pinMode(DownlinkLED_Pin, OUTPUT);
	pinMode(UplinkLED_Pin, OUTPUT);
	pinMode(HeartBeatLED, OUTPUT);

	//Check if all LEDs work
	digitalWrite(DownlinkLED_Pin, HIGH);
	digitalWrite(UplinkLED_Pin, HIGH);
	digitalWrite(HeartBeatLED, HIGH);
	delay(1000); // Let the user the time to see if it works properly
	digitalWrite(DownlinkLED_Pin, LOW);
	digitalWrite(UplinkLED_Pin, LOW);
	digitalWrite(HeartBeatLED, LOW);

	for (int i = 0; i < NumActivityLEDS; i++) {
		ledStateDuration[i] = LED_StateDuration;  // Init to max duration to avoid delaying the first use.
		linkActivityDuration[i] = MinActivityDuration;
		linkActive[i] = false;
	}
	return true;
};

void CansatRF_TransceiverBlinkingLEDs::setLinkActive(byte idx, bool active) {
	if (active == linkActive[idx]) {
		if (active) linkActivityDuration[idx] = 0;
	} else {
		if (linkActivityDuration[idx] > MinActivityDuration) {
			linkActive[idx] = active;
			linkActivityDuration[idx] = 0;
		}
	}
} //setLinkActive

void CansatRF_TransceiverBlinkingLEDs::update() {
	for (int i = 0 ; i < NumActivityLEDS; i ++ ) {
		if (ledStateDuration[i] > LED_StateDuration) {
			ledStateDuration[i] = 0;
			if (linkActive[i]) {
				digitalWrite(activityLED_Pin[i], !digitalRead(activityLED_Pin[i]));
			} else {
				digitalWrite(activityLED_Pin[i], LOW);
			}
		}
	} // for

	if (heartBeatDelay > HeartBeatPeriod) {
		digitalWrite(HeartBeatLED, !digitalRead(HeartBeatLED));
		heartBeatDelay = 0;
	}
}
