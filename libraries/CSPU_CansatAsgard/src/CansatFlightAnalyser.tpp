/*
 * CansatFlightAnalyser.tpp
 */

// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatFlightAnalyser.h"
#ifdef INCLUDE_FLIGHT_ANALYSIS_DATA
#include "BMP_Client.h"
#include "elapsedMillis.h"
#include <type_traits>

#define DBG_VELOCITY_BUFFER 0
#define DBG_VELOCITY 0
#define DBG_EJECTION 0
#define DBG_FLIGHT_CTRL_CODE 0
#define DBG_FLIGHT_CTRL_CODE_DISCARDED 0
#define DBG_FLIGHT_PHASE_EVALUATION 0

template<class RECORD>
CansatFlightAnalyser<RECORD>::CansatFlightAnalyser() :
	takeoffTimestamp(0) ,
	averageVelocity(InvalidVelocity),
	currentVelocityIndex(0),
	velocityAverageValid(false),
	lastRecordTimestamp(0),
	takeoffHappened(false),
	ejectionHappened(false),
	currentFlightStatus(FlightControlCode::StartingUp),
	lastFlightStatusUpdate(0),
	shouldUpdateFlightPhase(false),
	currentFlightPhase(CansatUndefinedFlightPhase)
{
	static_assert(std::is_base_of<CansatRecord, RECORD>::value,
			"RECORD must be derived from CansatRecord");
};

template<class RECORD>
void CansatFlightAnalyser<RECORD>::printVelocityTable() {
  for (int i = 0; i < NumberOfVelocitySamples; i++)
    Serial << "  " << i << ": " << velocityTable[i] << ENDL;
}

template<class RECORD>
void CansatFlightAnalyser<RECORD>::analyseDescentVelocity(const RECORD& rec) {
#if (DBG_VELOCITY_BUFFER == 1)
  printVelocityTable();
#endif
#if (DBG_VELOCITY == 1)
  Serial << "BEFORE: ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ", lastTs=" << lastRecordTimestamp << ENDL;
#endif
  if (rec.descentVelocity == BMP_Client::InvalidDescentVelocity) {
#if (DBG_VELOCITY == 1)
    Serial << "INVALID VELOCITY DETECTED velocity= " <<  rec.descentVelocity << ENDL;
#endif
    clearAverageVelocity();
    lastRecordTimestamp = rec.timestamp;
    return;
  }
  if ((rec.timestamp - lastRecordTimestamp) > (3 * CansatAcquisitionPeriod)) {
    clearAverageVelocity();
#if (DBG_VELOCITY == 1)
    Serial << "INVALID REC DETECTED ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ENDL;
#endif
    lastRecordTimestamp = rec.timestamp;
    return;
  }

  if (rec.timestamp < lastRecordTimestamp) {
    clearAverageVelocity();

#if (DBG_VELOCITY == 1)
    Serial << "INVALID PAST RECORD DETECTED ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ENDL;
#endif
  }
  averageVelocity = averageVelocity + (rec.descentVelocity - velocityTable[currentVelocityIndex]) / NumberOfVelocitySamples;
  velocityTable[currentVelocityIndex] = rec.descentVelocity;
  currentVelocityIndex++;
  if (currentVelocityIndex == NumberOfVelocitySamples) {
    currentVelocityIndex = 0;
    DPRINTSLN(DBG_VELOCITY, "Index reset"); //
    velocityAverageValid = true;
  }
  lastRecordTimestamp = rec.timestamp;
#if (DBG_VELOCITY_BUFFER == 1)
  Serial << "AFTER: ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ENDL;
  printVelocityTable();
#endif

  // Update take-off status
  if ((!takeoffHappened) && velocityAverageValid && (averageVelocity  < CansatVelocityThresholdForTakeoff )) {
    DPRINTLN(DBG_EJECTION, takeoffHappened);
    DPRINTLN(DBG_EJECTION, velocityAverageValid);
    DPRINTLN(DBG_EJECTION, averageVelocity);
    DPRINTLN(DBG_EJECTION, CansatVelocityThresholdForTakeoff);
    takeoffHappened = true;
    takeoffTimestamp = rec.timestamp;
#ifdef RF_ACTIVATE_API_MODE
    CansatXBeeClient *xbee = this->getXbeeClient();
    if (xbee != nullptr) {
      RF_OPEN_STRING(xbee);
      *xbee << millis() << ": 2-ary: Take off detected!";
      RF_CLOSE_STRING(xbee);
    }
#endif
    DPRINTS(DBG_EJECTION, "takeoff : timestamp set to ");
    DPRINT(DBG_EJECTION, takeoffTimestamp);
    DPRINTS(DBG_EJECTION, ", threshold (m/s) ");
    DPRINTLN(DBG_EJECTION, CansatVelocityThresholdForTakeoff);
  }

  // Update ejection status
  if ((!ejectionHappened) && takeoffHappened) {
    if ((rec.timestamp - takeoffTimestamp) > CansatMaxDelayFromTakeoffToEjection) {
      ejectionHappened = true;
#ifdef RF_ACTIVATE_API_MODE
      CansatXBeeClient *xbee = this->getXbeeClient();
      if (xbee != nullptr) {
        RF_OPEN_STRING(xbee);
        *xbee << millis() << ": 2-ary: ejection assumed!";
        RF_CLOSE_STRING(xbee);
      }
#endif
    }
  } // update ejection status.
}

template<class RECORD>
void CansatFlightAnalyser<RECORD>::updateFlightStatus(const RECORD &record) {
  // Changed too recently: do not change status.
  if (record.timestamp - lastFlightStatusUpdate < CansatMinDelayBetweenFlightPhaseChange) {
    DPRINTS(DBG_FLIGHT_CTRL_CODE_DISCARDED, "Upd flight status: DISCARDED (too recent), keep status ")
    DPRINTLN(DBG_FLIGHT_CTRL_CODE_DISCARDED, (int)  currentFlightStatus);
    return;
  }

  // Actually evaluate the flight status.
  lastFlightStatusUpdate = record.timestamp;
  shouldUpdateFlightPhase = true;
  float relativeAltitude = record.altitude - record.refAltitude;

  // ground conditions
  if (relativeAltitude < CansatMinAltitudeForFlightControl) {
    currentFlightStatus = FlightControlCode::GroundConditions;
  }

  // not descending
  else if (record.descentVelocity < CansatMinVelocityDescentLimit)  {
    currentFlightStatus = FlightControlCode::NotDescending;
  }

  // if below last phase altitude, we are in ground conditions
  else if (relativeAltitude < CansatFlightPhaseAltitude[CansatNumFlightPhases - 1]) {
    currentFlightStatus = FlightControlCode::GroundConditions;
  }

  // too fast
  else if (record.descentVelocity > CansatVelocityUpperSafetyLimit) {
    currentFlightStatus = FlightControlCode::HighDescentVelocityAlert;
  }

  // too slow
  else if (record.descentVelocity < CansatVelocityLowerSafetyLimit) {
    currentFlightStatus = FlightControlCode::LowDescentVelocityAlert;
  }

  // nominal descent
  else currentFlightStatus = FlightControlCode::NominalDescent;

#ifdef DBG_FLIGHT_CTRL_CODE
  static FlightControlCode previousStatus = FlightControlCode::StartingUp;
  if (currentFlightStatus != previousStatus) {
    Serial << "2-ary: flightStatus changed to " << (int)currentFlightStatus  << ENDL;
    previousStatus = currentFlightStatus;
  }
#endif
} // updateFlightStatus

template<class RECORD>
void CansatFlightAnalyser<RECORD>::updateFlightPhase(const RECORD& record) {
  // (No binary search implementation as there will never be more than 14 phases)

  // If flight phase shouldn't be updated, don't do anything
  if (!shouldUpdateFlightPhase) {
    return;
  }

  // If not in nominal descent, flight phase is undefined.
  if (currentFlightStatus != FlightControlCode::NominalDescent) {
    currentFlightPhase = CansatUndefinedFlightPhase;
  }
  else {
    // Evaluate phase
    float relativeAltitude = record.altitude - record.refAltitude;
    for (int i = 0; i < CansatNumFlightPhases; i++) {
      if (relativeAltitude >= CansatFlightPhaseAltitude[i]) {
        currentFlightPhase = i;
        DPRINTS(DBG_FLIGHT_PHASE_EVALUATION, "Current evaluated flight phase: ");
        DPRINTLN(DBG_FLIGHT_PHASE_EVALUATION, currentFlightPhase);
        break;
      }
    }

    // This check is included as a safety in case no phase was found previously:
    // it should never actually come here because this check is already performed in the
    // method updateFlightStatus().
    if (record.altitude - record.refAltitude < CansatFlightPhaseAltitude[CansatNumFlightPhases - 1]) {
      currentFlightPhase = CansatUndefinedFlightPhase;
      DPRINTSLN(DBG_DIAGNOSTIC, "WARNING: No flight phase could be found by SecondaryMissionController::updateFlightPhase() although in nominal descent. Setting flight phase to 15 and controller info to GroundConditions.");
    }
  }

  // Reset shouldUpdateFlightPhase property
  shouldUpdateFlightPhase = false;
}

template<class RECORD>
void CansatFlightAnalyser<RECORD>::manageSecondaryMission(RECORD & record)
{
  static uint32_t counter  = 0;
  counter++;
  bool bmpMeasuresStable = counter > StartupDelayForFlightAnalysis / CansatAcquisitionPeriod;
  // Before this delay, it is assumed that the BMP measures are not stabilized.

  if (bmpMeasuresStable) {
    analyseDescentVelocity(record);
    updateFlightStatus(record);
    updateFlightPhase(record);

    record.flightControlCode = currentFlightStatus;
    record.flightPhase = currentFlightPhase;

    manageProjectSpecificMission(record);
  }
  else {
	  record.flightControlCode = currentFlightStatus;
	  record.flightPhase = currentFlightPhase;
  }
}

template<class RECORD>
void CansatFlightAnalyser<RECORD>::clearAverageVelocity() {
  for (int i = 0; i < NumberOfVelocitySamples; i++) {
    velocityTable[i] = 0;
  }
  averageVelocity = 0;
  velocityAverageValid = false;
}

#ifdef RF_ACTIVATE_API_MODE
template<class RECORD>
bool CansatFlightAnalyser<RECORD>::begin(CansatXBeeClient* xbeeClient) {
	if (!SecondaryMissionController<RECORD>::begin(xbeeClient)) return false;
	takeoffHappened = false;
	takeoffTimestamp = 0;
	clearAverageVelocity();
	return true;
}
#else
template<class RECORD>
bool CansatFlightAnalyser<RECORD>::begin(Stream* RF_Stream) {
	if (!SecondaryMissionController<>::begin(RF_Stream)) return false;
	takeoffHappened = false;
	takeoffTimestamp = 0;
	clearAverageVelocity();
	return true;
}
#endif

template<class RECORD>
float CansatFlightAnalyser<RECORD>::getAverageVelocity() {
  if (velocityAverageValid == true) {
    return averageVelocity;
  }
  else {
    return InvalidVelocity;
  }
}

template<class RECORD>
bool CansatFlightAnalyser<RECORD>::takeoffDetected() {
  return takeoffHappened;
}

template<class RECORD>
void CansatFlightAnalyser<RECORD>::printInitialisationInfo(Stream& stream) {
	// NB: part (of all) of this should also be transmitted by radio.
	SecondaryMissionController<RECORD>::printInitialisationInfo();
	stream << "Cansat Flight Analysis parameters:" << ENDL;
	stream << "    Delay take-off to ejection: " << CansatMaxDelayFromTakeoffToEjection << " msec" << ENDL;
	stream << "    Velicity average period: " << CansatVelocityAveragingPeriod  << " sec" << ENDL;
	stream << "    Velocity threshold for take-off: " << CansatVelocityThresholdForTakeoff << " m/s" << ENDL;
	stream << "    Min. delay between flight phase change: " << CansatMinDelayBetweenFlightPhaseChange << " msec"  << ENDL;
	                              // would cause oscillations between flight phases.
	stream << "    Min. delay before flight control: " << CansatMinDelayBeforeFlightControl << " msec"  << ENDL;
	stream << "    Min. desc. velocity: " <<  CansatMinVelocityDescentLimit << " m/s" << ENDL;
	stream << "    Desc. velocity min. safety limit: " <<  CansatVelocityLowerSafetyLimit << " m/s" << ENDL;
	stream << "    Desc. velocity max. safety limit: " <<  CansatVelocityUpperSafetyLimit << " m/s" << ENDL;
	stream << "    Min. altitude for flight ctrl: " << CansatMinAltitudeForFlightControl << ENDL;
	stream << "    Flight phases: " << ENDL;
	uint16_t maxAltitude = 1500;
	for (int i = 0; i < CansatNumFlightPhases; i++) {
		stream << "      " << i << ": from " << maxAltitude << " downto " << CansatFlightPhaseAltitude[i]
			   << " m" << ENDL;
		maxAltitude = CansatFlightPhaseAltitude[i];
	}
}

#endif
