// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "GPS_Client.h" //includes the .h

#ifdef ARDUINO_ARCH_SAMD
#  include "SAMD_InterruptTimer.h"
#else
#include "TC_Timer.h"
#endif
#include "CansatConfig.h"
#define DBG 1
#define DBG_NMEA 		0
#define DBG_DIAGNOSTIC 	1
#define DBG_FREQUENCY 	1
#define DBG_TIMER_INFO 	0
#define DBG_ENABLE 		0
constexpr bool GPSECHO = false; // Set to true to echo whatever the GPS sends on Serial

#define PMTK_SET_NMEA_OUTPUT_GGAONLY "$PMTK314,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29" //This sentence doesn't appear in the official adafruit library but still gets accepted by the gps
#define PMTK_SET_NMEA_OUTPUT_GGAVTGONLY "$PMTK314,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29"

static Adafruit_GPS *gpsForInterrupt = NULL;
static bool gpsEnabled = true;

#ifdef __AVR__ // interruption routine supported by __AVR__ architectures (arduino uno)
GPS_Client::GPS_Client(SoftwareSerial& serialPort):
  myGps(&serialPort),
  lastLongitudeValue(0), lastLatitudeValue(0), lastAltitudeValue(0),
#ifdef INCLUDE_GPS_VELOCITY
  lastVelocityKnots(0), lastVelocityAngle(0),
#endif
  enablePin(0)  ,
  currentUpdateFrequency(Frequency::F5Hz) {}

/**A double constructor is required as the SAMD architecture doesn't accept a softwareserial */
SIGNAL(TIMER0_COMPA_vect) {
  char c = gpsForInterrupt->read();
  // if you want to debug, this is a good time to do it!
  if (GPSECHO)
    if (c) UDR0 = c;
  // writing direct to UDR0 is much much faster than Serial.print
  // but only one character can be written at a time.
}
#elif defined(ARDUINO_ARCH_SAMD)
/* Constructor and interruption routine supported by the ATSMAD21G18 (feather m0 express)
   or ATSAMD51 Cortex M4 (ItsyBitsy M4) */
GPS_Client::GPS_Client(HardwareSerial& serialPort):
  myGps(&serialPort),
  lastLongitudeValue(0), lastLatitudeValue(0), lastAltitudeValue(0),
#ifdef INCLUDE_GPS_VELOCITY
  lastVelocityKnots(0), lastVelocityAngle(0),
#endif
  enablePin(0),
  currentUpdateFrequency(Frequency::F5Hz) {}

// This is the version that works for both M0 (SAMD21) and M4 (SAMD51) boards.
static SAMD_InterruptTimer gpsTimer;

void TimerHandler() {
  if (!gpsForInterrupt) return;
  if (!gpsEnabled) return;
  char c = gpsForInterrupt->read();
  // if you want to debug, this is a good time to do it!
  if (GPSECHO) {
    if (c) Serial.print(c);
  }
}
#else
#error "UNSUPPORTED ARCHITECTURE IN GPS_Client.cpp"
#endif

void GPS_Client::configureGPS() {
  myGps.begin(9600); /**Sets an initialization baud rate for the gps. Tests show that it doesn't need to be changed in order to acquire data at 10HZ if using a GGAONLY NMEA sentence
	 WARNING: the baud rate sent to the gps, even if it doesn't accept it, will be conserved,
	 even if changing the code and uploading again, the only way to go back is to plug the gps out, losing the fix  */

  delay(1000); /**Gives time to the GPS to boot perfectly */
#ifdef INCLUDE_GPS_VELOCITY
  myGps.sendCommand(PMTK_SET_NMEA_OUTPUT_GGAVTGONLY);
#else
  myGps.sendCommand(PMTK_SET_NMEA_OUTPUT_GGAONLY);
/** This sets the GPS to output only GGA sentences:
  the default setting for the communication between the board and the GPS chip can accomodate at most 960 bytes of data if transmitting permanently.
  Taking into account a few different formatting characters and some flow interrupts, it is not safe to go more than 800 char/s.
  As a consequence:
  10 . RMC = 650 bytes => accepted
  10 . (RMC + GGA) = 1300 bytes => impossible
  10 . GGA = 650 bytes => working
*/
#endif
  changeFrequency(currentUpdateFrequency);
}

void GPS_Client::begin(const Frequency updateFrequency, uint8_t enablePinNbr, bool initiallyEnabled)
{
  enablePin = enablePinNbr;
  if (enablePinNbr) {
    pinMode(enablePinNbr, OUTPUT);
    DPRINTS(DBG_ENABLE, "GPS Enable on pin #");
    DPRINT(DBG_ENABLE, enablePinNbr);
    if (initiallyEnabled) {
      digitalWrite(enablePinNbr, HIGH);
      gpsEnabled = true;
    } else {
      digitalWrite(enablePinNbr, LOW);
      gpsEnabled = false;
    }
    DPRINTS(DBG_ENABLE, ", initial status=");
    DPRINTLN(DBG_ENABLE, (int) gpsEnabled);
  } else {
    gpsEnabled = true; // Open pin enables the GPS.
    DPRINTSLN(DBG_ENABLE, "GPS Enable pin not used");
  }
  currentUpdateFrequency = updateFrequency;
  gpsForInterrupt = &myGps;

#ifdef __AVR__
  // Timer0 is already used for millis() - we'll just interrupt somewhere
  // in the middle and call the "Compare A" function above
  OCR0A = 0xAF;
  TIMSK0 |= _BV(OCIE0A); //macro
#elif defined(ARDUINO_ARCH_SAMD)
#else
#error "UNSUPPORTED BOARD IN GPS_Client.cpp" /**If any of the architectures supported by the class isn't used (__AVR__ or feather m0) */
#endif

  if (gpsEnabled) {
    configureGPS();
    /** Sets output rate of the GPS to desired frequency (passed in as parameter) */
  }
}

void GPS_Client::enable(bool status) {
  if (status == gpsEnabled) return; // No action if enabling or disabling more than once.

  if (enablePin) {
    if (status) {
      digitalWrite(enablePin, HIGH);
      gpsEnabled = true; // be sure to set before calling configureGPS()!
      configureGPS();
#ifdef ARDUINO_ARCH_SAMD
      gpsTimer.enable(); // timer is started as part of the first call to
      // changeFrequency(), inside the configureGPS();
#endif
      DPRINTSLN(DBG_DIAGNOSTIC, "GPS Enabled");
    }
    else {
      digitalWrite(enablePin, LOW);
      gpsEnabled = false;
#ifdef ARDUINO_ARCH_SAMD
      gpsTimer.disable();
#endif
      DPRINTSLN(DBG_DIAGNOSTIC, "GPS Disabled");
    }
  }
  else {
    DPRINTSLN(DBG_ENABLE, "GPS_Client::enable(): no pin defined, ignored");
  }
}

void GPS_Client::changeFrequency(const Frequency updateFrequency) {
  currentUpdateFrequency = updateFrequency;
  if (!gpsEnabled) return; // Actual configuration occurs when GPS is enabled.
  switch (updateFrequency) {
    case Frequency::F1Hz :
      myGps.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);
      DPRINTLN(DBG_FREQUENCY, "GPS update frequency set to 1hz")
      break;
    case Frequency::F5Hz :
      myGps.sendCommand(PMTK_SET_NMEA_UPDATE_5HZ);
      DPRINTLN(DBG_FREQUENCY, "GPS update frequency set to 5hz")
      break;
    case Frequency::F10Hz :
#ifdef INCLUDE_GPS_VELOCITY
      if (currentUpdateFrequency == Frequency::F10Hz) {
        Serial.print("ERROR: MAX frequency is 5Hz when GPS velocity is used. Forced to 5Hz");
        myGps.sendCommand(PMTK_SET_NMEA_UPDATE_5HZ);
        break;
      }
#endif
      myGps.sendCommand(PMTK_SET_NMEA_UPDATE_10HZ);
      DPRINTLN(DBG_FREQUENCY, "GPS update frequency set to 10hz")
      break;
    default :
      DPRINTLN(DBG, "Unexpected frequency value. Terminating program.");
      DASSERT (false);
  }
#if defined(ARDUINO_ARCH_SAMD)
  uint32_t interruptPeriod = 1000 / ((int) updateFrequency * 100);
  if (!gpsTimer.changePeriod(interruptPeriod))
  {
    // This happens when the timer was never started before
    if (gpsTimer.start(interruptPeriod, TimerHandler))
    {
      DPRINTS(DBG_TIMER_INFO, "Timer period set to ")
      DPRINT(DBG_TIMER_INFO, interruptPeriod);
      DPRINTSLN(DBG_TIMER_INFO, " msec");
    }
    else {
      DPRINTSLN(DBG_DIAGNOSTIC, "ERROR: COULD NOT SET TIMER FOR GPS");
    }
  }
#endif
}

void GPS_Client::usePreviousValues(CansatRecord &record) {
  record.newGPS_Measures = false;
  record.GPS_LongitudeDegrees = lastLongitudeValue;
  record.GPS_LatitudeDegrees = lastLatitudeValue;
  record.GPS_Altitude = lastAltitudeValue;
#ifdef INCLUDE_GPS_VELOCITY
  record.GPS_VelocityKnots = lastVelocityKnots;
  record.GPS_VelocityAngleDegrees = lastVelocityAngle;
#endif
}

void GPS_Client::readData(CansatRecord & record)
{
  if (!gpsEnabled) {
    usePreviousValues(record);
    return;
  }

  bool parseResult;
  bool newReceived = myGps.newNMEAreceived();
  record.newGPS_Measures = false;

  if (newReceived) {
    DPRINTS(DBG_NMEA, "new NMEA received: ");
    DPRINTLN(DBG_NMEA, myGps.lastNMEA());
    parseResult = myGps.parse(myGps.lastNMEA());
    if (!parseResult) {
      DPRINTSLN(DBG_NMEA, "  Error parsing NMEA (ignored)");
    } else {
      DPRINTSLN (DBG_NMEA, "  Parsing OK");
    }
  }

  if (hasFix() && newReceived && parseResult) {
    DPRINTSLN(DBG_NMEA, "fix ok, new received, parsing OK");
    record.newGPS_Measures = true;
    record.GPS_LongitudeDegrees = myGps.longitudeDegrees;
    record.GPS_LatitudeDegrees = myGps.latitudeDegrees;
    lastLatitudeValue = myGps.latitudeDegrees;
    lastAltitudeValue = myGps.altitude;
#ifdef INCLUDE_GPS_VELOCITY
    record.GPS_VelocityKnots = myGps.speed;
    record.GPS_VelocityAngleDegrees = myGps.angle;
#endif
    record.GPS_Altitude = myGps.altitude;
  }
  else {
    //DPRINTSLN(DBG_NMEA, "fix not ok in in NMEA or no new data");
    usePreviousValues(record);
  }
}

bool GPS_Client::enabled() const {
  return gpsEnabled;
};

bool GPS_Client::hasFix() const {
  // Valid fixes are 1 (GPS fix), 2 (DGPS fix), 3 (PPS fix)
  return (gpsEnabled && ((myGps.fixquality == 1) || (myGps.fixquality == 2)  || (myGps.fixquality == 3)));
}
