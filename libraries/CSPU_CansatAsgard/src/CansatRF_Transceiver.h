/* CansatRF_Transceiver.h (see info in class doc comment. */

/* ----------------------------Includes and defines---------------------------- */
#include <type_traits>
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "CansatRecord.h"
#include "CansatInterface.h"
#include "elapsedMillis.h"
#include "SD_Logger.h"
#include "DoubleSerial.h"
#include "Serial2.h"
#include "CansatRF_TransceiverBlinkingLEDs.h"

#ifdef RF_ACTIVATE_API_MODE
#include "CansatXBeeClient.h"
#else
#include "LineStream.h"
#endif

//For debugging
#define DBG_STATION 0
#define DBG_INCOMING_MSG 0
#define DBG_INCOMING_RECORD 0
#undef SIMULATE_ON_USB_SERIAL  // Define to simulate the serial communication to XBee on Serial (debug only)

/* ---------------------------- Class definition ---------------------------- */

/** @ingroup CSPU_CansatAsgard
 *  @brief Base class for Cansat RF-Transceiver station (ground).
 *  - Downlink: it receives the data from the RF, stores it on an SD Card and sends to
 *              the RT-Processing on TTL-USB Serial (data records) and command PC on USB
 *              serial (anything else).
 *  - Uplink:   it just forwards from the command PC (TTL serial) to the RF.
 *
 * @par Hardware
 *  Wiring: TX-RX on USB-TTL converter and TX-RX on board must be crossed.
 *  Using Serial2 on Feather, it means  USB-TTL TX to pin #11
 *                                      USB-TTL RX to pin #10
 *
 * @par Software architecture
 * This is a template class, which implements by default a generic version of the
 * receiver, working with CansatRecords. Since C++ templates must be completely
 * defined in the header file, the implementation of the methods is located in file
 * CansatRF_Transceiver.tpp (included at the bottom of CansatRF_Transceiver.h) to
 * provide some separation between declaration and definition.
 * This class is expected to run almost unchanged for any Cansat project when
 * instantiated with the right Project-specific subclasses of CansatRecord.
 *
 * @par
 * The management of the various LEDs is performed by a dedicated utility
 * class: CansatRF_TransceiverBlinkingLEDs.
 *
 * @par Remarks
 * \li Project not using the RT_CanCommander features do not need to remove anything.
 * \li See example of use in 0000-00_Templates/xxxRF_Transceiver.ino.
 */
template<class RECORD = CansatRecord>
class CansatRF_Transceiver {

public:
  // ----------------------------Constants----------------------------
  /** When receiving records, and no particular condition is detected,
	 *  only 1 record every RecordFeedbackPeriod msec is displayed on the serial
	 *  interface. */
  static constexpr uint32_t RecordFeedbackPeriodInMsec = 2000;

  /** If false, useless information about the frame type is not sent to serial,
	 *  in order to improve readability.
	 *  If true, the frame type is sent to the serial interface when a CmdResponse
	 *  is received from the can.
	 *  This is used to allow for the RT-Commander GUI interface to
	 *  discriminate between the strings to be interpreted (the CmdResponse) and
	 *  the ones to simply display (any other). */
  static constexpr bool SendResponseFrameTypeToSerial = true;

  static constexpr byte RF_RxPinOnUno = 5;   /**< Rx pin when running on Uno board */
  static constexpr byte RF_TxPinOnUno = 6;   /**< Tx pin when running on Uno board */
  static constexpr byte TTL_RxPinOnUno = 10; /**< Rx pin when running on Uno board */
  static constexpr byte TTL_TxPinOnUno = 11; /**< Tx pin when running on Uno board */

  /**< For the LineStream Class. Longest line is the CSV header, keep margin for
	 * markers etc.. */
  static constexpr unsigned int MaxLineLength = 300;


  static constexpr const char* fourCharPrefix = "RfTr";  //**< File prefix on SD-Card */
#ifndef RF_TSCV_VERSION
#error "Please define RF_TSCV_VERSION"
#endif
#if (RF_TSCV_VERSION == 0)
  // Original RF_Transceiver without a PCB
  static constexpr byte SD_ChipSelectPinNumber = 13;

#elif (RF_TSCV_VERSION == 1)
  // 2025 RF_TRSV with PCB v1+
  static constexpr byte SD_ChipSelectPinNumber = 4;
  //** <SD-card CS min */
#else
#error "Unknown RF_TSCV version"
#endif
  static constexpr unsigned int requiredFreeMegs = 100;  //**< SD-Card required free space, in Mb */

  /** Constructor */
  CansatRF_Transceiver();

  virtual ~CansatRF_Transceiver(){};

  /** Initialize the whole receiver, including the Serial interface. It takes care of all error reporting,
	 *  so the calling method could ignore the return value and not provide any feedback,
	 *  unless there is additional initialization to perform.
	 *  This method must be called in the programs set-up method. */
  virtual bool begin();

  /** This method must be called in the main loop to run the RF-Transceiver */
  virtual void run();

protected:
  /** @name members to be overridden for specific project
   *  @{    **/

  /** Obtain the name of the current project
   *  @return a null-terminated string containing the name of the project.
   */
  virtual const char* getProjectName() {
    return "Cansat-CSPU project";
  };

  /** Obtain the Xbee client object */
  CansatXBeeClient* getXBeeClient() {
	  return &xb;
  };

  /** @} */  // group of methods to be overridden

  /** Call this method to blink the uplink LED.
   *  Blinking is transparently managed: just call this method
   *  with active=true before emitting, and with active=false
   *  afterwards.
   *  @param active True if the link is used, false otherwise.
   */
  virtual void setUplinkActive(bool active) {
	  theLEDs.setLinkActive(CansatRF_TransceiverBlinkingLEDs::UplinkIndex, active);
  }

#ifdef RF_ACTIVATE_API_MODE
  /** Check whether anything was received on the radio interface. Received information
	 *  is stored on the SD-Card, as text.
	 *  @param receivedA_String (out) True is a string was received.
	 *  @param receivedA_Record (out) True is a record was received.
	 */
  virtual bool receiveFrame(bool& receivedA_String, bool& receivedA_Record);
#endif
  virtual const char* getStringReceived() { return stringReceived; };
  virtual const RECORD& getRecordReceived() {return recordReceived; };

private:
  /* ----------- Hardware or Software Serials ----------- */
  //Define the different Serials  port
#ifdef SIMULATE_ON_USB_SERIAL
  HardwareSerial& RF = Serial;
#else
#if defined(ADAFRUIT_FEATHER_M0) || defined(ADAFRUIT_FEATHER_M0_EXPRESS)
  HardwareSerial& RF = Serial1;
  HardwareSerial& TTL = Serial2;
#elif defined(ARDUINO_AVR_UNO)
#include "SoftwareSerial.h"
  SoftwareSerial RF(RF_RxPinOnUno, RF_TxPinOnUno);
  SoftwareSerial TTL(TTL_RxPinOnUno, TTL_TxPinOnUno);
#else
#error "Unsupported board in RF-Transceiver"
#endif
#endif

  /* ------------- Other data members --------------------- */

  SD_Logger sd;     /**< logger for local storage */
  DoubleSerial ds;  /**<  Stream to send to both RF and Serial */
  bool sdAvailable; /**< true if the SD card is available */

#ifdef RF_ACTIVATE_API_MODE
#if !defined(ADAFRUIT_FEATHER_M0) && !defined(ADAFRUIT_FEATHER_M0_EXPRESS)
#error "RF_Transceiver requires Feather M0 or Feather M0 Express board to use the API mode"
#endif
  CansatXBeeClient xb;  ///< Xbee interface object.
  uint8_t* payloadPtr;  ///< Buffer for message payload.
  uint8_t payloadSize;  ///< Size of message payload buffer.
  char stringReceived[XBeeClient::MaxStringSize + 1];
  ///< In API mode we need a buffer to extract the string.
  RECORD recordReceived;  ///< a buffer to extract records into. $$$ Used specific CansatRecord subclass.
  String CSV_Buffer;      ///< a buffer to stream records into, as CSV.
  StringStream CSV_BufferStream;
  /**< a stream to fill the above CSV_Buffer.
	 *   NB: This buffering system is ugly highly inefficient, but time schedule
	 *       does not allow for a redesign... */
  //TODO: redesign buffering ?
#else
  const char* stringReceived; /**< In transparent mode, we just need a pointer to the
	 *   LineStream internal buffer. */
  LineStream ls;
#endif

  CansatRF_TransceiverBlinkingLEDs theLEDs;

  /** @name Private members not likely to be overridden
	 *  @{    **/
  /**<  Print a welcome banner on Serial output */
  virtual void printBanner();

  /** Initialize the RF-Transceiver.
	 *  If overridden, be sure to call begin() on the base class and check the return value.
	 * 	@return True if initialisation is succesfull.
	 */
  virtual bool initialiseReceiver();

  /** Perform any maintenance task during idle time */
  virtual void doIdle();

  /** Provide some feedback after a record was received to keep the user informed.
	 *  This method should be called whenever a record is received and takes care of limiting
	 *  the output to a readable amount.  */
  virtual void provideUserFeedbackOnRecord();

  /** @} */  // group of methods NOT likely to be overridden

};  // class

#include "CansatRF_Transceiver.tpp"
