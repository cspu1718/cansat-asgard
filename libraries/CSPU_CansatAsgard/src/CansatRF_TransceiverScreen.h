#pragma once

#include "SSD_1306_Client.h"
#include "elapsedMillis.h"

/** @inGroup CSPU_CansatAsgard
 *  @brief A utility class used to manage the display of a
 *  RF_Transceiver using a SSD 1306 OLED Feather Wing.
 */
class CansatRF_TransceiverScreen : public SSD_1306_Client {
  public:
	static constexpr int NumberOfGPS_Decimals = 5;
	static constexpr int GPS_StringSize = 3+4 + 2 * (9 + NumberOfGPS_Decimals);
	// 3 = "Lt:", 4= " Lg:", lat = [-]xx.ddddd, long [-]xxx.ddddd
	static constexpr int MaxMsgSize = 21;
	static constexpr int MaxProjectNameSize = 13; // in characters.
    /** Method to allow the object to perform routine maintenance:
     *  call as often as possible in the main loop.
     */
    virtual void run();

    /** Initialise le transceiver et l'écran
     *  @return true if initialization succeeded, false otherwise*/
    virtual bool begin(const char* projectName);

    /** Définit le nom du projet à afficher */
    virtual void setProjectName(const char* name);

    /** Update the GPS position on the screen. Last received
	 *  position is remembered.
	 *  @param msg The message to display.
	 *  @param withUpdate If true the screen is immediately updated.
	 */
    virtual void setGPS_Position(double latitude, double longitude, bool withUpdate = true);

    /** Update displayed message. The last message received is
	 *  stored and displayed indefinitely, except during a
	 *  temporary message display.
	 *  @param msg The message to display.
	 *  @param withUpdate If true the screen is immediately updated.
	 */
    virtual void displayMessage(const char* msg, bool withUpdate = true);

	/** Display a temporary message. It overwrites the currently
	 *  displayed message (even if it is temporary) for the
	 *  required duration and then displays the last standard
	 *  message again.
	 *  @param msg The message to display.
	 *  @param durationInSec The duration (in seconds) during
	 *         which the message must be displayed.
	 */
    virtual void displayTemporaryMessage(const char* msg, uint8_t durationInSec);

  protected:
    /** Met à jour l'écran avec les nouvelles données */
    void updateScreen();

  private:
    elapsedMillis elapsedSinceUpdate;
    char projectName[MaxProjectNameSize + 1] = "Unknown";
    char gpsPosition[GPS_StringSize + 1] = "(No GPS yet)";   /**< Current GPS position, as string. */
    char message[MaxMsgSize + 1] = "";             /**< Permanent message to display */
    char tmpMessage[MaxMsgSize + 1] = "";    /**<  Temporary msg to display */
    uint32_t tmpMessageExpiryTS;	/**< The TS at which the tmp msg expires */
};
