/*
 * CanSetupAndLoop.tpp
 * Implementation of CanSetupAndLoop template. This file is included from CanSetupAndLoop.h, but allows
 * to keep implementation in a different file.
 */
#pragma once

#include "CanSetupAndLoop.h"

#include <type_traits>
// Utility macros to process the thermistor information in the setup.
#ifndef QUOTE
#  define QUOTE(x) #x
#  define STR(x) QUOTE(x)
#endif

// Debugging switches
#define DBG_RECEPTION 0
#define DBG_SETUP 1
#ifndef DBG_DIAGNOSTIC // DBG_DIAGNOSTIC is normally defined in CansatConfig.h
#define DBG_DIAGNOSTIC 1
#endif



template<class ACQ_PROCESS, class RT_COMMANDER>
CanSetupAndLoop<ACQ_PROCESS, RT_COMMANDER>::CanSetupAndLoop() :
	process(),
	commander(RT_CommanderTimeoutInMsec)
{
	// Compile-time sanity check. This does not generated any runtime overhead.
	/*
	 static_assert(std::is_base_of<CansatAcquisitionProcess, ACQ_PROCESS>::value,
			"ACQ_PROCESS must be derived from CansatAcquisitionProcess");
	 static_assert(std::is_base_of<RT_CanCommander, RT_COMMANDER>::value,
				"RT_COMMANDER must be derived from RT_CanCommander");
	 Note: The above checks cannot possibly be performed anymore, since
	       CansatAcquisitionProcess and RT_CanCommander are now templates.
	       We should check that ACQ_PROCESS is derived from any specialisation
	       of the base template. Is that possible?
	 */
}

template<class ACQ_PROCESS, class RT_COMMANDER>
void CanSetupAndLoop<ACQ_PROCESS, RT_COMMANDER>::printBanner() const{
	  Serial << "#######    " 									    << ENDL;
	  Serial << "#          #     #     #  ######     #    #######" << ENDL;
	  Serial << "#        #   #   ##    #  #        #   #     #"    << ENDL;
	  Serial << "#       #     #  # #   #  #       #     #    #"    << ENDL;
	  Serial << "#       #######  #  #  #   #####  #######    #"    << ENDL;
	  Serial << "#       #     #  #   # #       #  #     #    #"    << ENDL;
	  Serial << "#       #     #  #    ##       #  #     #    #"    << ENDL;
	  Serial << "####### #     #  #     #  ######  #     #    #"    << ENDL;
}


template<class ACQ_PROCESS, class RT_COMMANDER>
void CanSetupAndLoop<ACQ_PROCESS, RT_COMMANDER>::printWelcomeBoard() const{
#ifdef INIT_SERIAL
  Serial << "============================================" << ENDL;
  Serial <<  getProjectName() << ENDL;
  printBanner();
  Serial << "============================================" << ENDL;
  Serial << "Hardware pins : " << ENDL;
  Serial << "  Serial debug (active LOW): A" << DebugCtrlPinNumber -A0 << ENDL;
  Serial << "  Thermistors:           1 : " << STR(THERMISTOR1_CLASS) <<
      ", pin=A" << Thermistor1_AnalogInPinNbr - A0 << ", Serial="
               << Thermistor1_Resistor << " Ohms" << ENDL;
#ifdef INCLUDE_THERMISTOR2
  Serial << "                         2 : " << STR(THERMISTOR2_CLASS)
     << ", pin=A" << Thermistor2_AnalogInPinNbr - A0 << ", Serial="
     << Thermistor2_Resistor << " Ohms" << ENDL;
#endif
#ifdef INCLUDE_THERMISTOR3
  Serial << "                         3 : " << STR(THERMISTOR3_CLASS)
     << ", pin=A" << Thermistor3_AnalogInPinNbr - A0 << ", Serial="
     << Thermistor3_Resistor << " Ohms" << ENDL;
#endif
  Serial << "  SD Card chip-select      : " <<  SD_CardChipSelect <<  ENDL;

  Serial << "Reference tension          : " << ThermistorTension << "V" << ENDL;
  Serial << "RT-Commander request code  : " << (byte) CansatFrameType::CmdRequest << ENDL;
  Serial << "Acquisition period         : " << CansatAcquisitionPeriod << " msec " << ENDL;
  Serial << ENDL;
#endif
} // PrintWelcomeBoard


template<class ACQ_PROCESS, class RT_COMMANDER>
void CanSetupAndLoop<ACQ_PROCESS, RT_COMMANDER>::setup() {
#ifdef INIT_SERIAL
  DINIT_WITH_TIMEOUT(USB_SerialBaudRate);
#if !defined(ADAFRUIT_FEATHER_M0) && !defined(ADAFRUIT_FEATHER_M0_EXPRESS)
  Serial << "*** Warning: Not running on Feather M0 or M0 Express board ??? ***" << ENDL << ENDL;
#endif
  printWelcomeBoard();
#endif
  process.begin();
  DPRINTSLN(DBG_SETUP, "AcquisitionProcess initialized");

  CansatHW_Scanner& hw = process.getHardwareScanner();
#ifdef RF_ACTIVATE_API_MODE
  rf = hw.getRF_XBee();
#else
  rf = hw.getRF_SerialObject();
#endif

  // if the RF interface is available, we use the RT-Commander,
  // otherwise, just run the AcquisitionProcess
  if (rf) {
#ifndef RF_ACTIVATE_API_MODE
    lineRF_Stream.begin(*rf, MaxUplinkMsgLength);
#endif
    commander.begin(*rf, process.getSdFat() , &process);
    DPRINTSLN(DBG_SETUP, "RT - Commander initialized");
  } else {
    DPRINTSLN(DBG_SETUP, "RT - Commander NOT IN USE");
  }
  DPRINTSLN(DBG_SETUP, "Setup complete");
  DPRINTSLN(DBG_SETUP, "------------------");
}

template<class ACQ_PROCESS, class RT_COMMANDER>
void CanSetupAndLoop<ACQ_PROCESS, RT_COMMANDER>::loop() {
  DPRINTSLN(DBG_LOOP, "*** Entering processing loop");
  if (rf) {
#ifdef RF_ACTIVATE_API_MODE
    if (rf->receive(msg, stringType, sequenceNbr)) {
      DPRINTSLN(DBG_RECEPTION, "Received string");

      // Process incoming message.
      switch (stringType) {
      case CansatFrameType::CmdResponse:
        DPRINTSLN(DBG_DIAGNOSTIC,
            "*** Received a Cmd Response ?? Ignored.")
        break;
      case CansatFrameType::StatusMsg:
        DPRINTSLN(DBG_DIAGNOSTIC,
            "*** Received a Status msg ?? Ignored.")
        break;
      case CansatFrameType::CmdRequest:
        // This is the only msg type the can should expect.
        break;
      default:
        DPRINTS(DBG_DIAGNOSTIC,
            "*** Error: unexpected StringType received (ignored):")
        DPRINT(DBG_DIAGNOSTIC, (int ) stringType)
        DPRINTS(DBG_DIAGNOSTIC, ", ")
        DPRINTLN(DBG_DIAGNOSTIC, msg)
        ;
      } // switch
    } // anything string received.
#else
    const char* msg = lineRF_Stream.readLine();
#endif
    if ((msg != nullptr) && (*msg != '\0')) {
      DPRINTS(DBG_LOOP_MSG, "Received '");
      DPRINT(DBG_LOOP_MSG, msg);
      DPRINTLN(DBG_LOOP_MSG, "'");
      commander.processCmdRequest(msg);
#ifdef RF_ACTIVATE_API_MODE
      *msg = '\0'; // reset msg buffer to avoid processing it again.
#endif
    }
    if (commander.currentState() == RT_COMMANDER::State_t::Acquisition) {
      process.run();
    }
  } else  {
    // rf not available.
    process.run();
  }
  DDELAY(DBG_LOOP, 500);  // Slow main loop down for debugging.
  DPRINTSLN(DBG_LOOP, "*** Exiting processing loop");
}


