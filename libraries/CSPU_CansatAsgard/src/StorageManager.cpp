/*
    StorageManager.cpp

    Created on: 19 janv. 2018
*/
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

//define DEBUG_CSPU
//define USE_ASSERTIONS  Do not define here: define in CansatConfig if needed.
//                       Defining it here causes problems during link of test program.
#undef USE_TIMER
#include "CansatConfig.h"
#include "Timer.h"

#include "StorageManager.h"

// DBG_STORAGE, DBG_DIAGNOSTIC are defined in CansatConfig.h
#define DBG_INIT_STORAGEMGR 0

StorageManager::StorageManager(unsigned int theCampainDurationInSec,
                               unsigned int theMinStoragePeriodInMsec) :
				recordSize(0),
				campainDuration(theCampainDurationInSec),
				minStoragePeriod(theMinStoragePeriodInMsec),
				initLogger(false),
				initEEPROM(false)
#ifndef IGNORE_EEPROMS
				,eeprom(10),
#endif
#ifndef CANSAT_IGNORE_SD_CARD
				,logger()
#endif
{
  // Do not debug or assert in constructor!
}

// silence warnings about unused parameters in next function if EEPROMS and/or
// SD card are ignored
#if (defined(IGNORE_EEPROMS) || defined(CANSAT_IGNORE_SD_CARD))
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif

bool StorageManager::init(CansatRecord& recordTemplate,
						  HardwareScanner* hwScanner,
						  const String& loggerFirstLine,
                          const char * /* fourCharPrefix */,
                          const byte chipSelectPinNumber,
                          const unsigned int /* requiredFreeMegs */,
                          const EEPROM_BankWriter::EEPROM_Key key)
{
  recordSize=recordTemplate.getBinarySize();

  DASSERT(recordSize > 0);
  DASSERT(campainDuration > 0);
  DASSERT(minStoragePeriod > 0);
  DASSERT(hwScanner != NULL);

#ifndef IGNORE_EEPROMS
  DPRINTSLN(DBG_INIT_STORAGEMGR, "Initializing EEPROM_BankWriter...");
  bool result1 = eeprom.init(key, *hwScanner, recordSize);
  if (result1 != true) {
    DPRINTSLN(DBG_DIAGNOSTIC, "Error initializing EEPROM_BankWriter.");
  } else {
    DPRINTSLN(DBG_INIT_STORAGEMGR, "EEPROM init ok.");
    DPRINTS(DBG_DIAGNOSTIC, "Records (found/free): ");
    DPRINT(DBG_DIAGNOSTIC, (eeprom.getTotalSize() - eeprom.getFreeSpace())/recordSize);
    DPRINTS(DBG_DIAGNOSTIC, "/");
    DPRINTLN(DBG_DIAGNOSTIC, eeprom.getNumFreeRecords());
    initEEPROM = true;
  }
#endif

#ifndef CANSAT_IGNORE_SD_CARD
  DPRINTSLN(DBG_DIAGNOSTIC, "Initialising SD logger...");
  logger.setChipSelect( chipSelectPinNumber);
  int result2 = logger.init(loggerFirstLine);
 
  switch (result2) {
    case 0:
      DPRINTS(DBG_DIAGNOSTIC, "Logger OK. Logging to ");
      DPRINTLN(DBG_DIAGNOSTIC, logger.fileName());
      initLogger = true;
      break;
    case 1:
      DPRINTSLN(DBG_DIAGNOSTIC, "Requested free space not available on SD Card");
      break;
    case -1:
      DPRINTSLN(DBG_DIAGNOSTIC, "*** SD Logger init failed");
      break;
  }
#else
  DPRINTS(DBG_DIAGNOSTIC, "SD Logger not initialized");
#endif

#if (defined(IGNORE_EEPROMS) || defined(CANSAT_IGNORE_SD_CARD))
  return (initLogger || initEEPROM);
#else
  return  true;
#endif
}

void StorageManager::storeOneRecord(const CansatRecord& record, const bool useEEPROM) {

  DPRINTSLN(DBG_STORAGE, "StorageManager::storeOneRecord");
  DBG_TIMER("StorageMgr::storeOnRecord");
  DASSERT(recordSize > 0);
  DASSERT(campainDuration > 0);
  DASSERT(minStoragePeriod > 0);


	// We can actually store the data, but in the right format according to destination.
	// The binary version of the data can only be stored as-is in memory.
	// The CSV version is larger but human-readable, and can be stored on the SD card, where
	// storage is cheap and unlimited.
#ifndef CANSAT_IGNORE_SD_CARD
	if (initLogger == true) {
		DPRINTS(DBG_STORAGE, "Storing to SD Card...");
		logger.log(record);
      	DPRINTSLN(DBG_STORAGE, "Stored to SD Card");
	}
#endif
#ifndef IGNORE_EEPROMS
	if ((initEEPROM == true) && useEEPROM) {
		DPRINTS(DBG_STORAGE, "Storing to EEPROM...");
		eeprom.storeOneRecord(binaryData, recordSize);
		DPRINTSLN(DBG_STORAGE, "Stored to EEPROM");
		// Do not check return codes: we can only fail silently and hope not all storage destination fail….
	}
#endif
	DPRINTSLN(DBG_STORAGE, "We're all proud of Lorenz");
}

// restore warnings about unused parameters if EEPROMS are ignored
#if (defined(IGNORE_EEPROMS) || defined(CANSAT_IGNORE_SD_CARD))
#pragma GCC diagnostic pop
#endif

void StorageManager::storeString(const String& stringData) {
#ifndef CANSAT_IGNORE_SD_CARD
	if (initLogger == true) {
	   logger.log(stringData);
	} else {
	    DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: cannot store string to SD_Logger ***");
	}
#endif
}

void StorageManager::storeString(const char* str) {
#ifndef CANSAT_IGNORE_SD_CARD
	if (initLogger == true) {
	   logger.log(str);
	} else {
	    DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: cannot store string to SD_Logger ***");
	}
#endif
}


void StorageManager::doIdle()
{
#ifndef CANSAT_IGNORE_SD_CARD
  if (initLogger) logger.doIdle();
#endif
#ifndef IGNORE_EEPROMS
  if (initEEPROM) eeprom.doIdle();
#endif
}

unsigned long StorageManager::getNumFreeEEEPROM_Records() const {
#ifdef IGNORE_EEPROMS
	return 0;
#else
  if (initEEPROM == true) {
    return eeprom.getNumFreeRecords();
  }
  else return 0;
#endif
}

bool StorageManager::LoggerOperational() const {
#ifndef CANSAT_IGNORE_SD_CARD
  return initLogger;
#else
  return false;
#endif
}

#ifdef CANSAT_IGNORE_SD_CARD
static String NoSD_FileMsg="(none)";
const String& StorageManager::getSD_FileName() const {
	return NoSD_FileMsg;
};
#else
const String& StorageManager::getSD_FileName() const {
	return logger.fileName();
};
#endif
