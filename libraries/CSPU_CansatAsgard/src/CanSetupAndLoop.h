/*
 * CanSetupAndLoop.h
 */

#pragma once

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop
#include "CansatConfig.h"    // Keep this include as first one: it includes
							 // DebugCSPU.h, Timer.h etc with the appropriate
							 // configuration.
#ifdef RF_ACTIVATE_API_MODE
#  include "CansatXBeeClient.h"
#else
#  include "LineStream.h"
#endif
#include "CansatAcquisitionProcess.h"
#include "RT_CanCommander.h"

/** @ingroup CSPU_CansatAsgard
   This a template for the main entry points of the software running on-board the can:
   setup() and loop(). Those functions are part of a template class that makes use of all
   the reusable code from previous projects.

   It should provide a fully operational CanSat supporting the primary mission and
   secondary missions, provided the appropriate subclasses are provided.

   @par Usage
   Your sketch file (.ino) should include:
   @code
   	   #include "CanSetupAndLoop.h"
   	   #include "MyProjectAcquisitionProcess.h"
   	   #include "MyProjectCanCommander.h"

	   class Can : public CanSetupAndLoop<MyProjectAcquisitionProcess, MyProjectCanCommander> {
    	 virtual const char* getProjectName() const {
      	  	  return "ICARUS";
    	 };
         virtual void printBanner()  const {
      	  	  // print here whatever banner is appropriate on Serial.
	     };
   	   Can casl;

   	   void setup() {
   	   	   casl.setup();
   	   }

   	   void loop() {
   	   	   casl.loop();
   	   }
   @endcode

   Note that the SecondaryMissionController does not appear here: it is managed by
   (your subclass of) the CansatAcquisitionProcess, entirely.
   TODO CHECK THIS MUST REMAIN: it would be best if the SecondaryMissionController to use
        WAS CONTROLLED FROM HERE...
        SHOULD WE USE TEMPLATE WHEREVER A getNewRecord() is used to instantiate the right subclass?

 */
template<class ACQ_PROCESS=CansatAcquisitionProcess<>, class RT_COMMANDER=RT_CanCommander<>>
class CanSetupAndLoop {
public:
	CanSetupAndLoop();
	virtual ~CanSetupAndLoop() {};
	void setup();
	void loop();
private:
	/** @name Private members likely to be overridden
	 *  @{    **/
	/** Obtain the name of the current project
	 *  @return a null-terminated string containing the name of the project.
	 */
	virtual const char* getProjectName() const { return "Cansat-CSPU project"; };

	/**  Print a welcome banner on Serial output */
	virtual void printBanner() const;

	/** @}  End of likely to be overridden private members*/

	/** @name Private members NOT likely to be overridden
	 *  @{    **/
	/** Print startup information on USB Serial.
	 */
	virtual void printWelcomeBoard() const ;
	/** @}  End of not-likely to be overridden private members*/

	// Private data members
	ACQ_PROCESS process; /**< The acquisition process class, in charge of populating
							  the data record with primary and secondary mission data */
	RT_COMMANDER commander;
	/**< The RT_CanCommander object, to support project-specific commands. */

#ifdef RF_ACTIVATE_API_MODE
	CansatXBeeClient* rf = NULL;
	char msg[CansatXBeeClient::MaxStringSize+1];
	CansatFrameType stringType= (CansatFrameType) 0;
	uint8_t sequenceNbr = 0;
#else
	LineStream lineRF_Stream;
	Stream* rf = NULL;
#endif
};

#include "CanSetupAndLoop.tpp"
