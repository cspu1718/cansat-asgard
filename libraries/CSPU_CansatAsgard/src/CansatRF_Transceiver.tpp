/* CansatRF_Transceiver.tpp : 
   Implementation file for template class CansatRF_Transceiver.

   This file is included by CansatRF-Transceiver.h and is not a compilation unit,
   hence the strange .tpp extension. 

   This allows for the complete definition of the template to be in the header file,
   as required, but the implementation to be separated anyway. 
 */
template<class RECORD>
CansatRF_Transceiver<RECORD>::CansatRF_Transceiver() :
	sd(SD_ChipSelectPinNumber),
	ds((uint16_t) CansatFrameType::DataRecord),
	sdAvailable(false),
	xb(CanXBeeAddressSH, CanXBeeAddressSL),
	payloadPtr(nullptr),
	payloadSize(0),
	CSV_BufferStream(CSV_Buffer)
{
	// Compile-time sanity check. This does not generated any runtime overhead.
	static_assert(std::is_base_of<CansatRecord, RECORD>::value, "RECORD must be derived from CansatRecord");
};

template<class RECORD>
bool CansatRF_Transceiver<RECORD>::begin() {
	// Init the serial ports. For the USB one, wait for 3 seconds and then
	// start without serial port (this is required for operation without
	// an external computer. 
	DINIT_WITH_TIMEOUT(USB_SerialBaudRate);
	printBanner();
	RF.begin(RF_SerialBaudRate);
	TTL.begin(USB_SerialBaudRate);
	Serial << "  RF Serial port initialized (" << RF_SerialBaudRate << " baud)" << ENDL;
	Serial << "  RT-Processing serial port initialized (" << USB_SerialBaudRate << " baud)" << ENDL;

	SPI.begin();
	pinMode(SD_ChipSelectPinNumber, OUTPUT);
	Serial << "  SPI bus initialized (SD cards CS=)" << SD_ChipSelectPinNumber << ")" << ENDL;

		

	// Initialise LED manager
	if (!theLEDs.begin()) {
		Serial << "Error initializing the LED manager" << ENDL;
		return false;
	}
	// Initialise software
	if (!initialiseReceiver()) {
		return false;
	}

	Serial << "Initialisation complete. Displaying 1 record every " << RecordFeedbackPeriodInMsec / 1000.0 << "s." << ENDL << ENDL;
	return true;
};

template<class RECORD>
void CansatRF_Transceiver<RECORD>::run() {
	bool downlinkActive = false;
	bool receivedA_String = false;
	bool receivedA_Record = false;
	bool uplinkActive = false;

	// **** Part 1: downlink ****
	// Read one message from the radio
#ifdef RF_ACTIVATE_API_MODE
	downlinkActive = receiveFrame(receivedA_String, receivedA_Record);
#else
	// In transparent mode, just read from a stream.
	// (returns NULL if no end-of-line or end-of-string marker is received)
	stringReceived = ls.readLine();
	receivedA_String = (stringReceived != nullptr);
	receivedA_Record = false; // in transparent mode, records are received as strings.

	//If anything received, store the data and give it to the computer
	if (receivedA_String) {
		downlinkActive = true;
		DPRINTS(DBG_INCOMING_MSG, "Received  string: ");
		DPRINTSLN(DBG_INCOMING_MSG, stringReceived);
		if (sdAvailable) {
			//Store data to the SD card
			DPRINTSLN(DBG_STATION, "Storing on SD Card...");
			sd.log(stringReceived, true);
		} else {
			DPRINTSLN(DBG_STATION, "*** Skipping string storage: sd not available ***");
		}
		//Using DoubleSerial
		DPRINTSLN(DBG_STATION, "Sending the received string to the serial ports...");
		ds << stringReceived;
	}
#endif
	theLEDs.setLinkActive(CansatRF_TransceiverBlinkingLEDs::DownlinkIndex, downlinkActive);

	// *** Part 2: uplink
	uplinkActive = Serial.available();
	if (uplinkActive) {
		Serial << ENDL << "--->"; // Start with ENDL: we could be in a stringPart sequence.
		char c = ' ';
		RF_OPEN_CMD_REQUEST((&xb));
		while (Serial.available()) {
			c = (char) Serial.read();
			// We do not transmit end-of-line characters.
			if ((c != '\n') && (c != '\r'))  {
#ifdef RF_ACTIVATE_API_MODE
				xb << c;
#else
				RF << c;
#endif
			}
			Serial << c; // Echo on terminal
		}
		RF_CLOSE_CMD_REQUEST((&xb));
		if ((c != '\n') && (c != '\r')) {
			// If the user didn't send an EOL, add one in the display.
			Serial << ENDL;
		}
		theLEDs.setLinkActive(CansatRF_TransceiverBlinkingLEDs::UplinkIndex, true);
	}	else {
		theLEDs.setLinkActive(CansatRF_TransceiverBlinkingLEDs::UplinkIndex, false);
	}

	// If it is an empty loop, check for maintenance tasks
	if (!uplinkActive && !downlinkActive) {
		doIdle();
	}

	theLEDs.update(); // Blink LEDs as required.
}; //  run()

template<class RECORD>
void CansatRF_Transceiver<RECORD>::printBanner() {
	Serial << getProjectName() << ENDL;
	Serial << " ######  #######       ####### " << ENDL;
	Serial << " #     # #                #    #####    ##   #    #  ####   ####  ###### # #    # ###### #####  " << ENDL;
	Serial << " #     # #                #    #    #  #  #  ##   # #      #    # #      # #    # #      #    # " << ENDL;
	Serial << " ######  #####   #####    #    #    # #    # # #  #  ####  #      #####  # #    # #####  #    # " << ENDL;
	Serial << " #   #   #                #    #####  ###### #  # #      # #      #      # #    # #      #####  " << ENDL;
	Serial << " #    #  #                #    #   #  #    # #   ## #    # #    # #      #  #  #  #      #   #  " << ENDL;
	Serial << " #     # #                #    #    # #    # #    #  ####   ####  ###### #   ##   ###### #    # " << ENDL << ENDL;
}

template<class RECORD>
bool CansatRF_Transceiver<RECORD>::initialiseReceiver() {
#if !defined(ADAFRUIT_FEATHER_M0) && !defined(ADAFRUIT_FEATHER_M0_EXPRESS)
	Serial << "*** Warning: Not running on Feather M0 or M0 Express board ??? ***" << ENDL << ENDL;
#endif
	//SD Card
	sdAvailable = false;
	Serial << "  Launching SD Logger..." << ENDL;
	signed char resultSD_Init = sd.init("", fourCharPrefix, requiredFreeMegs);
	switch (resultSD_Init) {
	case 0:
		DPRINTS(DBG_DIAGNOSTIC, "  Logger OK. Logging to ");
		DPRINT(DBG_DIAGNOSTIC, sd.fileName());
		DPRINTS(DBG_DIAGNOSTIC, ". ");
		DPRINT(DBG_DIAGNOSTIC, sd.getFreeSpaceInMBytes());
		DPRINTSLN(DBG_DIAGNOSTIC, " Mbytes availaible for storage");
		sdAvailable = true;
		break;
	case 1:
		DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: SD Init successful but required free space not available");
		break;
	case -1:
		DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: SD Init failed. Problem could be: no card, card not formatted, storage full, storage read-only, attempt to initialize twice etc...");
		break;
	default:
		DPRINTS(DBG_DIAGNOSTIC, "*** Unexpected SD init result: ");
		DPRINTLN(DBG_DIAGNOSTIC, resultSD_Init);
		break;
	}

#ifdef RF_ACTIVATE_API_MODE
	CSV_Buffer.reserve(200); // Avoid memory fragmentation.
	xb.begin(RF);
	Serial << "  Using XBee API mode, using XBees set '" << RF_XBEE_MODULES_SET << "'" << ENDL;

	Serial << "  Destination (Can) address=0x";
	Serial.print(CanXBeeAddressSH, HEX);
	Serial << "-0x";
	Serial.println(CanXBeeAddressSL, HEX);
	while (!xb.isModuleOnline()) {
		Serial << ENDL << " ***** ERROR: XBee module not online ****" << ENDL;
		Serial << "Retrying..." << ENDL;
	}
	xb.setTransmissionPower(XBeeTypes::TransmissionPowerLevel::level4);
	xb.printConfigurationSummary(Serial);
#else
	//LineStream
	if (ls.begin(RF, MaxLineLength)) {
		DPRINTSLN(DBG_DIAGNOSTIC, "  LineStream OK.");
	} else {
		DPRINTSLN(DBG_DIAGNOSTIC, "*** Error in LineStream init!");
	}
	Serial << "  Using XBee transparent (AT) mode" << ENDL;
	//DoubleSerial
	ds.begin(TTL);
	DPRINTSLN(DBG_DIAGNOSTIC, "  DoubleSerial OK.");
#endif

	DPRINTSLN(DBG_DIAGNOSTIC, "  Receiver initialization over.");
#ifdef DBG_DIAGNOSTIC
	Serial << "Sending control msg to RT-Processing" << ENDL;
#endif
	TTL << "Link to RT-Processing OK." << ENDL;
	TTL.flush();


#ifdef RF_ACTIVATE_API_MODE
	Serial << "Send commands to the can as CSV strings, starting with the CansatCmdRequestType" << ENDL
			<< "followed by the applicable parameters (as defined in CansatInterface.h)" << ENDL;
#else
	Serial << "Send commands to the can as CSV strings, starting with "1, " followed by the " << ENDL
			<< "CansatCmdRequestType, followed by the applicable parameters (as defined in CansatInterface.h)" << ENDL;
#endif

	Serial << "-------------------" << ENDL;
	Serial << "Header of expected data record:" << ENDL;
	recordReceived.printCSV(Serial, CansatRecord::DataSelector::All, CansatRecord::HeaderOrContent::Header);
	Serial << ENDL << "-------------------" << ENDL;
	return true;
}

template<class RECORD>
void CansatRF_Transceiver<RECORD>::doIdle() {
	sd.doIdle();
}

template<class RECORD>
void CansatRF_Transceiver<RECORD>::provideUserFeedbackOnRecord() {
	static uint32_t lastReceivedTS = 0;
	static uint32_t lastFeedbackTS = 0;
	static uint32_t currentRecordPeriod = 0;

	uint32_t newPeriod = recordReceived.timestamp - lastReceivedTS;
	uint32_t now = millis();
	bool timeForFeedback = (now - lastFeedbackTS) >= RecordFeedbackPeriodInMsec;

	if (newPeriod != currentRecordPeriod) {
		currentRecordPeriod = newPeriod;
		timeForFeedback = true;
	}

	// Provide some feedback every 2 secs
	if (timeForFeedback) {
		recordReceived.printCSV(Serial);
		Serial << " ... (delay since last = " << newPeriod << " msec)" << ENDL;
		lastFeedbackTS = now;
	}
	lastReceivedTS = recordReceived.timestamp;
}

#ifdef RF_ACTIVATE_API_MODE
template<class RECORD>
bool CansatRF_Transceiver<RECORD>::receiveFrame(bool& receivedA_String, bool& receivedA_Record) {
	receivedA_String = false;
	receivedA_Record = false;
	CansatFrameType stringType;
	static int currentStringPartIdx = -1; // keep track: if we are receiving string parts, we should not
	// insert newline characters unless a new string is started.
	uint8_t stringSeqNumber;
	bool gotRecord;
	if (xb.receive(recordReceived, stringReceived, stringType, stringSeqNumber, gotRecord)) {
		if (gotRecord) {
			receivedA_Record = true;
			if (currentStringPartIdx >= 0) {
				// We were in a multi-part string and it is now over.
				Serial << ENDL;
				if (sdAvailable) {
					sd.log("", true);
				}
				currentStringPartIdx = -1;
			}
			provideUserFeedbackOnRecord();
		} else
		{
			switch (stringType) {
			case CansatFrameType::CmdRequest:
				DPRINTSLN(DBG_DIAGNOSTIC,
						"*** Received a Cmd Request ?? Ignored.")
						;
				break;
			case CansatFrameType::CmdResponse:
			case CansatFrameType::StatusMsg:
			case CansatFrameType::StringPart:
				receivedA_String = true;
				break;
			default:
				DPRINTS(DBG_DIAGNOSTIC, "*** Error: unexpected CansatFrameType received with string (ignored):");
				DPRINTLN(DBG_DIAGNOSTIC, (int) stringType);
			} // switch
		} // gotRecord
	} // anything received


	//If anything valid received, store the data and give it to the computer
	if (receivedA_String) {
		bool endlBefore = false;
		bool endlAfter = false;
		bool newStringStarted = true;
		DPRINTS(DBG_INCOMING_MSG, "Received string: type=");
		DPRINT(DBG_INCOMING_MSG, (int) stringType);
		DPRINTS(DBG_INCOMING_MSG, ": ");
		DPRINTSLN(DBG_INCOMING_MSG, stringReceived);

		// String always go to serial, only records go to TTL
		// Add an ENDL after a string we received, unless it is a StringPart
		// Add an ENDL before a string we received if  (i) we were receiving string parts
		// and (ii) it is NOT a string part or it is a string part with sequence number lower
		// than the previous one.
		// We need to prepend every string with "* " unless it is
		// not the first part of a string.
		if (stringType == CansatFrameType::StringPart) {
			if (currentStringPartIdx < 0) {
				// This is the first part of a string, and the previous one was not a string part
				// No ENDL required.
				currentStringPartIdx = stringSeqNumber;
				newStringStarted = true;
			} else if (stringSeqNumber <= currentStringPartIdx) {
				// This is the first part of a string, and the previous one was the last part of
				// another string. Separate with a ENDL;
				endlBefore = true;
				newStringStarted = true;
				currentStringPartIdx = stringSeqNumber;
			}
			else if (stringSeqNumber == (currentStringPartIdx + 1)) {
				// This is the next part of the current string. No ENDL required
				currentStringPartIdx++;
				newStringStarted = false;
			}
			else {
				// We missed some parts, and assume we are still in the same string.
				Serial << ENDL << "*** Error: missing string parts " << currentStringPartIdx + 1 << " to "
						<< stringSeqNumber - 1 << "!" << ENDL;
				newStringStarted = true;
				currentStringPartIdx = stringSeqNumber;
			}
		} else {
			newStringStarted = true;
			// This is not a string part. If the previous one was a string part, terminate it with ENDL;
			// Terminate this one with ENDL;
			if (currentStringPartIdx >= 0) {
				endlBefore = true;
				currentStringPartIdx = -1;
			}
			endlAfter = true;
		}

		//Store data to the SD card
		if (sdAvailable) {
			DPRINTSLN(DBG_STATION, "Storing on SD Card...");

			if (endlBefore) {
				sd.log("", true);
			}
			sd.log(stringReceived, endlAfter);
		} else {
			DPRINTSLN(DBG_STATION, "*** Skipping string storage: sd not available ***");
		}
		//Send to serial
		if (endlBefore) Serial << ENDL;
		if (SendResponseFrameTypeToSerial && (stringType == CansatFrameType::CmdResponse)) {
			Serial << (int) CansatFrameType::CmdResponse << ",";
		} else if (newStringStarted == true) {
			Serial << "* ";
		}

		// Print string adding "* " after each newline inside the string
		char * current = strtok(stringReceived, "\n");
		// The first token, we print anyway: it goes either to the end of the string
		// or to the first '\'.
		Serial << current;
		// Then we print all subsequent tokens on a new line, prepended with "*_"
		current = strtok(nullptr, "\n");
		while (current) {
			Serial << ENDL << "* " << current;
			current = strtok(nullptr, "\n");
		}
		if (endlAfter) Serial << ENDL;
	}
	else if (receivedA_Record) {
		DPRINTSLN(DBG_INCOMING_RECORD, "Received a record:");
		// Convert to CSV string for storage & transmission
		recordReceived.printCSV(CSV_BufferStream);
		if (sdAvailable) {
			//Store data to the SD card
			DPRINTSLN(DBG_STATION, "Storing on SD Card...");
			sd.log(CSV_Buffer, true);
		} else {
			DPRINTSLN(DBG_STATION, "*** Skipping record storage: sd not available ***");
		}
		//Using DoubleSerial
		DPRINTSLN(DBG_STATION, "Sending the received record to the serial ports...");
		DPRINTLN(DBG_INCOMING_RECORD, CSV_Buffer.c_str());
		TTL << CSV_Buffer.c_str() << ENDL;
		CSV_Buffer = "";
	}
	return (receivedA_String || receivedA_Record);
}
#endif
