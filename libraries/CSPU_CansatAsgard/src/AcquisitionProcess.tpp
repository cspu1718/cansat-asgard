/*
   AcquisitionProcess.tpp
   Implementation file for template AcquisitionProcess
*/

// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

//define DEBUG_CSPU
//define USE_ASSERTIONS  Do not define here: define in CansatConfig if needed.
//                       Defining it here causes problems during link of test program.
#undef USE_TIMER
#include "CansatConfig.h"
#include "Timer.h"

#include "AcquisitionProcess.h"
#include "HardwareScanner.h"
#include <type_traits>

#define DBG_INIT_ACQ_PROCESS 0
#define DBG_RUN 0
#define DBG_DIAGNOSTIC 1
#define DBG_LED_CONFIG 1   // Print LED configuration at startup.
#ifndef DBG_INIT
#define DBG_INIT 0
#endif

#define INIT_INFO 1 // Set to 1 to have the progress of initialisation documented on Serial.

template<class HW_SCANNER>
AcquisitionProcess<HW_SCANNER>::AcquisitionProcess(
  unsigned int acquisitionPeriodInMsec) :  hwScanner()
{
  periodInMsec = acquisitionPeriodInMsec;
  elapsedTime = acquisitionPeriodInMsec + 1; // Ensure next run will trigger an acquisition cycle.
  heartbeatLED_State = LED_State::Off;
  static_assert(std::is_base_of<HardwareScanner, HW_SCANNER>::value,
  			"HW_SCANNER must be derived from HardwareScanner");
}

template<class HW_SCANNER>
AcquisitionProcess<HW_SCANNER>::~AcquisitionProcess() {
}

template<class HW_SCANNER>
void AcquisitionProcess<HW_SCANNER>::blinkHeartbeatLED() {
  if ( (heartbeatTimer >= 1000) && (heartbeatLED_State == LED_State::Off) ) {
    setLED(LED_Type::Heartbeat, LED_State::On);
    heartbeatTimer = 0 ;
    heartbeatLED_State = LED_State::On;
  }

  if ((heartbeatTimer >= 1000) && (heartbeatLED_State == LED_State::On) ) {
    setLED(LED_Type::Heartbeat, LED_State::Off);
    heartbeatTimer = 0 ;
    heartbeatLED_State = LED_State::Off;
  }
}

template<class HW_SCANNER>
void AcquisitionProcess<HW_SCANNER>::run() {
  blinkHeartbeatLED();

  if (elapsedTime >= periodInMsec) {
    DBG_TIMER("AcqProc::runReal");
    elapsedTime = 0;    // Reset timer here, to avoid adding processing time to the cycle duration.
    DPRINTSLN(DBG_RUN, "Running Process");
    setLED(LED_Type::Acquisition, LED_State::On);
    acquireDataRecord();
    setLED(LED_Type::Acquisition, LED_State::Off);
    updateCampaignStatus();

    if (isRecordRelevantForStorage()) {
      setLED(LED_Type::Storage, LED_State::On);
      storeDataRecord(isCampaignStarted());
      setLED(LED_Type::Storage, LED_State::Off);
    }
  } else doIdle();
}

template<class HW_SCANNER>
void AcquisitionProcess<HW_SCANNER>::initLED_Hardware()  {
  byte pin;
#if (DBG_LED_CONFIG == 1)
  Serial << "LED configuration:" << ENDL;
  Serial << "  Init        : " << hwScanner.getLED_PinNbr(LED_Type::Init) << ENDL;
  Serial << "  Storage     : " << hwScanner.getLED_PinNbr(LED_Type::Storage) << ENDL;
  Serial << "  Transmission: " << hwScanner.getLED_PinNbr(LED_Type::Transmission) << ENDL;
  Serial << "  Acquisition : " << hwScanner.getLED_PinNbr(LED_Type::Acquisition) << ENDL;
  Serial << "  Heartbeat   : " << hwScanner.getLED_PinNbr(LED_Type::Heartbeat) << ENDL;
  Serial << "  Campaign    : " << hwScanner.getLED_PinNbr(LED_Type::Campaign) << ENDL;
  Serial << "  UsingEEPROM : " << hwScanner.getLED_PinNbr(LED_Type::UsingEEPROM) << ENDL;
#endif
  pin=hwScanner.getLED_PinNbr(LED_Type::Init);
  if (pin) pinMode(pin, OUTPUT);
  pin=hwScanner.getLED_PinNbr(LED_Type::Storage);
  if (pin) pinMode(pin, OUTPUT);
  pin=hwScanner.getLED_PinNbr(LED_Type::Transmission);
  if (pin) pinMode(pin, OUTPUT);
  pin=hwScanner.getLED_PinNbr(LED_Type::Acquisition);
  if (pin) pinMode(pin, OUTPUT);
  pin=hwScanner.getLED_PinNbr(LED_Type::Heartbeat);
  if (pin) pinMode(pin, OUTPUT);
  pin=hwScanner.getLED_PinNbr(LED_Type::Campaign);
  if (pin) pinMode(pin, OUTPUT);
  pin=hwScanner.getLED_PinNbr(LED_Type::UsingEEPROM);
  if (pin) pinMode(pin, OUTPUT);

  setLED(LED_Type::Init, LED_State::On);
  setLED(LED_Type::Storage, LED_State::On);
  setLED(LED_Type::Transmission, LED_State::On);
  setLED(LED_Type::Acquisition, LED_State::On);
  setLED(LED_Type::Heartbeat, LED_State::On);
  setLED(LED_Type::Campaign, LED_State::On);
  setLED(LED_Type::UsingEEPROM, LED_State::On);
  delay(1000);  // All LEDs on for 1000 ms.
  setLED(LED_Type::Init, LED_State::Off);
  setLED(LED_Type::Storage, LED_State::Off);
  setLED(LED_Type::Transmission, LED_State::Off);
  setLED(LED_Type::Acquisition, LED_State::Off);
  setLED(LED_Type::Heartbeat, LED_State::Off);
  setLED(LED_Type::Campaign, LED_State::Off);
  setLED(LED_Type::UsingEEPROM, LED_State::Off);
  DPRINTSLN(DBG_INIT_ACQ_PROCESS,"initLED_Hardware done")
}

template<class HW_SCANNER>
void  AcquisitionProcess<HW_SCANNER>::begin() {
  DPRINTSLN(INIT_INFO, "  Initializing AcquisitionProcess");
  hwScanner.init();
  initLED_Hardware();
  setLED(LED_Type::Init, LED_State::On);
  setLED(LED_Type::Campaign, LED_State::On); // This LED remains On Until campaign is started.
  initCansatProject();			// Common Cansat initialisation
  initSpecificProject();
  setLED(LED_Type::Init, LED_State::Off);
  DPRINTSLN(INIT_INFO, "  AcquisitionProcess initialized.");
}

template<class HW_SCANNER>
void AcquisitionProcess<HW_SCANNER>::setLED(LED_Type type, LED_State status) {
  byte pinNumber = hwScanner.getLED_PinNbr(type);
  if (pinNumber==0) return;
#ifdef ARDUINO_TMinus
  digitalWrite(pinNumber, (status==LED_State::On) ? LOW : HIGH);
  // write LOW sets LED ON on TMINUS.
#else
  digitalWrite(pinNumber, (status==LED_State::On) ? HIGH : LOW);
#endif
}


