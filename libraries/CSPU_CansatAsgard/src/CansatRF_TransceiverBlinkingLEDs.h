/*
 * CansatRF_TransceiverBlinkingLEDs.h
 */

#pragma once
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop
#include "elapsedMillis.h"
#include "CansatConfig.h"
#

/** @ingroup CSPU_CansatAsgard
 * @brief A class for managing the blinking LEDs of the CansatRF_Transceiver: Heartbeat,
 * 		  Uplink and Downlink.
 *
 * Whenever there is activity on the up- and down- links, the corresponding LED blinks.
 * The Heartbeat LED always blinks.
 *
 * Activity will never change state more often than every 500 msec.
 *
 * @par Usage
 * 		\li call begin() during initialization.
 * 		\li call setLinkActive([UplinkIndex|DownlinkIndex],[true|false]) when starting and stopping activity.
 *		\li call update() from the main loop.
 */
class CansatRF_TransceiverBlinkingLEDs {
public:
  static constexpr byte UplinkIndex = 0;
  static constexpr byte DownlinkIndex = 1;
  CansatRF_TransceiverBlinkingLEDs(){};
  virtual ~CansatRF_TransceiverBlinkingLEDs(){};

  /** Initialize the object. Call before using the object.
	 *  @return True if initialisation was successful, false otherwise.
	 */
  virtual bool begin();

  /** Mark a link as active.
	 *  If the state is unchanged: reset time if active only.
	 *  If the state is different: change if MinActivityDuration is exceeded.
	 *  @param idx The index of the link considered.
	 *  @param active If true, the new status is 'active' otherwise it is
	 *  			  'inactive'.
	 */
  virtual void setLinkActive(byte idx, bool active);

  /** Actually turn LEDs on or off, depending on the link and LED status.
	 *  This method must be called in the main loop.
	 */
  virtual void update();
private:
  static constexpr byte NumActivityLEDS = 2;               // uplink and downlink.
  static constexpr unsigned long LED_StateDuration = 500;  // Duration a LED is on or off in msec.
#ifndef RF_TSCV_VERSION
#error "Please define symbol RF_TSCV_VERSION"
#endif
#if (RF_TSCV_VERSION == 0)
  // Original RF_Transceiver without a PCB
  static constexpr byte DownlinkLED_Pin = 9;  // Blinks when data is received from the can
  static constexpr byte UplinkLED_Pin = 12;   // Blinks when data sent to the can.
  static constexpr byte HeartBeatLED = 6;     // Always blinks.
#elif (RF_TSCV_VERSION == 1)
  // 2025 RF_TRSV with PCB v1+
  static constexpr byte DownlinkLED_Pin = 13;  // Blinks when data is received from the can
  static constexpr byte UplinkLED_Pin = 12;    // Blinks when data sent to the can.
  static constexpr byte HeartBeatLED = A5;     // Always blinks.
#else
#error "Unknown RF_TSCV version"
#endif
  static constexpr unsigned long MinActivityDuration = 500;  // (msec) the minimum duration the activity is considered on or off.
  static constexpr byte activityLED_Pin[NumActivityLEDS] = { UplinkLED_Pin, DownlinkLED_Pin };
  static constexpr unsigned long HeartBeatPeriod = 1000;  // msec.

  bool linkActive[NumActivityLEDS];
  elapsedMillis linkActivityDuration[NumActivityLEDS];  ///< Durations since the activity last changed state.
  elapsedMillis ledStateDuration[NumActivityLEDS];      ///< Durations since the LEDs last changed state.
  elapsedMillis heartBeatDelay;                         // Duration since heartbeat LED last blinking.
};                                                      // class
