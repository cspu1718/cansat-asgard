#include "CansatConfig.h"
#include "CansatRF_TransceiverScreen.h"
#include "CSPU_Debug.h"
#include "DoubleToString.h"

bool CansatRF_TransceiverScreen::begin(const char* projectName) {
  if (!SSD_1306_Client::begin()) {
    return false;
  }
  setProjectName(projectName);
  return true;
}

void CansatRF_TransceiverScreen::setProjectName(const char* name) {
  int numToCopy = strlen(name);
  if (numToCopy > MaxProjectNameSize) {
	strncpy(projectName, name, MaxProjectNameSize-1);
	projectName[MaxProjectNameSize-1] = '~';
	projectName[MaxProjectNameSize] = '\0';
  } else {
    strncpy(projectName, name, numToCopy);
    projectName[numToCopy] = '\0';
  }
  updateScreen();
}

void CansatRF_TransceiverScreen::setGPS_Position(double latitude, double longitude, bool withUpdate) {
  char buffer[20];

  strcpy(gpsPosition, "");
  doubleToString(latitude, buffer, NumberOfGPS_Decimals);
  strcat(gpsPosition, buffer);
  strcat(gpsPosition, ";");
  doubleToString(longitude, buffer, NumberOfGPS_Decimals);
  strcat(gpsPosition, buffer);
  if (withUpdate) {
	  updateScreen();
  }
}

void CansatRF_TransceiverScreen::displayMessage(const char* msg, bool withUpdate) {
  int numToCopy = strlen(msg);
  if (numToCopy > MaxMsgSize) {
	strncpy(message, msg, MaxMsgSize-1);
    message[MaxMsgSize-1]= '~';
    message[MaxMsgSize]= '\0';
  } else {
    strncpy(message, msg, numToCopy);
    message[numToCopy] = '\0';
  }

  if (withUpdate) {
	  updateScreen();
  }
}

void CansatRF_TransceiverScreen::displayTemporaryMessage(const char* msg, uint8_t durationInSec) {
  int numToCopy = strlen(msg);
  if (numToCopy > MaxMsgSize) {
	strncpy(message, msg, MaxMsgSize-1);
	tmpMessage[MaxMsgSize-1]= '~';
	tmpMessage[MaxMsgSize]= '\0';
  } else {
	strncpy(tmpMessage, msg, numToCopy);
	tmpMessage[numToCopy] = '\0';
  }
  tmpMessageExpiryTS = millis()+(durationInSec*1000);
  updateScreen();
}

void CansatRF_TransceiverScreen::updateScreen() {
  clear(false);

  // Afficher le nom du projet en haut à gauche
  printText(0, 0, projectName, false);

  // Afficher le message au centre
  if (tmpMessageExpiryTS and (tmpMessageExpiryTS > millis())) {
	printText(0, 12, tmpMessage, false);
  } else {
    printText(0, 12, message, false);
    tmpMessageExpiryTS=0;
  }

  // Afficher la position GPS en bas à gauche
  printText(0, 24, gpsPosition, false);

  // Afficher le timestamp en haut à droite
  char timestampStr[12];
  auto now = millis();
  now /= 1000;
  int sec = now % 60;
  now /= 60;
  int min = now % 60;
  now /= 60;
  sprintf(timestampStr, "%d:%02d:%02d", (int) now, min, sec);
  printText(85, 0, timestampStr, false);

  updateDisplay();
  elapsedSinceUpdate = 0;
}

void CansatRF_TransceiverScreen::run() {
  if (elapsedSinceUpdate > 1000) {
    updateScreen();
  }
}
