/*
 * SecondaryMissionController.tpp
 */

#include "CansatConfig.h"
#include "SecondaryMissionController.h"
#include <type_traits>

template<class RECORD>
SecondaryMissionController<RECORD>::SecondaryMissionController() :  elapsed(0),xbee(nullptr) {
	static_assert(std::is_base_of<CansatRecord, RECORD>::value,
			"RECORD must be derived from CansatRecord");
}

template<class RECORD>
 void SecondaryMissionController<RECORD>::run(RECORD& record) {
	if ((CansatSecondaryMissionPeriod == 0) || (elapsed >= CansatSecondaryMissionPeriod)) {
		elapsed=0;
		manageSecondaryMission(record);
	}
}

#ifdef RF_ACTIVATE_API_MODE
template<class RECORD>
bool SecondaryMissionController<RECORD>::begin(CansatXBeeClient* xbeeClient) {
	 xbee=xbeeClient;
	 Serial << "Initialisation of Secondary Mission Controller" << ENDL;
	 printInitialisationInfo();
	 Serial << "Secondary Mission Controller ok." << ENDL;
	 return true;
};
#else
template<class RECORD>
bool SecondaryMissionController<RECORD>::begin(Stream* RF_Stream) {
	 xbee=RF_Stream;
	 return true;
};
#endif

template<class RECORD>
void SecondaryMissionController<RECORD>::printInitialisationInfo(Stream& stream) {

}
