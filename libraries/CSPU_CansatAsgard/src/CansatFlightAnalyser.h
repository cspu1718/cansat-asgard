/*
 * CansatFlightAnalyser.h
 */

#pragma once

#include "CansatConfig.h"
#include "SecondaryMissionController.h"

#ifdef INCLUDE_FLIGHT_ANALYSIS_DATA
// This symbol must be defined in CansatConfig.h for this class to
// be meaningfull. Including this file without defining the symbol
// results in an error.

/** @ingroup CSPU_CansatAsgard
 *  @brief
 *  This is a general purpose SecondaryMissionController which takes care
 *  of analysis the primary-mission data about the altitude measurements.
 *  @par
 *  It first:
 *  	- computes an average descent velocity (using rolling average), which is
 *        available through method getAverageVelocity()),
 *      - detects take off (this information is available through the
 *        takeOffDetected() method.
 *      - detects ejection (by means of a timer of CansatMaxDelayFromTakeoffToEjection
 *        (constant defined in Cansatconfig.h) msec from take off
 *        detection (this information is available through the ejectionDetected()
 *        method).
 *  @par
 *  It then populates the additional fields of the CansatRecordWithFlightAnalysis,
 *  i.e.:
 *  	- The flight control code (to define whether the can is on the ground,
 *  	  not descending, is in nominal descent or in excessively high or low
 *  	  descent velocity).
 *  	- The flight phase, defined according to the altitudes defined in
 *  	  CansatConfig.h.
 *
 *  @design note
 *  It has been considered not to derive this class from SecondaryMissionController
 *  and to make use of it in the SecondaryMissionController, conditional to
 *  INCLUDE_FLIGHT_ANALYSIS_DATA. This would make sense, since flightCtrolCode and
 *  flightPhase are not exactly secondary mission information (not primary either,
 *  though). But it would have as major draw back: it would not make methods
 *  getAverageVelocity(), takeoffDetected() and ejectionDetected() available in
 *  the project-specific secondary mission controllers, and would therefore be
 *  a less general solution.
 */
template<class RECORD=CansatRecord>
class CansatFlightAnalyser: public SecondaryMissionController<RECORD> {
public:
	CansatFlightAnalyser();

	/** Initialize the controller before use
	 *  If additional parameters are required by subclasses and this method
	 *  if overridden, be sure to provide default values for each parameter,
	 *  retrieved from CansatConfig.h or from your project's
	 *  configuration file.
	 *  @return true if initialization is successful, false otherwise
	 */
#ifdef RF_ACTIVATE_API_MODE
	virtual bool begin(CansatXBeeClient* xbeeClient = nullptr) override;
#else
	virtual bool begin(Stream* RF_Stream = nullptr);
#endif

	float getAverageVelocity(); /**< return the velocity average if it is valid, or
	 	 	 	 	 	 	 	 	 value CansatFlightAnalyser::InvalidVelocity it is not. **/
	static constexpr uint8_t NumberOfVelocitySamples =
			(CansatVelocityAveragingPeriod * 1000 / CansatAcquisitionPeriod) + 1;
	/**< this is the number of the samples in order to fill entirely a table and get a valid velocity average */
	static constexpr uint16_t StartupDelayForFlightAnalysis = 10000;
	/**< Delay in msec to start using the altitude information to analyse the flight.
	        This delay is converted in a number of records to ignore at startup. */
	static constexpr float InvalidVelocity = 1000000;
	bool takeoffDetected(); /**< true if the takeoff has been detected */
	bool ejectionDetected() {
		return ejectionHappened;
	};

	void printInitialisationInfo(Stream& stream = Serial) override;
protected:
	/** Actually manage the secondary mission. This method is called every
	 *  CansatSecondaryMissionPeriod msec (defined in CansatConfig.h).
	 *  This method should NOT be overridden by each project. Instead it should
	 *  override method manageProjectSpecificMission() which is called by
	 *  manageSecondaryMission() as soon as the flight analysis has been completed,
	 *  and flightControlCode and flight phase as completed in the record.
	 *  @param record The last record, already complete with all primary
	 *                mission data and possibly the secondary mission
	 *                sensors data collected by the AcquisitionProcesss
	 */
	virtual void manageSecondaryMission(RECORD& record) override final;

private:
	/** Actually manage the project-specific part of the secondary mission.
	 *  This method is called every
	 *  CansatSecondaryMissionPeriod msec (defined in CansatConfig.h).
	 *  This method should be overridden by each project and can assume that
	 *  the primary mission data, and flightControlCode and flight phase
	 *  are available in the record.
	 *  @param record The last record, already complete with all primary
	 *                mission data and possibly the secondary mission
	 *                sensors data collected by the AcquisitionProcesss
	 */
	virtual void manageProjectSpecificMission(RECORD& record) {};

	/** Update flight status based on the incoming record, i.e. identify the
	       phase we currently are in.
	 */
	virtual void updateFlightStatus(const RECORD &record);

	/** This method stores the velocity within the record in a table and averages it.
	        Then, it checks if velocities got are valid and then allows us to know if the
	        takeoff has already happened, and whether the can is already out of the rocket.
	          @param record containing the descent velocity. Nothing is changed in the record.
	 */
	virtual void analyseDescentVelocity(const RECORD & record);

	/** Update the current flight phase if shouldUpdateFlightPhase is true.
	      @param record The record containing the absolute and reference altitude
	 */
	virtual void updateFlightPhase(const RECORD& record);

	void clearAverageVelocity(); /**< clear up all data of the table */
	void printVelocityTable(); /**< it prints the table with the descent velocity variable within the record */

	unsigned long takeoffTimestamp; /**< timestamp of the takeoff */
	float velocityTable[NumberOfVelocitySamples] = {}; /**< table used to store the velocity values of the record */
	float averageVelocity; /**< current average of the velocity in the table*/
	uint8_t currentVelocityIndex; /**< indicates which index or row of the table we are in */
	bool velocityAverageValid; /**< if true when the table is fully filled with  valid velocity values */
	unsigned long lastRecordTimestamp; /**< timestamp of the current last record */
	bool takeoffHappened; /**< if true the takeoff of the rocket is detected */
	bool ejectionHappened; /**< if true, the ejection of the can from the rocket happened */
	FlightControlCode currentFlightStatus; /**< The current flight status */
	unsigned long lastFlightStatusUpdate; /**< When was the controller info last updated.0 = never */
	bool shouldUpdateFlightPhase; /**< Should the flight phase be updated in the next call updateFlightPhase?
	                           (should be true if in nominal descent and delay for updating has passed) */
	uint8_t currentFlightPhase; /**< The current flight phase. 15=no flight phase could be determined. */

};
#include "CansatFlightAnalyser.tpp"

#else
#error Attempt to include CansatFlightAnalyser.h without defining INCLUDE_FLIGHT_ANALYSIS_DATA in CansatConfig.h.
#endif

