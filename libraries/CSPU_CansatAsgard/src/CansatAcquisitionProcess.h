/*
    CansatAcquisitionProcess.h
*/

#pragma once
#include "AcquisitionProcess.h"
#include "CansatHW_Scanner.h"
#include "BMP_Client.h"
#include "StorageManager.h"
#include "ThermistorClient.h"
#include "SecondaryMissionController.h"
#ifdef ARDUINO_ARCH_SAMD
#  include "GPS_Client.h"
#  include "Serial2.h"
#endif

/** @ingroup CSPU_CansatAsgard
 *  @brief A template class implementing the common features of a Cansat acquisition process:
    	- primary mission data collection
    	- storage on SD-Card, possibly EEPROMS
    	- transmission on the RF interface
    	- call to the secondary mission controller to acquire additional data
    	  and perform the required processing.
    	- etc.
	This class is implemented as a template to be used with any combination
	of a CansatRecord (to store the data), a CansatHW_Scanner (to manager the hardware),
	and a SecondaryMissionController (to perform the project-specific processing).

    Configuration is defined in CansatConfig.h. Please note that in case the record
    includes the flight analysis data (symbol INCLUDE_FLIGHT_ANALYSIS_DATA), the
    SecondaryMissionController used should be a subclass of CansatFlightAnalyser, in order
    to calculate those data. The template use it as default value for parameter
    SECONDARY_CONTROLLER.



    Each Cansat project should use a subclass that overrides some of the methods listed below
    to add the features required for the secondary mission.
 */
#ifdef INCLUDE_FLIGHT_ANALYSIS_DATA
#include "CansatFlightAnalyser.h"
using DefaultSecondaryController = CansatFlightAnalyser<>;
#else
using DefaultSecondaryController = SecondaryMissionController<>;
#endif

template<class RECORD=CansatRecord,
		 class SECONDARY_CONTROLLER=DefaultSecondaryController,
		 class HW_SCANNER=CansatHW_Scanner>
class CansatAcquisitionProcess : public AcquisitionProcess<HW_SCANNER> {
  public:
    /** Constructor */
	CansatAcquisitionProcess();
    virtual ~CansatAcquisitionProcess();

    /** Initialize the whole process.
     *  If your subclass has parameters in this method, be sure to have
     *  default values for all parameters (taken from CansatConfig.h or
     *  from your project config file), in order for the main sketch
     *  to be able to call it without arguments.
     */
    virtual void begin();

#ifdef RF_ACTIVATE_API_MODE
    virtual CansatXBeeClient* getXBeeClient() {
    	return this->getHardwareScanner().getRF_XBee();
    }
#endif


    /** Obtain a reference to the SecondaryMissionController object..
     *  CansatAcquisitionProcess takes care of calling its begin() method at startup,
     *  and its run() method at the right time within each acquisition cycle.
     *  Each project needs to subclass SecondaryMissionController (or
     *  CansatFlightAnalyser) and provide it own secondary mission controller as
     *  parameter to this template (beware that the constructor and the begin() methods
     *  must have default value for all parameters.
     *  @return A reference to the instance of SecondaryMissionController().
     */
    virtual SECONDARY_CONTROLLER& getSecondaryMissionController() { return secondary;}

    /** Obtain a pointer to the SdFat object (fully initialized and ready to use) if any
     *  @return Pointer to the SdFat object, or null if none available.
     */
    VIRTUAL SdFat* getSdFat() { return storageMgr.getSdFat();};

    /** Obtain a reference to the StorageManager object (fully initialized and ready to use).
     *  @return Reference to the StorageManager object.
     */
    VIRTUAL StorageManager& getStorageManager(){ return storageMgr;};

    VIRTUAL bool isCampaignStarted() override { return  campaignStarted; } ;

    /** Force the start of the measurement campaign.
     *  @param msg A message to send to document the reason of the start campaign.
     *  @param value An optional numeric value to document the reason of the start campaign
     */
    VIRTUAL void startMeasurementCampaign(const char* msg="", float value=0.0f);

    /** Force the interruption of the measurement campaign.
     */
    VIRTUAL void stopMeasurementCampaign();

  protected:
    /** Define whether the measurement campaign must be started
     *  This implementation detects the rocket take-off based on parameters defined
     *  in CansatConfig.h:
     *	 - MinSpeedToStartCampaign
     *	 - AltitudeThresholdToStartCampaign
     *	 - NumSamplesToAverageForStartingCampaign
     *  Campaign is started one the take-off has been detected either based on speed
     *  averaged during the required number of samples, or based on the Altitude
     *  threshold.
     */
     VIRTUAL void updateCampaignStatus() override;

    /** Send the data record on the RF link. This method manages the transmission LED,
     *  and delegates the actual sending to transmitRecordToXBeeModule().
     */
     VIRTUAL void sendDataRecordToRF();

    /** Store a data record on SD-Card and possibly EEPROM
     *  @param campaignStarted true if the campaign is started, false
     *         if the can is in pre-flight conditions.
     */
    VIRTUAL void storeDataRecord(const bool campaignStarted) override ;
    
    /** @name Protected methods that can be overridden by projects-specific subclasses.
      *  @{   */

    /** Method called whenever the run() method is called. Implement to perform  
     *  housekeeping operations, if any. If overriding this method, be sure to
     *  call CansatAcquisitionProcess::doIdle().
     *  Warning: this method is called very often: be sure not to perform housekeeping tasks
     *  everytime, if not strictly required. 
     */
    VIRTUAL void doIdle() override ;

 	/** @} */  // End of group of methods to be overridden.

    /** Unconditionnally transmit the data record to the XBee module for transmission
     *  @param destSH The destination address (high word)
     *  @param destSL The destination address (low word).  If destSH=destSL=0,
     *  	          the default address configured in the XBeeClient is used
     *  	          (it's the ground station address)
     */
    VIRTUAL void transmitRecordToXBeeModule(uint32_t destSH=0, uint32_t destSL=0);

  private:
     /** @name Private methods to be overridden by projects-specific subclasses.
      *  @{   */

     /** Override this method in your subclass to perform whatever project-specific initialization is
      *  required.
      */
     virtual void initSpecificProject() override {};

     /** Populate the record with secondary mission data
      *  This should be implemented in a project specific subclass.
      *  The provided record can safely be casted (dynamically) to the actual class of the record, as
      *  provided in method getNewCansatRecord()
      *  This method should not alter the timestamp, GPS or primary mission data present in the record.
      *  @param theRecord The record to populate with secondary mission data. */
      VIRTUAL void acquireSecondaryMissionData(RECORD&  theRecord) {};
  	 /** @} */  // End of group of methods to be overridden.

     /** Initialize the thermistorClient with 1 to 3 specific thermistors. This implementation
      * initializes with thermistor1=ThermistorNTCLE100E3, thermistor2=ThermistorNTCLG100E2104JB,
      * thermistor3=ThermistorVMA320 but can be overridden by subclasses to use other thermistors.
      * The number of thermistors actually used is defined by symbols INCLUDE_THERMISTOR2 and
      * INCLUDE_THERMISTOR3.
      */
     virtual void initThermistorClient(ThermistorClient& thermClient);

     /** Perform all initialization tasks common to all Cansat Projects (primary mission, RF, storage,
      * transmission of diagnostic etc.)
      * This method should not be overridden in project subclasses.
      */
     virtual void initCansatProject();

    /** Populate the record with data from sensors. This method should not be overridden in subclasses */
    VIRTUAL void acquireDataRecord();

    /** Configure the various output pins used to connect the LEDs controlled by the AcquisitionProcess */
    NON_VIRTUAL void initLED_Hardware();

    bool campaignStarted ; 				/**< Current status of the measurement campaign */
    float lastRecordAltitude;  			/**< The altitude present in the previous record (-1 if none). */
    BMP_Client bmp;					    /**< BMP client object for pressure/altitude measurements */
#ifdef ARDUINO_ARCH_SAMD
	GPS_Client gps;						/**< GPS client for GPS-data collection. */
#endif
	ThermistorClient thermistor;		/**< Thermistor client object, for temperature measurements */
	StorageManager storageMgr;			/**< Storage manager object for access to SD-Card and EEPROM */
	float averageSpeed;					/**< average vertical speed, for detection of rocket take-off */
	RECORD record;						/**< The record we'll populate and transmit regularly */
	SECONDARY_CONTROLLER secondary;     /**< The secondary mission controller we delegate project-specific
											 processing to */
};

#include "CansatAcquisitionProcess.tpp"
