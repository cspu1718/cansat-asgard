/*
 * ProjectRT_CanCommander.h
 */

#pragma once
#include "RT_CanCommander.h"
#include "CansatXBeeClient.h"

/* Test command request. A real project should not define a new
 * type but complete the existing one in CansatInterface.h.
 */
enum ProjectCmdRequest {
	aProjectCommand=100
};

/** @ingroup CSPU_CansatAsgard
 *  @brief This is dummy subclass of RT_CanCommander used by various test programs.
 *  It is not used operationally and just answer CmdRequest 100 with CmdResponse 101.
 */
class ProjectRT_CanCommander : public RT_CanCommander<> {
public:
	ProjectRT_CanCommander(unsigned long int theTimeOut) : RT_CanCommander<>(theTimeOut) {};
protected:
	virtual bool processProjectCommand(CansatCmdRequestType requestType, char* /* cmd */) override {
		Serial << "ProjectRT_CanCommander::processProjectCommand: type = "<< (int) requestType  << ENDL;
		if (requestType == (CansatCmdRequestType) ProjectCmdRequest::aProjectCommand) {
			RF_OPEN_CMD_RESPONSE(RF_Stream);
			*RF_Stream << "101,response args here";
			RF_CLOSE_CMD_RESPONSE(RF_Stream);
			return true;
		} else return false;
	};
};
