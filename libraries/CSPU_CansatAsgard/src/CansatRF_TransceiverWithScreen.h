#pragma once

#include "CansatRF_Transceiver.h"
#include "SSD_1306_Client.h"
#include "CansatRF_TransceiverScreen.h"
#include "CansatXBeeClient.h"

#ifndef RF_ACTIVATE_API_MODE
#error CansatRF_TransceiverWithScreen only supports API MODE. Define RF_ACTIVATE_API_MODE
#endif

/** @inGroup CSPU_CansatAsgard
 *  @brief  A subclass of CansatRF_Transceiver which embeds a
 *  		CansatRF_TransceiverScreen (Feather Wing OLED), and
 *  		uses it to display various informations.
 *  		It also support buttons.
 */
template<class RECORD>
class CansatRF_TransceiverWithScreen : public CansatRF_Transceiver<RECORD> {

  public:
	/** Initialize the object
	 *  @return True if everything ok, false otherwise.
	 */
	virtual bool begin() override;
    
	/** Run the receiver. Call this method as often as possible
	 *  in the main loop
	 */
	virtual void run() override ;

  protected:
    /** Update the GPS position on the screen. Last received
     *  position is remembered.
     */
    void updateGPSDisplay(double latitude, double longitude) {
      screen.setGPS_Position(latitude, longitude);
    }
    /** Update status message. The last message received is
     *  stored and displayed indefinitely, except during a
     *  temporary message display.
     */
    void updateMessage(const char* msg) {
      screen.displayMessage(msg);
    }
    /** Display a temporary message. It overwrites the currently
     *  displayed message (even if it is temporary) for the
     *  required duration and then displays the last standard
     *  message again.
     *  @param msg The message to display.
     *  @param durationInSec The duration (in seconds) during
     *         which the message must be displayed.
     */
	 void displayTemporaryMessage(const char* msg, uint8_t durationInSec) {
	   screen.displayTemporaryMessage(msg, durationInSec);
	 }

	 /** This method is overriden to intercept incoming frames and update
	  *  display.
	  */
	 virtual bool receiveFrame(bool& receivedA_String, bool& receivedA_Record) override;


  private:
	 /** Handler for single click on button A. Default behavior is
	  *  to send InitiateCmdMode command.
	  *  Override to do anything else.
	  */
	 virtual void handleSingleClickButtonA();
	 /** Handler for single click on button B. Default behavior is to
	  *  do nothing.
	  *  Override to do anything else.
	  */
	 virtual void handleSingleClickButtonB();
	 /** Handler for single click on button C. Default behaviour is
	  *  to send StartBuzzer command.
	  *  Override to do anything else.
	  */
	 virtual void handleSingleClickButtonC();
	 /** Handler for double click on button A. Default behavior is
	  *  to send TerminateCmdMode command.
	  *  Override to do anything else.
	  */
	 virtual void handleDoubleClickButtonA();
	 /** Handler for double click on button B. Default behavior is to
	  *  do nothing.
	  *  Override to do anything else.
	  */
	 virtual void handleDoubleClickButtonB();
	 /** Handler for double click on button C. Default behaviour is
	  *  to do nothing.
	  *  Override to do anything else.
	  */
	 virtual void handleDoubleClickButtonC();

    CansatRF_TransceiverScreen screen; /**< The embedded screen */
    elapsedMillis elapsedForGPS; /**< Duration since last GPS position update on screen */
    bool GPS_PositionChanged = false; /**< True if the GPS position changed since it was last displayed. */
    double lastGPS_Latitude; /**< The last latitude received from the can */
    double lastGPS_Longitude;/**< The last longitude received from the can */
    
}; // class CansatRF_TransceiverWithScreen

#include "CansatRF_TransceiverWithScreen.tpp"
