/**
    Test program for SecondaryMissionController class
    It just tests basic functionalities. Integration with
    the CansatAcquisitionProcess can be tested by running
    the can software limited to primary mission and empty
    secondary mission i.e. by running  the MainWithRT_Commander.ino
    script from folder 0000-00_Templates.

*/

#include "CansatConfig.h"
#include "SecondaryMissionController.h"
#include "CansatRecord.h"

int counter = 0;

class TestController : public SecondaryMissionController<> {
  protected:
    virtual void manageSecondaryMission(CansatRecord& record) override {
      Serial << millis() << ": manageSecondaryMission() called" << ENDL;
      counter++;
    }
};

CansatRecord record;
TestController controller;
elapsedMillis elapsed;
constexpr int numPeriods = 10;

void setup() {
  DINIT(115200);
  controller.begin();
  auto duration = (numPeriods + 0.5) * CansatSecondaryMissionPeriod;
  if (duration == 0) {
    // if CansatSecondaryMissionPeriod = 0, which is a valid value,
    // test during numPeriods cycles.
    // SecondaryMission should be called at every cycle
    duration = (numPeriods + 0.5) * CansatAcquisitionPeriod;
  }
  Serial << "Testing the SecondaryMissionController for " << duration / 1000.0 << " seconds..." << ENDL;
  elapsed = 0;
  while (elapsed < duration ) {
    controller.run(record);
    delay(CansatAcquisitionPeriod);
  }
  Serial << "  CansatSecondaryMissionPeriod=" << CansatSecondaryMissionPeriod << " msec" << ENDL;
  Serial << "  CansatAcquisitionPeriod=" << CansatAcquisitionPeriod << " msec" << ENDL;
  Serial << "  Running during " << duration << " msed (i.e " << numPeriods + 0.5 << " periods)." << ENDL;
  Serial << "  manageSecondaryMission() was called " << counter << " times in " << duration / 1000.0 << " sec."  << ENDL;

  if (counter == (numPeriods + 1)) {
    Serial << "Test OK" << ENDL;
  }
  else {
    Serial << " Test NOT ok! Secondary should have been called " << numPeriods + 1 << " times. " << ENDL;
  }
  
  if (CansatSecondaryMissionPeriod == 0) {
    Serial << "Be sure to test with non-null value as well (edit in CansatConfig.h)" << ENDL;
  }
  else {
    Serial << "Be sure to test CansatSecondaryMissionPeriod=0 as well (edit in CansatConfig.h)" << ENDL;
  }
  Serial << "End of job" << ENDL;

  Serial.flush();
  exit(0);
}

void loop() {}
