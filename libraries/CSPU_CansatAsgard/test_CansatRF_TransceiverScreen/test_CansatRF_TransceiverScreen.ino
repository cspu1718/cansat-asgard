#include "CansatRF_TransceiverScreen.h"
#include "CSPU_Test.h"
#define DEBUG
#include "CSPU_Debug.h"

CansatRF_TransceiverScreen transceiverScreen;

void setup() {
  DINIT(115200);

  // Initialisation de l'écran OLED
  if (!transceiverScreen.begin("Test Screen")) {
    Serial.println("Erreur d'initialisation de l'écran OLED !");
    while (true)
      ;  // Bloquer en cas d'échec
  }

  Serial.println("Ecran initialisé avec succès !");

  Serial << "You should see " << ENDL;
  Serial << "  (i): project name 'Test Screen'" << ENDL;
  Serial << "  (ii): No message" << ENDL;
  Serial << "  (iii): No GPS" << ENDL;
  Serial << "  (iv): timestamp not updated" << ENDL;
  CSPU_Test::pressAnyKey();

  // Test truncation
  char prj[transceiverScreen.MaxProjectNameSize+5];
  char msg[transceiverScreen.MaxMsgSize+5];
  strncpy(prj,"ProjectNameWayTooLong12345!",transceiverScreen.MaxProjectNameSize+1);
  strncpy(msg,"MessageWayTooLongToFitTheScreen12345!",transceiverScreen.MaxMsgSize+1);
  transceiverScreen.setProjectName(prj);
  transceiverScreen.displayMessage(msg);
  Serial << "Are the project name and message properly truncated?" << ENDL; 
  CSPU_Test::pressAnyKey();

  prj[transceiverScreen.MaxProjectNameSize] = '\0';
  msg[transceiverScreen.MaxMsgSize]='\0';
  transceiverScreen.setProjectName(prj);
  transceiverScreen.displayMessage(msg);
  Serial << "Are the project name and message properly untruncated?" << ENDL; 
  Serial << "Expected: '" << prj << "' and '" << msg << "'" << ENDL; 
  CSPU_Test::pressAnyKey();



  // Initialisation de l'affichage
  transceiverScreen.setProjectName("ProjectName");
  transceiverScreen.displayMessage("Test started");
  transceiverScreen.setGPS_Position(1.2345, 2.3456);
  Serial << "You should see:" << ENDL;
  Serial << "  (i)   A project name" << ENDL;
  Serial << "  (ii)  A running clock" << ENDL;
  Serial << "  (iii) A GPS position changed every 4 sec" << ENDL;
  Serial << "  (iv)  A temporary message of 2 sec at every GPS change" << ENDL;
  Serial << "  (v)   A message when pressing buttons A, B, C" << ENDL;
  Serial << "  (vi)  Debounce should make it impossible to press the button more than once every 500 ms" << ENDL;
  


}

void loop() {
  // Simule un timestamp mis à jour
  static elapsedMillis elapsedMsg, elapsedGPS;
  static double latitude = 1.11111;
  static double longitude = -2.34567;
  static int counter = 0;
  //char timestampBuffer[16];
  //snprintf(timestampBuffer, sizeof(timestampBuffer), "00: % 02d: % 02d", seconds / 60, seconds % 60);

  // Simule un message reçu
  if (elapsedMsg > 10000) {  // Nouveau message toutes les 10 secondes
    counter++;
    char msg[20];
    sprintf(msg, "Message #%d", counter);
    transceiverScreen.displayMessage(msg);
    elapsedMsg = 0;
    Serial.print("Timestamp: ");
    Serial.print(millis());
    Serial << " | GPS: Lat:" << latitude << ", Long:" << longitude << ENDL;
  }

  // Simule une position GPS toutes les 4 secondes avec un message temporaire de 2 sec
  if (elapsedGPS > 4000) {
    latitude += 1.11111;
    longitude -= 2.33333;
    transceiverScreen.setGPS_Position(latitude, longitude);
    transceiverScreen.displayTemporaryMessage("!GPS 2s!", 2);
    elapsedGPS = 0;
  }
  transceiverScreen.run();
  int clicks= 0;
  if (clicks = transceiverScreen.readButton(transceiverScreen.ButtonId::A)) {
    if (clicks == 1) transceiverScreen.displayTemporaryMessage("single click A",1); 
    else transceiverScreen.displayTemporaryMessage("double click A",1); 
    Serial << "Button A pressed at " << millis() << ENDL;
  }
  if (clicks = transceiverScreen.readButton(transceiverScreen.ButtonId::B)) {
    if (clicks == 1) transceiverScreen.displayTemporaryMessage("single click B",1); 
    else transceiverScreen.displayTemporaryMessage("double click B",1); 
    Serial << "Button A pressed at " << millis() << ENDL;
  }
  if (clicks = transceiverScreen.readButton(transceiverScreen.ButtonId::C)) {
    if (clicks == 1) transceiverScreen.displayTemporaryMessage("single click C",1); 
    else transceiverScreen.displayTemporaryMessage("double click C",1); 
    Serial << "Button A pressed at " << millis() << ENDL;
  }
}
