/*
   test_AcquisitionProcess

   A test sketch for the AcquisitionProcess class
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "AcquisitionProcess.h"

constexpr unsigned int AcqPeriod=3000; // msec.

class TestAcquisitionProcess : public AcquisitionProcess<> {
public:
    TestAcquisitionProcess() : AcquisitionProcess(AcqPeriod) {} ;
    
    virtual void storeDataRecord(const bool /* campaignStarted */) {
      Serial << millis() << ": storeData() called" << ENDL;
    }

    virtual void doIdle() {
      static elapsedMillis duration;
      if (duration > 5000) {
        duration = 0;
        Serial << millis() << ": performing idle tasks" << ENDL; 
        };
      }
  virtual void acquireDataRecord() {
          Serial << millis() << ": acquireData() called" << ENDL;
    };
};

TestAcquisitionProcess process;

void setup() {
  DINIT(115200);

  Serial << "Test of AcquisitionProcess" << ENDL;
  Serial << "** Initializing..." << ENDL;
  process.begin();
  Serial << "** Running Check that: " << ENDL;
  Serial << "   1. The Heartbeat blinks as expected (built-in LED)" << ENDL;
  Serial << "   2. Every " << AcqPeriod << " msec, there is an acquisition (msg+LED disturbed)" << ENDL;
  Serial << "   3. Every 5 sec, idle activities are triggered" << ENDL;
  
}

void loop() {
  process.run();
  delay(100); // Do not use 100% CPU: problems with USB!
}
