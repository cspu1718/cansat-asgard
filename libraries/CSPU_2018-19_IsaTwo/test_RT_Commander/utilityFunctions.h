void requestVisualCheck(const char* msg, bool separatorFirst=true);
void requestVisualCheckWithCmd(String &theCmd, const char* msg);
/* Build a request string with 0, 1 or 2 parameters (parameters are ignored if they are empty strings).  */
void buildRequest(String &theCmd, IsaTwoCmdRequestType reqType, const char* param1="", const char* param2="");
