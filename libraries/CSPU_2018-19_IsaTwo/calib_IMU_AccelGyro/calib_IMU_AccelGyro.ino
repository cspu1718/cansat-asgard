/*
   calib_IMU_AccelGyro.ino

   The calibration program for the IMU's accelerometers and gyroscopes.
   This program calibration Adafruit's LMS9DS0 or PrecisionNXP IMU (select with
   symbole USE_PRECISION_NXP below.

   Wiring: VCC, GND, SDA (=A4 on Uno), SCL (=A5 on Uno)
*/

//#define USE_PRECISION_NXP  // If defined, the PrecisionNXP IMU is used, otherwise the LMS9DS0 is used.

#define USE_ASSERTIONS
#include "DebugCSPU.h"
#include "IsaTwoRecord.h"
#ifdef USE_PRECISION_NXP
#  include "IMU_ClientPrecisionNXP.h"
#else
#  include "IMU_ClientLSM9DS0.h"
#endif
#include "Wire.h"
#include "elapsedMillis.h"

#define PRINT_ALL_IMU_DATA // define to print the content of each record read from IMU
#define PRINT_DELTA        // define to print the value of the delta when evaluating stability
#define DEBUG_COUNTERS
#define DEMONSTRATE_RESULTS // define to run the IMU and calibrate the values in order to check their validity

#ifdef USE_PRECISION_NXP
IMU_ClientPrecisionNXP imu;
#else
IMU_ClientLSM9DS0 imu;
#endif

IsaTwoRecord record;

typedef enum {
  WaitForStability,
  Averaging,
} State_t;

constexpr int durationForAveragingInSec = 5;
constexpr int RequiredNumStableValues = 5;  // The number of samples to consider to assess stability
constexpr int toleranceAccel = 500;   // The acceptable variation for a stable situation
constexpr int toleranceGyro = 100;
constexpr unsigned long DelayBetweenSamples = 200; // The delay between 2 readings, in msec.
constexpr float gravity = 9.81 ;
constexpr int NumSamples = (durationForAveragingInSec * 1000) / DelayBetweenSamples; // The number of samples to consider for averaging values

long int sumGyro = 0;
long int sumAccel = 0;
int sampleCounter = 0;
float accelAverage ;
float gyroAverage ;

float MeasureAccelUp[3], MeasureAccelDown[3], MeasureGyroUp[3], MeasureGyroDown[3];

int currentMeasurementPosition = -1; /* 0 to  5 :X up ,Xdown,Y up,Y down,Z up,Z down...*/
int stabilityCounter = 0;
int16_t previousAccel, previousGyro ;

float accelZero[3], accelResolution[3], gyroZero[3], gyroResolution[3];
State_t state;
int directionIndex; // The index in the above arrays (updated together with the currentMeasurementPosition)

void waitForReturn () {
  while (Serial.available()) Serial.read();
  Serial << F("Press return...") ;
  while (Serial.available() == 0) {
    delay(100);
  };
  Serial << F("OK") << ENDL;
}

void printArray(const float arr[3])
{
  Serial << arr[0] << ", " << arr[1] << ", " << arr[2];
}

void printAllResults() {
  Serial << "Collected readings:" << ENDL;
  Serial << "  Accel, UP  : "; printArray(MeasureAccelUp); Serial << ENDL;
  Serial << "  Accel, DOWN: "; printArray(MeasureAccelDown); Serial << ENDL;
  Serial << "  Gyro, UP   : "; printArray(MeasureGyroUp); Serial << ENDL;
  Serial << "  Gyro, DOWN : "; printArray(MeasureGyroDown); Serial << ENDL;

  for (auto i = 0; i < 3; i++) {
    accelZero[i]      = (MeasureAccelUp[i] + MeasureAccelDown[i]) / 2.0;
    // Remember gravity is interpreted as UPWARD acceleration
    accelResolution[i] = (2.0 * gravity) / (MeasureAccelUp[i] - MeasureAccelDown[i]);
    gyroZero[i]       = (MeasureGyroUp[i] + MeasureGyroDown[i]) / 2.0;
#ifdef USE_PRECISION_NXP
    gyroResolution[i] = GYRO_SENSITIVITY_250DPS;
#else
    gyroResolution[i] = LSM9DS0_GYRO_DPS_DIGIT_245DPS;
#endif
  }
  
  Serial << ENDL << F("Final results:please write those values in the calibration file for use by the RT-Processing ") << ENDL;
  Serial << "(use this if calibration is performed by the RT-Processing, and not on-board" << ENDL; 

  Serial << F("#AccelZeroG_X, AccelZeroG_Y, AccelZeroG_Z") << ENDL;

  Serial.print ( accelZero[0], 8);  Serial << ",";
  Serial.print ( accelZero[1], 8);  Serial << ",";
  Serial.println( accelZero[2], 8);

  Serial << F("#AccelResolution_X, AccelResolution_Y, AccelResolution_Z") << ENDL;
  Serial.print(accelResolution[0], 8); Serial << ",";
  Serial.print(accelResolution[1], 8); Serial << ",";
  Serial.println(accelResolution[2], 8);

  Serial << F("#GyroZeroRate_X, GyroZeroRate_Y, GyroZeroRate_Z = ") << ENDL;
  Serial.print( gyroZero[0], 8); Serial << ",";
  Serial.print( gyroZero[1], 8); Serial << ",";
  Serial.println( gyroZero[2], 8);

  Serial << F("#GyroResolution_X, GyroResolution_Y, GyroResolution_Z") << ENDL;
  Serial.print (gyroResolution[0], 8); Serial << ",";
  Serial.print (gyroResolution[1], 8); Serial << ",";
  Serial.println(gyroResolution[2], 8);

  Serial << ENDL << "Final result in C++ format, for use in the IMU_CalibrationData.h source file:" << ENDL;
  Serial << "(use this if calibration is performed on-board" << ENDL; 

  Serial << "const struct IMU_Calib { " << ENDL;
  Serial << "  float accelZeroG[3] =  { ";
  Serial.print(accelZero[0],8); Serial << "f,";
  Serial.print(accelZero[1],8); Serial << "f,";
  Serial.print(accelZero[2],8); Serial << "f};" << ENDL;
  Serial << "  float accelResolution[3] =  { ";
  Serial.print(accelResolution[0],8); Serial << "f,";
  Serial.print(accelResolution[1],8); Serial << "f,";
  Serial.print(accelResolution[2],8); Serial << "f};" << ENDL;
  Serial << "  float gyroZeroRate[3] =  { ";
  Serial.print(gyroZero[0],8); Serial << "f,";
  Serial.print(gyroZero[1],8); Serial << "f,";
  Serial.print(gyroZero[2],8); Serial << "f};" << ENDL;
  Serial << "  float gyroResolution[3] =  { ";
  Serial.print(gyroResolution[0],8); Serial << "f,";
  Serial.print(gyroResolution[1],8); Serial << "f,";
  Serial.print(gyroResolution[2],8); Serial << "f};" << ENDL;
  Serial << " // Insert here the magnetometers calibration data: " << ENDL;
  Serial << " // float magOffset[3] and float magTransformationMatrix[3][3]" << ENDL;
  Serial << "};" << ENDL;
}


bool requestNextCubePosition() {
  assert(currentMeasurementPosition<6); // N: keep at least 1 assertion in the file otherwise
                                        // otherwise the linker will not symbol fail()
                                        // (this is because libDebugCSPU.a comes first in the
                                        // linker command; how to change that in Arduino IDE ??)
  currentMeasurementPosition++;
  // update direction index
  directionIndex = currentMeasurementPosition / 2; // 0 =X, 1=Y, 2=Z

  bool done = false;
  switch (currentMeasurementPosition) {
    case 0:
      Serial << F("Put the cube in position, and press Enter: X up ") << ENDL;
      waitForReturn();
      break;
    case 1:
      Serial << F("put the cube in position,and press Enter: X down ") << ENDL;
      waitForReturn();
      break;
    case 2:
      Serial << F("Put the cube in position,and press Enter: Y up ") << ENDL;
      waitForReturn();
      break;
    case 3:
      Serial << F("Put the cube in position,and press Enter: Y down ") << ENDL;
      waitForReturn();
      break;
    case 4:
      Serial << F("Put the cube in position,and press Enter: Z up ") << ENDL;
      waitForReturn();
      break;
    case 5:
      Serial << F("Put the cube in position,and press Enter: Z down ") << ENDL;
      waitForReturn();
      break;
    case 6:
      Serial.println();
      Serial.println();
      done = true;
      break;
    default:
      Serial << F("error: unexpected state value") << ENDL;
      while (1) ;
  }
  return done;
}

void demonstrateResults()
{
  Serial << F("Press any key to demonstrate the effect of the calibration parameters.") << ENDL;
  Serial << F("Make sure you save the values first!") << ENDL;
  waitForReturn();

  while (1) {
    imu.readData(record);
    Serial << "Accel (m/s^2): " ;
    for (auto i = 0; i < 3; i++) {
      Serial << (record.accelRaw[i] - accelZero[i])*accelResolution[i] << ", ";
    }
    Serial << "   Gyro (dps): " ;
    for (auto i = 0; i < 3; i++) {
      Serial << (record.gyroRaw[i] - gyroZero[i])*gyroResolution[i] << ", ";
    }
    Serial << ENDL;
    delay(1000);
  }
}

bool processAverage() {
  bool done = false;
  sumAccel += record.accelRaw[currentMeasurementPosition / 2];
  sumGyro += record.gyroRaw[currentMeasurementPosition / 2];
  sampleCounter++;
#ifdef DEBUG_COUNTERS
  Serial << F("sampleCounter=") << sampleCounter << ENDL;
#endif
  if (sampleCounter >= NumSamples) {
    done = true;
    Serial << F(" Averaging done :  ");
    accelAverage = sumAccel / (float) NumSamples ;
    Serial << F("     accelAverage=") << accelAverage ;
    gyroAverage = sumGyro / (float) NumSamples;
    Serial << F("     gyroAverage=") << gyroAverage << ENDL  ;
    // Store result:
    if ((currentMeasurementPosition % 2) == 0 ) {
      // Direction is up
      MeasureAccelUp[directionIndex] = accelAverage;
      MeasureGyroUp[directionIndex] = gyroAverage ;
    }
    else {
      Serial << "TMP: storing in direction DOWN, index=" << directionIndex << ENDL;
      MeasureAccelDown[directionIndex] = accelAverage;
      MeasureGyroDown[directionIndex] = gyroAverage ;

    }
  }
  return done;
}

void setup() {
  DINIT(115200);
  Wire.begin();
#ifdef USE_PRECISION_NXP
  Serial << F("Calibrating Precision NXP 9-DOF IMU...") << ENDL;
#else
  Serial << F("Calibrating LSM9DS0 IMU...") << ENDL;
#endif
  if (!imu.begin())
  {
    Serial << F("Ooops, no IMU detected ... Check your wiring!") << ENDL;
    while (1);
  }
  else {
    Serial << F("IMU detected, wiring OK, proceed.") << ENDL;
  }
  Serial << "Sampling every " << DelayBetweenSamples << " msec." << ENDL;
  Serial << "Number of stable samples before averaging: " << RequiredNumStableValues << ENDL;
  Serial << "Averaging on " << NumSamples << " samples" << ENDL;

  state = State_t::WaitForStability;
  previousAccel = previousGyro = 0;
  stabilityCounter = 0;
  requestNextCubePosition();
}

void loop() {
  int16_t deltaAccel, deltaGyro;
  bool done;
  record.clear();
  imu.readData(record);
#ifdef PRINT_ALL_IMU_DATA
  Serial << "--------- record ----------" << ENDL;
  record.print(Serial, record.DataSelector::IMU);
  Serial << ENDL;
#endif

  switch (state) {

    case State_t::WaitForStability:
      deltaAccel = fabs( record.accelRaw[directionIndex] - previousAccel);
      deltaGyro = fabs( record.gyroRaw[directionIndex] - previousGyro);
#ifdef PRINT_DELTA
      Serial << "DeltaAccel=" << deltaAccel << ENDL;
      Serial << F("DeltaGyro=") <<  deltaGyro << ENDL;
#endif
      if ((deltaAccel < toleranceAccel) && (deltaGyro < toleranceGyro)) {
        stabilityCounter++;
      }
      else {
        stabilityCounter = 0;
        Serial << 'R';
      }
#ifdef DEBUG_COUNTERS
      Serial << F("stabilityCounter=") << stabilityCounter << ENDL;
#else
      Serial << '.';
#endif

      previousAccel = record.accelRaw[directionIndex];
      previousGyro  = record.gyroRaw[directionIndex];

      if (stabilityCounter >= RequiredNumStableValues) {
        state = Averaging;
        sumAccel = sumGyro = 0;
        sampleCounter = 0;
        Serial << F(" Stable! ") << ENDL;
      }
      break;

    case State_t::Averaging:
      done = processAverage();
      if (done) {
        done = requestNextCubePosition();
        if (!done) {
          state = WaitForStability;
          previousAccel = previousGyro = 0;
          stabilityCounter = 0;
        }
        else {
          printAllResults();
#ifdef DEMONSTRATE_RESULTS
          demonstrateResults();
#endif
          Serial << F("End of job") << ENDL;
          while (1) delay(100);
        }
      }
      Serial << F(".");
      break;

    default:
      Serial << F("error");
  }
  delay(DelayBetweenSamples);

}
