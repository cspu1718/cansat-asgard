/*
   CO2_Client.h
*/
#pragma once
#include "IsaTwoRecord.h"

/**@ingroup IsaTwoCSPU
   Class used to read the data from the CO2 sensor
*/

class CO2_Client {

  public:
	/** @brief Constructor of the class that transfers parameter's value to the private variable "analogInPinNumber"
     * @param theAnalogInPinNumber The pin from which the CO2 sensor is read.
     */
	CO2_Client(byte theAnalogInPinNumber);

    /** @brief Transfers the function's parameters to the private variables float referenceVoltage and int DAC_StepNumber.
      * @param referenceVoltage the reference voltage of the currently used board
      * @param defaultDAC_StepNumber the default number of steps the currently used board's DAC has
      * @return true when the initialisation was successful, false in case of error.
      */
    bool begin(float referenceVoltage, int defaultDAC_StepNumber);

    /** @brief Reads the analog value read from the pin the CO2_sensor is plugged in and converts that value to Volts
      * @param record The record the value (in volts) will be stored in
      */
    void readData(IsaTwoRecord& record);


  private:
    byte analogInPinNumber; /**<The pin number the CO2 sensor is plugged in */
    float referenceVoltage; /**<The reference voltage of the currently used board */
    int DAC_StepNumber;     /** <The number of ADC steps */
};
