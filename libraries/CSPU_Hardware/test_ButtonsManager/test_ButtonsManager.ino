/*
   Test program for class ButtonsManager

   Wiring: buttons between pins B*_PIN and GND
   WARNING: On the Feather pullups are required for ANALOG pins:
            the internal pull-up is too slow on analog pins.     
*/
// Silence warnings about unused parameters
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop
#include "ButtonsManager.h"
#include "elapsedMillis.h"

constexpr uint8_t B1_PIN = A0;
constexpr uint8_t B2_PIN = A1;
constexpr uint8_t B3_PIN = A2;
constexpr uint8_t B0_PIN = 10;
constexpr uint16_t RepeatDelay = 1000;

const char* TestSparator = "-----------------------------------";

uint32_t cmdCounter = 0;
uint32_t lastCmdExecution = 0; 
uint32_t delaySinceLastCmd = 0;

class TestCommand : public PrintButtonCommand {
  public:
    TestCommand(char cmdName) : PrintButtonCommand(cmdName) {};
    virtual void run() override {
      PrintButtonCommand::run();
      cmdCounter++;
      delaySinceLastCmd = millis() - lastCmdExecution;
      lastCmdExecution = millis();
    };
};

TestCommand cmdA('A');
TestCommand cmdB('B');
TestCommand cmdC('C');
TestCommand cmdD('D');

class TestButtonsMgr : public ButtonsManager<PrintButtonCommand, 4> {
  public:
    virtual ~TestButtonsMgr() {} ;
    virtual bool begin(uint16_t minDelayBetweenRepeats = RepeatDelay, bool allowAutoRepeat = false) {
      if (!this->ButtonsManager::begin(minDelayBetweenRepeats, allowAutoRepeat)) return false;
      configureButton(0, B0_PIN);
      configureButton(1, B1_PIN);
      configureButton(2, B2_PIN);
      configureButton(3, B3_PIN);
      configureCommand(1, &cmdA, &cmdD);
      configureCommand(2, &cmdB, nullptr);
      configureCommand(3, &cmdC, nullptr);
      return true;
    };
};

TestButtonsMgr buttons;

bool testCmdExecution(char cmdName, uint8_t buttonID, bool shift) {
  bool done = false;
  bool result = false;
  elapsedMillis elapsed = 0, LED_Elapsed = 0;
  if (shift) {
    Serial << "Please push 'shift' button (#0, pin " << buttons.getButtonPin(0)
           << ") together with button #" << buttonID << ", pin"
           << buttons.getButtonPin(buttonID) << "..." <<  ENDL;
  } else {
    Serial << "Please push button #" << buttonID << ", pin"
           << buttons.getButtonPin(buttonID) << "..." << ENDL;
  }
  while (!done) {
    if (LED_Elapsed > 1000) {
      digitalWrite(LED_BUILTIN, digitalRead(LED_BUILTIN) ? LOW : HIGH);
      LED_Elapsed=0;
    }
    PrintButtonCommand *cmd = buttons.getCommandToRun();
    if (cmd) {
      cmd->run();
      Serial << "  Got cmd '" << cmd->name << "' (expected '" << cmdName << "')" << ENDL;
      Serial << "  Detected button #" << cmd->buttonID << " (expected #" << buttonID << "')" << ENDL;
      Serial << "  Shift modifier = " << (cmd->shift ? "True" : "False")  << " (expected '" << (shift ? "True" : "False") << "')" << ENDL;
      result = ( (cmdName == cmd->name) &&
                 (buttonID == cmd->buttonID) &&
                 (shift == cmd->shift) );
      if (result) {
        Serial << "OK" << ENDL;
      } else {
        Serial << "*** ERROR, try again ";
        if (shift) Serial << "shift-" ;
        Serial << buttonID << "..." << ENDL;
      }
      done = result;
    }
    if (elapsed > 30000) {
      Serial << "*** Time-out, no button detected" << ENDL;
      done = true;
    }
    delay(200);
  }
  return result;
}
void testConfigurationErrors() {
  Serial << TestSparator << ENDL;
  Serial << "Testing configuration functions... (some error messages are NORMAL)." << ENDL;
  ButtonsManager<ButtonCommand, 4> mgr;
  mgr.begin();
  // mgr.configureButton(10, 13); // This causes an assert to fail and the program to exit.
  // Configure button without pin assignement KO
  DASSERT(!mgr.configureCommand(2, &cmdA));
  // Assign pin and check again
  mgr.configureButton(2, 12);
  DASSERT(mgr.configureCommand(2, &cmdA));
  // Configure all buttons.
  mgr.configureButton(0, 10);
  mgr.configureButton(1, 11);
  mgr.configureButton(3, 13);

  // Shift-0 command (not allowed)
  DASSERT(!mgr.configureCommand(0, nullptr, &cmdA));
  // 0 command : OK
  DASSERT(mgr.configureCommand(0, &cmdA));
  // command on other: OK
  DASSERT(mgr.configureCommand(2, &cmdA));
  // Shift command on other: KO since 0 is used
  DASSERT(!mgr.configureCommand(2, nullptr, &cmdA));
  // Cancel 0 config
  DASSERT(mgr.configureCommand(0, nullptr));
  // Shift command on other: OK since 0 is not used
  DASSERT(mgr.configureCommand(2, nullptr, &cmdA));
  Serial << "Configuration test OK" << ENDL;
}

void testExecutionOfCommands() {
  Serial << TestSparator << ENDL;
  Serial << "Testing execution of commands... " << ENDL;
  Serial << "ButtonsManager configurations:" << ENDL;
  buttons.printConfiguration(Serial);
  DASSERT(testCmdExecution('A', 1, false));
  DASSERT(testCmdExecution('B', 2, false));
  DASSERT(testCmdExecution('B', 2, true));
  DASSERT(testCmdExecution('C', 3, false));
  DASSERT(testCmdExecution('C', 3, true));
  DASSERT(testCmdExecution('D', 1, true));

  // Remove shift config and test again
  buttons.configureCommand(1, &cmdA, nullptr);
  buttons.configureCommand(0, &cmdD, nullptr);

  DASSERT(testCmdExecution('A', 1, false));
  DASSERT(testCmdExecution('B', 2, false));
  DASSERT(testCmdExecution('C', 3, false));
  DASSERT(testCmdExecution('D', 0, false));

}

/* Check the number of executions of command during 10 seconds,
   from next execution.
*/
bool checkRepetitions(uint32_t duration) {
  cmdCounter = 0;
  while (cmdCounter == 0) {
    buttons.run();
    delay(50);
  }
  cmdCounter = 0;
  Serial << "   Starting..." << ENDL;
  elapsedMillis elapsed;
  uint32_t minDelay = 99999999;
  while (elapsed < duration) {
    auto cmd = buttons.getCommandToRun();
    if (cmd) {
      cmd->run();
      Serial << "cmd '" << cmd->name << "': delay since last:" << delaySinceLastCmd << "msec" << ENDL;
      minDelay = min (minDelay, delaySinceLastCmd);
    }
  }
  Serial << "   Done. Nbr of executions: " << cmdCounter << " in "
         << duration / 10.0 << "sec." << ENDL;
  Serial << "         Min delay: " << minDelay << " msec" << ENDL;
  Serial << "         Min. expected delay: " << RepeatDelay << "msec)." << ENDL;
  if ( minDelay  < RepeatDelay) {
    Serial << "*** ERROR: Repetitions happen too fast." << ENDL;
    return false;
  } else {
    Serial << "OK" << ENDL;
    return true;
  }
}

void testRepetitions() {
  Serial << TestSparator << ENDL;
  Serial << "Testing repetition of commands... " << ENDL;
  Serial << "A: Without autorepeat enabled." << ENDL;
  Serial << "     Push button 1 faster than 1 time per second during 10 seconds" << ENDL;
  DASSERT(checkRepetitions(10000));
  buttons.begin(RepeatDelay, true);
  Serial << "     Keep button #1 pushed during 10 seconds" << ENDL;
  DASSERT(checkRepetitions(10000));
}

void testSingleAnalogButton() {
  ButtonsManager<PrintButtonCommand, 4> btn;
  btn.begin();
  btn.configureButton(0, 10);
  btn.configureCommand(0, &cmdA);
  btn.configureButton(1, A0);
  btn.configureCommand(1, &cmdB);
   btn.configureButton(2, A1);
  btn.configureCommand(2, &cmdC);
   btn.configureButton(3, A2);
  btn.configureCommand(3, &cmdD);
  btn.printConfiguration(Serial);
  elapsedMillis elapsed;
  Serial << "Test button on A0 (" << A0 << ") only , for 10 secs." << ENDL;
  while (elapsed < 10000) {
    btn.run();
    delay(100);
  }
  Serial << "Done" << ENDL;
}
void setup() {
  DINIT(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  Serial << "Testing class ButtonsManager..." << ENDL << ENDL;
  // testSingleAnalogButton(); // This test is redundant with next tests.
  testConfigurationErrors();
  buttons.begin();
  testExecutionOfCommands();
  testRepetitions(); 
  Serial << ENDL<< ENDL<<"Test over." << ENDL;
  exit(1);
}

void loop() {

}
