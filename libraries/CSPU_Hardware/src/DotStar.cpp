/*
 * DotStar.cpp
 */

#include "DotStar.h"
#include "DebugCSPU.h"

constexpr uint8_t delayAfterBit=1;

void DotStar::sendBit(uint8_t dataPin, uint8_t clockPin, bool high, uint16_t numRepeats)
{
   digitalWrite(clockPin, LOW);
   digitalWrite(dataPin, high ? HIGH : LOW);
   delay(delayAfterBit);
   for (int i = 0; i < numRepeats; i++) {
	   // Data is valid on the rising edge of the clock.
       digitalWrite(clockPin, HIGH);
       delay(delayAfterBit);
       digitalWrite(clockPin, LOW);
       delay(delayAfterBit);
   }
}

void DotStar::shutdownOnBoard() {
#ifdef ARDUINO_ITSYBITSY_M4
	constexpr uint8_t DataPin=8;
	constexpr uint8_t ClockPin=6;

	pinMode(DataPin, OUTPUT);
	pinMode(ClockPin, OUTPUT);
	// We must transfer 00 00 00 00 FF 00 00 00 FF  to shutdown the 1 pixel.
	sendBit(DataPin, ClockPin, 0, 32); // start frame
	sendBit(DataPin, ClockPin, 1, 8);  // start LED
	sendBit(DataPin, ClockPin, 0, 24); // RBG
	sendBit(DataPin, ClockPin, 1, 8);  // End frame.
#elif defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS) || defined(ADAFRUIT_FEATHER_M0)
	// Noop
#elif defined(ARDUINO_AVR_UNO)
	// Noop
#else
	Serial << "*** Error: DotStar::shutdownOnBoard() called on unsupported board" << ENDL;
#endif
}
