/*
 * ButtonsManager.tpp    Implementation of template ButtonsManager
 */

#pragma once
#include "ButtonsManager.h"
#include "ButtonCommand.h"
#include <type_traits>


#ifndef DBG_DIAGNOSTIC
#define DBG_DIAGNOSTIC 1
#endif
#define DBG_RUN 0  // Activate detailed debugging in method run() (causes continous output).


template<class CMD_CLASS, unsigned int NUM_BUTTONS>
ButtonsManager<CMD_CLASS, NUM_BUTTONS>::ButtonsManager() :
autoRepeat(false), repeatDelay(500),
elapsedSinceLastPush(0), releasedSinceLastPush(true) {
	static_assert(NUM_BUTTONS > 0 && NUM_BUTTONS < 100,
			"NUM_BUTTONS must be between 1 and 99 (inclusive)");
	static_assert(std::is_base_of<ButtonCommand,CMD_CLASS>::value,
			"CMD_CLASS must be derived from ButtonCommand");
	this->clearConfiguration();
}

template<class CMD_CLASS, unsigned int NUM_BUTTONS>
bool ButtonsManager<CMD_CLASS, NUM_BUTTONS>::begin(uint16_t minDelayBetweenRepeats, bool allowAutoRepeat) {
	this->autoRepeat=allowAutoRepeat;
	this->repeatDelay=minDelayBetweenRepeats;
	clearConfiguration();
	return true;
};

template<class CMD_CLASS, unsigned int NUM_BUTTONS>
void ButtonsManager<CMD_CLASS, NUM_BUTTONS>::configureButton(uint8_t buttonID, uint8_t pinNbr) {
	DASSERT(buttonID < NUM_BUTTONS);
	// Check pin not in use for another button, in not "no pin"
	if (pinNbr) {
		for (int i = 0; i<NUM_BUTTONS; i++) {
			if (i == buttonID) continue;
			if (this->buttonConfigs[i] == pinNbr){
				Serial << "*** Error: pin "<< pinNbr << " already in use for button #" << i << ENDL;
				return;
			}
		} // for
	} // if pinNbr
	this->buttonConfigs[buttonID] = pinNbr;
	pinMode(pinNbr, INPUT_PULLUP);
};

template<class CMD_CLASS, unsigned int NUM_BUTTONS>
bool ButtonsManager<CMD_CLASS, NUM_BUTTONS>::configureCommand(uint8_t buttonID, CMD_CLASS* mainCommand, CMD_CLASS* shiftCommand) {
	if (!checkButtonConfig(buttonID)) return false;
	if (shiftCommand != nullptr) {
		if (buttonID==0)  {
			DPRINTSLN(DBG_DIAGNOSTIC, "*** Error cannot assign shift-command to button #0");
			return false;
		} else {
			if (this->cmdConfigs[0].mainCommand != nullptr) {
				// Shift button is used as regular button, not shift button.
				DPRINTSLN(DBG_DIAGNOSTIC, "*** Error cannot assign shift-command when button #0 has an associated command.");
				return false;
			}
		}
	}
	// Configuration is valid.
	this->cmdConfigs[buttonID].mainCommand = mainCommand;
	this->cmdConfigs[buttonID].shiftCommand = shiftCommand;
	return true;
};

template<class CMD_CLASS, unsigned int NUM_BUTTONS>
void ButtonsManager<CMD_CLASS, NUM_BUTTONS>::clearConfiguration() {
	for (int i = 0; i<NUM_BUTTONS; i++) {
		buttonConfigs[i]=0;
		this->cmdConfigs[i].mainCommand = nullptr;
		this->cmdConfigs[i].shiftCommand = nullptr;
	}
}

template<class CMD_CLASS, unsigned int NUM_BUTTONS>
inline void ButtonsManager<CMD_CLASS, NUM_BUTTONS>::run() {
	CMD_CLASS* cmd = this->getCommandToRun();
	if (cmd) {
		cmd->run();
	}
};

template<class CMD_CLASS, unsigned int NUM_BUTTONS>
CMD_CLASS* ButtonsManager<CMD_CLASS, NUM_BUTTONS>::getCommandToRun() {
	int8_t pushedButton=-1;
	// Be carefull not to read unconfigured pins.!
	bool shiftPushed = (this->buttonConfigs[0]) && (!digitalRead(this->buttonConfigs[0]));
	CMD_CLASS* cmd = nullptr;
	for (int i = 1; i< NUM_BUTTONS; i++) {
		if ((this->buttonConfigs[i]) && (!digitalRead(this->buttonConfigs[i]))) {
			pushedButton = i;
			break;
		}
	}
	if (pushedButton == -1) {
		if (!shiftPushed) {
			// No button pushed whatsoever
			this->releasedSinceLastPush = true;
		} else {
			DPRINTSLN(DBG_RUN, "Only shift button pushed")
						// Only shift button pushed: execute command
						// if any
						if (this->cmdConfigs[0].mainCommand) {
							if ((this->elapsedSinceLastPush > this->repeatDelay) &&
									(this->releasedSinceLastPush || this->autoRepeat))
							{
								DPRINTSLN(DBG_RUN, "Executing command for Button #0");
								cmd = this->cmdConfigs[0].mainCommand;
								cmd->buttonID =0;
								cmd->shift = false;
								this->releasedSinceLastPush = false;
								this->elapsedSinceLastPush = 0;
							}
						}
						else {
							// Only shift pushed without a command configured on shift:
							// This is the same as all released.
							this->releasedSinceLastPush = true;
						}
		}
	} else {
#if (DBG_RUN == 1)
		Serial << "pushedButton = " << pushedButton << ", releasedSinceLastPush:" << (int) this->releasedSinceLastPush <<ENDL;
		Serial << "shift pin = " << this->buttonConfigs[0] << " Currently pushed: " << (int)shiftPushed << ENDL;
#endif
		// Another button pushed than #0 (shift)
		CommandConfig cfg = this->cmdConfigs[pushedButton];
		CMD_CLASS* tmpCmd;
		if (shiftPushed) {
			tmpCmd = cfg.shiftCommand;
			// If no shift cmd configured, run main even with shift.
			if (!tmpCmd) tmpCmd = cfg.mainCommand;
		}
		else tmpCmd =  cfg.mainCommand;
		if ( (tmpCmd) && (this->elapsedSinceLastPush > this->repeatDelay) &&
				((this->releasedSinceLastPush || this->autoRepeat))
		)
		{
#if (DBG_RUN == 1)
			if (shiftPushed) Serial << "Shift-";
			Serial << "Button #" << pushedButton << " pushed." << ENDL;
#endif
			this->releasedSinceLastPush = false;
			this->elapsedSinceLastPush = 0;
			cmd=tmpCmd;
			cmd->buttonID=pushedButton;
			cmd->shift=shiftPushed;
		}
	}
	return cmd;
};

#if (DBG == 1)
template<class CMD_CLASS, unsigned int NUM_BUTTONS>
void ButtonsManager<CMD_CLASS, NUM_BUTTONS>::printConfiguration(Stream& s) {
	s << "Buttons configuration:" << ENDL;
	for (int i =0;i< NUM_BUTTONS ; i++) {
		s << "  #" << i << " on pin " << this->buttonConfigs[i] << ENDL;
	} // for
	s << "Autorepeat: " << (this->autoRepeat ? "True" : "False")
				  << " - Repeat delay: " << this->repeatDelay << " msec" << ENDL;
};
#endif

template<class CMD_CLASS, unsigned int NUM_BUTTONS>
bool ButtonsManager<CMD_CLASS, NUM_BUTTONS>::checkButtonConfig(uint8_t buttonID) {
	DASSERT(buttonID < NUM_BUTTONS);
	if (!this->buttonConfigs[buttonID]) {
		DPRINTS(DBG_DIAGNOSTIC, "*** Error: undefined button #");
		DPRINTLN(DBG_DIAGNOSTIC, buttonID);
		return false;
	}
	else return true;
}

