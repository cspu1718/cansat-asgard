/*
 * ButtonsManager.h
 */

#pragma once

// Silence warnings about unused parameters
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "elapsedMillis.h"

#define DBG 1		// Include a couple of debugging methods.

/**
 * @ingroup CSPU_Hardware
 * @brief A class in charge of monitoring a number of push-buttons, connected to
 * any number of pins (max = NUM_BUTTONS).
 *
 * CMD_CLASS must be a subclass of ButtonCommand and be the base class of all commands
 *           associated with buttons.
 * NUM_BUTTONS must be a strictly positive integer (the number of buttons being managed.
 *
 * If button #0 has no associated command, it is used as "shift" button, and each
 * other button can be associated with both a regular and a shift command.
 *
 * @par Usage
 * Example for 4  buttons, on pins 10, 11, 12 and A0, used to trigger:
 * 		Button#0 is the "shift" button, CommandA on button#1, CommandB on shift-button#1,
 * 		CommandC on button#2 and CommandD on button#3.
 * 		Those 4 subclasses of ButtonCommand have be defined.
 * @code
 * 		ButtonsManager<4> mgr;
 * 		commandA theA_Cmd(...);
 * 		commandB theB_Cmd(...);
 * 		commandC theC_Cmd(...);
 * 		commandD theD_Cmd(...);
 *
 *
 *		void setup() {
 *			if !(mgr.begin()) {
 *				// handle error
 *				exit(1);
 *			}
 *			mgr.configureButton(0, 10);
 *			mgr.configureButton(1, 11);
 *			mgr.configureButton(2, A0);
 *			mgr.configureButton(3, A0);
 *			mgr.configureCommand(1, &theA_Cmd, &theB_Cmd);
 *			mgr.configureCommand(2, &theC_Cmd);
 *			mgr.configureCommand(3, &theD_Cmd);
 *			(...)
 *		}
 *
 *		void loop() {
 *			(...)
 *			mgr.run();
 *			(...)
 *		}
 * @endcode
 * Another option would be to use a subclass:
 * @code
 * class MyButtonMgr : public ButtonManager<4> {
 * public:
 * 		MyButtonMgr() : theA_Cmd(...), theB_Cmd(...),
 * 				    	theC_Cmd(...),theD_Cmd(...) {
 *  	};
 *  	bool begin() {
 *  		if !(ButtonManager.begin()) return false;
 *			configureButton(0, 10);
 *			configureButton(1, 11);
 *			configureButton(2, A0);
 *			configureButton(3, A0);
 *			configureCommand(1, &theA_Cmd, &theB_Cmd);
 *			configureCommand(2, &theC_Cmd);
 *			configureCommand(3, &theD_Cmd);
 *  		return true;
 *  	}
 * private:
 * 		commandA theA_Cmd;
 * 		commandB theB_Cmd(...);
 * 		commandC theC_Cmd(...);
 * 		commandD theD_Cmd(...);
 * };
 *
 * // -------------------------------
 * MyButtonManager mgr;
 *
 * 	void setup() {
 *		if !(mgr.begin()) {
 *			// handle error
 *			exit(1);
 *		}
 *		(...)
 * }
 *
 * void loop() {
 *		(...)
 *		mgr.run();
 *		OR
 *		auto cmd = mgr.getCommandToRun();
 *		if (cmd) {
 *			... // do whatever must be done with command.
 *			cmd.run();
 *			...
 *		}
 *		(...)
 * }
 * @endcode
 * See also sketch test_ButtonsManager.ino
 */
template<class CMD_CLASS, unsigned int NUM_BUTTONS>
class ButtonsManager {
public:
	/** Constructor */
	ButtonsManager();
	virtual ~ButtonsManager() {};

	/** Initialize the class (call this method once before use).
	 *  @param minDelayBetweenRepeats Delay in msec from the push of a button.
	 *  			After this delay, it is considered "pushed again" in the
	 *  			following circumstances:
	 *  				- if the button has been continuously pushed and
	 *  				  allowAutoRepeat is true;
	 *  				- if it was released since the last push.
	 *  @param allowAutoRepeat If a button is pushed for more than
	 *  			minDelayBetweenRepeats msec without without being released,
	 *  			should we consider it as an additional push?
	 *  @return True if initialisation was successful, false otherwise.
	 */
	virtual bool begin(uint16_t minDelayBetweenRepeats=1000, bool allowAutoRepeat=false) ;

	/** Associate a particular button with a pin. The pin is expected to:
	 * 	 - have an internal pullup that can be activated with pinMode(INPUT_PULLUP)
	 * 	   or to be physically equiped with an external pull-up resistor.
	 * 	 - to be usable as digital pin (beware that not all board can make use of
	 * 	   all analogic pin as digital pins.
	 * 	If the method is called more than once for a particular button, the last call
	 * 	overwrites any previous configuration.
	 *  @param buttonID The ID of the button to be configured (max = NUM_BUTTONS-1).
	 *  @param pinNbr   The ID of the pin to use (for analogic pins, use the A0, A1...
	 *  			    macroes. Use fake pin #0 to unconfigure a button.
	 */
	void configureButton(uint8_t buttonID, uint8_t pinNbr) ;

	/** Configure a command for execution when a button is pushed, alone or together
	 *  with the shift-button (button #0). Note that it is up to the user to either:
	 *   - Use button #0 as shift button, and associate shiftCommands to some other
	 *     buttons.
	 *   - Use button #0 as a regular button, and never associate shiftCommands to
	 *     any button.
	 *  The following configuration are forbidden:
	 *  	- Associating a shift-command to button #0.
	 *  	- Associating a main command to button #0 and associating a shift
	 *  	  commaned to any other button.
	 * 	@param buttonID The button to associate with the command (ID from 0 to
	 * 					NUM_BUTTONS-1).
	 * 	@param mainCommand A pointer to the command object to associated with the
	 * 				   button, when pressed alone (or nullptr if none must be
	 * 				   associated with the button)
	 * 	@param shiftCommand A pointer to the command object to associated with the
	 * 				   button, when pressed together with button #0 (or nullptr if
	 * 				   none must be associated with the button)
	 */
	bool configureCommand(uint8_t buttonID, CMD_CLASS* mainCommand, CMD_CLASS* shiftCommand=nullptr) ;

	/** @brief Clear all pin and command configuration from this object. */
	void clearConfiguration() ;

	/** @brief Check the state of the buttons, define whether a command must
	 *  be executed and execute it.
	 *  If you need to obtain the ButtonCommand object (e.g. to retrieve results),
	 *  then make use of getCommandToRun() and run the retrieved command (if
	 *  any) yourself.
	 */
	void run();

	/** @brief Check the state of the buttons and define whether a command must
	 *  be executed. If so, a pointer to the command is returned.
	 *  @Warning The CMD_CLASS may not be freed.
	 *  @return A pointer to the CMD_CLASS object that must be executed, or nullptr
	 *          if no command must be executed.
	 */
	CMD_CLASS* getCommandToRun();

	/** @brief Get the pin associated with a button.
	 *  @return the associated pin, or 0 if none.
	 */
	uint8_t getButtonPin(uint8_t buttonID) { return buttonConfigs[buttonID];};

#if (DBG == 1)
	/** Debugging method to print internal configuration
	 *  @param s The stream to print to.  */
	void printConfiguration(Stream& s);
#endif

protected:
	/** Check a button is configured.
	 *  @return True if button is configured, false otherwise
	 */
	bool checkButtonConfig(uint8_t buttonID);

private:
	/** The structure used to associate commands with buttons. Obviously, configuring command for
	 *  buttons 1-2 and for buttons 2-1 is incompatible (only the configuration for the mainButton
	 *  with the lowest ID will be considered).
	 */
	typedef struct {
		CMD_CLASS*  mainCommand;			/**< The command to execute when the button is pushed alone */
		CMD_CLASS*  shiftCommand;		    /**< The command to execute when button is pushed together with
												 the shift-button, ie button 0. */
	} CommandConfig;
	uint8_t buttonConfigs[NUM_BUTTONS];     /**< The pins for each button, numbered from 0 to NUM_BUTTONS-1.
									             0 = unused */
	CommandConfig cmdConfigs[NUM_BUTTONS];  /**< The associations of command with buttons or button combinations.
											     The index is the mainButtonID. */
	bool autoRepeat;				/**< If button is pushed for more than repeatDelay msec without without
										 being released, should we consider it as an additional push? */
	uint16_t repeatDelay;			/**< Delay in msec from the push of a button,
										 After this delay, it is considered "pushed again" in the
										 following circumstances:
										 	 - if the button has been continuously pushed and autoRepeat is true;
										 	 - if it was released since the last push. */
	elapsedMillis elapsedSinceLastPush; /**< Time elapsed since last push */
	bool releasedSinceLastPush;		/**< Did we detect a release of all buttons since last push? */
};

#include "ButtonsManager.tpp"
