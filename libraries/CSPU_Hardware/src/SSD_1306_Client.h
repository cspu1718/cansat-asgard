/*
    SSD_1306_Client.h
*/

#pragma once
#include "Adafruit_SSD1306.h"

/**
   @ingroup CSPU_Hardware
   @brief Client class for SSD1306 OLED Feather Wing display and button handling.
*/
class SSD_1306_Client {
  public:
    /** @enum ButtonId
        @brief Button identifiers mapped to pin numbers.
    */
    enum class ButtonId {
      A = 9, /**< Pin for Button A */
      B = 6, /**< Pin for Button B */
      C = 5  /**< Pin for Button C */
    };

    /**
      @brief Constructor for the SSD_1306_Client class.
    */
    SSD_1306_Client();

    /**
      @brief Initializes the display.
      @return True if initialization is successful, otherwise false.
    */
    bool begin();

    /**
      @brief Prints text on a specific line of the display.
      @param line The line number where the text will be printed.
      @param text The text to display.
      @param withUpdate If true, updates the display after printing.
    */
    void printLine(int line, const char* text, bool withUpdate = true);

    /**
      @brief Prints text at specific (x, y) coordinates on the display.
      @param x The x-coordinate for text positioning.
      @param y The y-coordinate for text positioning.
      @param text The text to display.
      @param withUpdate If true, updates the display after printing.
    */
    void printText(int x, int y, const char* text, bool withUpdate = true);

    /**
      @brief Clears the display.
      @param withUpdate If true, updates the display after clearing.
    */
    void clear(bool withUpdate = true);

    /**
      @brief Refreshes the display to show any new content.
    */
    void updateDisplay();

    /**
      @brief Checks if a specific button is pressed.
      @param buttonId The identifier for the button to check (ButtonId::A, ButtonId::B, or ButtonId::C).
      @return The number of clicks: 0, 1 or 2 (double-click).
    */
    int readButton(ButtonId buttonId);

  private:
    /** State of a particular button */
    enum class ButtonState {
        IDLE,
        ONE_DEBOUNCING,
        ONE_PRESSED,
        ONE_RELEASED,
        TWO_DEBOUNCING,
        TWO_PRESSED,
        FINAL_DEBOUNCING
    };
    /** Utility function to convert button Id to index */
    int getButtonIndex(ButtonId button) {
        switch (button) {
            case ButtonId::A: return 0;
            case ButtonId::B: return 1;
            case ButtonId::C: return 2;
            default: return -1; // Error case
        }
    }
    Adafruit_SSD1306 display; /**< SSD1306 display object. */
    ButtonState buttonStates[3] = { ButtonState::IDLE };
    unsigned long stateTS[3] = { 0 }; // Timestamps for state transitions


};
