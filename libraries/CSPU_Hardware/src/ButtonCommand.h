/*
 * ButtonCommand.h
 */

#pragma once

/** @ingroup CSPU_Hardware
 *  The abstract base class for all commands.
 *  Subclass this for every actual command.
 */
class UserCommand {
public:
	UserCommand() {};
	virtual ~UserCommand() {};
	virtual void run()=0;
};

/** @ingroup CSPU_Hardware
 *  The abstract base class for all commands associated with buttons
 *  by class ButtonsManager.
 *  Subclass this for every actual command.
 */
class ButtonCommand : public UserCommand {
public:
	ButtonCommand() : buttonID(-1), shift(false) {};
	virtual ~ButtonCommand() {};
	virtual void run()=0;
	virtual void run(int8_t buttonID, bool shift) {
		this->buttonID=buttonID;
		this->shift=shift;
		this->run();
	};

	int8_t buttonID; /**< An optional information about the button that
						  triggered the command (-1 = none). This information
						  is set by the button manager whenever the command
						  is run. */
	bool shift;		 /**< An optional information about the shift-modifier.
						  Was it activated? This information is set
						  by the button manager whenever the command
						  is run.*/
};

/** @ingroup CSPU_Hardware
 *  A dummy command used for testing: it just prints its name (a single character),
 *  the ID of the button pushed and the status of the shift modifier.
 */
class PrintButtonCommand : public ButtonCommand {
public:
	/** Constructor
	 * @param cmdName A single character used to identify the command.
	 */
  PrintButtonCommand(char cmdName = 'A') : name(cmdName) {};
  virtual ~PrintButtonCommand() {};
  virtual void run() override {
    Serial << "Command '"<< name << "' executed on button ";
    if (shift) Serial << "Shift-";
    Serial << "#" << buttonID << ENDL;
  }
  char name;
};


