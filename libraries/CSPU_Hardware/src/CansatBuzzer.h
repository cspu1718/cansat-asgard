/**
 * @file CansatBuzzer.h
 * @brief CansatBuzzer class for controlling a buzzer with a Adafruit Feather M0 Express.
 * 
 * This class provides methods to initialize and control a buzzer, including
 * simple beeping and ringing for a specified duration.
 * 
 * @author Arthur Hansoul
 * @date 21/02/2025
 */

#pragma once

#include "elapsedMillis.h"

/**
 * @ingroup CSPU_Hardware
 * @brief A class for the buzzer which defines the functions making the buzzer beep. 
          The sound waves shape is a square. They have been tested and deemed adequate considering the fact that it makes enough noise and that the code is considerably easier than the sinusoidale waves, making it easier to manipulate the rhythmn for the ring function.
 *
 */
class CansatBuzzer {

private:

  uint8_t pin;
  uint16_t waitingTime = 200;

public:
  /**
   * @brief Initializes the buzzer on the specified pin.
   * @param buzzerPin The pin number where the buzzer is connected.
   * @return True if initialization is successful.
   */
  bool begin(uint8_t buzzerPin) {
    pin = buzzerPin;
    pinMode(pin, OUTPUT);
    return true;
  };

  /**
   * @brief Generates a single cycle of a square wave sound.
   * @param waitingTime Specifies the duration for which the output pin remains in a HIGH or LOW state before toggling. 
   * This value directly influences the frequency of the square wave signal sent to the buzzer, determining the pitch of the sound.
   * A higher value results in a lower-pitched tone and a lower value results in a higher-pitched tone.
   */
  void highLow() {
    digitalWrite(pin, HIGH);
    delayMicroseconds(waitingTime);
    digitalWrite(pin, LOW);
    delayMicroseconds(waitingTime);
  };


  /**
   * @brief Starts the buzzer for a given duration (durationInMsec).
   * 
   * The buzzer continuously generates high-low pulses with highLow() for the specified duration.
   * 
   * @param durationInMsec Duration for which the buzzer should stay active (in milliseconds).
   */
  void start(uint16_t durationInMsec) {
    elapsedMillis elapsed;
    while (elapsed < durationInMsec) {
      highLow();
    }
  };



  /**
   * @brief Rings the buzzer for a specified duration using a list of start() and delay() .
   *
   * The buzzer follows a predefined rhythm composed of a sequence of beeps. 
   * The start's parameter defines the length of the beep noise.
   * The delay's parameter defines the length of the dela time between two start.
   * 
   * @param durationInSec The duration for which the buzzer should ring (in seconds).
   * 
   * @note This function is blocking everything else while it is functionning.
   */
  void ring(uint8_t durationInSec) {
    elapsedMillis elapsed;
    while (elapsed < durationInSec * 1000) {
      start(50);
      delay(25);
      start(100);
      delay(50);
      start(100);
      delay(50);
      start(100);
      delay(50);
      start(500);
      delay(1500);
    }
  };

  
};
