/*
 * SSD_1306_Client.cpp
 */

#include "CansatConfig.h"
#include "SSD_1306_Client.h"
#include <Wire.h>
#include "Adafruit_GFX.h"

#define DBG_BEGIN 1

SSD_1306_Client::SSD_1306_Client()
  : display(128, 32, &Wire, -1) {
}

bool SSD_1306_Client::begin() {
  pinMode((uint8_t) ButtonId::A, INPUT_PULLUP);
  pinMode((uint8_t) ButtonId::B, INPUT_PULLUP);
  pinMode((uint8_t) ButtonId::C, INPUT_PULLUP);
  
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    DPRINTSLN(DBG_BEGIN, "SSD_1306 allocation failed");
    return false;
  }
  
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  
  #if (DBG_BEGIN == 1)
    updateDisplay(); 
    delay(1000);
  #endif
  
  clear(); 
  display.display();
  return true;
}

void SSD_1306_Client::printLine(int lineNumber, const char* text, bool withUpdate) {
  int y = lineNumber * 8; 
  display.setCursor(0, y);
  display.println(text);
  
  if (withUpdate) {
    updateDisplay();
  }
}

void SSD_1306_Client::printText(int x, int y, const char* text, bool withUpdate) {
  display.setCursor(x, y);
  display.println(text);
  
  if (withUpdate) {
    updateDisplay();
  }
}

void SSD_1306_Client::clear(bool withUpdate) {
  display.clearDisplay();
  
  if (withUpdate) {
    updateDisplay();
  }
}

void SSD_1306_Client::updateDisplay() {
  display.display();
}

int SSD_1306_Client::readButton(ButtonId theButtonId) {
	constexpr unsigned long DEBOUNCE_TIME = 50;     // ms
	constexpr unsigned long DOUBLE_CLICK_DELAY = 300; // ms
	constexpr unsigned long LONG_PRESS_DELAY = 600;  // ms

	int idx = getButtonIndex(theButtonId);
    bool pressed = (digitalRead((int) theButtonId) == LOW);
    unsigned long now = millis();

    switch (buttonStates[idx]) {
        case ButtonState::IDLE:
            if (pressed) {
                buttonStates[idx] = ButtonState::ONE_DEBOUNCING;
                stateTS[idx] = now + DEBOUNCE_TIME;
            }
            break;

        case ButtonState::ONE_DEBOUNCING:
            if (now >= stateTS[idx]) {
                if (pressed) {
                    buttonStates[idx] = ButtonState::ONE_PRESSED;
                } else {
                    buttonStates[idx] = ButtonState::IDLE; // Noise
                }
            }
            break;

        case ButtonState::ONE_PRESSED:
            if (!pressed) {
                if (now - stateTS[idx] < LONG_PRESS_DELAY) {
                    buttonStates[idx] = ButtonState::ONE_RELEASED;
                    stateTS[idx] = now + DOUBLE_CLICK_DELAY;
                } else {
                	stateTS[idx] = now + DEBOUNCE_TIME;
                    buttonStates[idx] = ButtonState::FINAL_DEBOUNCING; // Long press, no click
                }
            }
            break;

        case ButtonState::ONE_RELEASED:
            if (pressed) {
                buttonStates[idx] = ButtonState::TWO_DEBOUNCING;
                stateTS[idx] = now + DEBOUNCE_TIME;
            } else if (now >= stateTS[idx]) {
                buttonStates[idx] = ButtonState::FINAL_DEBOUNCING;
                stateTS[idx] = now + DEBOUNCE_TIME;
                return 1; // Single click detected
            }
            break;

        case ButtonState::TWO_DEBOUNCING:
            if (now >= stateTS[idx]) {
                if (pressed) {
                    buttonStates[idx] = ButtonState::TWO_PRESSED;
                } else {
                    buttonStates[idx] = ButtonState::FINAL_DEBOUNCING;
                    stateTS[idx] = now + DEBOUNCE_TIME;
                    return 2; // Double click detected
                }
            }
            break;

        case ButtonState::TWO_PRESSED:
            if (!pressed) {
                if (now - stateTS[idx] < LONG_PRESS_DELAY) {
                    buttonStates[idx] = ButtonState::FINAL_DEBOUNCING;
                    stateTS[idx] = now + DEBOUNCE_TIME;
                    return 2; // Double click detected
                } else {
                	stateTS[idx] = now + DEBOUNCE_TIME;
                    buttonStates[idx] = ButtonState::FINAL_DEBOUNCING; // Long press, no double click
                }
            }
            break;

        case ButtonState::FINAL_DEBOUNCING:
            if (now >= stateTS[idx]) {
                buttonStates[idx] = ButtonState::IDLE;
            }
            break;
    }
    return 0; // No click detected
}
