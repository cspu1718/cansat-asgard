#define DEBUG
#include "SSD_1306_Client.h"
#include "CSPU_Debug.h"
#include "CSPU_Test.h"
#include "elapsedMillis.h"

elapsedMillis elapsed;  // Time since last screen clear.
SSD_1306_Client displayClient; // Instance de la classe SSD_1306_Client

uint32_t testDisplay() {
  uint16_t numErrors = 0;
  //ligne spécifique
  CSPU_Test::pressAnyKey("Press any key to start display test");
  displayClient.printText(0, 8, "Hello");
  if (!CSPU_Test::askYN("Do you see 'Hello' at 0,8")) {
    numErrors++;
  }
  displayClient.clear();
  //position donnée
  displayClient.printText(10, 20, "In progress");
  if (!CSPU_Test::askYN("Do you see 'In progress' at 10, 20")) {
    numErrors++;
  }
  // Effacer l'écran avant de tester les boutons
  displayClient.clear();
  return numErrors;  // Retourne le nombre total d'erreurs pour affichage en console
}

void setup() {
  DINIT(115200);
  if (displayClient.begin()) {
    Serial << "Init SSD1306 OK" << ENDL;
  } else {
    Serial.println("Failed to initialize SSD_1306");
  }

  auto errors = testDisplay();
  Serial << errors << "error(s) durning testDisplay" << ENDL;
  Serial << "Now testing buttons: please try clicks and double-clicks and check screen" << ENDL;
}

void handleButton(SSD_1306_Client::ButtonId buttonId) {
  int read = displayClient.readButton(buttonId);
  if (read) {
    displayClient.clear();  // Efface l'écran
    char msg[20];
    if (read == 1) sprintf(msg, "Single-Click (pin %d)", (int)buttonId);
    else sprintf(msg, "Double-Click (pin %d)", (int)buttonId);
    displayClient.printText(0, 0, msg);
    elapsed = 0;
  }
}

void loop() {
  handleButton(SSD_1306_Client::ButtonId::A);
  handleButton(SSD_1306_Client::ButtonId::B);
  handleButton(SSD_1306_Client::ButtonId::C);

  // Clear display after 1 second
  if (elapsed > 1000) {
    displayClient.clear();
    elapsed = 0;
  }
}
