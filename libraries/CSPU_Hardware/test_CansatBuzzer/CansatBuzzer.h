#pragma once

#include "elapsedMillis.h"

class CansatBuzzer {

private:
  uint8_t pin;
  uint16_t waitingTime = 200;
  uint16_t beepTime = 1000;

public:
  bool begin(uint8_t buzzerPin) {
    pin = buzzerPin;
    pinMode(pin, OUTPUT);
    return true;
  };


  void
  highLow() {
    digitalWrite(pin, HIGH);
    delayMicroseconds(waitingTime);
    digitalWrite(pin, LOW);
    delayMicroseconds(waitingTime);
  };


  void
  ring(uint8_t durationInSec) {
    elapsedMillis elapsed;
    while (elapsed < durationInSec * 1000) {
      start(50);
      delay(25);
      start(100);
      delay(50);
      start(100);
      delay(50);
      start(100);
      delay(50);
      start(500);
      delay(1500);
    }
  };


  void
  start(uint16_t durationInMsec) {
    elapsedMillis elapsed;
    while (elapsed < durationInMsec) {
      highLow();
    }
  };
};
