

#define DEBUG  ///< Enables debug mode to display information on the serial port.
#include "CSPU_Debug.h"
#include "CansatBuzzer.h"

CansatBuzzer buzzer;


constexpr uint8_t PinNbr = 9;


void setup() {
  DINIT(115200);  ///< Initializes serial communication at 115200 baud.
  Serial << "configuring buzzer...";
  
  bool result = buzzer.begin(PinNbr); ///< Initializes the buzzer on the specified pin.
  if (result == true) {
    Serial << " OK" << ENDL;
  } else {
    Serial << "Error !" << ENDL;
    while (true) ; ///< Infinite loop in case of initialization failure.
  }

  // Performs a series of buzzer tests
  for (int i = 0; i < 5; i++) {
    Serial << "Starting buzzer" << ENDL;
    buzzer.start(3000); ///< Activates the buzzer for 3 seconds.
    delay(3000);
  }

  Serial << "Ringing buzzer" << ENDL;
  buzzer.ring(150); ///< Rings the buzzer for 150 ms.

  Serial << "Finished running setup" << ENDL;
}

void loop() {
}
