
/*
   test_OsirisCanCommander.ino


   WARNING: This program allows for testing the RT_CanCommander in two different configurations:
  		-1- When RF API mode is NOT activated (symbol RF_ACTIVATE_API_MODE in CansatConfig.h)
  			The RT_CanCommander is not provided with an actual stream to the XBee module, but
  			with the usual Serial stream.
  			This allows for easy testing with a single board, running this sketch only but
  			does not test the code specific to the API-mode (mainly the processing of the
  			GetFile command).

  			Wiring:  SD-Card reader on SPI bus, CS= CS_SD (configure below)
          			 LED on digital pin #LED_PinNbr
           		 Easiest solution is to use an Adalogger Feather M0 Express board,
           		 using only the on-board SD-Card reader and built-in LED.


  		-2- When RF API mode is activated, this program simulates the ground station,
  		    and does not host the RT_Commander. It must run on a board connected to
  		    the ground XBee module configured in CansatConfig.h
  		    See wiring information in program test_XBeeClient_Receive.

  		    It will be connected, through the RF interface to the RT_CanCommander
  		    created by sketch test_OnBoardCanCommander. This sketch must run on a
  		    board connected to the corresponding can XBee module, as configured
  		    in CansatConfig.h (see wiring information in sketch
  		    test_XBeeClient_Send) with an SD-Card reader (same warning as above).

*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

constexpr byte SD_CS = 4;  // CS for SD-Card reader. If using an Adalogger Feather M0 Express board,
// the onboard SD reader is on pin 4 (which is not broken out).
constexpr byte LED_PinNbr = LED_BUILTIN;  // pin connected to LED. LED_BUILTIN is the on-boar one.

#include "CansatConfig.h"
#include "CansatXBeeClient.h"  // Include this first to avoid undefined symbol at link.
#include "OsirisCanCommander.h"
#include "CansatInterface.h"
#include "utilityFunctions.h"
#ifdef RF_ACTIVATE_API_MODE
#include "elapsedMillis.h"
#else
#include "OsirisAcquisitionProcess.h"
#endif

constexpr bool DebugSendCmdRequest = true;

#if !defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS) && !defined(ADAFRUIT_FEATHER_M0)
#error "This program only works on Feather MO [Express]"
#endif

/* Some commonly used commands: DO NOT HARDCODE numerical values: CansatInterface.h could be modified,
   and the test program should still work, unchanged. */
String initiateCmdMode;  /**< This command switches to the Command mode.*/
String terminateCmdMode; /**< This command switches to the Acquisition Mode.*/

int numErrors = 0;
#ifdef RF_ACTIVATE_API_MODE
CansatXBeeClient xbc(CanXBeeAddressSH, CanXBeeAddressSL);  // Defined in CansatConfig.h
HardwareSerial& RF = Serial1;
#else
SdFat sd;
OsirisCanCommander cmd(17);
OsirisAcquisitionProcess process;

#endif

// ===================== TEST FUNCTIONS =========================


void waitForResponse(unsigned long minDurationInMsec = 500L) {
  // If using the transparent mode, no waiting is required: messages.
  // are received synchronously. Wait anyway (useful for testing some timers).
  elapsedMillis ts = 0;
#ifdef RF_ACTIVATE_API_MODE
  char str[xbc.MaxStringSize];
  CansatFrameType strType;
  uint8_t seqNbr, lastSeqNbr;
  elapsedMillis delaySinceLastReceived = 0;
  bool receivingStringParts = false;
  uint32_t counter = 0;
  while ((ts < minDurationInMsec) || (delaySinceLastReceived < 500)) {
    while (xbc.receive(str, strType, seqNbr)) {
      if (receivingStringParts) {
        if (strType == CansatFrameType::StringPart) {
          if (seqNbr != lastSeqNbr + 1) {
            Serial << ENDL << "*** missed string part (expected" << lastSeqNbr + 1
                   << ", got " << seqNbr << ")" << ENDL;
          } else lastSeqNbr++;
          Serial << str;
          counter += strlen(str);
        } else {
          receivingStringParts = false;
          Serial << ENDL;
          Serial << "Received string parts for a total of " << counter << " characters" << ENDL;
          Serial << "Received a string: type=" << (int)strType << ", content='" << str << "'" << ENDL;
        }
      } else {
        if (strType == CansatFrameType::StringPart) {
          receivingStringParts = true;
          lastSeqNbr = seqNbr;
          Serial << "Starting a string in several parts, with seq=" << seqNbr << ":" << ENDL;
          Serial << str;
          counter = strlen(str);
        } else {
          Serial << "Received a string: type=" << (int)strType << ", content='" << str << "'" << ENDL;
        }
      }
      delaySinceLastReceived = 0;
    }  // while received

  }  // while duration
  // if still receiving string parts, there was no ENDL yet
  if (receivingStringParts) {
    Serial << ENDL;
    Serial << "Received string parts for a total of " << counter << " characters" << ENDL;
  }
#else
  while (ts < minDurationInMsec) {
    delay(10);
  }
#endif
}

void sendCmdRequest(const char* req, unsigned long responseDelayInMsec = 500) {
  if (DebugSendCmdRequest) Serial << "sendCmdRequest for '" << req << "'";
#ifdef RF_ACTIVATE_API_MODE
  xbc.openStringMessage(CansatFrameType::CmdRequest);
  xbc << req;
  xbc.closeStringMessage(300);
  if (DebugSendCmdRequest) Serial << " (using RF)" << ENDL;
#else
  if (DebugSendCmdRequest) Serial << " (locally)" << ENDL;
  cmd.processCmdRequest(req);
#endif
  waitForResponse(responseDelayInMsec);
}

void testOsirisSpecificCommands() {
  String req;
  // Test TurnMotor with missing parameter and/or invalid parameter.
  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, (int)CansatCmdRequestType::StartBuzzer);
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Sent a startBuzzer command (#17) without parameter (expecting response 19)");
  buildRequest(req, (int)CansatCmdRequestType::StartBuzzer, "1");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Sent a a startBuzzer command (#17) with duration 1 (expecting response 32)");
  buildRequest(req, (int)CansatCmdRequestType::StartBuzzer, "50");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Sent a a startBuzzer command (#17) with duration 40 (expecting response 32)");
  buildRequest(req, (int)CansatCmdRequestType::StartBuzzer, "10");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Sent a a startBuzzer command (#17) with duration 10 (expecting response 30)");

}
// ================================== SETUP ================================

void setup() {
  DINIT(115200);
#ifdef RF_ACTIVATE_API_MODE
  RF.begin(115200);
  xbc.begin(RF);  // no return value
  Serial << "RT-CanCommander unit test in API mode: this is the ground part" << ENDL;
  Serial << "Make sure the test_onBoardCommander sketch runs on the can board" << ENDL;
#else
  //process.init();
  cmd.begin(Serial, &sd, &process);
  pinMode(LED_PinNbr, OUTPUT);
  digitalWrite(LED_PinNbr, LOW);
  Serial << "RT-CanCommander unit test (simulating TRANSPARENT mode)" << ENDL;
  Serial << "  SD-Card CS = " << SD_CS << ENDL;
  Serial << "  LED is on pin #" << LED_PinNbr << ENDL;
  SPI.begin();
  if (!sd.begin(SD_CS)) {
    sd.initErrorHalt();
  }
  Serial << "  SD-Card OK" << ENDL;
#endif
  // Prepare frequent commands
  buildRequest(initiateCmdMode, CansatCmdRequestType::InitiateCmdMode);
  buildRequest(terminateCmdMode, CansatCmdRequestType::TerminateCmdMode);

  Serial << "Initialization OK" << ENDL;
  Serial << "-----------------------------------------------" << ENDL;
  testOsirisSpecificCommands();

  Serial << F("Number Of Errors: ") << numErrors << ENDL;
}

// ================================== LOOP ================================

void loop() {}


