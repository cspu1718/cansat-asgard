/*
 *   OsirisRF_Transceiver.h
 */

#pragma once
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatRF_TransceiverWithScreen.h"
#include "OsirisRecord.h"

/** @inGroup OsirisCSPU
 *  @brief The RF_Transeiver used by the Osiris project.
 */
class OsirisRF_Transceiver : public CansatRF_TransceiverWithScreen<OsirisRecord> {
protected:
  virtual const char* getProjectName() override {
    return "OSIRIS RfTscv";
  };
};
