/*
 * OsirisCSPU.h
 * 
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the library should include a tag @ingroup OsirisCSPU
 * in the class documentation block.
 */

 /** @defgroup OsirisCSPU OsirisCSPU library
 *  @brief The library of classes specific to the CanSat 2024/2025 Osiris project.
 *  
 *  The OsirisCSPU library contains all the code for the Osiris project, which is assumed
 *  not to be reused across project
 *  
 *  _Dependencies_\n
 *  This library requires the following other generic libraries (in addition to hardware-specific libraries,
 *  and standard Arduino libraries, which are not listed here):
 *  - DebugCSPU
 *  - TimeCSPU
 *  - elapsedMillis
 *  - cansatAsgardCSPU
 *  @todo TO BE COMPLETED.
 *  
 *  
 *  _History_\n
 *  The library was created by the 2024/2025 Cansat team (Osiris) based on the TorusCSPU,
 *  IsatisCSPU, IsaTwoCSPU, SpySeeCanCSPU, GMiniCSPU, EtheraCSPU and IcarusCSPU libraries.
 */
