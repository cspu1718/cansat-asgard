/*
   OsirisCanCommander.h
*/

#pragma once
#include "RT_CanCommander.h"
#include "CansatBuzzer.h"
#include "OsirisAcquisitionProcess.h"

/** @ingroup OsirisCSPU
    @brief the CanCommander for the Osiris project
*/
class OsirisCanCommander : public RT_CanCommander<OsirisAcquisitionProcess> {
public:
  OsirisCanCommander(unsigned long int theTimeOut);
  virtual ~OsirisCanCommander(){};


#ifdef RF_ACTIVATE_API_MODE
  void begin(CansatXBeeClient& xbeeClient, SdFat* theSd, OsirisAcquisitionProcess* theProcess);
#else
  void begin(Stream& theResponseStream, SdFat* theSd, OsirisAcquisitionProcess* theProcess);
#endif

protected:

  virtual bool processProjectCommand(CansatCmdRequestType requestType, char* cmd) override;
  /** process the BuzzerStart command*/
  void processReq_StartBuzzer(char*& nextCharAddress);

private:
  CansatBuzzer buzzer;
};
