/*
   OsirisCanCommander.cpp
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "CansatInterface.h"
#include "CansatXBeeClient.h"
#include "OsirisCanCommander.h"
#include "OsirisConfig.h"

#define DBG_DIAGNOSTIC 1
#define DBG_OS_CMD 1

OsirisCanCommander::OsirisCanCommander(unsigned long int theTimeOut)
  : RT_CanCommander(theTimeOut) {
}

#ifdef RF_ACTIVATE_API_MODE
void OsirisCanCommander::begin(CansatXBeeClient& xbeeClient, SdFat* theSd,
                               OsirisAcquisitionProcess* theProcess) {
  RT_CanCommander::begin(xbeeClient, theSd, theProcess);
  buzzer.begin(CanBuzzerPin);
}
#else
void OsirisCanCommander::begin(Stream& theResponseStream, SdFat* theSd, OsirisAcquisitionProcess* theProcess) {
  RT_CanCommander::begin(theResponseStream, theSd, theProcess);
  buzzer.begin(CanBuzzerPin);
}
#endif


void OsirisCanCommander::processReq_StartBuzzer(char*& nextCharAddress) {
  DPRINTSLN(DBG_OS_CMD, "StartBuzzer")
  long int durationInSec;
  if (!getMandatoryParameter(nextCharAddress, durationInSec, "Missing duration parameter")) {
    return;
  }

  if (durationInSec < 3 || durationInSec > 30) {
    DPRINTSLN(DBG_DIAGNOSTIC, "Invalid duration");
    RF_OPEN_CMD_RESPONSE(RF_Stream);
    *RF_Stream << (int)CansatCmdResponseType::InvalidBuzzerDuration << ','
               << durationInSec << ",Duration must be in range 3-30";
    RF_CLOSE_CMD_RESPONSE(RF_Stream);
    return;
  }

  RF_OPEN_CMD_RESPONSE(RF_Stream);
  *RF_Stream << (int)CansatCmdResponseType::BuzzerStarted << ", Buzzer On";
  RF_CLOSE_CMD_RESPONSE(RF_Stream);
  buzzer.ring(durationInSec);
  RF_OPEN_CMD_RESPONSE(RF_Stream);
  *RF_Stream << (int)CansatCmdResponseType::BuzzerStopped << ", Buzzer Off";
  RF_CLOSE_CMD_RESPONSE(RF_Stream);
}


bool OsirisCanCommander::processProjectCommand(CansatCmdRequestType requestType,
                                               char* cmd) {
  DPRINTSLN(DBG_OS_CMD, "processOsirisProjectCommand ");
  bool result = false;

  switch (requestType) {
    case CansatCmdRequestType::StartBuzzer:
      processReq_StartBuzzer(cmd);
      result = true;
      break;

    default:
      // Do not report error here: this is done by the superclass.
      result = false;
  }
  return result;
}
