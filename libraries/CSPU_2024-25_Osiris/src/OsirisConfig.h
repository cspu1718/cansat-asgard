/*
 * OsirisConfig.h
 *
 * Specific configuration for the Osiris project
 */

#pragma once
#include "CansatConfig.h"
#include "CansatInterface.h"
#include <type_traits>

//------------------------ RF links Configuration --------------------------
#ifdef RF_ACTIVATE_API_MODE
// Pair used for Can and Ground is defined in CansatConfig
// Warning: the Ground Xbee should be consistent with the one defined in
//          CansatConfig.h, since the RF-Transceiver takes its configuration
//      from there.
constexpr uint32_t OS_Can_XBeeAddressSH=CanXBeeAddressSH;
constexpr uint32_t OS_Can_XBeeAddressSL=CanXBeeAddressSL;
constexpr uint32_t OS_Ground_XBeeAddressSH=GroundXBeeAddressSH;
constexpr uint32_t OS_Ground_XBeeAddressSL=GroundXBeeAddressSL;

#if (RF_XBEE_MODULES_SET=='E') // We use set the E pair for can & ground.
constexpr uint32_t OS_Client2_XBeeAddressSH=XBeeAddressSH_13;
constexpr uint32_t OS_Client2_XBeeAddressSL=XBeeAddressSL_13;
constexpr uint32_t OS_Client3_XBeeAddressSH=XBeeAddressSH_14;
constexpr uint32_t OS_Client3_XBeeAddressSL=XBeeAddressSL_14;
constexpr uint32_t OS_Client4_XBeeAddressSH=XBeeAddressSH_15; 
constexpr uint32_t OS_Client4_XBeeAddressSL=XBeeAddressSL_15;
#elif (RF_XBEE_MODULES_SET=='F')  // We use pair F for Can & ground.
constexpr uint32_t OS_Client2_XBeeAddressSH=XBeeAddressSH_11;
constexpr uint32_t OS_Client2_XBeeAddressSL=XBeeAddressSL_11;
constexpr uint32_t OS_Client3_XBeeAddressSH=XBeeAddressSH_12;
constexpr uint32_t OS_Client3_XBeeAddressSL=XBeeAddressSL_12;
constexpr uint32_t OS_Client4_XBeeAddressSH=XBeeAddressSH_15;
constexpr uint32_t OS_Client4_XBeeAddressSL=XBeeAddressSL_15;
#elif (RF_XBEE_MODULES_SET=='G') // We use pair G for Can & Ground
constexpr uint32_t OS_Client2_XBeeAddressSH=XBeeAddressSH_12;
constexpr uint32_t OS_Client2_XBeeAddressSL=XBeeAddressSL_12;
constexpr uint32_t OS_Client3_XBeeAddressSH=XBeeAddressSH_13;
constexpr uint32_t OS_Client3_XBeeAddressSL=XBeeAddressSL_13;
constexpr uint32_t OS_Client4_XBeeAddressSH=XBeeAddressSH_11;
constexpr uint32_t OS_Client4_XBeeAddressSL=XBeeAddressSL_11;
#else
#error "Unexpected RF_XBEE_MODULE_SET in OsirisConfig.h"
#endif
#endif

/** @ingroup OsirisCSPU
 *  An enum identifying the various RF communication strategies
 *  considered for the Osiris project.
 */
enum class OsirisRF_Strategy {
  SingleBaseStation = 1,    /**< Single ground station for both regular telemetry and the radio secondary mission */
  Distinct2aryBaseStation = 2,   /**< One regular ground station for telemetry and another for secondary mission transmission.*/
};

/** @ingroup OsirisCSPU
 * The RF Strategy currently in use */
constexpr OsirisRF_Strategy OsirisSelectedRF_Strategy=
      OsirisRF_Strategy::SingleBaseStation;

constexpr uint8_t CanBuzzerPin = A3;