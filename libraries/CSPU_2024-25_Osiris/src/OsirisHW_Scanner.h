/*
 * OsirisHWScanner.h
 */

#pragma once

// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "CansatHW_Scanner.h"
#include "OsirisXBeeClient.h"

/** @ingroup OsirisCSPU
 *  A Osiris-specific subclass of the Cansat hardware scanner.
 *  Required since we use a specific subclass of XBeeClient.
 */
class OsirisHW_Scanner: public CansatHW_Scanner {
public:
	OsirisHW_Scanner();

#ifdef RF_ACTIVATE_API_MODE
	virtual OsirisXBeeClient* getRF_XBee() override {
		return (OsirisXBeeClient*) CansatHW_Scanner::getRF_XBee();
	};

	virtual CansatXBeeClient* getNewXBeeClient() override {
		return new	OsirisXBeeClient(GroundXBeeAddressSH,GroundXBeeAddressSL);
	};
#endif


};
