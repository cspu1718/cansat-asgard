#pragma once
#include "CansatRecord.h"
#include "CansatInterface.h"

#ifndef INCLUDE_FLIGHT_ANALYSIS_DATA
#error "OsirisRecord requires flight analysis data to be part of CansartRecord. Define INCLUDE_FLIGHT_ANALYSIS_DATA in CansatConfig.h"
#endif

/** @ingroup OsirisCSPU
    @brief The record carrying all data acquired or computed by the Osiris CanSat
*/
class OsirisRecord : public CansatRecord {
  public:
    char messageStatus; /**< ' ' (vide), 'T' (transmission), ou 'R' (réception) */
    uint16_t messageID; /**< ID du message reçu ou transmis, entre 0 et 9999 */
    
  protected:
    virtual void printCSV_SecondaryMissionData(Stream &str, bool startWithSeparator, bool finalSeparator) const override;
    virtual void printCSV_SecondaryMissionHeader(Stream &str, bool startWithSeparator, bool finalSeparator) const override ;
    virtual void clearSecondaryMissionData() override;
    virtual uint16_t getSecondaryMissionMaxCSV_Size() const override;
    virtual uint16_t getSecondaryMissionCSV_HeaderSize() const override;
    virtual void printSecondaryMissionData(Stream& str) const override;
    virtual uint8_t writeBinarySecondaryMissionData(uint8_t* const destinationBuffer, uint8_t bufferSize) const override;
    virtual uint8_t readBinarySecondaryMissionData(const uint8_t* const sourceBuffer, const uint8_t bufferSize) override;
    virtual uint8_t getBinarySizeSecondaryMissionData() const override;
        friend class OsirisRecord_Test; 



    };
