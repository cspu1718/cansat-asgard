/*
   OsirisAcquisitionProcess.h
*/

#pragma once
#include "CansatAcquisitionProcess.h"
#include "OsirisRecord.h"

/** @ingroup OsirisCSPU
    @brief the Acquisition process for the Osiris project
*/
class OsirisAcquisitionProcess: public CansatAcquisitionProcess<OsirisRecord> {
  public:
    OsirisAcquisitionProcess() {};
    virtual ~OsirisAcquisitionProcess() {};


  protected:
    virtual void doIdle() override {
      CansatAcquisitionProcess::doIdle();
      // do our own background tasks here, if any
    };

    virtual void initSpecificProject() override {
      // Perform specific project initialisation here
    };  

    virtual void acquireSecondaryMissionData(OsirisRecord &record) override {
      // Acquire secondary mission data here, if required.
    };
};

