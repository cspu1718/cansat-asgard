/*
   OsirisXBeeClient.cpp
*/
#include "CansatConfig.h"
#ifdef RF_ACTIVATE_API_MODE
#include "OsirisXBeeClient.h"
#define DBG_CONFIG 1
bool OsirisXBeeClient::doCheckXBeeModuleConfiguration(
  bool correctConfiguration,
  bool &configurationChanged,
  const uint32_t &sh, const uint32_t &sl) {
  autoWakeUpXBeeModule();
  bool result = checkCommonModuleConfiguration(correctConfiguration, configurationChanged, sh, sl);
  auto component = getXBeeSystemComponent(sh, sl);
  DPRINTS(DBG_CONFIG, "doCheckXBeeModuleConfiguration:component=")
  DPRINTLN(DBG_CONFIG, (int) component)
  switch (component) {
    case OsirisComponent::Can:
      if (!checkCanModuleConfiguration(correctConfiguration, configurationChanged, sh, sl)) {
        result = false;
      }
      break;
    case OsirisComponent::Client2:
      if (!checkClientModuleConfiguration( CansatClientID::Client2, correctConfiguration, configurationChanged, sh, sl)) {
        result = false;
      }
      break;
    case OsirisComponent::Client3:
      if (!checkClientModuleConfiguration(CansatClientID::Client3, correctConfiguration, configurationChanged, sh, sl)) {
        result = false;
      }
      break;
    case OsirisComponent::Client4:
      if (!checkClientModuleConfiguration(CansatClientID::Client4, correctConfiguration, configurationChanged, sh, sl)) {
        result = false;
      }
      break;

    case OsirisComponent::RF_Transceiver:
      if (!checkGroundModuleConfiguration(correctConfiguration, configurationChanged, sh, sl)) {
        result = false;
      }
      break;
    case OsirisComponent::Undefined:
      result = false;
      Serial << "*** Unidentified system component: " << getLabel(component) << ENDL;
      break;
    default:
      result = false;
      Serial << "*** Unexpected system component (" << (uint8_t) component << ")" << ENDL;
  }
  autoPutXBeeModuleToSleep();
  return result;
}

OsirisXBeeClient::OsirisComponent OsirisXBeeClient::getXBeeSystemComponent(
  const uint32_t &sh, const uint32_t &sl) {
  autoWakeUpXBeeModule();
  OsirisXBeeClient::OsirisComponent result = OsirisComponent::Undefined;
  uint32_t mySH = sh, mySL = sl;
  if ((mySH == 0) && (mySL == 0)) {
    if ((!queryParameter("SH", mySH)) || (!queryParameter("SL", mySL)) ) {
      DPRINTSLN(DBG_DIAGNOSTIC, "Cannot retrieve XBee address")
      autoPutXBeeModuleToSleep();
      return result;
    }
  }

  if ((mySH == OS_Can_XBeeAddressSH) && (mySL == OS_Can_XBeeAddressSL)) {
    result = OsirisComponent::Can;
  }
  else if ((mySH == OS_Client2_XBeeAddressSH) && (mySL == OS_Client2_XBeeAddressSL)) {
    result = OsirisComponent::Client2;
  }
  else if ((mySH == OS_Client3_XBeeAddressSH) && (mySL == OS_Client3_XBeeAddressSL)) {
    result = OsirisComponent::Client3;
  }
  else if ((mySH == OS_Client4_XBeeAddressSH) && (mySL == OS_Client4_XBeeAddressSL)) {
    result = OsirisComponent::Client4;
  }
  else if ((mySH == OS_Ground_XBeeAddressSH) && (mySL == OS_Ground_XBeeAddressSL)) {
    result = OsirisComponent::RF_Transceiver;
  }
  autoPutXBeeModuleToSleep();
  return result;
}

bool OsirisXBeeClient::printConfigurationSummary(Stream &stream,
    const uint32_t & sh, const uint32_t& sl) {

  autoWakeUpXBeeModule();
  if (!CansatXBeeClient::printConfigurationSummary(stream, sh, sl)) {
    autoPutXBeeModuleToSleep();
    return false;
  }
  stream << "  According to OsirisConfig.h: " << ENDL;
  stream << "    Role of this module: " << getLabel(getXBeeSystemComponent(sh, sl)) << ENDL;
  stream << "    Applied RF strategy: " << getLabel(OsirisSelectedRF_Strategy) << ENDL;
  autoPutXBeeModuleToSleep();

  return  true;
}

bool OsirisXBeeClient::checkCommonModuleConfiguration(
  bool correctConfiguration, bool &configurationChanged,
  const uint32_t& sh, const uint32_t& sl) {
  autoWakeUpXBeeModule();
  bool result = CansatXBeeClient::checkCommonModuleConfiguration(
                  correctConfiguration, configurationChanged, sh, sl);

  // Add here any Osiris-specific configuration common to all modules.
  autoPutXBeeModuleToSleep();
  return result;
}

bool OsirisXBeeClient::checkCanModuleConfiguration(
  bool correctConfiguration, bool &configurationChanged,
  const uint32_t& sh, const uint32_t& sl) {
  autoWakeUpXBeeModule();
  bool success = true;

  char name[10] = "nn_CAN_x";
  updateModuleID_Prefix(name, sh, sl);
  name[7] = RF_XBEE_MODULES_SET;
  if (!checkParameter(  "NI",
                        name,
                        correctConfiguration,
                        configurationChanged,
                        sh, sl)) success = false;
  // Do not change name, in order to keep the general Cansat name.
  switch (OsirisSelectedRF_Strategy) {
    case OsirisRF_Strategy::SingleBaseStation:
      if (!checkDevicetype( XBeeTypes::DeviceType::Coordinator,
                            correctConfiguration,
                            configurationChanged,
                            sh, sl)) success = false;
      break;
    case OsirisRF_Strategy::Distinct2aryBaseStation:
      // Do not check DH-DL: it's not relevant in API mode.
      // Secondary base station must be a router.
      if (!checkDevicetype( XBeeTypes::DeviceType::Coordinator,
                            correctConfiguration,
                            configurationChanged,
                            sh, sl)) success = false;
      break;

      Serial << "*** Current RF strategy (" << getLabel(OsirisSelectedRF_Strategy)
             << ") is not supported for Can" << ENDL;
      break;
    default:
      Serial << "Unexpected RF_Strategy (" << (uint8_t)OsirisSelectedRF_Strategy  << ")" << ENDL;
  }
  autoPutXBeeModuleToSleep();
  return success;

}

bool OsirisXBeeClient::checkClientModuleConfiguration(
  CansatClientID ClientID,
  bool correctConfiguration, bool &configurationChanged,
  const uint32_t& sh, const uint32_t& sl) {
  bool success = true, tmpResult;
  DPRINTSLN(DBG_CONFIG, "checkClientModuleConfiguration")
  autoWakeUpXBeeModule();
  char name[12] = "nn_CLI_x_X";
  updateModuleID_Prefix(name, sh, sl);
  name[7] = '0'+ (int) ClientID;
  name[9] = RF_XBEE_MODULES_SET;
  if (!checkParameter(  "NI",
                        name,
                        correctConfiguration,
                        configurationChanged,
                        sh, sl)) success = false;
  switch (OsirisSelectedRF_Strategy) {
    case OsirisRF_Strategy::SingleBaseStation:
    case OsirisRF_Strategy::Distinct2aryBaseStation:
      // Do not check DH-DL: it's not relevant in API mode.

      // Client must be an end-device to support sleep mode, but
      // the SM parameter will only be configured to 1 to turn
      // the XBee into an End-Device with pin sleep mode
      // activated at runtime, when sleep mode is configured.
      // This avoids the need to pull the SLEEP-RQ pin down
      // when ever it is not managed by the µController.
      // As a consequence, the device type will be detected as
      // End-device when operator by the subcan software, and
      // Router when operated by any other software which does not
      // change the SM parameter.
      if (!checkDevicetype( XBeeTypes::DeviceType::Router,
                            correctConfiguration,
                            configurationChanged,
                            sh, sl)) {
        if (!correctConfiguration) {
          if (getDeviceType(sh, sl) == XBeeTypes::DeviceType::EndDevice) {
            Serial << "WARNING: if the module is operated by the Ground SW, it could" << ENDL;
            Serial << "         have been dynamically configured as End-Device, wihch is normal!" << ENDL;
            success = true;
          } else success = false;
        } else success = false;
      }
      // Configure sleepRequest and onSleep pin
      tmpResult = checkParameter("D8", (uint8_t) 0x01,
                                 correctConfiguration, configurationChanged, sh, sl);
      success = success && tmpResult;
      tmpResult = checkParameter("D9", (uint8_t) 0x01,
                                 correctConfiguration, configurationChanged, sh, sl);
      success = success && tmpResult;
      break;
      Serial << "*** Current RF strategy (" << getLabel(OsirisSelectedRF_Strategy)
             << ") is not supported for Client" << ENDL;
      break;
    default:
      Serial << "Unexpected RF_Strategy (" << (uint8_t)OsirisSelectedRF_Strategy  << ")" << ENDL;
  }
  autoPutXBeeModuleToSleep();
  return success;
}

bool OsirisXBeeClient::checkGroundModuleConfiguration(
  bool correctConfiguration, bool &configurationChanged,
  const uint32_t& sh, const uint32_t& sl) {
  bool success = true;

  autoWakeUpXBeeModule();
  char name[14] = "nn_RF-TSCV_x";
  updateModuleID_Prefix(name, sh, sl);
  name[11] = RF_XBEE_MODULES_SET;
  if (!checkParameter(  "NI",
                        name,
                        correctConfiguration,
                        configurationChanged,
                        sh, sl)) success = false;
  // Do not change name, in order to keep the general Cansat name.
  switch (OsirisSelectedRF_Strategy) {
    case OsirisRF_Strategy::SingleBaseStation:
    case OsirisRF_Strategy::Distinct2aryBaseStation:
      // Do not check DH-DL: it's not relevant in API mode.
      // Ground must be a router.
      if (!checkDevicetype( XBeeTypes::DeviceType::Router,
                            correctConfiguration,
                            configurationChanged,
                            sh, sl)) success = false;
      break;
      Serial << "*** Current RF strategy (" << getLabel(OsirisSelectedRF_Strategy)
             << ") is not supported for RF-Transceiver" << ENDL;
      break;
    default:
      Serial << "Unexpected RF_Strategy (" << (uint8_t)OsirisSelectedRF_Strategy  << ")" << ENDL;
  }
  autoPutXBeeModuleToSleep();
  return success;
}

const char* OsirisXBeeClient::getLabel(OsirisComponent component) {
  switch (component) {
    case OsirisComponent::Can:
      return "MAIN CAN";
      break;
    case OsirisComponent::Client2:
      return "Client 2";
      break;
    case OsirisComponent::Client3:
      return "Client 3";
      break;
    case OsirisComponent::Client4:
      return "Client 4";
      break;
    case OsirisComponent::RF_Transceiver:
      return "GROUND STATION";
      break;
    case OsirisComponent::Undefined:
      return "UNIDENTIFIED";
      break;
    default:
      return "Unknown";
      DPRINTS(DBG_DIAGNOSTIC, "Unexpected system component: ");
      DPRINTLN(DBG_DIAGNOSTIC, (uint8_t) component);
  }
}
const char* OsirisXBeeClient::getLabel(OsirisRF_Strategy strategy) {
  switch (strategy) {
    case OsirisRF_Strategy::SingleBaseStation:
      return "Single Base Station";
      break;
    case OsirisRF_Strategy::Distinct2aryBaseStation:
      return "Distinct 2-ary Base Station";
      break;
    default:
      return "UNKNOWN";
      DPRINTS(DBG_DIAGNOSTIC, "Unexpected RF_Strategy: ");
      DPRINTLN(DBG_DIAGNOSTIC, (uint8_t) OsirisSelectedRF_Strategy);
  }
}
#endif
