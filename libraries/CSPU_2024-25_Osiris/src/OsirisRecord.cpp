// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "OsirisRecord.h"

#define DBG_BINARY 0

void OsirisRecord::printCSV_SecondaryMissionData(Stream & str, bool startWithSeparator, bool finalSeparator) const {
  if (startWithSeparator) str << separator;
  str << (char) messageStatus << separator;
  str << (uint16_t) messageID; // do not add a separator after the last variable

  if (finalSeparator) str << separator;
}

void OsirisRecord::printCSV_SecondaryMissionHeader(Stream &str, bool startWithSeparator, bool finalSeparator) const {
  if (startWithSeparator) str << separator;
  str << "(char) messageStatus,(uint16_t) messageID";
  if (finalSeparator) str << separator;
}

void OsirisRecord::clearSecondaryMissionData() {

  messageStatus=' '; // these are not the global variables
  messageID = 0;
}
uint16_t OsirisRecord::getSecondaryMissionMaxCSV_Size() const {
  return 1 /*,*/
         + 20 /*(char) messageStatus*/ + 1 /*,*/ // do not forget to add a comma after every controlled value except the last
         + 20 /*(uint16_t) messageID*/;
}

uint16_t OsirisRecord::getSecondaryMissionCSV_HeaderSize() const {
  return  1 /*,*/  + 41 /* (char) messageStatus, (uint16_t) messageID (amount of characters including comma)*/
          ;
}

void OsirisRecord::printSecondaryMissionData(Stream& str) const {
  str <<  "Message Status: " << (char) messageStatus  << ENDL;
  str <<  "Message ID: " << (uint16_t) messageID << ENDL; // result of the value given in the record (should not be altered)
}

uint8_t OsirisRecord::writeBinarySecondaryMissionData(uint8_t* const destinationBuffer, uint8_t bufferSize) const {
  uint8_t written = 0;
  uint8_t* dst = destinationBuffer;
  uint8_t remaining = bufferSize;
  DPRINTS(DBG_BINARY, " writeBinary: bufferSize=");
  DPRINTLN(DBG_BINARY, remaining);

  written += writeBinary(dst, remaining, (char) messageStatus);
  written += writeBinary(dst, remaining, (uint16_t) messageID);

  DPRINTS(DBG_BINARY, " writeBinary: (char) messageStatus=");
  DPRINTLN(DBG_BINARY, (char) messageStatus);
  DPRINTS(DBG_BINARY, " writeBinary: messageID=");
  DPRINTLN(DBG_BINARY, (uint16_t) messageID);
  DPRINTS(DBG_BINARY, "nbr bytes written:");
  DPRINTLN(DBG_BINARY, written);
  return written;

  ;
}
uint8_t OsirisRecord::readBinarySecondaryMissionData(const uint8_t* const sourceBuffer, const uint8_t bufferSize) {
  uint8_t read = 0,id =0xFF;
  const uint8_t* src = sourceBuffer;
  uint8_t remaining = bufferSize;
  read += readBinary(src, remaining, messageStatus);
  read += readBinary(src, remaining, messageID);


  DPRINTS(DBG_BINARY, " readBinary: (char) messageStatus=");
  DPRINTS (DBG_BINARY, (char) messageStatus);
DPRINTS (DBG_BINARY, " readBinary: messageID=");
  DPRINTLN(DBG_BINARY, (uint16_t) messageID);
  return read;
}

uint8_t OsirisRecord::getBinarySizeSecondaryMissionData() const {
  return sizeof(uint8_t  /* sourceID */)
         + sizeof(uint16_t /* messageID*/)
         ;
}
