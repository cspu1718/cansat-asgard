/*
   Test program for class IcarusRecord
*/

#include "CansatConfig.h"
#include "IcarusRecord.h"
#include "StringStream.h"

#define DBG 1

class IcarusRecord_Test {
  public:
    void testClear(IcarusRecord &rec) {
      DPRINTLN(DBG, "Test: clearing record [BEGIN]");

      initValues(rec);
      clearValues(rec);
      checkClearedValues(rec);

      DPRINTLN(DBG, "Test successful");
      DPRINTLN(DBG, "Test: clearing record [END]");
    }
    void testCSVLen(IcarusRecord &rec) {
      DPRINTLN(DBG, "Test: CSV length [BEGIN]");
      initValues(rec);

      String s;
      StringStream ss(s);

      rec.printCSV_SecondaryMissionData(ss, true, true);

      DASSERT(rec.getSecondaryMissionMaxCSV_Size() >= s.length()); // necessary to have at least as much characters written in the cpp as obtained

      s = "";

      rec.printCSV_SecondaryMissionHeader(ss, true, false);

      DASSERT(rec.getSecondaryMissionCSV_HeaderSize() == s.length()); // testing whether number of characters of record heaeder is equal to the numbers of chars of the data

      DPRINTLN(DBG, "Test successful");
      DPRINTLN(DBG, "Test: CSV length [END]");
    }
    void testReadWriteBinary(IcarusRecord &rec) {
      DPRINTLN(DBG, "Test: read/write binary [BEGIN]");

      initValues(rec);
      uint8_t binSize = rec.getBinarySize();
      byte bin[binSize];

      // NB: A check that the written size is the same as the record's binary size is
      //     performed in writeBinary() and readBinary()
      DASSERT(rec.writeBinary(bin, binSize));

      clearValues(rec);
      DASSERT(rec.readBinary(bin, binSize));

      checkValues(rec);

      DPRINTLN(DBG, "Test successful");
      DPRINTLN(DBG, "Test: read/write binary [END]");
    }
    void testCSVPrint(IcarusRecord &rec) {
      DPRINTLN(DBG, "Test: CSV print [BEGIN]");

      DPRINTLN(DBG, "Printing secondary mission CSV Header without additional separators")
      rec.printCSV_SecondaryMissionHeader(Serial, false, false);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Header without preceding separator, but with final separator")
      rec.printCSV_SecondaryMissionHeader(Serial, false, true);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Header without final separator, but with preceding separator")
      rec.printCSV_SecondaryMissionHeader(Serial, true, false);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Header with preceding and final separator")
      rec.printCSV_SecondaryMissionHeader(Serial, true, true);

      DPRINTLN(DBG, "\nPrinting secondary mission CSV Data without additional separators")
      rec.printCSV_SecondaryMissionData(Serial, false, false);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Data without preceding separator, but with final separator")
      rec.printCSV_SecondaryMissionData(Serial, false, true);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Data without final separator, but with preceding separator")
      rec.printCSV_SecondaryMissionData(Serial, true, false);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Data with preceding and final separator")
      rec.printCSV_SecondaryMissionData(Serial, true, true);            // Check wether the test succeded, therefore look wether instroctions correlate with the separators and their location
      DPRINTLN(DBG, "\nPrinting complete CSV Header");
      rec.printCSV(Serial, CansatRecord::DataSelector::All, CansatRecord::HeaderOrContent::Header);
      Serial << ENDL;
      rec.printCSV(Serial);

      DPRINTLN(DBG, "\nNote: Please check for yourself if the test was successful");
      DPRINTLN(DBG, "Test: CSV print [END]");
    }
    void testHumanReadablePrint(IcarusRecord &rec) {
      DPRINTLN(DBG, "Test: Human readable (secondary mission) print [BEGIN]");
      rec.printSecondaryMissionData(Serial);
      DPRINTLN(DBG, "Note: Please check for yourself if the test was successful"); // values printed should not have been altered
      DPRINTLN(DBG, "Test: Human readable print [END]");
      DPRINTLN(DBG, "Test: Human readable (complete record) print [BEGIN]");
      rec.print(Serial);
      DPRINTLN(DBG, "Note: Please check for yourself if the test was successful"); // values printed should not have been altered
      DPRINTLN(DBG, "Test: Human readable print [END]");
    } 

  private:
    void initValues(IcarusRecord &rec) {
      rec.motor1_Position = 34;
      rec.motor2_Position = 35; // different values chosen for the test to make sure variables or values are not being swapped
    }
    void clearValues(IcarusRecord &rec) {
      rec.clear();
    }
    void checkValues(IcarusRecord &rec) {
      DASSERT(rec.motor1_Position == 34);
      DASSERT(rec.motor2_Position == 35);
    }
    void checkClearedValues(IcarusRecord &rec) {
      DASSERT(rec.motor1_Position == 0);
      DASSERT(rec.motor2_Position == 0);
    }
} ;

void setup(){
  DINIT(115200);

  IcarusRecord rec;
  IcarusRecord_Test test;

  Serial << "Binary size of record:" << rec.getBinarySize() << " bytes" << ENDL;
  test.testClear(rec);
  test.testCSVLen(rec);
  test.testReadWriteBinary(rec);
  test.testCSVPrint(rec);
  test.testHumanReadablePrint(rec);

  DPRINTLN(DBG, "Testing procedure complete");
}

void loop() {
  // put your main code here, to run repeatedly:

}
