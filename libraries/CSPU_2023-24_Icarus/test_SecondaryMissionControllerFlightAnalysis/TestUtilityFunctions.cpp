/*
 * TestUtilityFunctions.cpp
 *
 */

#include "./TestUtilityFunctions.h"

#include "CansatConfig.h"

void RunA_RecordForRefAltitudeTest(IcarusRecord& record, float altitude) {
	record.altitude=altitude+(random(0,150)-75.0f)/100.0f;
	record.timestamp+=CansatAcquisitionPeriod;
	controller.run(record);
	recordCounter++;
	if ((recordCounter % 100) == 0) {
		Serial << ".";
	}
}

void printFlightInfo(IcarusRecord& rec) {
	float sec= (rec.timestamp % 60000) / 1000.0f;
	uint16_t min = rec.timestamp / 60000;
	uint8_t  hour = min / 60;
	min = min % 60;

	Serial << hour << ":" << min << ":" << sec << ", ts=" <<rec.timestamp << ", rel. alt.=" << rec.altitude - rec.refAltitude
		   << ", descVelocity=" << rec.descentVelocity << ", ";
	Serial <<",ctrlInfo=" << (int) rec.flightControlCode << ", phase=" << rec.flightPhase << ", motor 1 position=" << rec.motor1_Position << ", motor 2 position=" << rec.motor2_Position << ENDL;
}

/** Check controller info in record against expected values. Phase is not checked if 255 */
void checkCtrlInfo(IcarusRecord& record, FlightControlCode ctrlCode, const char* ctrlCodeMsg, uint8_t phase) {
	if (record.flightControlCode!=ctrlCode){
		printFlightInfo(record);
		Serial << "*** Error: controller code is NOT " << ctrlCodeMsg << ENDL;
		numErrors++;
		if (stopAfterFirstFailedCheck) {
			Serial << "Stopped because of 'stopAfterFirstFailedCheck' setting" << ENDL;
			delay(500);
			exit(-1);
		}
	}
	if ((phase != 255) && (record.flightPhase!=phase)){
		printFlightInfo(record);
		Serial << "*** Error:  flightPhase is NOT " << phase << ENDL;
		numErrors++;
		if (stopAfterFirstFailedCheck) {
			Serial << "Stopped because of 'stopAfterFirstFailedCheck' setting" << ENDL;
			delay(500);
			exit(-1);
		}
	}
}

/** Check whether flight phase is either phase1 or phase2 (used to support phase detectiong delay) */
void checkPhase(IcarusRecord& record, uint8_t phase1, uint8_t phase2) {
	if ((record.flightPhase!=phase1) && (record.flightPhase!=phase2)){
		printFlightInfo(record);
		Serial << "*** Error:  flightPhase is NOT " << phase1  << " nor " << phase2 << ENDL;
		numErrors++;
		if (stopAfterFirstFailedCheck) {
			Serial << "Stopped because of 'stopAfterFirstFailedCheck' setting" << ENDL;
			delay(500);
			exit(-1);
		}
	}
}

/* TMP: REMOVE! 
  void checkTarget(IcarusRecord& record, uint16_t target) {
	static uint16_t previousTarget=60000;
	static uint32_t lastChangeTS=0;
	if (record.parachuteTargetRopeLen!=target){
		if ((record.parachuteTargetRopeLen!=previousTarget) ||
				((lastChangeTS -record.timestamp) <= CansatMinDelayBetweenPhaseChange)) {
			// In this case, the delay in target update is abnormal

			Serial << "*** Error: target=" << record.parachuteTargetRopeLen << ", expected " << target
					<< ", TS=" << record.timestamp << ", lastChange=" <<lastChangeTS << ENDL;
			numErrors++;
			if (stopAfterFirstFailedCheck) {
				Serial << "Stopped because of 'stopAfterFirstFailedCheck' setting" << ENDL;
				delay(500);
				exit(-1);
			}
		}
	} // if != target
	if (record.parachuteTargetRopeLen != previousTarget) {
		previousTarget=record.parachuteTargetRopeLen;
		lastChangeTS=record.timestamp;
	}
}
*/

void wobbleAltitude(IcarusRecord& record){
	constexpr float altitudeWobbling=1.1*NoAltitudeChangeTolerance;
	static bool add=false;
	if (add) record.altitude+=altitudeWobbling;
	else record.altitude-=altitudeWobbling;
	add=!add;
}

void waitEndOfStartup(IcarusSecondaryMissionController& ctrl, IcarusRecord& record) {
  int numRecords = CansatMinDelayBeforeFlightControl/CansatAcquisitionPeriod + 1;
	Serial << "Waiting until start up is over (" << CansatMinDelayBeforeFlightControl/1000.0
		   << " sec. since startup  = " << numRecords << " records)...";
	while (numRecords > 0) {
     record.timestamp+=CansatAcquisitionPeriod;
		 ctrl.manageSecondaryMission(record);
     numRecords--;
	}
	Serial << " OK" << ENDL;
}

#ifdef TMP_REMOVE
void setReferenceAltitude(IcarusRecord& record){
	Serial << "Setting ref. altitude to " << record.altitude << "m..." ;
  record.refAltitude = record.altitude;
	/* TMP REMOVE while (fabs(record.refAltitude-record.altitude) > 0.1) {
		controller.run(record);
		record.timestamp+=CansatAcquisitionPeriod;
		recordCounter++;
		if ((recordCounter % 100)==0) Serial << "." ;
	}
	Serial << " OK" << ENDL;
  */
}
#endif

void printTestSummary() {
	Serial << ENDL << "===== Test over (processed " << recordCounter << " records). Errors so far: "
			<< numErrors  << " =====" << ENDL;
	if (numErrors && stopAfterFailedTest) {
		Serial << "Stopping because of 'stopAfterFailedTest' setting in test program" << ENDL;
		delay(500);
		exit(-1);
	}
}
