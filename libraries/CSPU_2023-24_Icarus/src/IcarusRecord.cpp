// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "IcarusRecord.h"

#define DBG_BINARY 0

void IcarusRecord::printCSV_SecondaryMissionData(Stream & str, bool startWithSeparator, bool finalSeparator) const {
  if (startWithSeparator) str << separator; 
  str << (int16_t) motor1_Position << separator;
  str << (int16_t) motor2_Position ; // do not add a separator after the last variable  

  if (finalSeparator) str << separator;
}

void IcarusRecord::printCSV_SecondaryMissionHeader(Stream &str, bool startWithSeparator, bool finalSeparator) const {
  if (startWithSeparator) str << separator;
  str << "motor1_Position,motor2_Position";
  if (finalSeparator) str << separator;
}

void IcarusRecord::clearSecondaryMissionData() {
  
  motor1_Position = 0; // these are not the global variables
  motor2_Position = 0;

}
uint16_t IcarusRecord::getSecondaryMissionMaxCSV_Size() const {
  return 1 /*,*/
    + 15 /* motor1_Position(int16_t)*/ +1 /*,*/ // do not forget to add a comma after every controlled value except the last
    + 15/* motor2_Position( int16_t) */
  
;}

uint16_t IcarusRecord::getSecondaryMissionCSV_HeaderSize() const {
  return  1 /*,*/  + 31 /* motor1_Position,motor2_Position (amount of characters including comma)*/
;}

void IcarusRecord::printSecondaryMissionData(Stream& str) const {
  str <<  "Position motor 1: " << (int16_t) motor1_Position  << ENDL;
  str <<  "Position motor 2: " << (int16_t) motor2_Position << ENDL; // result of the value given in the record (should not be altered)
}

uint8_t IcarusRecord::writeBinarySecondaryMissionData(uint8_t* const destinationBuffer, uint8_t bufferSize) const {
  uint8_t written = 0;
  uint8_t* dst = destinationBuffer;
  uint8_t remaining = bufferSize;
  DPRINTS(DBG_BINARY, " writeBinary: bufferSize=");
  DPRINTLN(DBG_BINARY, remaining);

  written += writeBinary(dst, remaining, (int16_t) motor1_Position );
  written += writeBinary(dst, remaining, (int16_t) motor2_Position);
   
  DPRINTS(DBG_BINARY, " writeBinary: motor1_Position=");
  DPRINTLN(DBG_BINARY, (int16_t) motor1_Position);
  DPRINTS(DBG_BINARY, " writeBinary: motor2_Position=");
  DPRINTLN(DBG_BINARY, (int16_t) motor2_Position);
  DPRINTS(DBG_BINARY, "nbr bytes written:");
  DPRINTLN(DBG_BINARY, written);
  return written;

;}
uint8_t IcarusRecord::readBinarySecondaryMissionData(const uint8_t* const sourceBuffer, const uint8_t bufferSize) {
  int16_t read = 0;
  const uint8_t* src = sourceBuffer;
  uint8_t remaining = bufferSize;
  read += readBinary(src, remaining, motor1_Position);
  read += readBinary(src, remaining, motor2_Position);

  
  DPRINTS(DBG_BINARY, " readBinary: motor1_Position=");
  DPRINTS (DBG_BINARY, (int16_t)motor1_Position);

  DPRINTS (DBG_BINARY, " readBinary: motor2_Position=");
  DPRINTLN(DBG_BINARY, (int16_t)motor2_Position);
  
  return read;
}

uint8_t IcarusRecord::getBinarySizeSecondaryMissionData() const {
  return sizeof(int16_t  /* motor1_Position */)
     + sizeof(int16_t /*motor2_Position*/)
;}
