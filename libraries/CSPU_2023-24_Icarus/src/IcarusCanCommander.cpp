/*
   IcarusCanCommander.cpp
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "CansatInterface.h"
#include "IcarusCanCommander.h"
#include "CansatXBeeClient.h"
#include "IcarusAcquisitionProcess.h"

#define DBG_READ_MSG 0
#define DBG_DIAGNOSTIC 1
#define DBG_MOTOR_CMD 0

IcarusCanCommander:: IcarusCanCommander(unsigned long int theTimeOut) :
  RT_CanCommander(theTimeOut) {
  theMotors[0] = nullptr;
  theMotors[1] = nullptr;
}

#ifdef RF_ACTIVATE_API_MODE
void  IcarusCanCommander::begin(CansatXBeeClient &xbeeClient, SdFat* theSd,
                                IcarusAcquisitionProcess* theProcess) {
  RT_CanCommander::begin(xbeeClient, theSd, theProcess);
  theMotors[0] = theProcess->getMotor(1);
  theMotors[1] = theProcess->getMotor(2);
}
#else
void IcarusCanCommander::begin(Stream& RF_Stream, SdFat* theSd, IcarusAcquisitionProcess* theProcess) {
  RT_CanCommander::begin(RF_Stream, theSd, theProcess);
  theMotors[0] = theProcess->getMotor(1);
  theMotors[1] = theProcess->getMotor(2);
}
#endif

void IcarusCanCommander::processReq_TurnMotor(char* &nextCharAddress) {
  DPRINTSLN(DBG_READ_MSG, "TurnMotor");
  uint8_t motor, power;
  int16_t numberOfClicks;

  if (!getMandatoryParameter(nextCharAddress, motor, "Missing motorID")) {
    return;
  }
  //Error if the motor not 1 or 2
  if ((motor < 1) || (motor > 2)) {
    DPRINTSLN(DBG_DIAGNOSTIC, "Invalid motorID");
    RF_OPEN_CMD_RESPONSE(RF_Stream);
    *RF_Stream << (int) CansatCmdResponseType::InvalidMotorID << ','
               << motor << ",Motor must be 1 or 2";
    RF_CLOSE_CMD_RESPONSE(RF_Stream);
    return;
  }
  if (theMotors[motor - 1] == nullptr) {
    Serial << "Error: motor with ID " << motor << "is null" << ENDL;
    // SEND REAL ERROR MSG
    return;
  }
  long int tmp;
  if (!getMandatoryParameter(nextCharAddress, tmp, "Missing numberOfClicks")) {
    return;
  }
  if ((tmp > 32672) || (tmp < -32768)) {
    DPRINTSLN(DBG_DIAGNOSTIC, "Invalid number of clicks");
    RF_OPEN_CMD_RESPONSE(RF_Stream);
    *RF_Stream << (int) CansatCmdResponseType::InvalidNbrOfClicks << ','
               << tmp << ",Number of clicks must be in range -32768 to 32672";
    RF_CLOSE_CMD_RESPONSE(RF_Stream);
    return;
  }
  numberOfClicks = (int16_t) tmp;

  if (!getMandatoryParameter(nextCharAddress, tmp, "Missing power")) {
    return;
  }
  if ((tmp < 0) || (tmp > 255)) {
    DPRINTSLN(DBG_DIAGNOSTIC, "Invalid power");
    RF_OPEN_CMD_RESPONSE(RF_Stream);
    *RF_Stream << (int) CansatCmdResponseType::InvalidPower << ','
               << tmp << ",Power must be in range 0-255";
    RF_CLOSE_CMD_RESPONSE(RF_Stream);
    return;
  }
  else power = (uint8_t) tmp;

  DPRINTSLN(DBG_MOTOR_CMD, "Command ok");
  DPRINTS(DBG_MOTOR_CMD, "Motor id: ");
  DPRINTLN(DBG_MOTOR_CMD, motor);
  DPRINTS(DBG_MOTOR_CMD, "Number of clicks: ");
  DPRINTLN(DBG_MOTOR_CMD, numberOfClicks);
  DPRINTS(DBG_MOTOR_CMD, "Power: ");
  DPRINTLN(DBG_MOTOR_CMD, power);

  bool result = true;
  if (numberOfClicks > 0) {
    DPRINTSLN(DBG_MOTOR_CMD, "Turning forward ");
    result = theMotors[motor - 1]->TurnForward((uint16_t) numberOfClicks, power);
  }
  if (numberOfClicks < 0) {
    DPRINTSLN(DBG_MOTOR_CMD, "Turning backwards ");
    result = theMotors[motor - 1]->TurnBackwards((uint16_t) - numberOfClicks, power);
  }

  RF_OPEN_CMD_RESPONSE(RF_Stream);

  if (result) {
    *RF_Stream << (int) CansatCmdResponseType::MotorMovementOver << ','
               << motor << ",Turned " << numberOfClicks << " clicks ok";
  }
  else {
    *RF_Stream << (int) CansatCmdResponseType::MotorMovementFailed << ','
               << motor << ",Failure in turning during " << numberOfClicks << " clicks";
  }
  RF_CLOSE_CMD_RESPONSE(RF_Stream);
}

void IcarusCanCommander::processReq_ZeroPosition (char* &nextCharAddress) {

  theMotors[0]->ZeroPosition();
  theMotors[1]->ZeroPosition(); {
    RF_OPEN_CMD_RESPONSE(RF_Stream);
    *RF_Stream << (int) CansatCmdResponseType::PositionSetToZero << ','
               << ",Motors position set to zero";
    RF_CLOSE_CMD_RESPONSE(RF_Stream);
  }
}


bool IcarusCanCommander::processProjectCommand(CansatCmdRequestType requestType,
    char* cmd) {
  DPRINTS(DBG_READ_MSG, "process IcarusProjectCommand ");
  bool result = true;

  switch (requestType) {
    case CansatCmdRequestType::TurnMotor:
      processReq_TurnMotor(cmd);
      break;
    case CansatCmdRequestType::ZeroPosition:
      processReq_ZeroPosition(cmd);
      break;
    default:
      // Do not report error here: this is done by the superclass.
      result = false;
  } // Switch
  return result;
}
