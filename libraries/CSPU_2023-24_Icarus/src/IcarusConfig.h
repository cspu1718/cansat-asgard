#pragma once
#include "CansatConfig.h"
#ifdef NOT_DEFINED_HERE_ANYMORE_SEE_CANSAT_CONFIG_H
#undef TEST_IN_400 // Define for integration test only
// -------
// From this line constants should be moved to CansatConfig.h, once the flight control
// feature is factorised in a generic class.
constexpr unsigned int CansatMaxDelayFromTakeoffToEjection = 20000;
          /**< Over this limit in msec, the can is assumed to be outside the rocket. */
constexpr uint8_t CansatVelocityAveragingPeriod = 2; /**< sec*/
constexpr float CansatVelocityThresholdForTakeoff = -2.00;
          /**< m/s positive when falling but during the takeoff
           *   we are going up so it is negative*/


constexpr uint32_t CansatMinDelayBetweenFlightPhaseChange=500;   // msec. The minimum duration between 2 successive phase detection,
                              // to avoid that a couple of aberrant readings or high-frequency changes
                              // would cause oscillations between flight phases.
constexpr uint32_t CansatMinDelayBeforeFlightControl=10000;// msec The minimum delay between the startup of the processor
                              // and the startup of the flight control process. This delay does
                              // not apply to the calculation of the flight status and phase.
#ifdef TEST_IN_400
constexpr float CansatMinVelocityDescentLimit=-3;        // m/s The minimum descent velocity from which the can is assumed
                              // to be descending.
constexpr float CansatVelocityLowerSafetyLimit=-3;       // m/s The minimum descent velocity the can should always keep
                                  // when it is descending (ie. descent velocity > CansatVelocityDescentLimit).
constexpr float CansatVelocityUpperSafetyLimit=16;      // m/s The maximum descent velocity the can should always keep,
                                  // when it is descending (ie. descent velocity > CansatVelocityDescentLimit).
#else
constexpr float CansatMinVelocityDescentLimit=2;        // m/s The minimum descent velocity from which the can is assumed
                              // to be descending.
constexpr float CansatVelocityLowerSafetyLimit=5;       // m/s The minimum descent velocity the can should always keep
                                  // when it is descending (ie. descent velocity > CansatVelocityDescentLimit).
constexpr float CansatVelocityUpperSafetyLimit=16;      // m/s The maximum descent velocity the can should always keep,
                                  // when it is descending (ie. descent velocity > CansatVelocityDescentLimit).
#endif
constexpr uint8_t CansatUndefinedFlightPhase=15;      // Conventional value for the flight phase when
                              // the can is not in nominal descent mode.
#ifdef TEST_IN_400
constexpr uint8_t CansatNumFlightPhases=3;            // number of flight phases defined. Valid range: 1-14 (15=no phase).
#else
constexpr uint8_t CansatNumFlightPhases=5;            // number of flight phases defined. Valid range: 1-14 (15=no phase).
#endif
/* Tables below define CansatNumFlightPhases altitudes.
 * Altitudes must be decreasing from index 0
 * Phase 0 is defined when altitude is above CansatFlightPhaseAltitude[0]
 * For 0 < i < CansatNumFlightPhases:
 * Phase i is defined when altitude is above CansatFlightPhaseAltitude[i]
 * and below CansatFlightPhaseAltitude[i-1].
 * Below altitude CansatFlightPhaseAltitude[CansatNumFlightPhases-1], ground condition is assumed.
 * Example:
 *   CansatFlightPhaseAltitude[CansatNumFlightPhases]= { 800, 650, 500, 350, 200};
 * Results in the phases:
 *    0 above (or at) 800 m,
 *    1 in range ]800;650] m
 *    2 in range ]650;500] m
 *    3 in range ]500;350] m
 *    4 in range ]350;200] m
 *    Ground conditions under 200m.
 */
 
#ifdef TEST_IN_400
constexpr uint16_t CansatFlightPhaseAltitude[CansatNumFlightPhases]= { 13, 8, 3};
#else
constexpr uint16_t CansatFlightPhaseAltitude[CansatNumFlightPhases]= { 800, 650, 500, 350, 200};
#endif
constexpr float CansatMinAltitudeForFlightControl=CansatFlightPhaseAltitude[CansatNumFlightPhases-1];
    /**< m The altitude below which "ground conditions"
      (i.e before take-off or after landing) are assumed. */

// --- END of constats to be moved to CansatConfig.h in a future version.
#endif // NOT_DEFINED_HERE_ANYMORE_SEE_CANSAT_CONFIG_H

constexpr uint8_t Power = 255;  // Power for motor movement (0-255). Less than 80
							   // does not move the motor, with more than 80 clicks are not
							   // reliably detected.
constexpr uint16_t MaxDelayBetweenClicksInMsec = 1000;

constexpr uint8_t Forward_Pin_M1 = 5; // input A2 on DRV.
constexpr uint8_t Reverse_Pin_M1 = 6; // input A1 on DRV.
constexpr uint8_t clickPinNumber_M1 = 12; //use 9 when working on the Icarus reference circuit

constexpr uint8_t Forward_Pin_M2 = A1; // input B1 on DRV.
constexpr uint8_t Reverse_Pin_M2 = A2; // input B2 on DRV.
constexpr uint8_t clickPinNumber_M2 = 9; //use 12 when working on the Icarus reference circuit

#ifdef TEST_IN_400
constexpr int8_t target_PositionM1[CansatNumFlightPhases] = {2, 0, 4};
constexpr int8_t target_PositionM2[CansatNumFlightPhases] = {0, 2, 0};
#else
constexpr int8_t target_PositionM1[CansatNumFlightPhases] = {6, 0, 12, 0, 6};
constexpr int8_t target_PositionM2[CansatNumFlightPhases] = {0, 6, 0, 12, 6};
#endif
constexpr uint8_t MaxClickDuringLoop = 3;
constexpr uint16_t DelayAfterZero = 5000; // msec
constexpr uint16_t DelayAfterTarget = 1000; // msec
