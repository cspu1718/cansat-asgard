/*
 * IcarusCSPU.h
 * 
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the library should include a tag @ingroup IcarusCSPU
 * in the class documentation block.
 */

 /** @defgroup IcarusCSPU IcarusCSPU library
 *  @brief The library of classes specific to the CanSat 2023/2024 Icarus CSPU project.
 *  
 *  The IcarusCSPU library contains all the code for the IcarusCSPU project, which is assumed
 *  not to be reused across project
 *  
 *  _Dependencies_\n
 *  This library requires the following other generic libraries (in addition to hardware-specific libraries,
 *  and standard Arduino libraries, which are not listed here):
 *  - DebugCSPU
 *  - TimeCSPU
 *  - elapsedMillis
 *  - cansatAsgardCSPU
 *  @todo TO BE COMPLETED.
 *  
 *  
 *  _History_\n
 *  The library was created by the 2023/2024 Cansat team (IcarusCSPU) based on the IcarusCSPU, GMiniCSPU
 *  TorusCSPU, IsatisCSPU, IsaTwoCSPU and SpySeeCanCSPU libraries.
 */
