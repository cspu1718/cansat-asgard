/*
   IcarusCanCommander.h
*/

#pragma once
#include "RT_CanCommander.h"
#include "DC_MotorWithFeedback.h"
#include "IcarusAcquisitionProcess.h"


/** @ingroup IcarusCSPU
    @brief A subclass of RT_CanCommander adding support for the Icarus-specific commands
    as specified in CansatInterface.h
*/
class IcarusCanCommander : public RT_CanCommander<IcarusAcquisitionProcess> {
  public:
    /** @brief Constructor
          @param theTimeOut The duration (in milliseconds) for the Command mode to time out (and switch back to acquisition).
    */
    IcarusCanCommander(unsigned long int theTimeOut);
    virtual ~IcarusCanCommander() {};

#ifdef RF_ACTIVATE_API_MODE
    /** @brief Method used to initialize the object when using the RF API mode.
        @param xbeeClient The interface to the XBee module to use for output .
        @param theSd Initialized object of type SdFat that will be used by the class for various Sd Card related actions.
        @param theProcess Initialized object of type AcquisitionProcess used for interaction with it by the class when handling commands.

    */
    void begin(CansatXBeeClient& xbeeClient, SdFat* theSd = NULL, IcarusAcquisitionProcess* theProcess = NULL);
#else
    /** @brief Method used to initialize various pointers.
        @param RF_Stream The output stream used by the class.
        @param theSd Initialized object of type SdFat that will be used by the class for various Sd Card related actions.
        @param theProcess Initialized object of type AcquisitionProcess used for interaction with it by the class when handling commands.

    */
    void begin(Stream& RF_Stream, SdFat* theSd = NULL, IcarusAcquisitionProcess* theProcess = NULL);
#endif


  protected:
    /** Process a project specific command request while in command mode.
        This method implements Icarus-specific commands.
       @param requestType the request type value
       @param cmd Pointer to the first character after the command type. Command parameter can be parsed from
              this position which should point to a separator if any parameter is present, or to the final '\0'
              if none is provided.
       @return True if the command was processed, false otherwise.
    */
    virtual bool processProjectCommand(CansatCmdRequestType requestType, char* cmd)  override;
    /** Process the request to move the motors. */
    void processReq_TurnMotor (char* &nextCharAddress);
    /** Process the request to set the motors to zero position. */
    void processReq_ZeroPosition (char* &nextCharAddress);
  private:
    DC_MotorWithFeedback* theMotors[2];
};
