#pragma once
#include "CansatRecord.h"
#include "CansatInterface.h"

#ifndef INCLUDE_FLIGHT_ANALYSIS_DATA
#error "IcarusRecord requires flight analysis data to be part of CansartRecord. Define INCLUDE_FLIGHT_ANALYSIS_DATA in CansatConfig.h"
#endif

/** @ingroup IcarusCSPU
    @brief The record carrying all data acquired or computed by the Icarus CanSat
*/
class IcarusRecord : public CansatRecord {
  public:
    int16_t motor1_Position;
    int16_t motor2_Position;
    

       
  protected:
    virtual void printCSV_SecondaryMissionData(Stream &str, bool startWithSeparator, bool finalSeparator) const;
    virtual void printCSV_SecondaryMissionHeader(Stream &str, bool startWithSeparator, bool finalSeparator) const;
    virtual void clearSecondaryMissionData();
    virtual uint16_t getSecondaryMissionMaxCSV_Size() const;
    virtual uint16_t getSecondaryMissionCSV_HeaderSize() const;
    virtual void printSecondaryMissionData(Stream& str) const;
    virtual uint8_t writeBinarySecondaryMissionData(uint8_t* const destinationBuffer, uint8_t bufferSize) const;
    virtual uint8_t readBinarySecondaryMissionData(const uint8_t* const sourceBuffer, const uint8_t bufferSize);
    virtual uint8_t getBinarySizeSecondaryMissionData() const;
        friend class IcarusRecord_Test; 



    };
