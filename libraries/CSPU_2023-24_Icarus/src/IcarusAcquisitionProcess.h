/*
   IcarusAcquisitionProcess.h
*/

#pragma once
#include "CansatAcquisitionProcess.h"
#include "IcarusSecondaryMissionController.h"
#include "DC_MotorWithFeedback.h"
#include "IcarusSecondaryMissionController.h"


/** @ingroup IcarusCSPU
    @brief the Acquisition process for the Icarus project
*/
class IcarusAcquisitionProcess: public CansatAcquisitionProcess<IcarusRecord, IcarusSecondaryMissionController> {
  public:
    IcarusAcquisitionProcess() {};
    virtual ~IcarusAcquisitionProcess() {};


    /** Obtain the Motor object managed by the secondary mission controller */
    DC_MotorWithFeedback* getMotor(uint8_t motorID) {
      return this->getSecondaryMissionController().getMotor(motorID);
    };

  protected:
    /** @name Protected methods overridden from base class.
        See detailed description in base classes.
        @{   */

    virtual void doIdle() override  {
      CansatAcquisitionProcess::doIdle();
      // do our own background tasks here, if any
    };

    /** @} */

  private:
    /** @name Private methods overridden from base class.
        See detailed description in base classes.
        @{   */

    virtual void initSpecificProject () override {
      // Perform specific project initialisation here
    };

    virtual void acquireSecondaryMissionData(CansatRecord & /*record*/) override {
      // Acquire secondary mission data here, if required.
    } ;
    /** @} */
};
