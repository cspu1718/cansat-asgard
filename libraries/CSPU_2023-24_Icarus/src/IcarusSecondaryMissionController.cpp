/*
   IcarusSecondaryMissionController.cpp
*/

// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "IcarusSecondaryMissionController.h"
#include "elapsedMillis.h"
#define DBG_VELOCITY_BUFFER 0
#define DBG_VELOCITY 0
#define DBG_EJECTION 0
#define DBG_FLIGHT_CTRL_CODE 0
#define DBG_FLIGHT_CTRL_CODE_DISCARDED 0
#define DBG_FLIGHT_PHASE_EVALUATION 0
#define DBG_STATE 0
#define DBG_POSITIVE 0
#define DBG_NEGATIVE 0
#define DBG_MOTORS 0

#undef DBG_AIRBRAKES_CHANGE

void IcarusSecondaryMissionController::printVelocityTable() {
  for (int i = 0; i < NumberOfVelocitySamples; i++)
    Serial << "  " << i << ": " << velocityTable[i] << ENDL;
}

void IcarusSecondaryMissionController::analyseDescentVelocity(const IcarusRecord& rec) {
#if (DBG_VELOCITY_BUFFER == 1)
  printVelocityTable();
#endif
#if (DBG_VELOCITY == 1)
  Serial << "BEFORE: ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ", lastTs=" << lastRecordTimestamp << ENDL;
#endif
  if (rec.descentVelocity == BMP_Client::InvalidDescentVelocity) {
#if (DBG_VELOCITY == 1)
    Serial << "INVALID VELOCITY DETECTED velocity= " <<  rec.descentVelocity << ENDL;
#endif
    clearAverageVelocity();
    lastRecordTimestamp = rec.timestamp;
    return;
  }
  if ((rec.timestamp - lastRecordTimestamp) > (3 * CansatAcquisitionPeriod)) {
    clearAverageVelocity();
#if (DBG_VELOCITY == 1)
    Serial << "INVALID REC DETECTED ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ENDL;
#endif
    lastRecordTimestamp = rec.timestamp;
    return;
  }

  if (rec.timestamp < lastRecordTimestamp) {
    clearAverageVelocity();

#if (DBG_VELOCITY == 1)
    Serial << "INVALID PAST RECORD DETECTED ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ENDL;
#endif
  }
  averageVelocity = averageVelocity + (rec.descentVelocity - velocityTable[currentVelocityIndex]) / NumberOfVelocitySamples;
  velocityTable[currentVelocityIndex] = rec.descentVelocity;
  currentVelocityIndex++;
  if (currentVelocityIndex == NumberOfVelocitySamples) {
    currentVelocityIndex = 0;
    DPRINTSLN(DBG_VELOCITY, "Index reset"); //
    velocityAverageValid = true;
  }
  lastRecordTimestamp = rec.timestamp;
#if (DBG_VELOCITY_BUFFER == 1)
  Serial << "AFTER: ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ENDL;
  printVelocityTable();
#endif

  // Update take-off status
  if ((!takeoffHappened) && velocityAverageValid && (averageVelocity  < CansatVelocityThresholdForTakeoff )) {
    DPRINTLN(DBG_EJECTION, takeoffHappened);
    DPRINTLN(DBG_EJECTION, velocityAverageValid);
    DPRINTLN(DBG_EJECTION, averageVelocity);
    DPRINTLN(DBG_EJECTION, CansatVelocityThresholdForTakeoff);
    takeoffHappened = true;
    takeoffTimestamp = rec.timestamp;
#ifdef RF_ACTIVATE_API_MODE
    CansatXBeeClient *xbee = getXbeeClient();
    if (xbee != nullptr) {
      RF_OPEN_STRING(xbee);
      *xbee << millis() << ": 2-ary: Take off detected!";
      RF_CLOSE_STRING(xbee);
    }
#endif
    DPRINTS(DBG_EJECTION, "takeoff : timestamp set to ");
    DPRINT(DBG_EJECTION, takeoffTimestamp);
    DPRINTS(DBG_EJECTION, ", threshold (m/s) ");
    DPRINTLN(DBG_EJECTION, CansatVelocityThresholdForTakeoff);
  }

  // Update ejection status
  if ((!ejectionHappened) && takeoffHappened) {
    if ((rec.timestamp - takeoffTimestamp) > CansatMaxDelayFromTakeoffToEjection) {
      ejectionHappened = true;
#ifdef RF_ACTIVATE_API_MODE
      CansatXBeeClient *xbee = getXbeeClient();
      if (xbee != nullptr) {
        RF_OPEN_STRING(xbee);
        *xbee << millis() << ": 2-ary: ejection assumed!";
        RF_CLOSE_STRING(xbee);
      }
#endif
    }
  } // update ejection status.
}

void IcarusSecondaryMissionController::updateFlightStatus(const IcarusRecord &record) {
  // Changed too recently: do not change status.
  if (record.timestamp - lastFlightStatusUpdate < CansatMinDelayBetweenFlightPhaseChange) {
    DPRINTS(DBG_FLIGHT_CTRL_CODE_DISCARDED, "Upd flight status: DISCARDED (too recent), keep status ")
    DPRINTLN(DBG_FLIGHT_CTRL_CODE_DISCARDED, (int)  currentFlightStatus);
    return;
  }

  // Actually evaluate the flight status.
  lastFlightStatusUpdate = record.timestamp;
  shouldUpdateFlightPhase = true;
  float relativeAltitude = record.altitude - record.refAltitude;

  // ground conditions
  if (relativeAltitude < CansatMinAltitudeForFlightControl) {
    currentFlightStatus = FlightControlCode::GroundConditions;
  }

  // not descending
  else if (record.descentVelocity < CansatMinVelocityDescentLimit)  {
    currentFlightStatus = FlightControlCode::NotDescending;
  }

  // if below last phase altitude, we are in ground conditions
  else if (relativeAltitude < CansatFlightPhaseAltitude[CansatNumFlightPhases - 1]) {
    currentFlightStatus = FlightControlCode::GroundConditions;
  }

  // too fast
  else if (record.descentVelocity > CansatVelocityUpperSafetyLimit) {
    currentFlightStatus = FlightControlCode::HighDescentVelocityAlert;
  }

  // too slow
  else if (record.descentVelocity < CansatVelocityLowerSafetyLimit) {
    currentFlightStatus = FlightControlCode::LowDescentVelocityAlert;
  }

  // nominal descent
  else currentFlightStatus = FlightControlCode::NominalDescent;

#ifdef DBG_FLIGHT_CTRL_CODE
  static FlightControlCode previousStatus = FlightControlCode::StartingUp;
  if (currentFlightStatus != previousStatus) {
    Serial << "2-ary: flightStatus changed to " << (int)currentFlightStatus  << ENDL;
    previousStatus = currentFlightStatus;
  }
#endif
} // updateFlightStatus

void IcarusSecondaryMissionController::updateFlightPhase(const IcarusRecord& record) {
  // (No binary search implementation as there will never be more than 14 phases)

  // If flight phase shouldn't be updated, don't do anything
  if (!shouldUpdateFlightPhase) {
    return;
  }

  // If not in nominal descent, flight phase is undefined.
  if (currentFlightStatus != FlightControlCode::NominalDescent) {
    currentFlightPhase = CansatUndefinedFlightPhase;
  }
  else {
    // Evaluate phase
    float relativeAltitude = record.altitude - record.refAltitude;
    for (int i = 0; i < CansatNumFlightPhases; i++) {
      if (relativeAltitude >= CansatFlightPhaseAltitude[i]) {
        currentFlightPhase = i;
        DPRINTS(DBG_FLIGHT_PHASE_EVALUATION, "Current evaluated flight phase: ");
        DPRINTLN(DBG_FLIGHT_PHASE_EVALUATION, currentFlightPhase);
        break;
      }
    }

    // This check is included as a safety in case no phase was found previously:
    // it should never actually come here because this check is already performed in the
    // method updateFlightStatus().
    if (record.altitude - record.refAltitude < CansatFlightPhaseAltitude[CansatNumFlightPhases - 1]) {
      currentFlightPhase = CansatUndefinedFlightPhase;
      DPRINTSLN(DBG_DIAGNOSTIC, "WARNING: No flight phase could be found by SecondaryMissionController::updateFlightPhase() although in nominal descent. Setting flight phase to 15 and controller info to GroundConditions.");
    }
  }

  // Reset shouldUpdateFlightPhase property
  shouldUpdateFlightPhase = false;
}

void IcarusSecondaryMissionController::manageMotors(IcarusRecord &/*record*/)
{
  DPRINTS(DBG_STATE, "MngMotors, state=");
  DPRINTLN(DBG_STATE, (int) motorState);

  // When this method is called, the flight status and flight status are updated.
  switch (currentFlightStatus) {
    case FlightControlCode::GroundConditions:
    case FlightControlCode::NotDescending:
    case FlightControlCode::NoData:
    case FlightControlCode::StartingUp:
    case FlightControlCode::LowDescentVelocityAlert:
    case FlightControlCode::HighDescentVelocityAlert:
      moveMotorsOneStepTowardZero();
      break;

    case FlightControlCode::NominalDescent:
      if (currentFlightPhase != CansatUndefinedFlightPhase) {
        manageMotorsInNominalDescent();
      }
      break;
    default:
      DPRINTS(DBG_DIAGNOSTIC, "Error: unexpected flight status: ");
      DPRINTLN(DBG_DIAGNOSTIC, (int) currentFlightStatus);
  }
}

void IcarusSecondaryMissionController::manageMotorsInNominalDescent() {
  int16_t motor1_Position = theMotor1->getPosition();
  int16_t motor2_Position = theMotor2->getPosition();
  uint16_t numberOfClicks;

  bool motor1ok, motor2ok;

  DPRINTS(DBG_MOTORS, "nomDescent: motors positions = ");
  DPRINT(DBG_MOTORS, motor1_Position);
  DPRINTS(DBG_MOTORS, ",");
  DPRINTLN(DBG_MOTORS, motor2_Position);
  DPRINTS(DBG_STATE, "nominal, flight phase defined = ")
  DPRINTLN(DBG_STATE, currentFlightPhase);
  switch (motorState) {
    case MotorState::TOTARGET:
      motor1ok = (motor1_Position == target_PositionM1[currentFlightPhase]);
      motor2ok = (motor2_Position == target_PositionM2[currentFlightPhase]);
      if (!motor1ok) {
        DPRINTSLN(DBG_MOTORS, "motor1 not ok");
        int delta = target_PositionM1[currentFlightPhase] - motor1_Position;
        DPRINTS(DBG_MOTORS, "delta=");
        DPRINTLN(DBG_MOTORS, delta);
        if (delta > 0) {
          DPRINTSLN(DBG_POSITIVE, "Positive position delta for the motor 1");
          numberOfClicks = (delta >= MaxClickDuringLoop ? MaxClickDuringLoop : delta);
          theMotor1->TurnForward((uint16_t) numberOfClicks, Power);
        } else {
          DPRINTSLN(DBG_NEGATIVE, "Negative position delta for the motor 1");
          numberOfClicks = (-delta >= MaxClickDuringLoop ? MaxClickDuringLoop : -delta);
          theMotor1->TurnBackwards((uint16_t) numberOfClicks, Power);
        }
        return;
      }

		if (!motor2ok) {
			DPRINTSLN(DBG_MOTORS, "motor2 not ok");
			int delta = target_PositionM2[currentFlightPhase] - motor2_Position;
			if (delta > 0) {
				DPRINTSLN(DBG_POSITIVE, "Positive position delta for the motor 2");
				numberOfClicks = (delta >= MaxClickDuringLoop ? MaxClickDuringLoop : delta);
				theMotor2->TurnForward((uint16_t) numberOfClicks, Power);
			} else {
				DPRINTSLN(DBG_NEGATIVE, "Negative position delta for the motor 2");
				numberOfClicks = (-delta >= MaxClickDuringLoop ? MaxClickDuringLoop : -delta);
				theMotor2->TurnBackwards((uint16_t) numberOfClicks, Power);
			}
		}
		if (motor1ok && motor2ok) {
			DPRINTSLN(DBG_STATE,"switching to WAITAFTERTARGET");
			motorState = MotorState::WAITAFTERTARGET;
			motorTimer = 0;
		}
		break;

	case MotorState::TOZERO:
		moveMotorsOneStepTowardZero();
		if ((theMotor1->getPosition() == 0) && (theMotor2->getPosition() == 0)) {
			DPRINTSLN(DBG_STATE,"switching to WAITAFTER0");
			motorState = MotorState::WAITAFTER0;
			motorTimer=0;
		}
		break;

	case MotorState::WAITAFTER0:
		if (motorTimer > DelayAfterZero) {
			DPRINTSLN(DBG_STATE,"switching to TOTARGET");
			motorState = MotorState::TOTARGET;
		}
		break;

	case MotorState::WAITAFTERTARGET:
		if (motorTimer > DelayAfterTarget) {
			DPRINTSLN(DBG_STATE,"switching to TOZERO");
			motorState = MotorState::TOZERO;
		}
		break;
	default:
		DPRINTS(DBG_DIAGNOSTIC, "Error: unexpected motor state: ");
		DPRINTLN(DBG_DIAGNOSTIC, (int) motorState);
	} // switch
} // method


void IcarusSecondaryMissionController::moveMotorsOneStepTowardZero() {
  int numberOfClicks;
  int16_t motor1_Position = theMotor1->getPosition();
  int16_t motor2_Position = theMotor2->getPosition();
  DPRINTS(DBG_MOTORS, "motors positions = ");
  DPRINT(DBG_MOTORS, motor1_Position);
  DPRINTS(DBG_MOTORS, ",");
  DPRINTLN(DBG_MOTORS, motor2_Position);
  if (motor1_Position > 0) {
    DPRINTSLN(DBG_POSITIVE, "Positive motor 1 position");
    if (motor1_Position >= MaxClickDuringLoop) {
      numberOfClicks = MaxClickDuringLoop;
    }
    else {
      numberOfClicks = motor1_Position;
    }
    theMotor1->TurnBackwards((uint16_t) numberOfClicks, Power);
  }
  if (motor1_Position < 0) {
    DPRINTSLN(DBG_POSITIVE, "Negative motor 1 position");
    if (-motor1_Position >= MaxClickDuringLoop) {
      numberOfClicks = MaxClickDuringLoop;
    }
    else {
      numberOfClicks = -motor1_Position;
    }
    theMotor1->TurnForward((uint16_t) numberOfClicks, Power);
  }

  if (motor2_Position > 0) {
    DPRINTSLN(DBG_POSITIVE, "Positive motor 2 position");
    if (motor2_Position >= MaxClickDuringLoop) {
      numberOfClicks = MaxClickDuringLoop;
    }
    else {
      numberOfClicks = motor2_Position;
    }
    theMotor2->TurnBackwards((uint16_t) numberOfClicks, Power);
  }
  if (motor2_Position < 0) {
    DPRINTSLN(DBG_NEGATIVE, "Negative motor 2 position");
    if (-motor2_Position >= MaxClickDuringLoop) {
      numberOfClicks = MaxClickDuringLoop;
    }
    else {
      numberOfClicks = -motor2_Position;
    }
    theMotor2->TurnForward((uint16_t) numberOfClicks, Power);
  }
}

void IcarusSecondaryMissionController::manageSecondaryMission(IcarusRecord & record)
{
  // ----- Next instructions to be moved to the super class $$$$
  static uint32_t counter  = 0;
  counter++;
  IcarusRecord& myIcarusRecord = record;

  if (counter > StartupDelayForFlightAnalysis / CansatAcquisitionPeriod) {
    // Before this delay, it is assumed that the BMP measures are not stabilized.
    analyseDescentVelocity(myIcarusRecord);
    updateFlightStatus(myIcarusRecord);
    updateFlightPhase(myIcarusRecord);
  }

  myIcarusRecord.flightControlCode = currentFlightStatus;
  myIcarusRecord.flightPhase = currentFlightPhase;

  if (counter >  CansatMinDelayBeforeFlightControl / CansatAcquisitionPeriod) {

    // ---------- this block of instructions in subclass -----
    manageMotors(myIcarusRecord);
    // Add motors position in record
    myIcarusRecord.motor1_Position = theMotor1->getPosition();
    myIcarusRecord.motor2_Position = theMotor2->getPosition();
  }
}

void IcarusSecondaryMissionController::clearAverageVelocity() {
  for (int i = 0; i < NumberOfVelocitySamples; i++) {
    velocityTable[i] = 0;
  }
  averageVelocity = 0;
  velocityAverageValid = false;
}

bool IcarusSecondaryMissionController::initIcarus() {
  takeoffHappened = false;
  takeoffTimestamp = 0;
  DRV8833_HBridgeMotor * theDC_Motor = new DRV8833_HBridgeMotor;
  DC_MotorWithFeedback * motorWithFeedback = new DC_MotorWithFeedback;

  if ((motorWithFeedback == nullptr) || (theDC_Motor == nullptr)) {
    DPRINTSLN(DBG_DIAGNOSTIC, "*** ERROR ALLOCATING Motor1");
    return false;
  }

  bool result;
  result = theDC_Motor->begin(Forward_Pin_M1, Reverse_Pin_M1);
  if (result == false) {
    DPRINTSLN(DBG_DIAGNOSTIC, "*** ERROR in begin DRVmotor1");
    return false;
  }
  result = motorWithFeedback->begin(*theDC_Motor, clickPinNumber_M1, MaxDelayBetweenClicksInMsec);
  if (result == false) {
    DPRINTSLN(DBG_DIAGNOSTIC, "*** ERROR in begin motor1 with feedback");
    return false;
  }
  theMotor1 = motorWithFeedback;

  theDC_Motor = new DRV8833_HBridgeMotor;
  motorWithFeedback = new DC_MotorWithFeedback;
  if ((motorWithFeedback == nullptr) || (theDC_Motor == nullptr)) {
    DPRINTSLN(DBG_DIAGNOSTIC, "*** ERROR ALLOCATING Motor2");
    return false;
  }

  result = theDC_Motor->begin(Forward_Pin_M2, Reverse_Pin_M2);
  if (result == false) {
    DPRINTSLN(DBG_DIAGNOSTIC, "*** ERROR in begin DRVmotor2");
    return false;
  }
  result = motorWithFeedback->begin(*theDC_Motor, clickPinNumber_M2, MaxDelayBetweenClicksInMsec);
  if (result == false) {
    DPRINTSLN(DBG_DIAGNOSTIC, "*** ERROR in begin motor2 with feedback");
    return false;
  }
  theMotor2 = motorWithFeedback;
  motorState = MotorState::WAITAFTER0; // We assume the original position is 0 for both motors.
  motorTimer = 0;
  clearAverageVelocity();

  return true;
}

#ifdef RF_ACTIVATE_API_MODE
bool IcarusSecondaryMissionController::begin(CansatXBeeClient* xbeeClient) {
  if (!SecondaryMissionController<IcarusRecord>::begin(xbeeClient)) return false;
  return initIcarus();
}
#else
bool IcarusSecondaryMissionController::begin(Stream* RF_Stream) {
  if (!SecondaryMissionController::begin(RF_Stream)) return false;
  return initIcarus();
}
#endif


float IcarusSecondaryMissionController::getAverageVelocity() {
  if (velocityAverageValid == true) {
    return averageVelocity;
  }
  else {
    return InvalidVelocity;
  }
}

bool IcarusSecondaryMissionController::takeoffDetected() {
  return takeoffHappened;
}

void IcarusSecondaryMissionController::printInitialisationInfo(Stream& stream) {
	// NB: part (of all) of this should also be transmitted by radio.
	SecondaryMissionController<IcarusRecord>::printInitialisationInfo();
	stream << "Initialization of Icarus 2-ary Mission Controller:" << ENDL;
	stream << "  Flight analysis parameters:" << ENDL;
	stream << "    Delay take-off to ejection: " << CansatMaxDelayFromTakeoffToEjection << " msec" << ENDL;
	stream << "    Velicity average period: " << CansatVelocityAveragingPeriod  << " sec" << ENDL;
	stream << "    Velocity threshold for take-off: " << CansatVelocityThresholdForTakeoff << " m/s" << ENDL;
	stream << "    Min. delay between flight phase change: " << CansatMinDelayBetweenFlightPhaseChange << " msec"  << ENDL;
	                              // would cause oscillations between flight phases.
	stream << "    Min. delay before flight control: " << CansatMinDelayBeforeFlightControl << " msec"  << ENDL;
	stream << "    Min. desc. velocity: " <<  CansatMinVelocityDescentLimit << " m/s" << ENDL;
	stream << "    Desc. velocity min. safety limit: " <<  CansatVelocityLowerSafetyLimit << " m/s" << ENDL;
	stream << "    Desc. velocity max. safety limit: " <<  CansatVelocityUpperSafetyLimit << " m/s" << ENDL;
	stream << "    Min. altitude for flight ctrl: " << CansatMinAltitudeForFlightControl << ENDL;
	stream << "    Flight phases: " << ENDL;
	uint16_t maxAltitude = 1500;
	for (int i = 0; i < CansatNumFlightPhases; i++) {
		stream << "      " << i << ": from " << maxAltitude << " downto " << CansatFlightPhaseAltitude[i]
			   << " m" << ENDL;
		maxAltitude = CansatFlightPhaseAltitude[i];
	}

	stream << "  Icarus-specific config:" << ENDL;
	stream << "    Motor power: " << Power << ENDL;
	stream << "    Max. delay between clicks: " << MaxDelayBetweenClicksInMsec << " msec" << ENDL;
	stream << "    Max nbr clicks/loop: " <<  MaxClickDuringLoop << ENDL;
	stream << "    Delay after 0: " << DelayAfterZero << " msec" << ENDL;
	stream << "    Delay after target: " << DelayAfterTarget << " msec" << ENDL;
	stream << "    Motor targets in phases:" << ENDL;
	for (int i = 0; i < CansatNumFlightPhases; i++) {
		stream << "      Ph." << i << ": " << target_PositionM1[i] << ", " << target_PositionM2[i] << ENDL;
	}
}
