/*
   IMU classes configuration
*/

#pragma once

#include "CansatConfig.h"

// ************************************************************************
// **** Sensors Stable settings (see calibration info on top of file) *****
// ************************************************************************

#define NXP_A 'A'							// IMU ids (use characters for readability
#define NXP_B 'B'
#define LSM_A 'a'
#define LSM_B 'b'
#define LSM_C 'c'							// The operational one.
// WARNING : complete below if adding new values !

#define IMU_REFERENCE	LSM_C 				// Valid values: one of the defines above

#define AHRS_FUSION_MADGWICK 'M'
#define AHRS_FUSION_MAHONY	 'm'
#define AHRS_FUSION_ALGORITHM AHRS_FUSION_MADGWICK // Valid values: one of the defines above

