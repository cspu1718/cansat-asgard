/*
 * CSPU_Test.cpp
 *
 */

#define DEBUG_CSPU
#include "CSPU_Debug.h"
#include "CSPU_Test.h"
#include "elapsedMillis.h"

void CSPU_Test::requestCheck(const char* msg,
			uint16_t &numErrors,
			bool separatorFirst) {
  char answer = ' ';
  if (separatorFirst) {
    Serial << ENDL;
  }
  while (Serial.available() > 0) {
    Serial.read();
  }

  while ((answer != 'y') && (answer != 'n')) {
    Serial << ENDL << msg << F(". Is this ok (y/n) ? ");
    Serial.flush();
    while (Serial.available() == 0) {
      delay(300);
    }
    answer = Serial.read();
  }
  if (answer == 'n') {
    numErrors++;
  }
  Serial<< ENDL;
}

bool CSPU_Test::askYN(const char* question, bool separatorFirst) {
  char answer = ' ';
  if (separatorFirst) {
    Serial << ENDL;
  }
  while (Serial.available() > 0) {
    Serial.read();
  }

  while ((answer != 'y') && (answer != 'n')) {
    Serial << ENDL << question << F(" (y/n) ? ");
    Serial.flush();
    while (Serial.available() == 0) {
      delay(300);
    }
    answer = Serial.read();
  }
  Serial<< ENDL<<ENDL;
  return (answer == 'y');
}

uint8_t CSPU_Test::inputSingleDigitInteger(const char* question,
											uint8_t min, uint8_t max,
											bool separatorFirst) {
  char answer = ' ';
  if (separatorFirst) {
    Serial << ENDL;
  }

  while ((answer < '0'+ min) || (answer > '0' + max )) {
	while (Serial.available() > 0) {
	    Serial.read();
	}
    Serial << ENDL << question << " (" << min << '-' << max << ") ? ";
    Serial.flush();

    while (Serial.available() == 0) {
      delay(300);
    }
    answer = Serial.read();
  }
  Serial<< ENDL;
  return (answer - '0');
}

char CSPU_Test::pressAnyKey() {
	Serial << "Enter any character + 'Enter' to proceed..." <<ENDL;
	Serial.flush();
	// remove any previous character in input queue
	while (Serial.available() > 0) {
		Serial.read();
	}
	while (Serial.available() == 0) {
		delay(300);
	}
	return Serial.read();
}

char CSPU_Test::pressAnyKey(const char* msg) {
	Serial << msg << ENDL;
	return pressAnyKey();
}

char CSPU_Test::keyPressed() {
	// remove any previous character in input queue
	if (Serial.available() > 0) {
		char c= Serial.read();
		while (Serial.available()) {
			Serial.read();
			delay(300);
		}
		return c;
	} else return 0;
}

void CSPU_Test::heartBeat(uint8_t LED_Pin) {
  static elapsedMillis elapsed=0;
  if (elapsed > 500) {
    digitalWrite(LED_Pin, !digitalRead(LED_Pin));
    elapsed = 0;
  }
}



