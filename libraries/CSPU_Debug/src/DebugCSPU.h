/* 
   DebugCSPU.h
  */
 /** @file DebugCSPU.h
    @brief Main header for the Common debugging macros and utilities library.

    DebugCTIV.h includes all other header files except Timer.h, and should be the only one explicitely included,
    unless timers are used.
    Be sure to (un)define USE_ASSERTIONS and DEBUG_CSPU to include the actual features in your test executable
    (and be sure not to include them in your operation executables).

    See library documentation of usage information. 
 *******************************************************************************************/
 
 /** @file */ 
 /** @defgroup CSPU_Debug CSPU_Debug library
 *  @brief  Common debugging macros and utilities for any Arduino project.
 *  
 *  The DebugCSPU library contains various objects, macro-definitions etc to support tracing & debugging 
 *  on the serial interface, with a zero memory foot-print in the operational software.
 *  
 *  Tracing can be enabled globally by defining the DEBUG_CSPU preprocessor symbol, and enabled specifically
 *  for part of a source file by defining custom  preprocessor. Each output macro will only result in 
 *  debugging code if the associated custom symbol is defined to a non-null integer.
 *  This allows for specifically turning on or off the debugging instructions for a specific part of the 
 *  software: typically, use a symbol like DBG_XXXX for the debugging of a particular feature, possibly 
 *  even using DBG_XXXX for tracing the main steps of the feature, and DBG_XXXX_VERBOSE for generating 
 *  a more detailed output.
 *  
 *  When the main DEBUG_CSPU symbol is undefined, the memory footprint of the library is zero. This removes
 *  any need to manually strip the final code from debugging instructions (nevertheless, the library 
 *  could be the only reason to initialize the serial interface: be sure to avoid initializing it 
 *  if no debugging is enabled anywhere). 
 *  
 *  When the main DEBUG_CSPU symbol is defined, the strings passed to the various debugging macros are
 *  stored in program memory.
 *  
 *  In addition to the tracing macros, the library provides support for assertions in a way that allows 
 *  for stripping out  the overhead code completely.  It does not use  the standard features provided 
 *  by <assert.h> which result in about 120 bytes in dynamic memory due to globals as soon 
 *  as __ASSERT_USE_STDERR is defined). 
 *  
 *  When symbol USE_ASSERTIONS is defined, only minimal context information is  maintained but still
 *  results in significant overhead in program memory.  When symbol USE_ASSERTIONS is not defined, 
 *  DASSERT() results in 0 memory overhead.
 *  
 *  
 *  This library also includes:
 *  @li File VirtualMethods.h which defines macros VIRTUAL, VIRTUAL_FOR_TEST and NON_VIRTUAL. They can be 
 *  used to replace the ‘virtual’ keyword in class header files in order to have some methods virtual 
 *  in the test environment and not in the operational environment (select by defining the 
 *  USE_TEST_VIRTUAL_METHODS symbol during the build). This allows for overriding some methods in
 *  test subclasses required to simulate specific conditions, while avoiding to vtable overhead in 
 *  the final software.
 *  @li File StringStream.h which provides a very useful class for streaming into a string buffer (it is 
 *  used  e.g. to output the acquired data in CSV format for storage on the SD Card. 
 *  @li Files Timer.h and Timer.cpp which provides a utility class and macro for profiling code. 
 *  @li The NULL definition (if not provided anywhere else), since std::nullptr is not 
 *  compatible with Arduino v1.6 (see #NULL) and the NULL definition is in stddef.h, which has a significant
 *  memory footprint. 
 *  
 *  @par Usage
 *  Only include header DebugCSPU.h, which includes any other useful header except Timer.h. 
 *  If using the DINIT_WITH_[ANALOG_]PIN macro, select whatever digital or analog pin to define whether the
 *  the program should wait for the readiness of the USB serial line. This will happen only if
 *  the selected pin is pulled down. Please not that an external pull-up is not required for digital pins
 *  (internal pull-up is used) but well for an analog pin (use a 1 Mohm resistor to Vcc).
    @code
    #define USE_ASSERTIONS // Comment this line out for operational software
    #define DEBUG_CSPU
    #include "DebugCSPU.h"
    #define DBG_xxxx 0
    #define DBG_yyyy 1

    void setup() {
      DINIT(115200); // or DINIT_WITH_PIN(115200, 5) or DINIT_WITH_ANALOG_PIN(115200, A5)
      	  	  	  	 // or DINIT_WITH_TIMEOUT(115200)
      (...)
    }

    //anywhere else in your code:
    DPRINTS (DBG_xxx, "My value:");
    DPRINTLN(DBG_xxx, value);
    DASSERT(value > 5);
    @endcode
 *
 *  @remark
 *  We could not find any way to detect whether Serial.begin() has previously
 *  been called or not. This library currently assumes the Serial port has been initialized,
 *  and defines macro DINIT() even when DEBUG_CSPU is not defined.
 *  
 *  @par Technical note
 *  preprocessor symbol __FILE_NAME__ provides the full path and symbol __BASE_FILE__ 
 *  provides the file as passed to gcc, which happens to be the full path as well. As a consequence, 
 *  every single assertion consumes a significant amount of program memory (can be about 100-150 bytes 
 *  depending on the file path!). A detailed investigation concluded that gcc does not provide the base 
 *  name of the file and there is no portable compile-time solution to strip the path or even to limit 
 *  it to the last 20 characters.
 *  
 *  @par Dependencies
 *  None.
 *  
 *  
 *  @par History
 *  The library was created by the 2017-18 Cansat team (ISATIS)
 *  The library has been retested on board Adafruit Feather M0 Express by the 2018-19 Cansat team (IsaTwo)
 *  
 *  @{
 */

#pragma once

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "AssertCSPU.h"
#include "SerialStream.h"

#ifndef NULL 
/** @ingroup CSPU_Debug
  @brief Definition of NULL macro to avoid inclusion of large standard header. 

  This is required to avoid including stddef.h for the NULL definition.
  Using std::nullptr is not an option because in Arduino v1.6, the required gcc option is 
  not activated to make nullptr available in namespace std.
*/
#define NULL (void *) 0
#endif


// Call DINIT once in setup() if the serial port to the computer is used in any library
// or in any part of the sketch..
// Macros do not call functions in order to reduce memory usage.
/** @ingroup CSPU_Debug
 *  @brief Initialise the Serial port to the computer for use by this library or any other.
 *  Be aware that this macro include a loop to wait until the Serial port is actually ready.
 *  If using a board where the Serial port is not ready unless a terminal is actually connected,
 *  consider using DINIT(baudRate, activationPin) to avoid blocking when no terminal is used.
 *
 *  @par NB: The DINIT family of macroes include a DASSERT: the only reason is to make sure
 *  any sketch using DINITxxx contains at least one assertion when assertion are active.
 *  It prevents a number of link issues when the __failCSPU symbol used by assertions is not linked while other libraries will need it.
 *  @param baudRate The baud rate to use on the serial link.
 *  
 *  This macro is defined, even if symbol DEBUG_CSPU is undefined.
 */
#define DINIT(baudRate)  Serial.begin(baudRate); while (!Serial); DASSERT(Serial); Serial.println(F("Serial link OK"));
/** @ingroup CSPU_Debug
 *  @brief Initialise the Serial port to the computer for use by this library or any other
 *  and wait for readiness if the activationPin is LOW only.
 *  If using a board where the Serial port is not ready unless a terminal is actually connected,
 *  the activationPin can be used to define whether to wait for the port readiness or not. 
 *  Configuration of the digital activationPin is included.
 *  Be sure to pull activation pin up (with 10k resistor) to ensure it is HIGH when not pulled-down
 *  This is mandatory for boards like the Feather M0 Express with which the port will never be
 *  ready is no terminal is connected. 
 *  @note Unless you really need to block the program as long as the serial
 *  connection is not established, consider using DINIT_WITH_TIMEOUT.
 *  @par See DINIT documentation for the reason for the "useless" DASSERT
 *  @param baudRate The baud rate to use on the serial link.
 *  @param activationPin The digital pin used to defined whether the Serial port must be initialized or not
 *                        (initialized if connected to GND, not initialized otherwise).
 *  
 *  This macro is defined, even if symbol DEBUG_CSPU is undefined.
 */
#define DINIT_IF_PIN(baudRate, activationPin)  { \
      pinMode(activationPin, INPUT_PULLUP);    \
      Serial.begin(baudRate);                  \
      if(digitalRead(activationPin)==LOW) {    \
         while (!Serial);                      \
         DASSERT(Serial);					   \
         Serial.println(F("Serial link OK"));  \
      } else { Serial.println(F("Serial link not checked")); }}
/** @ingroup CSPU_Debug
 *  @brief Initialise the Serial port to the computer for use by this library or any other
 *  and wait for readiness if the activationPin is LOW only.
 *  If using a board where the Serial port is not ready unless a terminal is actually connected,
 *  the activationPin can be used to define whether to wait for the port readiness or not.
 *  Configuration of the analog activationPin is included.
 *  Be sure to pull activation pin up (with 10k resistor) to ensure it is HIGH when not pulled-down
 *  This is mandatory for boards like the Feather M0 Express with which the port will never be
 *  ready is no terminal is connected.
 *  @note Unless you really need to block the program as long as the serial
 *  connection is not established, consider using DINIT_WITH_TIMEOUT.
 *  @param baudRate The baud rate to use on the serial link.
 *  @param activationPin The analog pin used to defined whether the Serial port must be initialized or not
 *                        (initialized if connected to GND, not initialized otherwise).
 *
 *  This macro is defined, even if symbol DEBUG_CSPU is undefined.
 */
#define DINIT_IF_ANALOG_PIN(baudRate, activationPin) { \
      Serial.begin(baudRate);                  \
      if(analogRead(activationPin)<=500) {    \
         while (!Serial);                      \
         DASSERT(Serial);					   \
         Serial.println(F("Serial link OK"));  \
      } else { Serial.println(F("Serial link not checked")); }}

/** Initialize Serial interface and wait for at most 3 seconds
 *  until it is available. Use this in operational software which
 *  must be able to operate both with and without a computer
 *  connected: with a connection the very first characters sent
 *  on Serial will be received; without a connection, the
 *  software will run with a 3 secs delay.
 *  @param baudRate The baud rate to be used on the serial line.
 */
void CSPU_InitSerialWithTimeout(unsigned int baudRate=115200);

/** This macro is used to transparently insert an always true assertion.
 *  This is required to fix a link error and force the assertion code
 *  to be linked earlier.
 *  It is otherwise just an alias to CSPU_InitSerialWithTimeout().
 *  @par See DINIT documentation for the reason for the "useless" DASSERT
 *       The delay is only to make sure the assertion is always true, in
 *       a way that cannot be detected at compile time.
 *  @param baudRate The baud rate to use on the serial link.
 *
 */
#define DINIT_WITH_TIMEOUT(baudRate) {CSPU_InitSerialWithTimeout(baudRate); delay(1); DASSERT(millis()>=1);}

#if defined(DEBUG_CSPU) || defined(CSPU_DEBUG)
// -------- Define tracing macros -----

// Actual macros.
// Print a debugging STRING msg, if the moduleTag != 0 (no final carriage return)
// The string is not stored in dynamic memory
#define DPRINTS1(moduleTag, msg)   if(moduleTag!=0) Serial.print(F(msg));
#define DPRINTS2(moduleTag, msg,format)   if(moduleTag!=0) Serial.print(F(msg),format);

// Print a debugging STRING msg, if the moduleTag != 0 (with final carriage return)
// The string is not stored in dynamic memory
#define DPRINTSLN1(moduleTag, msg)  if(moduleTag!=0) Serial.println(F(msg));
#define DPRINTSLN2(moduleTag, msg,format)  if(moduleTag!=0) Serial.println(F(msg),format);

// Print a debugging NUMERIC value, if the moduleTag != 0 (no final carriage return)
#define DPRINT1(moduleTag, numValue)    if(moduleTag!=0) Serial.print(numValue);
#define DPRINT2(moduleTag, numValue,format)    if(moduleTag!=0) Serial.print(numValue,format);

// Print a debugging NUMERIC value, if the moduleTag != 0 (with final carriage return)
#define DPRINTLN1(moduleTag, numValue)  if(moduleTag!=0) Serial.println(numValue);
#define DPRINTLN2(moduleTag, numValue,format)  if(moduleTag!=0) Serial.println(numValue,format);

// Wait for x msec if the moduleTag != 0  is active.
#define DDELAY(moduleTag,durationInMsec)    if(moduleTag!=0) delay(durationInMsec);

// Print free ram if the moduleTag != 0
#define DFREE_RAM(moduleTag) if(moduleTag!=0)  { Serial.print(F("Free RAM: ")); Serial.print(freeRam()); Serial.println(F(" bytes."));}

// Allow for variable number of arguments (2 or 3)  (a bit tricky, but fool-proof).
// Variable number of arguments for printing string is not supported by Serial.print() but provided
// for consistency.
// Ref: https://stackoverflow.com/questions/11761703/overloading-macro-on-number-of-arguments
#define DGET_MACRO(moduleTag, _1,_2,NAME,...) NAME

#define DPRINT(moduleTag, ...) DGET_MACRO(moduleTag, __VA_ARGS__, DPRINT2, DPRINT1)(moduleTag,__VA_ARGS__)
#define DPRINTLN(moduleTag, ...) DGET_MACRO(moduleTag, __VA_ARGS__, DPRINTLN2, DPRINTLN1)(moduleTag,__VA_ARGS__)
#define DPRINTS(moduleTag, ...) DGET_MACRO(moduleTag, __VA_ARGS__, DPRINTS2, DPRINTS1)(moduleTag,__VA_ARGS__)
#define DPRINTSLN(moduleTag, ...) DGET_MACRO(moduleTag, __VA_ARGS__, DPRINTSLN2, DPRINTSLN1)(moduleTag,__VA_ARGS__)

// -------- Function prototypes
int freeRam();  // Return free dynamic memory in bytes.
#else
// Define all macros but DINIT() as blank lines.
/** @ingroup CSPU_Debug
 *  @brief Print a string on the Serial output, without a final carriage-return, provided the debugging is activated.
 *  The macro takes care of running the F() macro on msg to store it in program memory. 
 *  @param moduleTag A preprocessor symbol which must be defined to 1 for the instruction to have any effect. 
 *  @param msg The string to print.
 *  @pre The serial interface has been initialised (using DINIT() macro). 
 *  
 *  if DEBUG_CSPU is undefined (as should be in the operational software), this macro expands to no code.
 */
#define DPRINTS(moduleTag, msg,...)
/** @ingroup CSPU_Debug
 *  @brief Print a string on the Serial output, with a final carriage-return, provided the debugging is activated.
 *  The macro takes care of running the F() macro on msg to store it in program memory. 
 *  @param moduleTag A preprocessor symbol which must be defined to 1 for the instruction to have any effect. 
 *  @param msg The string to print.
 *  @pre The serial interface has been initialised (using DINIT() macro). 
 *  
 *  if DEBUG_CSPU is undefined (as should be in the operational software), this macro expands to no code.
 */
#define DPRINTSLN(moduleTag, msg,...)
/** @ingroup CSPU_Debug
 *  @brief Print a numerical value on the Serial output, without a final carriage-return, provided the debugging is activated.
 *  @pre The serial interface has been initialised (using DINIT() macro). 
 *  @param moduleTag A preprocessor symbol which must be defined to 1 for the instruction to have any effect.
 *  @param numValue A numerical value (integer or float). 
 *  
 *  if DEBUG_CSPU is undefined (as should be in the operational software), this macro expands to no code.
 */
#define DPRINT(moduleTag, numValue,...)
/** @ingroup CSPU_Debug
 *  @brief Print a numerical value on the Serial output, with a final carriage-return, provided the debugging is activated.
 *  @pre The serial interface has been initialised (using DINIT() macro). 
 *  @param moduleTag A preprocessor symbol which must be defined to 1 for the instruction to have any effect.
 *  @param numValue A numerical value (integer or float). 
 *  
 *  if DEBUG_CSPU is undefined (as should be in the operational software), this macro expands to no code.
 */
#define DPRINTLN(moduleTag, numValue,...)
/** @ingroup CSPU_Debug
 *  @brief Pause program execution for some time, provided the debugging is activated.
 *  @param moduleTag A preprocessor symbol which must be defined to 1 for the instruction to have any effect.
 *  @param durationInMsec The duration of the requested pause.
 *  
 *  if DEBUG_CSPU is undefined (as should be in the operational software), this macro expands to no code.
 */
#define DDELAY(moduleTag,durationInMsec,...)
/** @ingroup CSPU_Debug
 *  @brief Print the amount of free dynamic memory on the serial interface, provided the debugging is activated.
 *  @param moduleTag A preprocessor symbol which must be defined to 1 for the instruction to have any effect.
 *  
 *  if DEBUG_CSPU is undefined (as should be in the operational software), this macro expands to no code.
 */
#define DFREE_RAM(moduleTag)
#endif

 /** @ingroup CSPU_Debug
  * Utility function to dump a chunk of memory on Serial.
  * @param data Pointer to the first byte to dump.
  * @param len  The number of bytes to dump.
  * @warning Beware of byte alignment issues...
  */
 void dumpMemory(const uint8_t* data, size_t len);
/** @} */
