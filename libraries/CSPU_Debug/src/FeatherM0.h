/*
 * FeatherM0.h
 */

 /** This file collects a number of macroes specific for the Feather M0 [Express] board
  *  and probably for any board equiped with SAMD_0 controllers (but this is to be validated).
  */

#if defined(ARDUINO_SAMD_ZERO) && !defined(ADAFRUIT_FEATHER_M0_EXPRESS) && !defined(ADAFRUIT_FEATHER_M0)

// This is required for the Arduino M0 board, not the Adafruit one 
// (AdaFruit did fix this on the Adafruit M0 by redirecting Serial call to USB calls.
# if defined(SERIAL_PORT_USBVIRTUAL)
   // required for Serial operations on Zero based board 
   #define Serial '''SERIAL_PORT_USBVIRTUAL'''
#endif

#endif
