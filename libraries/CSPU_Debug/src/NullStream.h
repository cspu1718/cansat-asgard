/*
 * NullStream.h
 */

#pragma once
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"


class NullStream : public Stream {
	/** @brief A stream class which discards any outgoing data, and never returns any
	 * 		   input data.
	 *  It is used as a sink to replace unavailable streams.
	 */
public:
	virtual ~NullStream() {};
	int available() { return 0; };
	void flush() { return; };
	int peek() { return -1; };
	int read(){ return -1; };
	size_t write( uint8_t u_Data ){ return 1; };
	size_t write(const uint8_t *buffer, size_t size) { return size; };
};
#pragma GCC diagnostic pop

extern NullStream theNullStream;
