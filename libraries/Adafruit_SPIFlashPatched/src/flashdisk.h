#ifndef FLASHDISK_H
#define FLASHDISK_H

#include "Adafruit_SPIFlash_FatFsPatched.h"

void flashdisk_activate(Adafruit_SPIFlash_FatFs* _flashFatFs);

#endif
