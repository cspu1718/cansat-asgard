/**
  Test for the emitting part of CansatXBeeClient.h (using the Can-side XBee module)
  Test with Feather M0 board.

  The contents of the record and testString are defined in TestCansatRecord.h
  (because it is shared by the emitter and receiver sketches.

  Wiring: µC to XBee module.
       Feather/ItsyBitsy
       3.3V to VCC
       RX   to DOUT  CONFIRMED although it is the opposite to connect to XCTU!
       TX   to DIN   CONFIRMED although it is the opposite to connect to XCTU!
       GND  to GND
   Instructions: TO BE COMPLETED

*/

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#ifndef ARDUINO_ARCH_SAMD
#  error "This program only works on SAMD boards (Feather MO_Express, ItsyBitsy M4 Express, etc.)"
#endif

#include "CansatConfig.h"
#include "CansatXBeeClient.h"
#include "CansatRecordExample.h"

constexpr bool SendRecordsToAllKnownXBees = true; // If true, records are sent to all known XBee modules, except
// the one used to send (for debugging only).
constexpr bool AbortIfUsingWrongXBeeModule = false; // If true, the test is aborted in case the XBee module is not the expected one.
#define DBG 1

#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

HardwareSerial &RF = Serial1;
CansatXBeeClient xbc(GroundXBeeAddressSH, GroundXBeeAddressSL); // Defined in CansatConfig.h
CansatRecordExample myRecord;
char myString[xbc.MaxStringSize];
CansatFrameType stringType;
bool isRecord;
uint32_t mySH, mySL;

void sendRecordIfNotMyself(uint32_t destSH = 0, uint32_t destSL = 0)
{
  if ((destSH != mySH) || (destSL != mySL)) {
    if ((destSH == 0) && (destSL == 0)) {
        Serial << "Sending record to default destination address";
      } else {
        Serial << "Sending record to 0x";
        Serial.print(destSH, HEX);
        Serial << "-0x";
        Serial.print(destSL, HEX);
      }
    if (xbc.send(myRecord, 0, destSH, destSL)) {
      Serial << " OK" << ENDL;
    } else {
      Serial << " *** Error sending record !" << ENDL;
    }
  }
  delay(1000); // XBee will not get an ack, retries can take time...
}
void sendRecordToAll()
{
  sendRecordIfNotMyself(XBeeAddressSH_01, XBeeAddressSL_01);
  sendRecordIfNotMyself(XBeeAddressSH_02, XBeeAddressSL_02);
  sendRecordIfNotMyself(XBeeAddressSH_03, XBeeAddressSL_03);
  sendRecordIfNotMyself(XBeeAddressSH_04, XBeeAddressSL_04);
  sendRecordIfNotMyself(XBeeAddressSH_05, XBeeAddressSL_05);
  sendRecordIfNotMyself(XBeeAddressSH_06, XBeeAddressSL_06);
  sendRecordIfNotMyself(XBeeAddressSH_07, XBeeAddressSL_07);
  sendRecordIfNotMyself(XBeeAddressSH_08, XBeeAddressSL_08);
  sendRecordIfNotMyself(XBeeAddressSH_09, XBeeAddressSL_09);
  sendRecordIfNotMyself(XBeeAddressSH_10, XBeeAddressSL_10);
  sendRecordIfNotMyself(XBeeAddressSH_11, XBeeAddressSL_11);
  sendRecordIfNotMyself(XBeeAddressSH_12, XBeeAddressSL_12);
}

void printCurrentChannel() {
  uint8_t channel;
  if (xbc.queryParameter("CH", channel) )
  {
    Serial << "Current channel: " << channel << ENDL;
  } else {
    Serial << "*** Error querying channel" << ENDL;
  }
}

void setup() {
  DINIT(115200);

  digitalWrite(LED_BUILTIN, HIGH);
  Serial << ENDL << ENDL;
  Serial << "***** EMITTER SKETCH (testing CansatXBeeClient) *****" << ENDL;
  Serial << "Initializing Serials and communications..." << ENDL;
  RF.begin(115200);
  xbc.begin(RF); // no return value

  myRecord.initValues();

  Serial << "Initialisation over. Test record content (binary size = " << myRecord.getBinarySize() << " bytes):" << ENDL;
  myRecord.print(Serial);
  Serial << ENDL << "Test string : " << ENDL;
  Serial << "'" << CansatRecordExample::testString << "' (length=" << strlen(CansatRecordExample::testString) << ")" << ENDL;
  Serial << "-------------------" << ENDL << ENDL;


  // Set whatever DH-DL value: it should not have any effect on API mode operation.
  if (!xbc.setParameter("DH", (uint32_t) 0x12345678) ||
      !xbc.setParameter("DL", (uint32_t) 0x87654321))
  {
    Serial << "*** Error modifying DH-DL to an irrelevant value" << ENDL;
    exit(-1);
  }
  xbc.queryParameter("SH", mySH);
  xbc.queryParameter("SL", mySL);

  Serial << "Configuration of XBee module (assumed to be the can side of set " << RF_XBEE_MODULES_SET << ")" << ENDL;
  xbc.printConfigurationSummary(Serial);
  auto xbeeRole = xbc.getXBeeSystemComponent();
  if (xbeeRole != CansatXBeeClient::CansatComponent::Can) {
    Serial << "Error: this XBee module is '" << xbc.getLabel(xbeeRole) << "' while it should be '"
           << xbc.getLabel(CansatXBeeClient::CansatComponent::Can) << "'." << ENDL;
    Serial << "Check the content of Cansat config (RF_XBEE_SET, address of XBee) and connected module." << ENDL;
    if (AbortIfUsingWrongXBeeModule) {
      Serial << "**** ABORTED ***" << ENDL;
      Serial.flush();
      exit(-1);
    } else Serial << "**** Performing test anyway ***" << ENDL;
  }
  Serial << "Sending to GroundStation " << RF_XBEE_MODULES_SET << " (" ;
  PrintHexWithLeadingZeroes(Serial, GroundXBeeAddressSH);
  Serial << " - ";
  PrintHexWithLeadingZeroes(Serial, GroundXBeeAddressSL);
  Serial << ")" << ENDL;
}

void loop() {
  printCurrentChannel();
  if (SendRecordsToAllKnownXBees) sendRecordToAll();
  else sendRecordIfNotMyself();

  Serial << "Done sending record to all XBees" << ENDL;
  delay(1000);

  Serial << "Sending a string message " << ENDL;
  xbc.openStringMessage(CansatRecordExample::TestStringType, CansatRecordExample::SequenceNumber);
  xbc << CansatRecordExample::testString;
  if (xbc.closeStringMessage()) {
    Serial << "Sent test string" << ENDL;
  } else {
    Serial << "Error sending test string " << ENDL;
  }
  delay(1000);

  Serial << " Testing performance" << ENDL; 
  // Testing performance
  elapsedMillis ts = 0;
  for (int i = 0; i < 10; i++) {
    if (!xbc.send(myRecord)) {
      Serial << "Error sending record" << ENDL;
    }
  }
  Serial << "Sent 10 records in " << ts << " msec (without ack), ie. " << ts / 10.0 << " msec/record" << ENDL << ENDL;
  delay(1000);
}
