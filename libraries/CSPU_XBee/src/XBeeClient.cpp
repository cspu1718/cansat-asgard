/**
   XBeeClient.cpp
   Started on 21 April 2019
*/
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "XBeeClient.h" // This one must be outside the ifdef to have the symbol defined or undefined.

#ifdef RF_ACTIVATE_API_MODE

#include <XBee.h>
#include "elapsedMillis.h"

#define DBG_SEND 		0
#define DBG_NO_TIMELY_TX_STATUS 0 // Activate to have a message when ack time-out expires
#define DBG_RECEIVE 	0
#define DBG_STREAM 		0
#define DBG_DIAGNOSTIC 	1
#define DBG_QUERY 		0
#define DBG_REMOTE_QUERY 0
#define DBG_SET 		0
#define DBG_RESTORE		0
#define DBG_RETRIES		0
#define DBG_CHECK		0
#define DBG_POWER_LEVEL 0
#define DBG_SLEEP		0
#define DBG_AUTO_SLEEP	0
#define DBG_PERFORM_SLEEP_MODE_CONSISTENCY_CHECKS 1
#define DBG_NETWORK_JOINING 0

static constexpr uint32_t AT_ResponseDelay=2000; // ms.
static constexpr uint32_t RemoteAT_ResponseDelay=2000; // ms.
static constexpr uint8_t MaxStringValueLength=20; // maximum number of characters
												  // in a string parameter value
												  // (including final '\0')

XBeeClient::XBeeClient(uint32_t defaultDestinationSH, uint32_t defaultDestinationSL) :
  xbee(),
  defaultReceiverAddress(defaultDestinationSH, defaultDestinationSL),
  msgBuffer(),
  sstr(msgBuffer),
#ifdef ARDUINO_AVR_UNO
  swSerialStream(nullptr),
#endif
  hwSerialStream(nullptr),
  sleepRequestPin(0),
  onSleepPin(0),
  transparentWakeUpPerformed(0)
{
	msgBuffer.reserve(200);
}

void XBeeClient::begin(HardwareSerial &stream) {
  xbee.begin(stream);
  hwSerialStream=&stream;
}

#ifdef ARDUINO_AVR_UNO
void XBeeClient::begin(SoftwareSerial &stream) {
  xbee.begin(stream);
  swSerialStream=&stream;
}
#endif

void XBeeClient::waitForNetworkJoining() {
	uint8_t ai;
	uint32_t entry=millis();
	DPRINTS(DBG_DIAGNOSTIC, "Waiting for XBee to join network...");
	if (!queryParameter("AI", ai)) {
		DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: cannot read parameter AI to check joining");
		return;
	}
	while (ai != 0x00) {
		DPRINTS(DBG_NETWORK_JOINING, "  AI=");
		DPRINTLN(DBG_NETWORK_JOINING, ai);
		delay(300);
		if (!queryParameter("AI", ai)) {
			DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: cannot read parameter AI to check joining");
			return;
		}
	}
	DPRINTS(DBG_DIAGNOSTIC, "OK, joined after (sec): ");
	DPRINTLN(DBG_DIAGNOSTIC, (millis()-entry)/1000.0);
	return;
}

void XBeeClient::configureSleepMngtPins(uint8_t theSleepRequestPin, uint8_t theOnSleepPin, bool useSleepMode) {

	uint8_t originalSM_Value;
	if (!queryParameter("SM",originalSM_Value)) {
		DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: cannot query SM parameter to configure sleep mode");
		originalSM_Value=0xFF;
	}

	if (theSleepRequestPin) {
		pinMode(theSleepRequestPin, OUTPUT);
		digitalWrite(theSleepRequestPin, LOW); // not in sleep mode by default.
		delay(1);
		bool sleepConfigOK=true;
		DPRINTS(DBG_DIAGNOSTIC, "  XBee SLEEP_RQ configured on pin #");
		DPRINTLN(DBG_DIAGNOSTIC, theSleepRequestPin);
		if (useSleepMode) {
			uint8_t CE_Value;
			if (!queryParameter("CE",CE_Value)) {
				DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: cannot query CE parameter to configure sleep mode");
				sleepConfigOK = false;
			}
			if (sleepConfigOK && (CE_Value != 0x00)) {
				DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: request for XBee sleep mode ignored because CE=1");
				DPRINTSLN(DBG_DIAGNOSTIC, "***        coordinator cannot sleep");
				sleepConfigOK=false;
			}
			if (sleepConfigOK) {
				if (!setParameter("SM", (uint8_t) 1)) {
					DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: cannot set SM to 1 to enable pin sleep mode.");
					sleepConfigOK = false;
				} else {
					DPRINTSLN(DBG_DIAGNOSTIC, "  SM parameter set to 1.");
					if (originalSM_Value != 1) waitForNetworkJoining();
				}
			}
		} else {
			if (!setParameter("SM", (uint8_t) 0)) {
				DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: cannot set SM to 0 to disable pin sleep mode.");
			}
			else {
				DPRINTSLN(DBG_DIAGNOSTIC, " Pin sleep mode inactive");
				if (originalSM_Value != 0) waitForNetworkJoining();
			}
		}
		if (sleepConfigOK && useSleepMode){
			sleepRequestPin=theSleepRequestPin;
			DPRINTSLN(DBG_DIAGNOSTIC, "  Pin sleep mode active");
		} else {
			sleepRequestPin=0;
			DPRINTSLN(DBG_DIAGNOSTIC, "  Pin sleep mode inactive (pin always LOW)");
		}
	}
	else {
		DPRINTSLN(DBG_DIAGNOSTIC, "  XBee SLEEP_RQ pin not connected.");
		if (originalSM_Value != 0) {
			if (!setParameter("SM", (uint8_t) 0)) {
				DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: cannot set SM to 0 to disable pin sleep mode.");
			}
			else {
				DPRINTSLN(DBG_DIAGNOSTIC, " Pin sleep mode inactive");
				waitForNetworkJoining();
			}
		}
		sleepRequestPin=0;
	} // else theSleepRequestPin

	if (theOnSleepPin) {
		if (!setParameter("D9", (uint8_t) 1)) {
			DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: cannot set D9 to 1 to enable IN/SLEEP signal.");
		} else {
			onSleepPin=theOnSleepPin;
			DPRINTSLN(DBG_DIAGNOSTIC, "  D9 parameter set to 1.");
			DPRINTS(DBG_DIAGNOSTIC, "  XBee ON/SLEEP configured on pin #");
			DPRINTLN(DBG_DIAGNOSTIC, theOnSleepPin);
		}
	} else {
		onSleepPin=0;
		DPRINTSLN(DBG_DIAGNOSTIC, "  ON/SLEEP pin not used");
	}
}


bool XBeeClient::send(	uint8_t* data,
                        uint8_t dataSize,
                        int timeOutCheckResponse,
                        uint32_t destSH, uint32_t destSL) {
  if (dataSize > MaxPayloadSize) {
    DPRINTS(DBG_DIAGNOSTIC, "XBeeClient::send: payload too large: ");
    DPRINT(DBG_DIAGNOSTIC, dataSize);
    DPRINTSLN(DBG_DIAGNOSTIC, " bytes");
    return false;
  }

  XBeeAddress64 receiverAddress(destSH, destSL);
  if ((destSH == 0L) && (destSL == 0L)) {
    receiverAddress = defaultReceiverAddress;
  }
#if DBG_SEND == 1
  Serial << "Sending to ";
  PrintHexWithLeadingZeroes(Serial, receiverAddress.getMsb());
  Serial << "-";
  PrintHexWithLeadingZeroes(Serial, receiverAddress.getLsb());
  Serial << ", timeout=" << timeOutCheckResponse << " msec" << ENDL;
#endif

  if (DisplayOutgoingFramesOnSerial) {
	  Serial << "Sending..." << ENDL;
	  printFrame(data,dataSize);
  }

  autoWakeUpXBeeModule();

  bool result = false;
  ZBTxRequest zbTx = ZBTxRequest(receiverAddress, data, dataSize); //Creating an object to send the data
  ZBTxStatusResponse txStatus = ZBTxStatusResponse();
  xbee.send(zbTx);
  if (timeOutCheckResponse == 0) {
    // If timeout is null we do not check for reception.
	autoPutXBeeModuleToSleep();
    return true;
  }

  if (xbee.readPacket(timeOutCheckResponse)) { //Wait up to timeOutCheckResponse ms a response from the other Xbee
    if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE) {
      DPRINTS(DBG_SEND, "We got the ZB_TX_STATUS_RESPONSE...");
      xbee.getResponse().getZBTxStatusResponse(txStatus);
      // get the delivery status, the fifth byte
      uint8_t status = txStatus.getDeliveryStatus();
      DPRINTS(DBG_SEND, "Delivery status = ");
      DPRINTLN(DBG_SEND, status);

      if (txStatus.getDeliveryStatus() == SUCCESS) {
#if (DBG_SEND==1)
        Serial << F("SUCCESS, delivered to 16-bit address ");
        PrintHexWithLeadingZeroes(Serial, txStatus.getRemoteAddress());
        Serial << ENDL;
#endif
        result = true;
      } else {
        // ***ERROR***
        DPRINTSLN(DBG_DIAGNOSTIC, "***ERROR*** The other XBee didn't receive our packet");
      }
    }
  } else if (xbee.getResponse().isError()) {
    // ***ERROR*** we had no response
    DPRINTSLN(DBG_DIAGNOSTIC, "***ERROR*** reading packet. ErrCode=");
    DPRINTLN(DBG_SEND, xbee.getResponse().getErrorCode());
    result = DBG_DIAGNOSTIC;
  } else {
    // ***ERROR***
    DPRINTSLN(DBG_NO_TIMELY_TX_STATUS, "***ERROR*** - No timely TX Status Response");
  }

  autoPutXBeeModuleToSleep();

  return result;
}

bool XBeeClient::receive(uint8_t* &payload, uint8_t& payloadSize) {
  if (ModuleInSleepMode()) return false;

  bool result = false;
  static ZBRxResponse rx; 	// Make it static to preserve the object and the buffer which we return as payload.
  	  	  	  	  	  	  	// Could be unnecessary if the internal buffer is a global, but who knows...
  ModemStatusResponse msr;

  xbee.readPacket(); //Checks the communication at every loop
  if (xbee.getResponse().isAvailable()) {
    DPRINTS(DBG_RECEIVE, "We got something: ");
    uint8_t apiId = xbee.getResponse().getApiId();
    if (apiId == ZB_RX_RESPONSE) {
      DPRINTSLN(DBG_RECEIVE, "a zb rx packet!");
      //Now fill our zb rx class
      xbee.getResponse().getZBRxResponse(rx);
      DPRINTS(DBG_RECEIVE, "Remote address: 0x");
      DPRINT(DBG_RECEIVE,  rx.getRemoteAddress64().getMsb());
      DPRINTLN(DBG_RECEIVE,  rx.getRemoteAddress64().getLsb());

      uint8_t resp = rx.getOption();
      if (resp == ZB_PACKET_ACKNOWLEDGED) {
        // the sender got an ACK
        DPRINTSLN(DBG_RECEIVE, "Sender got en ACK - good");

        payload = rx.getData();
        payloadSize = rx.getDataLength();
        if (DisplayIncomingFramesOnSerial) {
        	  Serial << "Received payload:" << ENDL;
        	  printFrame(payload,payloadSize);
          }
        result = true;
      } else {
        //***ERROR***
        DPRINTSLN(DBG_DIAGNOSTIC, "***ERROR*** Received packet but sender didn't get an ACK. API ID=");
        DPRINTLN(DBG_DIAGNOSTIC, apiId);
        DPRINTS(DBG_DIAGNOSTIC, ", ZBRxResponse: ");
        DPRINTLN(DBG_DIAGNOSTIC, resp);
      }
    } else if (apiId == MODEM_STATUS_RESPONSE) {
      xbee.getResponse().getModemStatusResponse(msr);
      // the local XBee sends this response on certain events, like association/dissociation
      DPRINTS(DBG_RECEIVE, "A certain event happened association/dissociation: ");
      auto msrStatus = msr.getStatus() ;
      if (msrStatus == ASSOCIATED) {
        DPRINTSLN(DBG_RECEIVE, "It's an association - Yeay this is great!");
        result = true;
      } else if (msrStatus == DISASSOCIATED) {
        //***ERROR***
        DPRINTSLN(DBG_DIAGNOSTIC, "*** Error MSR status: dissociation");
      } else {
        //***ERROR***
        DPRINTS(DBG_DIAGNOSTIC, "*** Error MSR status unknown: ");
        DPRINTLN(DBG_DIAGNOSTIC, msrStatus);
      }
    }
    else if (apiId == ZB_TX_STATUS_RESPONSE) {
      // This we receive after a message is sent for which we did not wait for
      // the TX_STATUS_RESPONSE because the user did not require an ack.
      // At this stage the message is just ignored.
      DPRINTSLN(DBG_RECEIVE, "(ignored) ZB_TX_STATUS_RESPONSE ");
    }
    else if (apiId == ZB_EXPLICIT_RX_RESPONSE) {
      // This message is only received if JN=1
      DPRINTSLN(DBG_RECEIVE, "ZB_EXPLICIT_RX_RESPONSE");
      DPRINTSLN(DBG_DIAGNOSTIC, "Zigbee network joined!");
    }
    else if (apiId == RouteRecordIndicator) {
        DPRINTSLN(DBG_DIAGNOSTIC, "Received Route Record Indicator (ignored)");
      }
    else {
      DPRINTS(DBG_RECEIVE, "(ignored) Unexpected API ID=0x");
      DPRINTLN(DBG_RECEIVE, apiId, HEX);
    }
  } else if (xbee.getResponse().isError()) {
    //***ERROR***
    DPRINTS(DBG_DIAGNOSTIC, "*** ERROR in reading packet. Err code=");
    DPRINTLN(DBG_DIAGNOSTIC, xbee.getResponse().getErrorCode());
    switch (xbee.getResponse().getErrorCode()) {
      case CHECKSUM_FAILURE:
        DPRINTLN(DBG_DIAGNOSTIC, "Checksum failure");
        break;
      case PACKET_EXCEEDS_BYTE_ARRAY_LENGTH:
        DPRINTS(DBG_DIAGNOSTIC, "Packet exceeds byte-array length. Should not exceed (bytes)  ");
        DPRINTLN(DBG_DIAGNOSTIC, MAX_FRAME_DATA_SIZE - 10);
        break;
      case UNEXPECTED_START_BYTE:
        DPRINTLN(DBG_DIAGNOSTIC, "Unexpected start byte");
        break;
      default:
        DPRINTLN(DBG_DIAGNOSTIC, "Unknown error code.");
    }
  }

  return result;
}

void XBeeClient::openStringMessage(uint8_t type, uint8_t seqNbr) {
  msgBuffer = "  ";		// Reserve 2 bytes.
  msgBuffer[0] = type; // Store value of byte, not string representation.
  msgBuffer[1] = seqNbr; // Optional sequence number transported in this byte
  	  	  	  	  	  	 // which is needed to keep even alignment of the string.
  DPRINTS(DBG_STREAM,  "XBeeClient::openStringMessage. buffer='");
  DPRINT(DBG_STREAM,  (msgBuffer.c_str() + 2));
  DPRINTS(DBG_STREAM,  "', type=");
  DPRINTLN(DBG_STREAM, (uint8_t) msgBuffer[0]);
}

bool XBeeClient::closeStringMessage(int timeOutCheckResponse) {
  DPRINTS(DBG_STREAM,  "XBeeClient::closeStringMessage. buffer='");
  DPRINT(DBG_STREAM,  (msgBuffer.c_str() + 2));
  DPRINTS(DBG_STREAM,  "', type=");
  DPRINTLN(DBG_STREAM, (uint8_t) msgBuffer[0]);

  auto sLength = msgBuffer.length()-2; // First 2 bytes are not the string
  if (sLength == 0) {
    DPRINTS(DBG_DIAGNOSTIC, "Error: closing empty message. Type=");
    DPRINTLN(DBG_DIAGNOSTIC, (uint8_t) msgBuffer[0]);
    return false;
  }
  if (sLength > MaxStringSize) {
    DPRINTS(DBG_DIAGNOSTIC, "Error: string message too long:");
    DPRINT(DBG_DIAGNOSTIC, sLength);
    DPRINTS(DBG_DIAGNOSTIC, ", Max=");
    DPRINT(DBG_DIAGNOSTIC, MaxStringSize);
    DPRINTS(DBG_DIAGNOSTIC, ", ");
    DPRINTLN(DBG_DIAGNOSTIC, (msgBuffer.c_str() + 2));
    return false;
  }

  uint8_t* payload = (uint8_t *) msgBuffer.c_str();
  uint8_t payloadSize = (uint8_t) msgBuffer.length() + 1;
  if (DisplayOutgoingFramesOnSerial) {
 	  printString(payload, payloadSize);
  }

  // Also send the terminating '\0'.
  return send(payload, payloadSize, timeOutCheckResponse);
}

bool XBeeClient::getString(	char* str, uint8_t &stringType,
							uint8_t& stringSeqNbr,
							const uint8_t* data, uint8_t dataSize) const {
	bool result=false;

	if (dataSize <=2) {
		DPRINTS(DBG_DIAGNOSTIC, "*** Error: received empty string");
		*str ='\0';
		return result;
	}
	const char* s = (const char*) (data + 2);
    stringType= data[0];
    stringSeqNbr=data[1];

    if (dataSize != (uint8_t) strlen(s) + 3) {
		// 2 bytes + string + '\0'
		DPRINTS(DBG_DIAGNOSTIC, "*** XBClient::getString: Error: inconsistent size. size=");
		DPRINT(DBG_DIAGNOSTIC, dataSize);
		DPRINTS(DBG_DIAGNOSTIC, ", String size, from third byte=");
		DPRINTLN(DBG_DIAGNOSTIC, strlen(s));
	} else {
		memcpy(str, data+2, strlen(s)+1);
		result=true;
	}

	return result;
}

bool XBeeClient::wakeUpXBeeModule() {
	bool didWake=false;
 	if (sleepRequestPin && (digitalRead(sleepRequestPin)==HIGH)) {
 		digitalWrite(sleepRequestPin,LOW);
 		if (onSleepPin) {
 			// wait forON/SLEEP TO be HIGH if pin is connected only
 			// (actively polling for response to too time consuming).
 			uint8_t counter=0;
 			while (!isModuleOnline()) {
 				counter++;
 				delay(1);
 				if (counter > 10) {
 					DPRINTSLN(DBG_DIAGNOSTIC, "*** ERROR: waited 10 msec and XBee still asleep");
 					break;
 				}
 			}
 		}
 		delay(10); // According to spec: on a PRO module, there is a 6 msec delay
 				   // between wake-up and polling parent, which takes about 2 msec.
 				   // so 10 msec to be on the safe side
 		DPRINTSLN(DBG_SLEEP, "XBee out of sleep mode");
 		didWake=true;
 	}
 	return didWake;
 }

 bool XBeeClient::putXBeeModuleToSleep(bool wait) {
 	if (sleepRequestPin && !digitalRead(sleepRequestPin)) {
 		digitalWrite(sleepRequestPin,HIGH);
 		bool result=true;
 		uint8_t counter=1;
 		if (wait) do {
 			if (counter > 50) {
 				result=false;
 				DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: waited 500 msec and XBee is still not asleep");
 				break;
 			}
 			delay(10); // some time is required to go to sleep. Sometimes,
 					   // 12 msec is enough, sometimes 150 is not!
 		} while (isModuleOnline());
 		if (result) DPRINTSLN(DBG_SLEEP, "XBee now in sleep mode");
 		return result;
 	} else return false;
 }

 bool XBeeClient::autoWakeUpXBeeModule() {
	 bool result=false;
	 if (!sleepRequestPin) return false;
	 DPRINTS(DBG_AUTO_SLEEP, "autoWakeUp IN. wakeUpPerformed=");
	 DPRINTLN(DBG_AUTO_SLEEP, transparentWakeUpPerformed)
	 if (transparentWakeUpPerformed==0) {
		 if (wakeUpXBeeModule()) {
			 transparentWakeUpPerformed++;
			 result=true;
		 }
	 } else {
		 // This is not the first wake up, so the module should already be awake.
		 // If it is not, we have an unbalanced autoWakeUp/autoPutToSleep pair
		 // somewhere.
#if (DBG_PERFORM_SLEEP_MODE_CONSISTENCY_CHECKS==1)
		 if (ModuleInSleepMode()) {
			 DPRINTS(DBG_DIAGNOSTIC, "ERROR: internal inconsistency in XBClient::autoWakeUpModule(). transparentWakeUpPerformed=");
			 DPRINT(DBG_DIAGNOSTIC, transparentWakeUpPerformed);
			 DPRINTSLN(DBG_DIAGNOSTIC, " while module in sleep mode!");
		 }
#endif
		 transparentWakeUpPerformed++;
	 }
	 DPRINTS(DBG_AUTO_SLEEP, "autoWakeUp OUT. wakeUpPerformed=");
	 DPRINTLN(DBG_AUTO_SLEEP, transparentWakeUpPerformed)
	 return result;

 }

 bool XBeeClient::autoPutXBeeModuleToSleep(bool wait) {
	 bool result=false;
	 if (!sleepRequestPin) return false;
	 DPRINTS(DBG_AUTO_SLEEP, "autoPutToSleep IN. wakeUpPerformed=");
	 DPRINTLN(DBG_AUTO_SLEEP, transparentWakeUpPerformed)
  	 if (transparentWakeUpPerformed> 0) {
  		 if (transparentWakeUpPerformed==1) {
  			 result=putXBeeModuleToSleep(wait);
  		 }
  		 transparentWakeUpPerformed--;
  	 }
	 DPRINTS(DBG_AUTO_SLEEP, "autoPutToSleep OUT. wakeUpPerformed=");
	 DPRINTLN(DBG_AUTO_SLEEP, transparentWakeUpPerformed)
	 return result;

 }

bool XBeeClient::isModuleOnline(const uint32_t& sh, const uint32_t& sl) {
	if (onSleepPin) {
		return (digitalRead(onSleepPin)==HIGH);
	} else {
		uint8_t value, valueSize=1;
		bool myBool;
		return queryParameterOnce("CE", &value, valueSize, myBool, sh, sl);
	}
}

bool XBeeClient::queryParameter(	const char* param,
									uint8_t value[], uint8_t &valueSize,
									const uint32_t& sh, const uint32_t& sl,
									uint8_t maxRetries) {
	bool result, doNotRetry=true;

	do {
		DPRINTS(DBG_RETRIES,"Attempting query of ");
		DPRINTLN(DBG_RETRIES,param);
		result=queryParameterOnce(param,value,valueSize,doNotRetry,sh,sl);
	} while (!result && !doNotRetry && maxRetries--);
	return result;
}

bool XBeeClient::queryParameter(const char* param, uint64_t &value,const uint32_t& sh, const uint32_t& sl) {
	return queryIntegerParameter(param, value, (uint8_t) 8,sh,sl);
}

bool XBeeClient::queryParameter(const char* param, uint32_t &value,const uint32_t& sh, const uint32_t& sl) {
	DPRINTSLN(DBG_QUERY, "QueryParameter/uint32_t");
	uint64_t longVal=value;
	bool result= queryIntegerParameter(param, longVal, (uint8_t)4,sh,sl);
	value=(uint32_t)longVal;
	return result;
}

bool XBeeClient::queryParameter(const char* param, uint16_t &value,const uint32_t& sh, const uint32_t& sl) {
	uint64_t longVal=value;
	bool result= queryIntegerParameter(param, longVal, (uint8_t)2,sh, sl);
	value=(uint16_t)longVal;
	return result;
}

bool XBeeClient::queryParameter(const char* param, uint8_t &value,const uint32_t& sh, const uint32_t& sl) {
	uint64_t longVal=value;
	bool result= queryIntegerParameter(param,longVal, (uint8_t)1, sh, sl);
	value= (uint8_t) longVal;
	return result;
}

bool XBeeClient::queryParameter(const char* param, bool &value,const uint32_t& sh, const uint32_t& sl) {
	uint8_t hexVal[1];
	uint8_t hexValSize=1;
	bool result=queryParameter(param, hexVal, hexValSize,sh,sl);
	if (result) {
		value= (hexVal[0] != 0);
	}
	return result;
}

bool XBeeClient::queryParameter(const char* param,
								char value[], uint8_t valueSize,
								const uint32_t& sh, const uint32_t& sl){
	uint8_t hexVal[valueSize-1];
	uint8_t hexValSize=valueSize-1;

	bool result=queryParameter(param, hexVal, hexValSize, sh, sl);
	if (result) {
		for (int i = 0; i<hexValSize; i++) {
			value[i]=(char) hexVal[i];
		}
		value[hexValSize]='\0';
	}
	return result;
}

bool XBeeClient::printIntegerParameter(Stream &stream, const char* name, const uint32_t & sh, const uint32_t& sl)
{
	uint64_t value;
	stream << F("  ") << name << "    : ";
	if (!queryParameter(name, value, sh, sl)) {
		stream << F("*** Error *** ") << ENDL;
		return false;
	} else {
		stream << F("0x"); stream.print((uint16_t) value, HEX);
		return true;
	}
}

bool XBeeClient::printSleepModeConfiguration(Stream &stream, const uint32_t & sh, const uint32_t& sl) {
	uint8_t SM_Value;
	uint64_t iValue1, iValue2;
	bool globalResult=true;

	autoWakeUpXBeeModule();
	if (queryParameter("SM", SM_Value, sh, sl)){
		stream << "  ";
		printSleepModeConfigurationLabel(stream, SM_Value);
		stream << ENDL;
	}
	else {
		globalResult = false;
	}

	if (!queryParameter("SP", iValue1, sh, sl)) {
			iValue1=0;
			globalResult = false;
	}
	stream << F("  SP-SN : 0x"); stream.print((unsigned long) iValue1, HEX);
	if (!queryParameter("SN", iValue2, sh, sl)) {
		iValue1=0;
		globalResult = false;
	}
	stream << F(" - 0x"); stream.print((unsigned long) iValue2, HEX);
	stream << F(" (Sleep period=") << iValue1*20.0/1000.0
		   << F("sec, Num. sleep cycles=") << (unsigned long) iValue2 << ")" << ENDL;
	stream << F("          (Routers/Coord. allow SP*SN=") << iValue1*iValue2*20.0/1000.0 << F("sec sleep by End-Devices)") << ENDL;
	globalResult &= printIntegerParameter(stream, "SO", sh, sl);
	stream << F(" (Sleep options, 0=no option)") << ENDL;

    autoPutXBeeModuleToSleep();
    return globalResult;
}


bool XBeeClient::printNetworkParameters(Stream &stream, const uint32_t & sh, const uint32_t& sl) {
	uint64_t iValue1;
	bool bValue;
	bool globalResult=true;

	autoWakeUpXBeeModule();

	if (!queryParameter("SC", iValue1, sh, sl)){
		globalResult = false;
		stream << F("  SC    : *** Error ***");

	} else  {
		stream << F("  SC    : ");
		stream.print((uint16_t) iValue1, HEX);
		stream << F(" (Scanned channels: ");
		printScannedChannels(stream, iValue1);
		stream << F(")") << ENDL;
	}

	if (!queryParameter("JN", bValue, sh, sl)) {
		globalResult=false;
		stream << F("  JN    : *** Error *** ") << ENDL;
	} else stream << F("  JN    : ") << (int) bValue;
	stream << F(" (Join notification enabled, 1=Yes/0=no)") << ENDL;

	if (!queryParameter("JV", bValue, sh, sl)){
		globalResult = false;
		stream << F("  JV    : *** Error ***");

	} else stream << F("  JV    : ") << (int) bValue ;
    stream << F(" (Coord. join verification by Rooter/End device)") << ENDL;

	globalResult &= printIntegerParameter(stream, "NJ", sh, sl);
	stream << F(" (Node join time, 0xFF=always allow)") << ENDL;

	if (!queryParameter("ID", iValue1, sh, sl)) {
		iValue1=0;
		globalResult = false;
	}
	stream << F("  ID-OP : "); PrintHexWithLeadingZeroes(stream, iValue1);
	if (!queryParameter("OP", iValue1, sh, sl)) {
		iValue1=0;
		globalResult = false;
	}
	stream << F(" - "); PrintHexWithLeadingZeroes(stream, iValue1);
	stream << F(" (PAN ID/Extended PAN ID)") << ENDL;

	if (!queryParameter("OI", iValue1, sh, sl)) {
			iValue1=0;
			globalResult = false;
		}
	stream << F("  OI    : 0x"); stream.print((uint16_t) iValue1, HEX);
	stream << F(" (Operating 16-bits PAN Id)") << ENDL;

	if (!queryParameter("AO", bValue, sh, sl)) {
		globalResult=false;
		stream << F("  AO    : *** Error *** ") << ENDL;
	} else stream << F("  AO    : ") << (int) bValue;
	stream << F(" (If 1, msg is broadcasted when joining network)") << ENDL;

	globalResult &= printIntegerParameter(stream, "CH", sh, sl);
	stream << F(" (current channel (R-Only), 0=no PAN joined)") << ENDL;

	globalResult &= printIntegerParameter(stream, "MY", sh, sl);
	stream << F(" (16-bit network address, 0xFFFE=no PAN joined, 0x0=coord.)") << ENDL;

	if (!queryParameter("NH", iValue1, sh, sl)) {
		globalResult=false;
		stream << F("  NH    : *** Error *** ") << ENDL;
	} else stream << F("  NH    : ") << (int) iValue1;
	stream << F(" (unicast hops. Timeout=(50*NH)+100 msec=") << ((50*(uint8_t) iValue1)+100)/1000.0 << F(" s)") << ENDL;

	globalResult &= printIntegerParameter(stream, "AI", sh, sl);
	stream << F(" (Association indicator 0=success, FF=Init,no status determined yet)") << ENDL;
	globalResult &= printIntegerParameter(stream, "EE", sh, sl);
	stream << F(" (Encription enable 0=disabled)") << ENDL;
	globalResult &= printIntegerParameter(stream, "NC", sh, sl);
	stream << F(" (Remaining child entries)") << ENDL;

	autoPutXBeeModuleToSleep();

	return globalResult;
}


bool XBeeClient::printConfigurationSummary(Stream &stream, const uint32_t & sh, const uint32_t& sl) {
	constexpr uint8_t MaxStringValueLength=20;
	char sValue[MaxStringValueLength];
	uint64_t iValue1, iValue2;
	bool bValue;
	uint8_t SM_Value;
	bool globalResult=true;
	constexpr const char * Separator="  ---------------";

	autoWakeUpXBeeModule();

	stream << F("-- Summary of XBee configuration: --") << ENDL;
	if (!queryParameter("NI", sValue, MaxStringValueLength, sh, sl)){
		strcpy(sValue, "*** Error ***"); // Do not exceed MaxStringValueLength-1 !
		autoPutXBeeModuleToSleep();
		return false;  // If the first param fails (after retries)
					   // We'll assume there is no XBee on the serial line.
	}
	stream << F("  NI    : '") << sValue << F("' (Human-readable name)") << ENDL;

	if (!queryParameter("SH", iValue1,sh,sl)){
		iValue1=0;
		globalResult = false;
	}
	stream << F("  SH-SL : "); PrintHexWithLeadingZeroes(stream, (uint32_t) iValue1);
	if (!queryParameter("SL", iValue1, sh, sl)){
		iValue1=0;
		globalResult = false;
	}
	stream << " - "; PrintHexWithLeadingZeroes(stream,  (uint32_t) iValue1); stream << ENDL;


	stream << F("  DH-DL : not relevant in API mode!") << ENDL;

	globalResult &= printIntegerParameter(stream, "ZS", sh, sl);
	stream << F(" (ZigBee Stack profile. Must be consistent accross network)") << ENDL;

	if (!queryParameter("D6", iValue1, sh, sl)){
		iValue1=0;
		globalResult = false;
	}

	globalResult &= printIntegerParameter(stream, "BD", sh, sl);
	stream << F(" (Baud rate on serial line, 7=115200 baud)") << ENDL;

	globalResult &= printIntegerParameter(stream, "AP", sh, sl);
	stream << F(" (API Enabled 0=disabled, 1=enabled, 2=enabled with escape chars)") << ENDL;

	bool bResult=true;
	if (!queryParameter("CE", bValue, sh, sl)){
		globalResult = false;
		bResult = false;
	}
	if (!queryParameter("SM", SM_Value, sh, sl)){
		globalResult = false;
		bResult=false;
	}
	if (bResult) {
		stream << F("  CE-SM : ") << bValue << " - " << SM_Value << " ";
		stream << F("  Device type: ") << getLabel(getDeviceType(sh,sl));
	} else {
		stream << F("  CE-SM : *** Error ***");
	}
	stream << ENDL;


	stream << Separator << ENDL;
	globalResult &= printSleepModeConfiguration(stream, sh,sl);
	stream << Separator << ENDL;
	globalResult &= printNetworkParameters(stream, sh,sl);
	stream << Separator << ENDL;

	bResult=true;
	if (!queryParameter("VR", iValue1, sh, sl)) {
		bResult=false;
		globalResult=false;
	}
	if (!queryParameter("HV", iValue2, sh, sl)) {
		bResult=false;
		globalResult=false;
	}
	if (bResult) {
		stream << F("  VR-HV : 0x");
		stream.print((uint16_t) iValue1, HEX);
		stream << " - 0x";
		stream.print((uint16_t) iValue2, HEX);
		stream << F(" (firmware version - Hardware version (R-Only))") << ENDL;
	} else stream << F("  VR-HV : *** Error *** ") << ENDL;

	globalResult &= printIntegerParameter(stream, "PL", sh, sl);
	stream << F(" (Transmit power level (range: 0-4, XBee Pro=0-12-14-16-18 dBM (boost mode enabled))") << ENDL;

#if RF_XBEE_MODEL == RF_XBEE_S2C
	globalResult &= printIntegerParameter(stream, "PM", sh, sl);
	stream << F(" (boost mode disabled/enabled)") << ENDL;
#endif
	globalResult &= printIntegerParameter(stream, "SM", sh, sl);
	stream << F(" (sleep mode 0=disabled, 1=pin sleep, 2=cyclic sleep, 3=cyclic, pin wake)") << ENDL;

	stream << Separator << ENDL;
	globalResult &= printIntegerParameter(stream, "P0", sh, sl);
	stream << F(" (PWM/PWMO/DIO10 (pin 6) configuration, 4=digital output, default low") << ENDL;
	globalResult &= printIntegerParameter(stream, "P1", sh, sl);
	stream << F(" (DIO4 (pin 11) configuration, 4=digital output, default low") << ENDL;
	globalResult &= printIntegerParameter(stream, "P2", sh, sl);
	stream << F(" (DIO12 (pin 4) configuration, 4=digital output, default low") << ENDL;
	globalResult &= printIntegerParameter(stream, "P3", sh, sl);
	stream << F(" (DOUT (pin 2) configuration, 1=UART data out") << ENDL;
	globalResult &= printIntegerParameter(stream, "P4", sh, sl);
	stream << F(" (DIN (pin 3) configuration, 1=UART data in") << ENDL;
	// P5-P6-P7-P8-P9 not relevant for through-hole component.

	stream << Separator << ENDL;
	globalResult &= printIntegerParameter(stream, "D0", sh, sl);
	stream << F(" (DIO0 (pin 20) configuration, 4=digital output, default low") << ENDL;
	globalResult &= printIntegerParameter(stream, "D1", sh, sl);
	stream << F(" (DIO1 (pin 19) configuration, 4=digital output, default low") << ENDL;
	globalResult &= printIntegerParameter(stream, "D2", sh, sl);
	stream << F(" (DIO2 (pin 18) configuration, 4=digital output, default low") << ENDL;
	globalResult &= printIntegerParameter(stream, "D3", sh, sl);
	stream << F(" (DIO3 (pin 17) configuration, 4=digital output, default low") << ENDL;
#if RF_XBEE_MODEL == RF_XBEE_S2C
	globalResult &= printIntegerParameter(stream, "D4", sh, sl);
	stream << F(" (DIO4 (pin 11) configuration, 4=digital output, default low") << ENDL;
#endif
	globalResult &= printIntegerParameter(stream, "D5", sh, sl);
	stream << F(" (DIO5 (pin 15) configuration, 4=digital output, default low") << ENDL;
	globalResult &= printIntegerParameter(stream, "D6", sh, sl);
	stream << F(" (DIO6/RTS (pin 16) configuration, 4=digital output, default low") << ENDL;
	globalResult &= printIntegerParameter(stream, "D7", sh, sl);
	stream << F(" (DIO7/CTS (pin 12) configuration, 4=digital output, default low") << ENDL;
	globalResult &= printIntegerParameter(stream, "D8", sh, sl);
	stream << F(" (SleepReq (pin 9) configuration, 1=input to sleep & wake") << ENDL;
	globalResult &= printIntegerParameter(stream, "D9", sh, sl);
	stream << F(" (On/Sleep (pin 13) configuration, 1=on-sleep output") << ENDL;

	autoPutXBeeModuleToSleep();

	return globalResult;
}

void XBeeClient::printScannedChannels(Print &stream, uint16_t SC_Value) {
	bool first=true;
	for (int i=0; i< 16; i++) {
		if (SC_Value & 0x1) {
			if (!first) stream << "-";
			first=false;
			stream << 11+i;
		}
		SC_Value = SC_Value >> 1;
	}
}

void XBeeClient::printSleepModeConfigurationLabel(Print &stream, uint8_t SM_Value) {
	switch (SM_Value) {
	case 0:	stream << "Sleep mode disabled"; break;
	case 1: stream << "Pin sleep enabled";  break;
	case 2: stream << "Cyclic sleep enabled"; break;
	case 3: stream << "Cyclic sleep, pin wake"; break;
	default: stream << "(unexpected SM value)";
	}
}

bool XBeeClient::checkXBeeModuleConfiguration(const uint32_t& sh, const uint32_t& sl) {
	bool dummyBool, result;
	autoWakeUpXBeeModule();
	result= doCheckXBeeModuleConfiguration(false, dummyBool, sh, sl);
	autoPutXBeeModuleToSleep();
	return result;
}

bool XBeeClient::doCheckXBeeModuleConfiguration(bool /* correctConfiguration */,
								  bool & /* configurationChanged */ ,
								  const uint32_t& /* sh */, const uint32_t& /* sl */) {
	DPRINTSLN(DBG_DIAGNOSTIC,
		"doCheckXBeeModuleConfiguration() must be implemented by subclasses of XBeeClient");
	return false;
}

bool XBeeClient::setParameter(const char* param, uint8_t value[], uint8_t valueSize,
		const uint32_t& sh, const uint32_t& sl, uint8_t maxRetries) {
	bool result, doNotRetry;
	do {
		DPRINTS(DBG_RETRIES,"Attempting set of ");
		DPRINTLN(DBG_RETRIES,param);
		result=setParameterOnce(param, value, valueSize, doNotRetry,sh,sl);
	} while (!result && !doNotRetry && maxRetries--);
	return result;
}

bool XBeeClient::setParameter(const char* param, uint64_t value,
		const uint32_t& sh, const uint32_t& sl) {
	DPRINTSLN(DBG_SET,"SetParameter/uint64_t");
	return setIntegerParameter(param, value, (uint8_t) 8, sh,sl);
}

bool XBeeClient::setParameter(const char* param, uint32_t value,
		const uint32_t& sh, const uint32_t& sl) {
	DPRINTSLN(DBG_SET,"SetParameter/uint32_t");
	return setIntegerParameter(param, value, (uint8_t) 4, sh, sl);
}

bool XBeeClient::setParameter(const char* param, uint16_t value,
		const uint32_t& sh, const uint32_t& sl) {
	return setIntegerParameter(param, value, (uint8_t) 2,sh,sl);
}

bool XBeeClient::setParameter(const char* param, uint8_t value,
		const uint32_t& sh, const uint32_t& sl) {
	return setIntegerParameter(param, value, (uint8_t) 1, sh, sl);
}

bool XBeeClient::setParameter(const char* param, const char* value,
		const uint32_t& sh, const uint32_t& sl) {
	return setParameter(param, (uint8_t*) value, strlen(value),sh,sl);
}

bool XBeeClient::setParameter(const char* param, bool value,
		const uint32_t& sh, const uint32_t& sl) {
	uint8_t hexValue = (value ? 1 : 0);
	uint8_t hexSize=1;
	return setParameter(param, (uint8_t*) &hexValue, hexSize,sh,sl);
}

#undef NON_OPERATIONAL_METHOD
#ifdef NON_OPERATIONAL_METHOD // See comment in method doc block.
bool XBeeClient::restoreDefaultConfiguration() {
	// Restoring will possibly change the serial baud rate (default is 9600).
	// We'll need to change the baud rate to 9600 to reconfigure it.
	uint8_t setting;
	long baudRate;
	if (!getXBeeSerialBaudRate(baudRate, setting)) {
		DPRINTSLN(DBG_DIAGNOSTIC, "Could not get baud rate before restoring configuration");
		return false;
	}
	DPRINTS(DBG_RESTORE, "current baud rate=");
	DPRINTLN(DBG_RESTORE, baudRate);

#undef TMP_BAUD // This change of baud rate works. Why does the restore fail??
#ifdef TMP_BAUD
	uint8_t val[1]={0x3};
	if (!setParameter("BD", val,1)) {
			DPRINTSLN(DBG_DIAGNOSTIC, " TMP: Could change baudrate to 9600");
			return false;
		}
#else
	if (!sendAT_Command("RE")) {
		DPRINTSLN(DBG_DIAGNOSTIC, "Could not restore configuration");
		return false;
	}
#endif
Serial << "WAITING AFTER RESTORE/TMP_BAUD.... " << ENDL;
delay(5000);
	// Now baud rate is set to 9600.
#ifdef ARDUINO_AVR_UNO
	if (swSerialStream) {
		swSerialStream->end();
		swSerialStream->begin(9600);
	} else {
		hwSerialStream->end();
		hwSerialStream->begin(9600);
		xbee.begin(*swSerialStream);
	}
#else
	hwSerialStream->end();
	hwSerialStream->begin(9600);
	while (!*hwSerialStream); // No effect
	DPRINTSLN(DBG_RESTORE, "HW serial now ready at 9600 baud");
	xbee.begin(*hwSerialStream); // No effect (just stores a ref to the Serial object).
#endif
	if (!setParameter("BD", &setting, 1)) {
		DPRINTS(DBG_DIAGNOSTIC, "Could not change baudrate back to");
		DPRINTLN(DBG_DIAGNOSTIC, setting);
		return false;
	}
#ifdef ARDUINO_AVR_UNO
	if (swSerialStream) {
		swSerialStream->end();
		swSerialStream->begin(baudRate);
	} else {
		hwSerialStream->end();
		hwSerialStream->begin(baudRate);
	}
#else
	hwSerialStream->end();
	hwSerialStream->begin(baudRate);
#endif
	return true;

}
#endif

bool XBeeClient::softwareReset() {
	if (sendAT_Command("FR")) {
		delay(200);
		return true;}
	else return false;
};

bool XBeeClient::getXBeeSerialBaudRate(long &baudRate, uint8_t &xbeeSetting) {
	uint64_t value;
	if (!queryParameter("BD", value)) {
		return -1;
	}
	switch(value) {
	case 0x1: baudRate=2400; break;
	case 0x2: baudRate=4800; break;
	case 0x3: baudRate=9600; break;
	case 0x4: baudRate=19200; break;
	case 0x5: baudRate=38400; break;
	case 0x6: baudRate=57600; break;
	case 0x7: baudRate=115200; break;
	case 0x8: baudRate=230400; break;
	case 0x9: baudRate=460800; break;
	case 0xA: baudRate=921600; break;
	default:
		DPRINTLN(DBG_DIAGNOSTIC, "Invalid value for 'BD': ");
		DPRINTLN(DBG_DIAGNOSTIC, (uint32_t) value);
		return false;
	}
	xbeeSetting = (uint8_t) value;
	return true;
}

bool XBeeClient::queryIntegerParameter(const char* param, uint64_t &value, uint8_t numBytes,
			const uint32_t& sh, const uint32_t& sl) {
	uint8_t hexVal[8];
	uint8_t hexValSize=numBytes;
#if (DBG_QUERY==1)
	Serial << "QueryIntegerParameter param="<< param << ENDL;
#endif
	bool result=queryParameter(param, hexVal, hexValSize, sh, sl);
	if (result) {
		// MSB is byte 0
		value=0;
		for (int i = 0; i<hexValSize; i++) {
			value <<= 8;
			value+=hexVal[i];
		}
#if (DBG_QUERY==1)
	Serial << "QueryIntegerParameter val="<< value << ", numBytes=" << numBytes << ENDL;
#endif
	}
	return result;
}

bool XBeeClient::setIntegerParameter(const char* param, uint64_t value, uint8_t numBytes,
		const uint32_t& sh, const uint32_t& sl) {
	uint8_t hexVal[8];
#if (DBG_SET==1)
	Serial << "SetIntegerParameter param="<< param << ", numBytes=" << numBytes;
	Serial << " val=" << value << ENDL;
#endif
	// MSB is byte 0 ! Don't take any chance guessing whether we are running little-endian or big-endian
	for (int i=numBytes-1; i>=0; i--){
		hexVal[i] = value & 0xFF;
		value >>= 8;
	}
	return setParameter(param, hexVal, numBytes, sh, sl);
}

bool XBeeClient::queryParameterOnce(const char* param,
		uint8_t value[], uint8_t &valueSize,
		bool &doNotRetry, const uint32_t & sh, const uint32_t& sl) {
	DPRINTS(DBG_QUERY, "QueryParameterOnce param=");
	DPRINTLN(DBG_QUERY, param);
	DPRINTS(DBG_QUERY, "valueSize=");
	DPRINTLN(DBG_QUERY, valueSize);
	if(param[2]!= '\0') {
		DPRINTS(DBG_DIAGNOSTIC, "Error: invalid XBee parameter (aborted):");
		DPRINTLN(DBG_DIAGNOSTIC, param);
		return false;
	}

	if ((sh==0) && (sl==0)) {
		return queryLocalParameterOnce(param, value , valueSize,
				doNotRetry);
	}
	else {
		XBeeAddress64 remote(sh, sl);
		return queryRemoteParameterOnce(param, value, valueSize,
				doNotRetry, remote);
	}
}

#if (DBG_DIAGNOSTIC==1)
static void printResponseStatus(const char* param, uint8_t status) {
	Serial << param << F(": Error code: 0x");
	switch (status) {
	case 0: Serial << F("0 (OK)");	break;
	case 1: Serial << F("1 (Error)");	break;
	case 2: Serial << F("2 (Invalid cmd)");	 break;
	case 3: Serial << F("3 (Invalid param.)");	break;
	case 4: Serial << F("4 (Transm. failure)");	break;
	default:
		Serial << status << F(" (Unknown)");
	}
	Serial << ENDL;
}
#endif

bool XBeeClient::queryLocalParameterOnce(const char* param,
		uint8_t value[], uint8_t &valueSize,
		bool &doNotRetry) {
	DPRINTS(DBG_QUERY, "QueryLocalParameterOnce param=");
	DPRINTLN(DBG_QUERY, param);
	DPRINTS(DBG_QUERY, "valueSize=");
	DPRINTLN(DBG_QUERY, valueSize);
	if(param[2]!= '\0') {
		DPRINTS(DBG_DIAGNOSTIC, "Error: invalid XBee parameter (aborted):");
		DPRINTLN(DBG_DIAGNOSTIC, param);
		return false;
	}
	AtCommandRequest req((uint8_t*) param);
	AtCommandResponse response;
	bool result=false, responseReceived=false;
	doNotRetry=false; // Only forbid retry in case it is hopeless.
	xbee.send(req);
	// wait for AT_ResponseDelay msec max for answer, ignore whatever is received in the meantime
	elapsedMillis elapsed=0;
	while (!responseReceived && xbee.readPacket(AT_ResponseDelay-elapsed)) {
		// got a response! Should be an AT command response
		if (xbee.getResponse().getApiId() == AT_COMMAND_RESPONSE) {
			xbee.getResponse().getAtCommandResponse(response);
			responseReceived=true;
			if (response.isOk()) {
				DPRINTSLN(DBG_QUERY, "Command successful");
				if (response.getValueLength() > 0) {
					DPRINTS(DBG_QUERY, "value length= ");
					DPRINTLN(DBG_QUERY, response.getValueLength());
					DPRINTS(DBG_QUERY, "response value [](HEX): ");
					for (int i = 0; i < response.getValueLength(); i++) {
						DPRINT(DBG_QUERY, i);
						DPRINTS(DBG_QUERY, "=");
						DPRINT(DBG_QUERY, (response.getValue()[i]<0x10 ? "0": ""));
						DPRINT(DBG_QUERY,response.getValue()[i], HEX);
						DPRINTS(DBG_QUERY, " ");
					}
					DPRINTSLN(DBG_QUERY, " ");
					DPRINTS(DBG_QUERY, "response value (HEX): ");
					for (int i = 0; i < response.getValueLength(); i++) {
						DPRINT(DBG_QUERY, (response.getValue()[i]<0x10 ? "0": ""));
						DPRINT(DBG_QUERY,response.getValue()[i], HEX);
					}
					DPRINTSLN(DBG_QUERY,"");

					if (response.getValueLength() > valueSize) {
						DPRINTS(DBG_DIAGNOSTIC,"XBeeClient::QueryParameterOnce: value too large! Param=");
						DPRINT(DBG_DIAGNOSTIC, param);
						DPRINTS(DBG_DIAGNOSTIC, ", bufferSize=")
								DPRINT(DBG_DIAGNOSTIC,valueSize);
						DPRINTS(DBG_DIAGNOSTIC,", valueSize=")
						DPRINTLN(DBG_DIAGNOSTIC,response.getValueLength());
						doNotRetry=true;
					} else {
						valueSize=response.getValueLength();
						for (int i = 0; i < valueSize; i++) {
							value[i]=response.getValue()[i];
						}

						result=true;
					}
				} else {
					// Null-length response: we assume it is an error unless the
					// valueSize is 0.
					if (valueSize >0) {
						DPRINTSLN(DBG_DIAGNOSTIC, "Error: null-length response received.");
					} else result=true;
				}
			}
			else {   // response nok.
#if (DBG_DIAGNOSTIC==1)
				printResponseStatus(param, response.getStatus());
#endif
				doNotRetry=(response.getStatus() >1);
			}
		}
		else if (xbee.getResponse().getApiId() == ExtendedTransmitStatus) {
			DPRINTSLN(DBG_SET,"Expected AT response but got ExtendedTransmitStatus ");
			DPRINTSLN(DBG_SET,"TODO extract delivery status from payload byte 5");
			DPRINTSLN(DBG_SET,"TODO extract discovery status from payload byte 6");
			DPRINTSLN(DBG_SET," (ignored)");
		}
		else {
			DPRINTS(DBG_SET,"Expected AT response but got ");
			DPRINT(DBG_SET,xbee.getResponse().getApiId(), HEX);
			DPRINTSLN(DBG_SET," (ignored)");
		}
	}
	if (!result) {
		// AT command failed
		if (responseReceived) {
			if (xbee.getResponse().isError()) {
				DPRINTS(DBG_DIAGNOSTIC,"Error reading packet. Error code: ");
				DPRINTLN(DBG_DIAGNOSTIC,xbee.getResponse().getErrorCode());
			} else {
				DPRINTSLN(DBG_DIAGNOSTIC,"Invalid response.");
			}
		}
		else {
			DPRINTSLN(DBG_SET,"No response from radio");
		}
	}
	return result;
}


bool XBeeClient::queryRemoteParameterOnce(const char* param,
		uint8_t value[], uint8_t &valueSize,
		bool &doNotRetry, XBeeAddress64 &remote) {
#if (DBG_REMOTE_QUERY==1)
	Serial<< "QueryRemoteParameterOnce param=" << param <<  ", valueSize=" <<valueSize << ENDL;
	Serial<< "   doNotRetry=" << doNotRetry << ", remote=";
	PrintHexWithLeadingZeroes(Serial, remote.getMsb());
	Serial << '-';
	PrintHexWithLeadingZeroes(Serial, remote.getLsb());
	Serial << ENDL;
#endif
	if(param[2]!= '\0') {
		DPRINTS(DBG_DIAGNOSTIC, "Error: invalid XBee parameter (aborted):");
		DPRINTLN(DBG_DIAGNOSTIC, param);
		return false;
	}
	RemoteAtCommandRequest req(remote, (uint8_t*) param);
	RemoteAtCommandResponse response;
	bool result=false, responseReceived=false;
	doNotRetry=false; // Only forbid retry in case it is hopeless.
	xbee.send(req);
	// wait for RemoteAT_ResponseDelay msec max for answer,
	// ignore whatever is received in the meantime
	elapsedMillis elapsed=0;
	while (!responseReceived && xbee.readPacket(RemoteAT_ResponseDelay-elapsed)) {
		// got a response! Should be an AT command response
		if (xbee.getResponse().getApiId() == REMOTE_AT_COMMAND_RESPONSE) {
			xbee.getResponse().getAtCommandResponse(response);
			responseReceived=true;
			if (response.isOk()) {
				DPRINTSLN(DBG_REMOTE_QUERY, "Command successful");
				if (response.getValueLength() > 0) {
					DPRINTS(DBG_REMOTE_QUERY, "value length= ");
					DPRINTLN(DBG_REMOTE_QUERY, response.getValueLength());
					DPRINTS(DBG_REMOTE_QUERY, "response value [](HEX): ");
					for (int i = 0; i < response.getValueLength(); i++) {
						DPRINT(DBG_REMOTE_QUERY, i);
						DPRINTS(DBG_REMOTE_QUERY, "=");
						DPRINT(DBG_REMOTE_QUERY, (response.getValue()[i]<0x10 ? "0": ""));
						DPRINT(DBG_REMOTE_QUERY,response.getValue()[i], HEX);
						DPRINTS(DBG_REMOTE_QUERY, " ");
					}
					DPRINTSLN(DBG_REMOTE_QUERY, " ");
					DPRINTS(DBG_REMOTE_QUERY, "response value (HEX): ");
					for (int i = 0; i < response.getValueLength(); i++) {
						DPRINT(DBG_REMOTE_QUERY, (response.getValue()[i]<0x10 ? "0": ""));
						DPRINT(DBG_REMOTE_QUERY,response.getValue()[i], HEX);
					}
					DPRINTSLN(DBG_REMOTE_QUERY,"");

					if (response.getValueLength() > valueSize) {
						DPRINTS(DBG_DIAGNOSTIC,"XBeeClient::QueryParameterOnce: value too large! Param=");
						DPRINT(DBG_DIAGNOSTIC, param);
						DPRINTS(DBG_DIAGNOSTIC, ", bufferSize=")
								DPRINT(DBG_DIAGNOSTIC,valueSize);
						DPRINTS(DBG_DIAGNOSTIC,", valueSize=")
						DPRINTLN(DBG_DIAGNOSTIC,response.getValueLength());
						doNotRetry=true;
					} else {
						valueSize=response.getValueLength();
						for (int i = 0; i < valueSize; i++) {
							value[i]=response.getValue()[i];
						}

						result=true;
					}
				} else {
					// Null-length response: we assume it is an error unless the
					// valueSize is 0.
					if (valueSize >0) {
						DPRINTSLN(DBG_DIAGNOSTIC, "Error: null-length response received.");
					} else result=true;
				}
			}
			else {
#if (DBG_DIAGNOSTIC==1)
				printResponseStatus(param, response.getStatus());
#endif
				doNotRetry=(response.getStatus() >1);
			}
		}
		else if (xbee.getResponse().getApiId() == ExtendedTransmitStatus) {
			DPRINTSLN(DBG_SET,"Expected Remote AT response but got ExtendedTransmitStatus ");
			DPRINTSLN(DBG_SET,"TODO extract delivery status from payload byte 5");
			DPRINTSLN(DBG_SET,"TODO extract discovery status from payload byte 6");
			DPRINTSLN(DBG_SET," (ignored)");
		}
		else {
			DPRINTS(DBG_SET,"Expected Remote AT response but got ");
			DPRINT(DBG_SET,xbee.getResponse().getApiId(), HEX);
			DPRINTSLN(DBG_SET," (ignored)");
		}
	}
	if (!result) {
		// AT command failed
		if (responseReceived) {
			if (xbee.getResponse().isError()) {
				DPRINTS(DBG_DIAGNOSTIC,"Error reading packet. Error code: ");
				DPRINTLN(DBG_DIAGNOSTIC,xbee.getResponse().getErrorCode());
			} else {
				DPRINTSLN(DBG_DIAGNOSTIC,"Invalid response.");
			}
		}
		else {
			DPRINTSLN(DBG_SET,"No response from radio");
		}
	}
	return result;
}

bool XBeeClient::setParameterOnce(const char* param,
		uint8_t value[], uint8_t valueSize,
		bool &doNotRetry, const uint32_t& sh, const uint32_t& sl) {
	DPRINTS(DBG_QUERY, "SetParameterOnce param=");
	DPRINTLN(DBG_QUERY, param);
	DPRINTS(DBG_QUERY, "valueSize=");
	DPRINTLN(DBG_QUERY, valueSize);
	if(param[2]!= '\0') {
		DPRINTS(DBG_DIAGNOSTIC, "Error: invalid XBee parameter (aborted):");
		DPRINTLN(DBG_DIAGNOSTIC, param);
		return false;
	}

	if ((sh==0) && (sl==0)) {
		return setLocalParameterOnce(param, value , valueSize,
				doNotRetry);
	}
	else {
		XBeeAddress64 remote(sh, sl);
		return setRemoteParameterOnce(param, value, valueSize,
				doNotRetry, remote);
	}
}


bool XBeeClient::setLocalParameterOnce(const char* param,
		uint8_t value[], uint8_t valueSize, bool &doNotRetry) {
	DPRINTS(DBG_SET, "SetLocalParameterOnce. Setting ");
	DPRINT(DBG_SET, param);
	DPRINTS(DBG_SET, " to value ");
	for (int i = 0; i < valueSize; i++) {
		DPRINT(DBG_SET, i);
		DPRINT(DBG_SET, ": 0x");
		DPRINT(DBG_SET, (value[i]<0x10 ? "0": ""));
		DPRINT(DBG_SET,value[i], HEX);
		DPRINTS(DBG_SET," ");
	}
	DPRINTSLN(DBG_SET,"");
	if(param[2]!= '\0') {
		DPRINTS(DBG_DIAGNOSTIC, "Error: invalid XBee parameter (aborted):");
		DPRINTLN(DBG_DIAGNOSTIC, param);
		return false;
	}
	AtCommandRequest req((uint8_t*) param, value, valueSize);
	AtCommandResponse response;
	bool result=false, responseReceived=false;
	doNotRetry=false; // forbid retries only when hopeless.
	xbee.send(req);
	// wait for AT_ResponseDelay msec max for answer, ignore whatever is received in the meantime
	elapsedMillis elapsed=0;
	while (!responseReceived && xbee.readPacket(AT_ResponseDelay-elapsed)) {
		// got a response! Should be an AT command response
		if (xbee.getResponse().getApiId() == AT_COMMAND_RESPONSE) {
			xbee.getResponse().getAtCommandResponse(response);
			responseReceived=true;
			if (response.isOk()) {
				DPRINTSLN(DBG_SET, "Command successful");
				result=true;
			}
			else {
#if (DBG_DIAGNOSTIC==1)
				printResponseStatus(param, response.getStatus());
#endif
				doNotRetry=(response.getStatus() >1);			}
		}
		else if (xbee.getResponse().getApiId() == ExtendedTransmitStatus) {
			DPRINTSLN(DBG_SET,"Expected AT response but got ExtendedTransmitStatus ");
			DPRINTSLN(DBG_SET,"TODO extract delivery status from payload byte 5");
			DPRINTSLN(DBG_SET,"TODO extract discovery status from payload byte 6");
			DPRINTSLN(DBG_SET," (ignored)");
		}
		else {
			DPRINTS(DBG_SET,"Expected AT response but got 0x");
			DPRINT(DBG_SET,xbee.getResponse().getApiId(), HEX);
			DPRINTSLN(DBG_SET," (ignored)");
		}
	}
	if (!result) {
		// AT command failed
		if (xbee.getResponse().isError()) {
			DPRINTS(DBG_DIAGNOSTIC,"Error reading packet. Error code: ");
			DPRINTLN(DBG_DIAGNOSTIC,xbee.getResponse().getErrorCode());
		}
		else {
			DPRINTSLN(DBG_SET,"No response from radio");
		}
	}
	return result;
}


bool XBeeClient::setRemoteParameterOnce(const char* param,
		uint8_t value[], uint8_t valueSize,
		bool &doNotRetry, XBeeAddress64 &remote) {
	DPRINTS(DBG_SET, "SetRemoteParameterOnce. Setting ");
	DPRINT(DBG_SET, param);
	DPRINTS(DBG_SET, " to value ");
	for (int i = 0; i < valueSize; i++) {
		DPRINT(DBG_SET, i);
		DPRINT(DBG_SET, ": 0x");
		DPRINT(DBG_SET, (value[i]<0x10 ? "0": ""));
		DPRINT(DBG_SET,value[i], HEX);
		DPRINTS(DBG_SET," ");
	}
	DPRINTSLN(DBG_SET,"");
	if(param[2]!= '\0') {
		DPRINTS(DBG_DIAGNOSTIC, "Error: invalid XBee parameter (aborted):");
		DPRINTLN(DBG_DIAGNOSTIC, param);
		return false;
	}
	RemoteAtCommandRequest req(remote,(uint8_t*) param, value, valueSize);
	RemoteAtCommandResponse response;
	bool result=false, responseReceived=false;
	doNotRetry=false; // forbid retries only when hopeless.
	xbee.send(req);
	// wait for AT_ResponseDelay msec max for answer, ignore whatever is received in the meantime
	elapsedMillis elapsed=0;
	while (!responseReceived && xbee.readPacket(RemoteAT_ResponseDelay-elapsed)) {
		// got a response! Should be an AT command response
		if (xbee.getResponse().getApiId() == REMOTE_AT_COMMAND_RESPONSE) {
			xbee.getResponse().getAtCommandResponse(response);
			responseReceived=true;
			if (response.isOk()) {
				DPRINTSLN(DBG_SET, "Command successful");
				result=true;
			}
			else {
#if (DBG_DIAGNOSTIC==1)
				printResponseStatus(param, response.getStatus());
#endif
				doNotRetry=(response.getStatus() >1);			}
		}
		else if (xbee.getResponse().getApiId() == ExtendedTransmitStatus) {
			DPRINTSLN(DBG_SET,"Expected AT response but got ExtendedTransmitStatus ");
			DPRINTSLN(DBG_SET,"TODO extract delivery status from payload byte 5");
			DPRINTSLN(DBG_SET,"TODO extract discovery status from payload byte 6");
			DPRINTSLN(DBG_SET," (ignored)");
		}
		else {
			DPRINTS(DBG_SET,"Expected Remote AT response but got 0x");
			DPRINT(DBG_SET,xbee.getResponse().getApiId(), HEX);
			DPRINTSLN(DBG_SET," (ignored)");
		}
	}
	if (!result) {
		// AT command failed
		if (xbee.getResponse().isError()) {
			DPRINTS(DBG_DIAGNOSTIC,"Error reading packet. Error code: ");
			DPRINTLN(DBG_DIAGNOSTIC,xbee.getResponse().getErrorCode());
		}
		else {
			DPRINTSLN(DBG_SET,"No response from radio");
		}
	}
	return result;
}


bool XBeeClient::checkParameter(	const char* param,
									uint64_t value,
									bool updateConfiguration,
									bool &configurationChanged,
									const uint32_t& sh, const uint32_t& sl)
{
	DPRINTSLN(DBG_CHECK,"CheckParameter/uint64_t");
	uint64_t currentValue;

	if (!queryParameter(param, currentValue, sh, sl)) return false;
	if (currentValue != value) {
#if (DBG_DIAGNOSTIC==1)
		Serial << param << " is not OK: ";
		PrintHexWithLeadingZeroes(Serial,currentValue);
		Serial  <<  " instead of ";
		PrintHexWithLeadingZeroes(Serial, value);
		if (!updateConfiguration) Serial << ENDL;
#endif

		if (!updateConfiguration) return false;
		if (!setParameter(param, value, sh, sl)) {
			DPRINTLN(DBG_DIAGNOSTIC, " (update failed)");
			return false;
		}
		DPRINTLN(DBG_DIAGNOSTIC, " (updated)");
		configurationChanged=true;
	}
	else {
		DPRINT(DBG_CHECK, param);
		DPRINTSLN(DBG_CHECK, " OK");
	}
    return true;
}

bool XBeeClient::checkParameter(	const char* param,
									uint32_t value,
									bool updateConfiguration,
									bool &configurationChanged,
									const uint32_t& sh, const uint32_t& sl)
{
	DPRINTSLN(DBG_CHECK,"CheckParameter/uint32_t");
	uint32_t currentValue;

	if (!queryParameter(param, currentValue, sh,sl)) return false;
	if (currentValue != value) {
#if (DBG_DIAGNOSTIC==1)
		Serial << param << " is not OK: ";
		PrintHexWithLeadingZeroes(Serial,currentValue);
		Serial  <<  " instead of ";
		PrintHexWithLeadingZeroes(Serial, value);
		if (!updateConfiguration) Serial << ENDL;
#endif

		if (!updateConfiguration) return false;
		if (!setParameter(param, value, sh, sl)) {
			DPRINTLN(DBG_DIAGNOSTIC, " (update failed)");
			return false;
		}
		DPRINTLN(DBG_DIAGNOSTIC, " (updated)");
		configurationChanged=true;
	}
	else {
		DPRINT(DBG_CHECK, param);
		DPRINTSLN(DBG_CHECK, " OK");
	}
    return true;
}

bool XBeeClient::checkParameter(	const char* param,
									uint16_t value,
									bool updateConfiguration,
									bool &configurationChanged,
									const uint32_t& sh, const uint32_t& sl)
{
	DPRINTSLN(DBG_CHECK,"CheckParameter/uint16_t");
	uint16_t currentValue;

	if (!queryParameter(param, currentValue, sh,sl)) return false;
	if (currentValue != value) {
#if (DBG_DIAGNOSTIC==1)
		Serial << param << " is not OK: ";
		PrintHexWithLeadingZeroes(Serial,currentValue);
		Serial  <<  " instead of ";
		PrintHexWithLeadingZeroes(Serial, value);
		if (!updateConfiguration) Serial << ENDL;
#endif

		if (!updateConfiguration) return false;
		if (!setParameter(param, value, sh,sl)) {
			DPRINTLN(DBG_DIAGNOSTIC, " (update failed)");
			return false;
		}
		DPRINTLN(DBG_DIAGNOSTIC, " (updated)");
		configurationChanged=true;
	}
	else {
		DPRINT(DBG_CHECK, param);
		DPRINTSLN(DBG_CHECK, " OK");
	}
    return true;
}

bool XBeeClient::checkParameter(	const char* param,
									uint8_t value,
									bool updateConfiguration,
									bool &configurationChanged,
									const uint32_t& sh, const uint32_t& sl)
{
#if (DBG_CHECK==1)
	Serial << "CheckParameter/uint8_t, param=" << param << ", value=" << value << ENDL;
	Serial << "sh-sl: "; PrintHexWithLeadingZeroes(Serial, sh);
	Serial << " - : "; PrintHexWithLeadingZeroes(Serial, sl);
	Serial << ENDL;
#endif
	uint8_t currentValue;

	if (!queryParameter(param, currentValue,sh,sl)) return false;
	if (currentValue != value) {
#if (DBG_DIAGNOSTIC==1)
		Serial << param << " is not OK: ";
		PrintHexWithLeadingZeroes(Serial, currentValue);
		Serial  <<  " instead of ";
		PrintHexWithLeadingZeroes(Serial, value);
		if (!updateConfiguration) Serial << ENDL;
#endif

		if (!updateConfiguration) return false;
		if (!setParameter(param, value, sh,sl)) {
			DPRINTLN(DBG_DIAGNOSTIC, " (update failed)");
			return false;
		}
		DPRINTLN(DBG_DIAGNOSTIC, " (updated)");
		configurationChanged=true;
	}
	else {
		DPRINT(DBG_CHECK, param);
		DPRINTSLN(DBG_CHECK, " OK");
	}
    return true;
}

bool XBeeClient::checkParameter(	const char* param,
									const char* value,
									bool updateConfiguration,
									bool &configurationChanged,
									const uint32_t& sh, const uint32_t& sl)
{
#if (DBG_CHECK==1)
	Serial << "CheckParameter/const char*, param=" << param << ", value=" << value << ENDL;
	Serial << "sh-sl: "; PrintHexWithLeadingZeroes(Serial, sh);
	Serial << " - : "; PrintHexWithLeadingZeroes(Serial, sl);
	Serial << ENDL;
#endif
	char currentValue[MaxStringValueLength];

	if (!queryParameter(param, currentValue,MaxStringValueLength, sh, sl)) return false;
	if (strcmp(currentValue,value)) {
#if (DBG_DIAGNOSTIC==1)
		Serial << param << " is not OK: '";
		Serial  << currentValue <<  "' instead of '" << value << "'";
		if (!updateConfiguration) Serial << ENDL;
#endif

		if (!updateConfiguration) return false;
		if (!setParameter(param, value, sh,sl)) {
			DPRINTLN(DBG_DIAGNOSTIC, " (update failed)");
			return false;
		}
		DPRINTLN(DBG_DIAGNOSTIC, " (updated)");
		configurationChanged=true;
	}
	else {
		DPRINT(DBG_CHECK, param);
		DPRINTSLN(DBG_CHECK, " OK");
	}
    return true;
}

bool XBeeClient::checkParameter(	const char* param,
									bool value,
									bool updateConfiguration,
									bool &configurationChanged,
									const uint32_t& sh, const uint32_t& sl)
{
	DPRINTSLN(DBG_CHECK,"CheckParameter/bool");
	bool currentValue;

	if (!queryParameter(param, currentValue, sh,sl)) return false;
	if (currentValue != value) {
#if (DBG_DIAGNOSTIC==1)
		Serial << param << " is not OK: 0x";
		Serial.print(currentValue, HEX);
		Serial  <<  " instead of ";
		Serial.print(value, HEX);
		if (!updateConfiguration) Serial << ENDL;
#endif

		if (!updateConfiguration) return false;
		if (!setParameter(param, value, sh, sl)) {
			DPRINTLN(DBG_DIAGNOSTIC, " (update failed)");
			return false;
		}
		DPRINTLN(DBG_DIAGNOSTIC, " (updated)");
		configurationChanged=true;
	}
	else {
		DPRINT(DBG_CHECK, param);
		DPRINTSLN(DBG_CHECK, " OK");
	}
    return true;
}

bool XBeeClient::checkDevicetype( XBeeTypes::DeviceType value,
		 	 	 	 	bool correctConfiguration,
						bool &configurationChanged,
						const uint32_t& sh, const uint32_t& sl)
{
	XBeeTypes::DeviceType currentType=getDeviceType(sh, sl);
	if (currentType==value) {
		DPRINTSLN(DBG_CHECK, "Device type OK");
		return true;
	}
#if (DBG_DIAGNOSTIC==1)
	Serial << "Device type is not OK: " << getLabel(currentType);
	Serial  <<  " instead of "<< getLabel(value);
	if (!correctConfiguration) Serial << ENDL;
#endif
	if (correctConfiguration) {
		if (setDeviceType(value, sh, sl)) {
			DPRINTSLN(DBG_DIAGNOSTIC, " (updated)");
			configurationChanged=true;
			return true;
		} else return false;
	} else return false;
}

XBeeTypes::DeviceType XBeeClient::getDeviceType(const uint32_t& sh, const uint32_t& sl) {
	bool CE_Value, SM_Value;
	if (!queryParameter("CE", CE_Value, sh, sl)) return XBeeTypes::DeviceType::Unknown;
	if (!queryParameter("SM", SM_Value, sh, sl)) return XBeeTypes::DeviceType::Unknown;
	if (CE_Value) return XBeeTypes::DeviceType::Coordinator;
	else {
		if (SM_Value) return XBeeTypes::DeviceType::EndDevice;
		else return XBeeTypes::DeviceType::Router;
	}
}

bool XBeeClient::setDeviceType(XBeeTypes::DeviceType type,
		const uint32_t& sh,const uint32_t& sl) {
	bool result;
	switch(type) {
	case XBeeTypes::DeviceType::Coordinator:
		// Set SM to 0 first! (it disables the Sleep Mode)
		result = setParameter("SM", false, sh, sl);
		if (!setParameter("CE", true, sh, sl)) result=false;
		break;
	case XBeeTypes::DeviceType::Router:
		// Set SM to 0 first!
		result = setParameter("SM", false, sh, sl);
		if (!setParameter("CE", false, sh, sl)) result=false;
		break;
	case XBeeTypes::DeviceType::EndDevice:
		// Set CE to 0 first!
		// This sets the sleeping mode to 1 (pin sleep enabled).
		result = setParameter("CE", false, sh, sl);
		if (!setParameter("SM", true, sh, sl)) result=false;
		break;
	case XBeeTypes::DeviceType::Unknown:
	default:
		result=false;
		DPRINTS(DBG_SET, "Invalid device type received: ");
		DPRINTLN(DBG_DIAGNOSTIC, (uint8_t) type);

	}
	return result;
}
const char* XBeeClient::getLabel(XBeeTypes::DeviceType value) {
	switch(value) {
	case XBeeTypes::DeviceType::Coordinator:
		return "Coordinator";
		break;
	case XBeeTypes::DeviceType::Router:
		return "Router";
		break;
	case XBeeTypes::DeviceType::EndDevice:
		return "EndDevice";
		break;
	case XBeeTypes::DeviceType::Unknown:
		return "Unknown";
		break;
	default:
		return "** Invalid device type **";
	}
}

bool XBeeClient::setTransmissionPower(XBeeTypes::TransmissionPowerLevel level,
		const uint32_t& sh, const uint32_t& sl) {
  autoWakeUpXBeeModule();
  bool result = setParameter("PL", (uint8_t) level, sh , sl);
  if (!result){
	  DPRINTS(DBG_DIAGNOSTIC,"*** Error configuring RF Power to level ");
  } else {
	  DPRINTS(DBG_DIAGNOSTIC,"RF Power set to level ");
  }
  DPRINTLN(DBG_DIAGNOSTIC, (int) level);
#if (DBG_POWER_LEVEL==1)
  printConfigurationSummary(Serial, sh, sl);
#endif
  autoPutXBeeModuleToSleep();
  return  result;
}

bool XBeeClient::ModuleInSleepMode() const {
	return (sleepRequestPin && digitalRead(sleepRequestPin));
}

#endif // API_MODE
