/*
 * GMiniCSPU.h
 * 
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the library should include a tag @ingroup GMiniCSPU
 * in the class documentation block.
 */

 /** @defgroup GMiniCSPU GMiniCSPU library
 *  @brief The library of classes specific to the CanSat 2021/2022 G-Mini project.
 *  
 *  The GMiniCSPU library contains all the code for the G-Mini project, which is assumed
 *  not to be reused across project
 *  
 *  _Dependencies_\n
 *  This library requires the following other generic libraries (in addition to hardware-specific libraries,
 *  and standard Arduino libraries, which are not listed here):
 *  - DebugCSPU
 *  - TimeCSPU
 *  - elapsedMillis
 *  - cansatAsgardCSPU
 *  @todo TO BE COMPLETED.
 *  
 *  
 *  _History_\n
 *  The library was created by the 2021/2022 Cansat team (G-Mini) based on the TorusCSPU,
 *  IsatisCSPU, IsaTwoCSPU and SpySeeCanCSPU libraries.
 */
