/*
 * GMiniRecordExample.h
 */

#pragma once
#include "GMiniConfig.h"
#include "GMiniRecord.h"
#include "CansatInterface.h"

/**  @ingroup GMiniCSPU
 *   @brief A subclass of GMiniRecord, used for tests.
 * 	 This class is not used operationally, but included in the library
 * 	 because it is shared by several tests.
 *
 * @warning: This class is used in the tests of the GMini RF communication
 * 		     strategies. Do no modify it without ensuring compatibility
 */
class GMiniRecordExample: public GMiniRecord {
public:

	// Test-only methods
	void initValues() ;
	bool checkValues(bool ignoreTimestamp=false);

	// Some constants shared by test programs.
	static constexpr const char* testString=
			"This is a reasonably long test string for the CansatXBeeClient. How nice!";
	static constexpr CansatFrameType TestStringType=CansatFrameType::StatusMsg;
	static constexpr uint8_t SequenceNumber=37;

};
