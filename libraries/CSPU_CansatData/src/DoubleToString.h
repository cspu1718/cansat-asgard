/*
 * DoubleToString.h
 */

#pragma once

/** Convert a double to string, controlling number of decimals
 *  The value is rounded and the resulting string (in buffer)
 *  is null-terminated.
 *  @param value The double to convert
 *  @param buffer The string buffer to fill. It is expected to
 *  be large enough.
 *  @param precision The required number of decimals. */
void doubleToString(double value, char* buffer, int precision);




