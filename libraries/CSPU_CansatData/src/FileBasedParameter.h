/*
 * FileBasedParameter.h
 */

#pragma once
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <SdFat.h>
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_FILE_PARAM 0
#define PARAM_FILE_INCLUDE_DELETE_METHODS

/** A class providing services to read/write configuration parameters from an SD card.
 *  Each parameter is stored in its own file, named params/parameterName.txt.
 *  This is used for parameters which needs to be changed without recompiling/re-uploading
 *  the program.
 *
 *  @example
 *  @code
 *    // obtain parameter xxxx, with type uint8_t
 *	  SdFat sd(...);
 *	  uint8_t myParameter;
 *    FileBasedParameter<uint8_t>::read(sd, "xxxx", myParameter, 0);
 *    // use myParameter;
 *  @endcode
 */
class FileBasedParameter {
	public:
	/** Read the value of a parameter from the file on the SD card.
	 *  If the file cannot be found, the default value is returned.
	 *  @param sd The fully initialized SdFat object to use to access the file system.
	 *  @param parameterName The name of the parameter (file will be named params/parameterName.txt).
	 *  @param value (out) The variable to set to the parameter value.
	 *  @param defaultValue The value to use in case the file cannot be read, for any
	 *  	   reason (file system not accessible, inexistent file...). It will be cast
	 *  	   to type T.
	 *  @return True if the parameter was successfully read, false if it failed and the
	 *          default value is returned.
	 */
	template<class T>
	static bool read(SdFat& sd, const char* parameterName, T& value, const T defaultValue) {
		File f;
		if (!openParamFile(sd, parameterName, f, false)) {
			value=(T) defaultValue;
			DPRINTSLN(DBG_FILE_PARAM, "returning default value");
			return false;
		}
		parseA_Value(f, value);
		f.close();
		DPRINTS(DBG_FILE_PARAM, "read value ");
		DPRINTLN(DBG_FILE_PARAM, value,10);
		return true;
	};

	/** Write the value of an integer parameter to file on the SD card.
	 *  If the file exists, it is overwritten
	 *  @param sd The fully initialized SdFat object to use to access the file system.
	 *  @param parameterName The name of the parameter (file will be named params/(first8charsOfParameterName).txt).
	 *         Warning: the 8 first characters of the parameter file name must be unique
	 *         otherwise files will silently be overwritten.
	 *  @param value  The value to store in the file.
	 *  @return True if the parameter was successfully written, false if it failed for any
	 *  		reason
	 */
	template<class T>
	static bool write(SdFat& sd, const char* parameterName, const T& value) {
		File f;
		if (!openParamFile(sd, parameterName, f, true)) return false;
		f.print(value);
		f.close();
		DPRINTS(DBG_FILE_PARAM, "Wrote value ");
		DPRINTLN(DBG_FILE_PARAM, value);
		return true;
	};
	/** Write the value of a float parameter to file on the SD card.
	 *  If the file exists, it is overwritten
	 *  @param sd The fully initialized SdFat object to use to access the file system.
	 *  @param parameterName The name of the parameter (file will be named params/(first8charsOfParameterName).txt).
	 *         Warning: the 8 first characters of the parameter file name must be unique
	 *         otherwise files will silently be overwritten.
	 *  @param value  The value to store in the file.
	 *  @return True if the parameter was successfully written, false if it failed for any
	 *  		reason
	 */
	static bool write(SdFat& sd, const char* parameterName, const float& value) {
		File f;
		if (!openParamFile(sd, parameterName, f, true)) return false;
		f.print(value,6); // Not more than 6 digits, otherwise parsing can fail.
		f.close();
		DPRINTS(DBG_FILE_PARAM, "Wrote value ");
		DPRINTLN(DBG_FILE_PARAM, value, 6);
		return true;
	};

#ifdef PARAM_FILE_INCLUDE_DELETE_METHODS
	/** Delete the file of a particular parameter from the SD card.
	 *  @param sd The fully initialized SdFat object to use to access the file system.
	 *  @param parameterName The name of the parameter (file will be named
	 *  	   params/(first8charsOfParameterName).txt).
	 *         Warning: the 8 first characters of the parameter file name must be unique
	 *         otherwise files will silently be overwritten.
	 *  @return True if the file was successfully deleted or does not exist, false if
	 *  	    deletion failed for any reason
	 */
	static bool deleteFile(SdFat& sd, const char* parameterName);

	/** Delete all parameter files from the SD card, and the 'params' directory
	 *  @param sd The fully initialized SdFat object to use to access the file system.
	 *  @return True if the file was successfully deleted or does not exist, false if
	 *  	    deletion failed for any reason
	 */
	static bool deleteAll(SdFat& sd);
#endif

	static void getParamFileName(const char* parameterName, char fileName[13]);

	protected:
	static constexpr const char* DirectoryPath="/params";

	static bool openParamFile(SdFat& sd, const char* parameterName, File &f, bool write);

	static bool goToParamsDirectory(SdFat& sd) ;

	static void parseA_Value(File& theOpenFile, long int& value) {value=theOpenFile.parseInt();};
	static void parseA_Value(File& theOpenFile, long unsigned int& value) { value= (long unsigned int) theOpenFile.parseInt();};
	static void parseA_Value(File& theOpenFile, unsigned char& value) { value= (unsigned char) theOpenFile.parseInt();};
	static void parseA_Value(File& theOpenFile, signed char& value) { value= (signed char) theOpenFile.parseInt();};
	static void parseA_Value(File& theOpenFile, short int& value) { value= (short int) theOpenFile.parseInt();};
	static void parseA_Value(File& theOpenFile, short unsigned int& value) { value= (short unsigned int) theOpenFile.parseInt();};
	static void parseA_Value(File& theOpenFile, float& value) {value=theOpenFile.parseFloat();};

};
