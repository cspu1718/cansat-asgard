/*
 * DoubleToString.cpp
 */

#include "CansatConfig.h"
#include "CSPU_Debug.h"
#include "DoubleToString.h"

#define DBG_DBL_2_STR 0

void doubleToString(double value, char* buffer, int precision) {
  long intPart = (long)value;  // Extract integer part
  double decimalPart = value - intPart;  // Extract decimal part
  if (decimalPart < 0) {
    decimalPart = -decimalPart;
  }
  // Convert integer part to string
  sprintf(buffer, "%ld.", intPart);

  // rounding
  float delta=0.5;
  int i = precision;
  while (i-- > 0) delta /= 10;
  decimalPart += delta;
#if (DBG_DBL_2_STR == 1)
  Serial << " Delta: ";
  Serial.print(delta, precision+3);
  Serial << " Rounded decimal part: ";
  Serial.println(decimalPart, precision+3);
#endif
  // Handle decimals manually
  // If first decimal positions are 0, the zeroes are lost.
  int lostZeroes=0;
  for (int i = 0; i < precision; i++) {
	decimalPart *= 10;
	if (decimalPart < 1.0) {
		lostZeroes++;
		DPRINTSLN(DBG_DBL_2_STR, "lost 1 zero");
	}
#if (DBG_DBL_2_STR == 1)
  Serial << " Decimal part (after multiplying by 10: ";
  Serial.println(decimalPart, precision+3);
#endif
  }

  // If the last decimal is still 0, we should ONLY print zeroes.
  for (int i = 0; i < lostZeroes; i++) {
	  strcat(buffer, "0");
  }
  if (lostZeroes < precision) {
    sprintf(buffer + strlen(buffer), "%ld", (long)decimalPart);
  }

#if (DBG_DBL_2_STR == 1)
  Serial << "  value=";
  Serial.print(value, precision+3);
  Serial << ", result = '" << buffer << "', decimalPart = " << decimalPart << ENDL;
#endif
}


