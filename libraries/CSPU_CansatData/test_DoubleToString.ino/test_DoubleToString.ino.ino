/*
 * Test program for DoubleToString function 
 */

#include "CSPU_Debug.h"
#include "DoubleToString.h"

int errorCounter = 0;

bool checkConversion(double value, char* buffer, uint8_t precision, const char* expected) {
  bool result = true;
  doubleToString(value, buffer, precision);
  Serial << "Testing ";
  Serial.print(value, precision + 3);
  Serial << ", with " << precision << " decimals:";
  if (strcmp(buffer, expected) != 0) {
    Serial << "*** Error: Expected '" << expected << "', got: '" << buffer << "'" << ENDL;
    errorCounter++;
    result = false;
  } else {
    Serial << "'" << buffer << "': OK" << ENDL;
  }
  return result;
}

void setup() {
  DINIT(115200);
  char buffer[25];

  checkConversion(1.2, buffer, 5, "1.20000");
  checkConversion(1.2, buffer, 3, "1.200");
  checkConversion(1.21111, buffer, 5, "1.21111");
  checkConversion(1.2111, buffer, 5, "1.21110");
  checkConversion(-2.34567, buffer, 5, "-2.34567");
  checkConversion(-4.67897, buffer, 5, "-4.67897");
  checkConversion(-44.34507, buffer, 5, "-44.34507");
  checkConversion(-7.1233, buffer, 5, "-7.12330");

  // Rounding
  checkConversion(1.199996, buffer, 5, "1.20000");
  checkConversion(1.200004, buffer, 3, "1.200");
  checkConversion(1.211108, buffer, 5, "1.21111");
  checkConversion(1.211102, buffer, 5, "1.21110");
  checkConversion(-2.345671, buffer, 5, "-2.34567");
  checkConversion(-4.67896999, buffer, 5, "-4.67897");
  checkConversion(-44.345073, buffer, 5, "-44.34507");
  checkConversion(-7.01233001, buffer, 5, "-7.01233");
  checkConversion(-7.001233001, buffer, 5, "-7.00123");
  checkConversion(-7.0001233001, buffer, 5, "-7.00012");
  checkConversion(7.00001233001, buffer, 5, "7.00001");
  checkConversion(-7.000001233001, buffer, 5, "-7.00000");
  checkConversion(-7.000005556, buffer, 5, "-7.00001");


  Serial << "Terminated with " << errorCounter << " error(s)." << ENDL;
}

void loop() {
}
