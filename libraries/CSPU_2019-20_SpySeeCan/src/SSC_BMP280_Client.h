/* 
 *  SSC_BMP280_Client.h
 */
#pragma once

#include "SSC_Record.h"
#include "BMP_Client.h"
#include "Adafruit_BMP280.h"
#include "Adafruit_Sensor.h"
#include "Wire.h"
/** @ingroup SpySeeCanCSPU
    xxxxxxx
*/
class SSC_BMP280_Client : public BMP_Client {
  public:
  
    /** @brief Read sensor data and populate data record.
     *  @param record The data record to populate (fields temparature, altitude and pressure).
     *  @return True if reading was successful.
     */
    bool readData(SSC_Record& record);
};
