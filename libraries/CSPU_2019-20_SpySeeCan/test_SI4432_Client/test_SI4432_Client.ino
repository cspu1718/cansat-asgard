///                 FEATHER      SI4432
///                 GND----------GND-\ (ground in)
///                              SDN-/ (shutdown in)
///                 3V3----------VCC   (3.3V in)
///  interrupt 0 pin D2----------NIRQ  (interrupt request out)
///          SS pin D10----------NSEL  (chip select in)
///             SCK pin----------SCK   (SPI clock in)
///            MOSI pin----------SDI   (SPI Data in)
///            MISO pin----------SDO   (SPI data out)
///                           /--GPIO0 (GPIO0 out to control transmitter antenna TX_ANT)
///                           \--TX_ANT (TX antenna control in) RFM22B only
///                           /--GPIO1 (GPIO1 out to control receiver antenna RX_ANT)
///                           \--RX_ANT (RX antenna control in) RFM22B only


#include "SI4432_Client.h"

SI4432_Client si4432(5, 10);
powerDBm_t readings[10];
powerDBm_t readings2[11];
void setup() {
  Serial.begin(115200);
  while(!Serial);
  si4432.begin();
}

void loop() {
  Serial.println("1 read : ");
  Serial.flush();
  Serial.println(si4432.readPower(500));
  Serial.println(si4432.readPower(450));
  Serial.println(si4432.readPower(550));
  si4432.readPower(readings, 10, 433, 866,30);
  si4432.readPower(readings2, 11, 433, 533, 10);
  Serial.println("10 reads");
  for(unsigned int i = 0; i< 10; i++){
    Serial.print("First table");
    Serial.print("Channel ");
    Serial.print(i);
    Serial.print(" : ");
    Serial.print(readings[i]);
    Serial.println("  (dbm) ");
  }
  Serial.println();
  for(unsigned int i = 0; i< 11; i++){
    Serial.print("Second table");
    Serial.print("Channel ");
    Serial.print(i);
    Serial.print(" : ");
    Serial.print(readings2[i]);
    Serial.println("  (dbm) ");
  }
  Serial.println();Serial.println();
  delay(500);
}
