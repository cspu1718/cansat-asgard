/*
   EtheraSecondaryMissionController.h
*/

#pragma once
#include "float.h"
#include "CansatXBeeClient.h"
#include "SecondaryMissionController.h"
#include "EtheraRecord.h"
#include "EtheraConfig.h"
#include "BMP_Client.h"
#ifdef RF_ACTIVATE_API_MODE
#  include "CansatXBeeClient.h"
#endif
#include "LinearStepper.h"
#include "RSSI_CycleManager.h"

/** @ingroup EtheraCSPU
 * @brief our secondary mission manager for the Ethera Project
  This class can :
  - use the velocity in the record to make a moving average
  - check if the velocity in the record are valid
  - check if the average is valid
  - put the velocity values in a table and clear it if he receives an invalid velocity
  - detects the takeoff
  - manages airbrakes and RSSI monitoring
  By doing this last action, this class can actually modify one of the data contained in the records.
*/
class EtheraSecondaryMissionController: public SecondaryMissionController {

  public:
    EtheraSecondaryMissionController() :
      rssiCycleManagerOK(false),
      linearStepper(nullptr),
	  // ---- Next to be moved to superclass
      takeoffTimestamp(0) ,
	  averageVelocity(InvalidVelocity),
	  currentVelocityIndex(0),
      velocityAverageValid(false),
	  lastRecordTimestamp(0),
      takeoffHappened(false),
	  ejectionHappened(false),
	  currentFlightStatus(FlightControlCode::StartingUp),
	  lastFlightStatusUpdate(0),
	  shouldUpdateFlightPhase(false),
	  currentFlightPhase(CansatUndefinedFlightPhase) { };

    /** Initialize the controller before use
        @return true if initialization is successful, false otherwise
    */
#ifdef RF_ACTIVATE_API_MODE
    virtual bool begin(CansatXBeeClient* xbeeClient = nullptr) override;
#else
    virtual bool begin(Stream* RF_Stream = nullptr);
#endif

    void manageSecondaryMission(CansatRecord & record) override;

    LinearStepper * getLinearStepper() {
      return linearStepper;
    };


    float getAverageVelocity(); /**< return the velocity average if it is valid **/
    static constexpr uint8_t NumberOfVelocitySamples =
    		(CansatVelocityAveragingPeriod * 1000 / CansatAcquisitionPeriod) + 1;
    		/**< this is the number of the samples in order to fill entirely a table and get a valid velocity average */
    static constexpr uint16_t StartupDelayForFlightAnalysis = 10000;
    		/**< Delay in msec to start using the altitude information to analyse the flight.
    		 *  This delay is converted in a number of records to ignore at startup. */
    static constexpr float InvalidVelocity = 1000000;
    bool takeoffDetected(); /**< true if the takeoff has been detected */
    bool ejectionDetected() {return ejectionHappened; };
  protected:

  private:
     /** Initialize all Ethera-specific resources
      * @return True if initialisation ok, false otherwise.
      */
     bool initEthera();

	 /** Update airbrakes position based on flight status (as updated by updateStatus()
	  *  and populate the record accordingly.
	  */
    void manageAirBrakes(EtheraRecord &record);

    /** Set airbrakes to position 0 (=closed) and possibly force by an additional
     *  EtheraNumStepsToUseToForceZero steps. Forcing is only performed at most every
     *  EtheraDelayBetweenForcedZero msec.
     */
    void closeAndForceAirbrakes();

    RSSI_CycleManager rssiCycleManager; /**< The class managing RSSI measures */
    bool rssiCycleManagerOK;	    /**< If false, the RSSI cycle manager is not used */
    LinearStepper* linearStepper;	/**< The interface to the linear steppers controlling the airbrakes.
     	 	 	 	 	 	 	 	 	 If null, air-brakes are not managed. */

    // -------------------------------------------
    // Methods and data members below to move to superclass SecondaryMissionControllerWithFlightControl .
    // This required a subclass of CansatRecord including the flightControlCode (CansatRecordWithFlightControl?).
    // Also move all methods related to the average velocity calculation to superclass.
    // Ditto for updateStatus().
    // --------------------------------------------
    /** Update flight status based on the incoming record, i.e. identify the
	 *  phase we currently are in.
	 */
	virtual void updateFlightStatus(const EtheraRecord &record);

	/** This method stores the velocity within the record in a table and averages it.
	 *  Then, it checks if velocities got are valid and then allows us to know if the
	 *  takeoff has already happened, and whether the can is already out of the rocket.
	      @param record containing the descent velocity. Nothing is changed in the record.
	 */
    virtual void analyseDescentVelocity(const EtheraRecord & record);

	/** Update the current flight phase if shouldUpdateFlightPhase is true.
		@param record The record containing the absolute and reference altitude
	*/
	virtual void updateFlightPhase(const EtheraRecord& record);

    void clearAverageVelocity(); /**< clear up all data of the table */
    void printVelocityTable(); /**< it prints the table with the descent velocity variable within the record */

    unsigned long takeoffTimestamp; /**< timestamp of the takeoff */
    float velocityTable[NumberOfVelocitySamples] = {}; /**< table used to store the velocity values of the record */
    float averageVelocity; /**< current average of the velocity in the table*/
    uint8_t currentVelocityIndex; /**< indicates which index or row of the table we are in */
    bool velocityAverageValid; /**< if true when the table is fully filled with  valid velocity values */
    unsigned long lastRecordTimestamp; /**< timestamp of the current last record */
    bool takeoffHappened; /**< if true the takeoff of the rocket is detected */
    bool ejectionHappened; /**< if true, the ejection of the can from the rocket happened */
    FlightControlCode currentFlightStatus; /**< The current flight status */
    unsigned long lastFlightStatusUpdate; /**< When was the controller info last updated.0 = never */
    bool shouldUpdateFlightPhase; /**< Should the flight phase be updated in the next call updateFlightPhase?
    										   (should be true if in nominal descent and delay for updating has passed) */
	uint8_t currentFlightPhase; /**< The current flight phase. 15=no flight phase could be determined. */


};
