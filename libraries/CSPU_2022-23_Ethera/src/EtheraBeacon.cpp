
#include "EtheraBeacon.h"
#include "EtheraConfig.h"
#include "CSPU_Debug.h"
#include "EtheraRecord.h"

EtheraBeacon::EtheraBeacon() :  rf69(RFM69_ChipSelectPin, RFM69_InterruptPin)
{}

void EtheraBeacon::setInfoInMsg() {
	char strTS[20];
	sprintf(strTS, " msg #%ld ts=%ld ", seqNumber, millis());
	memcpy(&beaconMessage[strlen(MsgPrefix)], strTS, strlen(strTS));
}

void EtheraBeacon::initBeaconMessage() {
	strcpy(beaconMessage, MsgPrefix);
	beaconMessage[0] = (char) (('0' + beaconID));
	for (int i = strlen(MsgPrefix); i < RH_RF69_MAX_MESSAGE_LEN; i++) {
		beaconMessage[i] = '*';
	}
	beaconMessage[RH_RF69_MAX_MESSAGE_LEN - 1] = '\0';

	setInfoInMsg();
	Serial << "Beacon message initialized to '" << beaconMessage << "'" << ENDL;
}

bool EtheraBeacon::begin(int8_t id) {
  Serial << "Ethera Beacon #" << id << "." << ENDL;
  beaconID = id;
  seqNumber = 0;
  elapsedSinceLastMsg = BeaconPeriod[beaconID];

  initBeaconMessage();

  pinMode(RFM69_ResetPin, OUTPUT);
  digitalWrite(RFM69_ResetPin, LOW);

  // manual reset
  digitalWrite(RFM69_ResetPin, HIGH);
  delay(10);
  digitalWrite(RFM69_ResetPin, LOW);
  delay(10);

  if (!rf69.init()) {
    Serial.println("*** RFM69 radio init failed.");
    return false;
  }
  Serial.println("  RFM69 radio init OK!");
  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM (for low power module)
  // No encryption

  rf69.setTxPower(BeaconPower[beaconID], true);  // range from 14-20 for power, 2nd arg must be true for 69HCW
  Serial << "  Power set to " << BeaconPower[beaconID] << ENDL;

  if (!rf69.setFrequency(BeaconFrequency[beaconID])) {
    Serial.println("*** setFrequency failed. Aborted.");
    return false;
  }
  Serial << "  Frequency set to " << BeaconFrequency[beaconID] << " MHz" << ENDL;


  if (BeaconUseEncryption[beaconID]) {
    Serial << "  Encryption active" << ENDL;
    unsigned char key[BeaconEncryptionKeySize];
    for (int i = 0 ; i < BeaconEncryptionKeySize ; i ++) {
      key[i] = BeaconEncryptionKey[beaconID][i];
      Serial.print(BeaconEncryptionKey[beaconID][i], HEX);
      Serial << '-';
    }
    Serial << ENDL;
    rf69.setEncryptionKey(key);
  }
  else Serial << "  No encryption" << ENDL;
  Serial << "  Beacon Period=" << BeaconPeriod[beaconID] << "ms" << ENDL;
  Serial << "Beacon initialization OK" << ENDL;

  return true;
}

void EtheraBeacon::run() {
  if (elapsedSinceLastMsg > BeaconPeriod[beaconID]) {
    seqNumber++;
    setInfoInMsg();
    rf69.send((uint8_t *) beaconMessage, strlen(beaconMessage));
    elapsedSinceLastMsg = 0;
    Serial << BeaconFrequency[beaconID] << " MHz";
    if (BeaconUseEncryption[beaconID]) {
    	Serial << " (E)";
    }
    Serial << ": Sent '" << beaconMessage << ENDL;
  }
}
