/*
   EtheraCanCommander.cpp
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "CansatInterface.h"
#include "EtheraCanCommander.h"
#include "CansatXBeeClient.h"
#include "EtheraAcquisitionProcess.h"

#define DBG_READ_MSG 0
#define DBG_DIAGNOSTIC 1
#define DBG_SET_POSITION 0

EtheraCanCommander:: EtheraCanCommander(unsigned long int theTimeOut) :
  RT_CanCommander(theTimeOut) {
}

#ifdef RF_ACTIVATE_API_MODE
void  EtheraCanCommander::begin(CansatXBeeClient &xbeeClient, SdFat* theSd,
                                CansatAcquisitionProcess* theProcess) {
  RT_CanCommander::begin(xbeeClient, theSd, theProcess);
  EtheraAcquisitionProcess* etheraProcess = (EtheraAcquisitionProcess*) theProcess;
  theLinearStepper = etheraProcess->getLinearStepper();
}
#else
void EtheraCanCommander::begin(Stream& RF_Stream, SdFat* theSd, CansatAcquisitionProcess* theProcess) {
  RT_CanCommander::begin(RF_Stream, theSd, theProcess);
  EtheraAcquisitionProcess* etheraProcess = (EtheraAcquisitionProcess*) theProcess;
  theLinearStepper = etheraProcess->getLinearStepper();
}
#endif

void EtheraCanCommander::processReq_AirBrakes_Set(char* &nextCharAddress) {
  DPRINTSLN(DBG_READ_MSG, "AirBrakes_Set");
  long int position;
  if (!getMandatoryParameter(nextCharAddress, position, "Missing airbrakes position")) {
    return;
  }
  if (position < 0 || position > 100) {
    DPRINTSLN(DBG_DIAGNOSTIC, "Invalid position");
    RF_OPEN_CMD_RESPONSE(RF_Stream);
    *RF_Stream << (int) CansatCmdResponseType::AirBrakes_InvalidPosition << ','
               << position << ",AirBrakes position must be in range 0-100";
    RF_CLOSE_CMD_RESPONSE(RF_Stream);
    return;
  }
  DPRINTS(DBG_SET_POSITION,"Settings position to ");
  DPRINTLN(DBG_SET_POSITION,position);
  
  theLinearStepper->setPosition(position);
  
  RF_OPEN_CMD_RESPONSE(RF_Stream)
  *RF_Stream << (int)CansatCmdResponseType::AirBrakes_Status << "," <<  position
             << ", AirBrakes position set." ;
  RF_CLOSE_CMD_RESPONSE(RF_Stream);
}

void EtheraCanCommander::processReq_AirBrakes_Get(char* &nextCharAddress) {
  DPRINTSLN(DBG_READ_MSG, "Airbrakes_Get");
  int position;
  position = theLinearStepper->getPosition();
  RF_OPEN_CMD_RESPONSE(RF_Stream)
  *RF_Stream << (int)CansatCmdResponseType::AirBrakes_Status << "," <<  position
             << ", AirBrakes position get." ;
  RF_CLOSE_CMD_RESPONSE(RF_Stream);
}

void EtheraCanCommander::processReq_AirBrakes_SetNumSteps(char* &nextCharAddress) {
  DPRINTSLN(DBG_READ_MSG, "AirBrakes_SetNumSteps");
  long int numSteps;
  if (!getMandatoryParameter(nextCharAddress, numSteps, "Missing airbrakes steps number ")) {
    return;
  }
  theLinearStepper->setTotalNumberOfSteps(numSteps);

  RF_OPEN_CMD_RESPONSE(RF_Stream)
  *RF_Stream << (int)CansatCmdResponseType::AirBrakes_NumSteps << "," <<  numSteps
             << ", Number of steps set." ;
  RF_CLOSE_CMD_RESPONSE(RF_Stream);
}

void EtheraCanCommander::processReq_AirBrakes_GetNumSteps(char* &nextCharAddress) {
  DPRINTSLN(DBG_READ_MSG, "AirBrakes_GetNumSteps");
  int numSteps;
  numSteps = theLinearStepper->getTotalNumberOfSteps();
  RF_OPEN_CMD_RESPONSE(RF_Stream)
  *RF_Stream << (int)CansatCmdResponseType::AirBrakes_NumSteps << "," <<  numSteps
             << ", Number of steps get." ;
  RF_CLOSE_CMD_RESPONSE(RF_Stream);
}

void EtheraCanCommander::processReq_AirBrakes_ForceZero(char* &nextCharAddress) {
  DPRINTSLN(DBG_READ_MSG, "AirBrakes_ForceZero");
  long int numExtraSteps;
  if (!getMandatoryParameter(nextCharAddress, numExtraSteps, "Missing airbrakes extra steps number ")) {
    return;
  }
  theLinearStepper->forceToZero(numExtraSteps);
  RF_OPEN_CMD_RESPONSE(RF_Stream)
  *RF_Stream << (int)CansatCmdResponseType::AirBrakes_ForcedToZero << "," <<  numExtraSteps
             << ", Airbrakes forced to zero." ;
  RF_CLOSE_CMD_RESPONSE(RF_Stream);
}

bool EtheraCanCommander::processProjectCommand(CansatCmdRequestType requestType,
    char* cmd) {
  DPRINTS(DBG_READ_MSG, "process EtheraProjectCommand ");
  bool result = true;

  switch (requestType) {
    case CansatCmdRequestType::AirBrakes_Set:
      processReq_AirBrakes_Set(cmd);
      break;
    case CansatCmdRequestType::AirBrakes_Get:
      processReq_AirBrakes_Get(cmd);
      break;
    case CansatCmdRequestType::AirBrakes_GetNumSteps:
      processReq_AirBrakes_GetNumSteps(cmd);
      break;
    case CansatCmdRequestType::AirBrakes_SetNumSteps:
      processReq_AirBrakes_SetNumSteps(cmd);
      break;
    case CansatCmdRequestType::AirBrakes_ForceZero:
      processReq_AirBrakes_ForceZero(cmd);
      break;
    default:
      // Do not report error here: this is done by the superclass.
      result = false;
  } // Switch
  return result;
}
