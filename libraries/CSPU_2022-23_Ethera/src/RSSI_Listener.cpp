/*
   RSSI_Listener.cpp
*/

#include "RSSI_Listener.h"
#include "DebugCSPU.h"

#define DBG_BEGIN 1
#define DBG_GET_RSSI 0
#define DBG_CONFIG_RF 0
#define DBG_CONVERT_RSSI 0
#define DBG_SET_REF_VALUE 1


float RSSI_Listener::currentFrequency=0;

bool RSSI_Listener::begin(RH_RF69 &rf69, float frequency, bool useEncryption, const unsigned char* key,
                          uint16_t timeoutInMsec, int8_t beaconID) {
  this->beaconID = beaconID;
  this->timeout = timeoutInMsec;
  this->frequency = frequency;
  this->useEncryption = useEncryption;
  if (useEncryption) {
    DPRINTSLN(DBG_BEGIN, "  Configuring encryption");
    memcpy(this->key, key, BeaconEncryptionKeySize);
    rf69.setEncryptionKey(this->key);
  }
  this->rf69 = &rf69;
  this->status = ListenerStatus::ReadyForRSSI;
  return  true;
}

bool RSSI_Listener::measureRSSI0(float x0, uint16_t timeoutInMsec) {
  int retryNumber = 0;
  for (int i = 0; i < NumSamplesForRSSI0; i++) {
    float rssi;
    if ( getRSSI(rssi, timeoutInMsec )) {
      rssi0 += rssi;
      Serial << " rssi " << i << " is " << rssi << ENDL;
    }
    else
    {
      rssi0 = 0;
      if (retryNumber < 3) {
        i = 0;
        retryNumber++;
        Serial << "reseting" << ENDL;
      }
      else {
        return false;
      }

    } // for
  }
  rssi0 /= NumSamplesForRSSI0;
  return setReferenceValues(x0, rssi0);
}

bool RSSI_Listener::configureRF() {
	// Only reconfigure the receiver if the frequency was changed.
	if (fabs(currentFrequency - frequency) > 0.001f)
	{
		if (!rf69->setFrequency(frequency)) {
			DPRINTS(DBG_CONFIG_RF, "Cannot set frequency to ");
			DPRINTLN(DBG_CONFIG_RF, frequency);
			return false;
		} else {
			DPRINTS(DBG_CONFIG_RF, "Frequency set to ");
			DPRINTLN(DBG_CONFIG_RF, frequency);
		}

		if (useEncryption == true) {
			rf69->setEncryptionKey(key);
			DPRINTSLN(DBG_CONFIG_RF, "Encryption key set.");
		} else {
			rf69->setEncryptionKey(nullptr);
			DPRINTSLN(DBG_CONFIG_RF, "Encryption disabled");
		}
		// Do not set current frequency before configuration is complete.
		currentFrequency = frequency;
		// Make sure a previously received message would not be erroneously
		// associated with the new frequency: read possibly present msg just
		// to discard it.
		uint8_t buf[RH_RF69_MAX_MESSAGE_LEN];
		uint8_t len = sizeof(buf);
		rf69->recv(buf, &len);
	}
	return true;
}

bool RSSI_Listener::getRSSI(float &rssi, uint16_t timeoutInMsec) {
  if (!checkStatus(ListenerStatus::ReadyForRSSI)) return false; // begin was not called.
  uint16_t actualTimeout = (timeoutInMsec ? timeoutInMsec : timeout);

  if (!configureRF()) return false;

  // Now wait for a message
  uint8_t buf[RH_RF69_MAX_MESSAGE_LEN + 1];
  uint8_t len = sizeof(buf);

  DPRINTS(DBG_GET_RSSI, "Waiting for  (ms) max ");
  DPRINTLN(DBG_GET_RSSI, actualTimeout);
  if (rf69->waitAvailableTimeout(actualTimeout))  {
    if (rf69->recv(buf, &len)) {
      buf[len] = 0;
      DPRINTS(DBG_GET_RSSI, "Got a msg: ");
      DPRINTLN(DBG_GET_RSSI, (char*) buf);

      if (beaconID != -1) {
    	  // Check the message is what we expect from a beacon.
    	  // 1. Check first character is the beacon ID.
    	  if (buf[0] != '0'+beaconID) {
#ifdef DBG_DIAGNOSTIC
    		  Serial << "Beacon ID in message is " << (char) buf[0] << " but should be " << beaconID << ENDL;
    		  Serial << "Message discarded." << ENDL;
#endif
    		  return false;
    	  }
    	  DPRINTSLN(DBG_GET_RSSI, "   Beacon ID validated.");
    	  // 2. Check characters from the 3rd one are "Ethera Beacon"
    	  if (strncmp((const char*) &buf[2], "Ethera Beacon", strlen("Ethera Beacon")) != 0) {
#ifdef DBG_DIAGNOSTIC
    		 Serial << "Beacon message should include 'Ethera Beacon' from character 3" << ENDL;
    		 Serial << "Received '" << (char*) buf << "'" << ENDL;
    		 Serial << "Message discarded." << ENDL;
#endif
    		 return false;
    	  }
    	  DPRINTSLN(DBG_GET_RSSI, "   Message prefix validated. ");
      } else {
    	  // The message is from a can, it should not contain our prefix from character 3.
      	  // 2. Check characters from the 3rd one are "Ethera Beacon"
    	  if (strncmp((const char*) &buf[2], "Ethera Beacon", strlen("Ethera Beacon")) == 0) {
#ifdef DBG_DIAGNOSTIC
    		  Serial << "Can message should not include 'Ethera Beacon' from character 3" << ENDL;
    		  Serial << "Received '" << (char*) buf << "'" << ENDL;
    		  Serial << "Message discarded." << ENDL;
#endif
    		  return false;
    	  }
    	  DPRINTSLN(DBG_GET_RSSI, "   Can message validated. ");
      }

      rssi = rf69->lastRssi();
      if (rssi > MaxRSSI_Value || rssi < MinRSSI_Value) {
#ifdef DBG_DIAGNOSTIC
        Serial << " Irrelevant Values got " << rssi << " bigger than " << MaxRSSI_Value << " lower than " << MinRSSI_Value << " THERE MUST BE AN ERROR" << ENDL;
#endif
      }
      DPRINTS(DBG_GET_RSSI, "Got RSSI: ");
      DPRINTLN(DBG_GET_RSSI, rssi);
      return true;
    } else {
      return false;
    }
  } else {
	  DPRINT(DBG_GET_RSSI, frequency);
	  DPRINTSLN(DBG_GET_RSSI, " MHz: no message received");
    return false;
  }
}

bool RSSI_Listener::measureDistance(float &rssi, float &distance) {
  if (getRSSI(rssi)) {
    return convertRSSI_ToDistance(rssi, distance);
  } else return false;

}

bool RSSI_Listener::convertRSSI_ToDistance(float rssi, float &distance) {
  if (!checkStatus(ListenerStatus::ReadyForDistance)) return false; // rssi0 or x0 not available.
  float experimentalConstant = pow(10, rssi / 20.0f);
  distance = k0 / experimentalConstant;
#if DBG_CONVERT_RSSI == 1
  Serial << "Converting RSSI ";
  Serial.print( rssi, 5);
  Serial << " dBm using k0 =";
  Serial.print( k0, 10);
  Serial << ". experimental constant=";
  Serial.print(experimentalConstant, 10);
  Serial << ". Distance=" << distance << "m." << ENDL;
#endif
  return true;
}

bool RSSI_Listener::setReferenceValues(float x0, float rssi0) {
	if (!checkStatus(ListenerStatus::ReadyForRSSI)) {
		return false;
	}
	this->x0 = x0;
	this->rssi0 = rssi0;
	this->k0 = pow(10, (rssi0 + 20 * log10(x0)) / 20);
	status = ListenerStatus::ReadyForDistance;
#ifdef DBG_SET_REF_VALUE
	Serial << "  " << frequency << "MHz: set RSSI0 to " << rssi0 << ", x0 to " << x0 <<  " and k0 to ";
	Serial.println(k0, 10);
#endif
	return true;
}

bool RSSI_Listener::checkStatus(ListenerStatus minStatus) {
  if (status < minStatus) {
    DPRINTS(DBG_DIAGNOSTIC, "RSSI_Listener status KO: expected=");
    DPRINT(DBG_DIAGNOSTIC, (int) minStatus);
    DPRINTS(DBG_DIAGNOSTIC, ", actual= ");
    DPRINTLN(DBG_DIAGNOSTIC, (int) status);
    return false;
  } else return true;
}
