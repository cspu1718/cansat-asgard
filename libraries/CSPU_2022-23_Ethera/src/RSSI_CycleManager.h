/*
 * RSSI_CycleManager.h
 */

#pragma once
#include "EtheraRecord.h"
#include "RSSI_Listener.h"
#include "EtheraConfig.h"

/**
 * @ingroup EtheraCSPU
 * A class to manage the various RSSI_Listeners and cycle through them (one listener is
 * used at each call to run() (a call to run should happen at each measurement cycle).
 *
 */
class RSSI_CycleManager {
public:
	/** Constructor
	 *  @param CS_Pin The µC pin connected to the Chip-select pin of the RFM69
	 *  @param interruptPin The µC pin used for interrupts from the RFP69
	 */
	RSSI_CycleManager(uint8_t CS_Pin=RFM69_ChipSelectPin, uint8_t interruptPin=RFM69_InterruptPin);

	/** Initialize whatever must be before using the object: the RFM69 module,
	 *  one RSSI_Listener for each beacon and each can being monitored.
	 * @param numRetries The number of retries to perform using
	 *        each listener in case nothing was received. On successful measure,
	 *        the next listener will be used.
	 * @return: true if initialization is successful, false otherwise.
	 */
	bool begin(uint8_t numRetries=1);

	/** Perform one measurement. Measurements are performed using the various
	 *  RSSI_Listeners in a cycle (limited to the ones which are in status ReadyForDistance),
	 *  possibly using each of them several times, according to the parameter provided
	 *  in the call to begin().
	 * @param record The record to put results in, if any. */
	void run(EtheraRecord &record);

private:
	/** Set the currentListener index to the next value corresponding to a listener ready to
	 *  provide rssi + distance measurements. If none can be found, current listener is set to -1.
	 *  The current number of retries is also reset to 0.
	 */
	void moveToNextListener();

	uint8_t maxNumRetries; /**< The maximum number of retries to perform when a listener fails
							    to provide a measure. */
	uint8_t numRetries; /**< The current number of retries with the current listener. */
	RSSI_Listener listeners[NumBeacons+NumTrackedCans];
						  /**< The set of listeners to query */
	RH_RF69 rf69;		  /**< The interface to the radio module */
	int8_t currentListenerIdx; /**< The index of the listener (in the listeners array)
								  to use next. -1 = none. */
};
