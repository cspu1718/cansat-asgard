/*
   EtheraSecondaryMissionController.cpp
*/

// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "EtheraSecondaryMissionController.h"
#include "elapsedMillis.h"
#define DBG_VELOCITY_BUFFER 0
#define DBG_VELOCITY 0
#define DBG_EJECTION 0
#define DBG_FLIGHT_CTRL_CODE 0
#define DBG_FLIGHT_CTRL_CODE_DISCARDED 0
#define DBG_FLIGHT_PHASE_EVALUATION 0

#define DBG_AIRBRAKES_CHANGE 1

void EtheraSecondaryMissionController::printVelocityTable() {
  for (int i = 0; i < NumberOfVelocitySamples; i++)
    Serial << "  " << i << ": " << velocityTable[i] << ENDL;
}

void EtheraSecondaryMissionController::analyseDescentVelocity(const EtheraRecord& rec) {
#if (DBG_VELOCITY_BUFFER == 1)
  printVelocityTable();
#endif
#if (DBG_VELOCITY == 1)
  Serial << "BEFORE: ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ", lastTs=" << lastRecordTimestamp << ENDL;
#endif
  if (rec.descentVelocity == BMP_Client::InvalidDescentVelocity) {
#if (DBG_VELOCITY == 1)
    Serial << "INVALID VELOCITY DETECTED velocity= " <<  rec.descentVelocity << ENDL;
#endif
    clearAverageVelocity();
    lastRecordTimestamp = rec.timestamp;
    return;
  }
  if ((rec.timestamp - lastRecordTimestamp) > (3 * CansatAcquisitionPeriod)) {
    clearAverageVelocity();
#if (DBG_VELOCITY == 1)
    Serial << "INVALID REC DETECTED ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ENDL;
#endif
    lastRecordTimestamp = rec.timestamp;
    return;
  }

  if (rec.timestamp < lastRecordTimestamp) {
    clearAverageVelocity();

#if (DBG_VELOCITY == 1)
    Serial << "INVALID PAST RECORD DETECTED ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ENDL;
#endif
  }
  averageVelocity = averageVelocity + (rec.descentVelocity - velocityTable[currentVelocityIndex]) / NumberOfVelocitySamples;
  velocityTable[currentVelocityIndex] = rec.descentVelocity;
  currentVelocityIndex++;
  if (currentVelocityIndex == NumberOfVelocitySamples) {
    currentVelocityIndex = 0;
    DPRINTSLN(DBG_VELOCITY, "Index reset"); //
    velocityAverageValid = true;
  }
  lastRecordTimestamp = rec.timestamp;
#if (DBG_VELOCITY_BUFFER == 1)
  Serial << "AFTER: ts=" << rec.timestamp << ", avg velocity=" << averageVelocity << ", currentIdx=" << currentVelocityIndex << ENDL;
  printVelocityTable();
#endif

  // Update take-off status
  if ((!takeoffHappened) && velocityAverageValid && (averageVelocity  < CansatVelocityThresholdForTakeoff )) {
    DPRINTLN(DBG_EJECTION, takeoffHappened);
    DPRINTLN(DBG_EJECTION, velocityAverageValid);
    DPRINTLN(DBG_EJECTION, averageVelocity);
    DPRINTLN(DBG_EJECTION, CansatVelocityThresholdForTakeoff);
    takeoffHappened = true;
    takeoffTimestamp = rec.timestamp;
#ifdef RF_ACTIVATE_API_MODE
     CansatXBeeClient *xbee=getXbeeClient();
    if(xbee != nullptr){
      RF_OPEN_STRING(xbee);
      *xbee << millis() << ": 2-ary: Take off detected!";
      RF_CLOSE_STRING(xbee);
    }
#endif
    DPRINTS(DBG_EJECTION, "takeoff : timestamp set to ");
    DPRINT(DBG_EJECTION, takeoffTimestamp);
    DPRINTS(DBG_EJECTION, ", threshold (m/s) ");
    DPRINTLN(DBG_EJECTION, CansatVelocityThresholdForTakeoff);
  }

  // Update ejection status
  if ((!ejectionHappened) && takeoffHappened) {
	  if ((rec.timestamp - takeoffTimestamp) > CansatMaxDelayFromTakeoffToEjection) {
		ejectionHappened = true;
#ifdef RF_ACTIVATE_API_MODE
		  CansatXBeeClient *xbee=getXbeeClient();
		if(xbee != nullptr){
		  RF_OPEN_STRING(xbee);
		  *xbee << millis() << ": 2-ary: ejection assumed!";
		  RF_CLOSE_STRING(xbee);
		}
#endif
	 }
  } // update ejection status.
}

void EtheraSecondaryMissionController::updateFlightStatus(const EtheraRecord &record) {
	// Changed too recently: do not change status.
	if (record.timestamp - lastFlightStatusUpdate < CansatMinDelayBetweenFlightPhaseChange) {
		DPRINTS(DBG_FLIGHT_CTRL_CODE_DISCARDED, "Upd flight status: DISCARDED (too recent), keep status ")
		DPRINTLN(DBG_FLIGHT_CTRL_CODE_DISCARDED, (int)  currentFlightStatus);
		return;
	}

	// Actually evaluate the flight status.
	lastFlightStatusUpdate = record.timestamp;
	shouldUpdateFlightPhase = true;
	float relativeAltitude=record.altitude - record.refAltitude;

	// ground conditions
	if(relativeAltitude < CansatMinAltitudeForFlightControl) {
		currentFlightStatus = FlightControlCode::GroundConditions;
	}

	// not descending
	else if (record.descentVelocity < CansatMinVelocityDescentLimit)  {
		currentFlightStatus = FlightControlCode::NotDescending;
	}

	// if below last phase altitude, we are in ground conditions
	else if (relativeAltitude < CansatFlightPhaseAltitude[CansatNumFlightPhases-1]) {
		currentFlightStatus = FlightControlCode::GroundConditions;
	}

	// too fast
	else if (record.descentVelocity > CansatVelocityUpperSafetyLimit) {
		currentFlightStatus = FlightControlCode::HighDescentVelocityAlert;
	}

	// too slow
	else if (record.descentVelocity < CansatVelocityLowerSafetyLimit) {
		currentFlightStatus = FlightControlCode::LowDescentVelocityAlert;
	}

	// nominal descent
	else currentFlightStatus = FlightControlCode::NominalDescent;

#ifdef DBG_FLIGHT_CTRL_CODE
	static FlightControlCode previousStatus=FlightControlCode::StartingUp;
	if (currentFlightStatus != previousStatus) {
		Serial << "2-ary: flightStatus changed to " << (int)currentFlightStatus  << ENDL;
		previousStatus = currentFlightStatus;
	}
#endif
} // updateFlightStatus

void EtheraSecondaryMissionController::updateFlightPhase(const EtheraRecord& record) {
	// (No binary search implementation as there will never be more than 14 phases)

	// If flight phase shouldn't be updated, don't do anything
	if(!shouldUpdateFlightPhase) {
		return;
	}

	// If not in nominal descent, flight phase is undefined.
	if(currentFlightStatus != FlightControlCode::NominalDescent) {
		currentFlightPhase = CansatUndefinedFlightPhase;
	}
	else {
		// Evaluate phase
		float relativeAltitude = record.altitude - record.refAltitude;
		for(int i = 0; i < CansatNumFlightPhases; i++) {
			if(relativeAltitude >= CansatFlightPhaseAltitude[i]) {
				currentFlightPhase = i;
				DPRINTS(DBG_FLIGHT_PHASE_EVALUATION, "Current evaluated flight phase: ");
				DPRINTLN(DBG_FLIGHT_PHASE_EVALUATION, currentFlightPhase);
				break;
			}
		}

		// This check is included as a safety in case no phase was found previously:
		// it should never actually come here because this check is already performed in the
		// method updateFlightStatus().
		if(record.altitude - record.refAltitude < CansatFlightPhaseAltitude[CansatNumFlightPhases-1]) {
			currentFlightPhase = CansatUndefinedFlightPhase;
			DPRINTSLN(DBG_DIAGNOSTIC, "WARNING: No flight phase could be found by SecondaryMissionController::updateFlightPhase() although in nominal descent. Setting flight phase to 15 and controller info to GroundConditions.");
		}
	}

	// Reset shouldUpdateFlightPhase property
	shouldUpdateFlightPhase = false;
}

void EtheraSecondaryMissionController::closeAndForceAirbrakes(){
	static elapsedMillis elapsedSinceLastForcedZero = 0;
#ifdef DBG_AIRBRAKES_CHANGE
	if (linearStepper->getPosition() != 0) {
	  DPRINTSLN(DBG_AIRBRAKES_CHANGE, "Airbrakes position set to 0.");
	}
#endif
	linearStepper->resetPosition();

	if (elapsedSinceLastForcedZero > EtheraDelayBetweenForcedZero) {
		linearStepper->forceToZero(EtheraNumStepsToUseToForceZero);
		elapsedSinceLastForcedZero = 0;
		DPRINTSLN(DBG_AIRBRAKES_CHANGE, "Airbrakes forced to 0");
	}
}

void EtheraSecondaryMissionController::manageAirBrakes(EtheraRecord &record)
{
	// cannot do anything if no stepper object
	if (!linearStepper) {
		return;
	}

	// Remember whether the brakes where ever opened, to avoid forcing if they are
	// still in takeoff position (saves battery).
	static bool everOpenedBrakes = false;
#ifdef DBG_AIRBRAKES_CHANGE
	auto originalBrakesPosition = linearStepper->getPosition();
#endif

	// When this method is called, the flight status and flight status are updated.
	switch(currentFlightStatus) {
	case FlightControlCode::HighDescentVelocityAlert:
		linearStepper->setPosition(AirbrakesMaxOpening);
		break;
	case FlightControlCode::LowDescentVelocityAlert:
		if (everOpenedBrakes) {
			closeAndForceAirbrakes();
		} else {
			linearStepper->setPosition(0);
		}
		break;
	case FlightControlCode::GroundConditions:
	case FlightControlCode::NotDescending:
	case FlightControlCode::NoData:
	case FlightControlCode::StartingUp:
		linearStepper->resetPosition(); // This is required to close the brakes
										// when closing to the ground.
		break;
	case FlightControlCode::NominalDescent:
		if (currentFlightPhase == CansatUndefinedFlightPhase) {
			if (everOpenedBrakes) {
				closeAndForceAirbrakes();
			} else {
				linearStepper->setPosition(0);
			}
		}
		else {
			uint8_t newPosition = EtheraAirbakesPosition[currentFlightPhase];
			if (everOpenedBrakes && (newPosition == 0)) {
				closeAndForceAirbrakes();
			}
			else  {
				linearStepper->setPosition(newPosition);
				everOpenedBrakes = true;
			}
		}
		break;
	default:
		DPRINTS(DBG_DIAGNOSTIC, "Error: unexpected flight status: ");
		DPRINTLN(DBG_DIAGNOSTIC, (int) currentFlightStatus);
	}

#ifdef DBG_AIRBRAKES_CHANGE
	if (originalBrakesPosition != linearStepper->getPosition()) {
		DPRINTS(DBG_AIRBRAKES_CHANGE, "Airbrakes position set to ");
		DPRINTLN(DBG_AIRBRAKES_CHANGE, linearStepper->getPosition());
	}
#endif
}

void EtheraSecondaryMissionController::manageSecondaryMission(CansatRecord & record)
{
  // ----- Next instructions to be moved to the super class $$$$
  static uint32_t counter  = 0;
  counter++;
  EtheraRecord& myEtheraRecord = (EtheraRecord&) record;

  if (counter > StartupDelayForFlightAnalysis / CansatAcquisitionPeriod) {
  	  // Before this delay, it is assumed that the BMP measures are not stabilized.
	  analyseDescentVelocity(myEtheraRecord);
	  updateFlightStatus(myEtheraRecord);
	  updateFlightPhase(myEtheraRecord);
  }

  myEtheraRecord.flightControlCode = currentFlightStatus;
  myEtheraRecord.flightPhase = currentFlightPhase;

  if (counter >  CansatMinDelayBeforeFlightControl/ CansatAcquisitionPeriod) {

	  // ---------- this block of instructions in subclass -----

	  if (rssiCycleManagerOK) {
		  rssiCycleManager.run(myEtheraRecord);
	  }
	  if (linearStepper) {
		  manageAirBrakes(myEtheraRecord);
		  // Add brakes position in record
		  myEtheraRecord.brakePosition=linearStepper->getPosition();
	  }
  }
}

void EtheraSecondaryMissionController::clearAverageVelocity() {
  for (int i = 0; i < NumberOfVelocitySamples; i++) {
    velocityTable[i] = 0;
  }
  averageVelocity = 0;
  velocityAverageValid = false;
}

bool EtheraSecondaryMissionController::initEthera() {
  takeoffHappened = false;
  takeoffTimestamp = 0;

  rssiCycleManagerOK = rssiCycleManager.begin();
  if (!rssiCycleManagerOK) {
	  DPRINTSLN(DBG_DIAGNOSTIC, "*** ERROR initialising RSSI cycle manager. ");
  }
  // If not OK, still try to initialize the other mission.

  LinearStepper * localLinearStepper = new LinearStepper;
  if (localLinearStepper == nullptr) {
    DPRINTSLN(DBG_DIAGNOSTIC, "*** ERROR ALLOCATING LinearStepper");
  } else {
    if (!localLinearStepper->begin(	LinearStepperDirectionPin,
    								LinearStepperStepPin,
									LinearStepperSleepPin,
									LinearStepperNumSteps)) {
    	delete localLinearStepper;
    	localLinearStepper = nullptr;
    }
    else {
    	localLinearStepper->setSlowDownFactor(LinearStepperSlowDownFactor);
    	// We assume the airbrakes are closed at startup
    	localLinearStepper->setCurrentPosition(0);
    }
  }

  clearAverageVelocity();
  this->linearStepper = localLinearStepper;
  return (linearStepper || rssiCycleManagerOK);
}

#ifdef RF_ACTIVATE_API_MODE
bool EtheraSecondaryMissionController::begin(CansatXBeeClient* xbeeClient) {
  if (!SecondaryMissionController::begin(xbeeClient)) return false;
  return initEthera();
}
#else
bool EtheraSecondaryMissionController::begin(Stream* RF_Stream) {
	if (!SecondaryMissionController::begin(RF_Stream)) return false;
	return initEthera();
}
#endif


float EtheraSecondaryMissionController::getAverageVelocity() {
  if (velocityAverageValid == true) {
    return averageVelocity;
  }
  else {
    return InvalidVelocity;
  }
}

bool EtheraSecondaryMissionController::takeoffDetected() {
  return takeoffHappened;
}
