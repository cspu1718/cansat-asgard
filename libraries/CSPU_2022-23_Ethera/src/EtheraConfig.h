/*
   EtheraConfig.h

   Specific configuration for the Ethera project
*/

#pragma once
#include "CansatConfig.h"

// ***************************************
// **  Secondary Mission 1: airbrakes   **
// ***************************************

// Configuration of linear steppers
constexpr uint8_t LinearStepperDirectionPin = 12;
constexpr uint8_t LinearStepperStepPin = 6;
constexpr uint8_t LinearStepperSleepPin = 9;
constexpr uint32_t LinearStepperNumSteps = 4823;
constexpr uint8_t LinearStepperSlowDownFactor = 4;  /**< The speed of the linear stepper
													     is reduced by this factor. 1= max speed. */


// EtheraSecondaryMissionController

// -------
// From this line constants should be moved to CansatConfig.h, once the flight control
// feature is factorised in a generic class.
constexpr unsigned int CansatMaxDelayFromTakeoffToEjection = 15000;
					/**< Over this limit in msec, the can is assumed to be outside the rocket. */
constexpr uint8_t CansatVelocityAveragingPeriod = 2; /**< sec*/
constexpr float CansatVelocityThresholdForTakeoff = -2.00;
					/**< m/s positive when falling but during the takeoff
					 *   we are going up so it is negative*/


constexpr uint32_t CansatMinDelayBetweenFlightPhaseChange=500;   // msec. The minimum duration between 2 successive phase detection,
														  // to avoid that a couple of aberrant readings or high-frequency changes
														  // would cause oscillations between flight phases.
constexpr uint32_t CansatMinDelayBeforeFlightControl=10000;// msec The minimum delay between the startup of the processor
														  // and the startup of the flight control process. This delay does
														  // not apply to the calculation of the flight status and phase.
constexpr float CansatMinVelocityDescentLimit=2; 	 		  // m/s The minimum descent velocity from which the can is assumed
														  // to be descending.
constexpr float CansatVelocityLowerSafetyLimit=5; 		  // m/s The minimum descent velocity the can should always keep
											     	      // when it is descending (ie. descent velocity > EtheraVelocityDescentLimit).
constexpr float CansatVelocityUpperSafetyLimit=16; 		  // m/s The maximum descent velocity the can should always keep,
												  	      // when it is descending (ie. descent velocity > EtheraVelocityDescentLimit).

constexpr uint8_t CansatUndefinedFlightPhase=15;		  // Conventional value for the flight phase when
														  // the can is not in nominal descent mode.
constexpr uint8_t CansatNumFlightPhases=2; 					  // number of flight phases defined. Valid range: 1-14 (15=no phase).
/* Tables below define CansatNumFlightPhases altitudes.
 * Altitudes must be decreasing from index 0
 * Phase 0 is defined when altitude is above CansatFlightPhaseAltitude[0]
 * For 0 < i < CansatNumFlightPhases:
 * Phase i is defined when altitude is above CansatFlightPhaseAltitude[i]
 * and below CansatFlightPhaseAltitude[i-1].
 * Below altitude CansatFlightPhaseAltitude[CansatNumFlightPhases-1], ground condition is assumed.
 * Example:
 *   CansatFlightPhaseAltitude[CansatNumFlightPhases]= { 800, 650, 500, 350, 200};
 * Results in the phases:
 * 		0 above (or at) 800 m,
 * 		1 in range ]800;650] m
 * 		2 in range ]650;500] m
 * 		3 in range ]500;350] m
 * 		4 in range ]350;200] m
 * 		Ground conditions under 200m.
 */
constexpr uint16_t CansatFlightPhaseAltitude[CansatNumFlightPhases]= { 600, 250};
constexpr float CansatMinAltitudeForFlightControl=CansatFlightPhaseAltitude[CansatNumFlightPhases-1];
		/**< m The altitude below which "ground conditions"
			(i.e before take-off or after landing) are assumed. */

// --- END of constats to be moved to CansatConfig.h in a future version.

constexpr uint32_t EtheraDelayBetweenForcedZero = 5000;
	/**< (msec). When airbrakes must be closed,they are set to position 0 and
	 *   then regularly forced to 0 to account for the cases where the motor
	 *   did not succeed in reached the actual 0 position. */
constexpr uint16_t EtheraNumStepsToUseToForceZero = 150;
	/**< The number of steps to use whenever the airbrakes are forced beyond position
	 *   zero.  */
constexpr uint8_t AirbrakesMaxOpening = 58;
constexpr uint16_t EtheraAirbakesPosition[CansatNumFlightPhases]=
		{ 0,
		  AirbrakesMaxOpening,
		};


// ****************************************************
// **  Secondary Mission 2: distance from RF power   **
// ****************************************************
constexpr float MinRSSI_Value = -100;
constexpr float MaxRSSI_Value = 0;
constexpr uint16_t RSSI_MeasurementTimeout = CansatAcquisitionPeriod/2; // msec
struct ReferenceValue_t {
	float x0;    // The reference distance (m)
	float rssi0; // The corresponding rssi (dBm)
};

// Configuration of tracked beacons
constexpr uint8_t NumBeacons = 2;
constexpr uint8_t MaxNumBeacons = 2;
constexpr uint8_t NumSamplesForRSSI0 = 5;
constexpr uint8_t BeaconEncryptionKeySize = 16;
constexpr unsigned int BeaconPeriod[MaxNumBeacons] = {200, 200}; /*msec*/
constexpr float BeaconFrequency[MaxNumBeacons] = {433.3, 434.5};
constexpr bool BeaconUseEncryption[MaxNumBeacons] = { true, true };
constexpr int8_t BeaconPower[MaxNumBeacons] = {20, 20};  // in dBm. Range from 14-20 for RF69M
constexpr unsigned char BeaconEncryptionKey[MaxNumBeacons][BeaconEncryptionKeySize] ={
                                                 { 0x02, 0x04, 0x06, 0x08, 0x10, 0x12, 0x14, 0x16, 
                                                   0x15, 0x13, 0x11, 0x09, 0x07, 0x05, 0x03, 0x01 } ,
                                                 { 0x4, 0x06, 0x08, 0x10, 0x12, 0x14, 0x16, 0x18,
                                                   0x01, 0x03, 0x05, 0x07, 0x09, 0x11, 0x13, 0x15 }
                                             };
constexpr ReferenceValue_t BeaconReferences[MaxNumBeacons] = {
								{100.0f,  -73.0f },
								{100.0f, -77.0f }  // If x0 = 0, the entry is ignored.
							};

// Configuration of tracked CanSats
constexpr uint8_t NumTrackedCans=2;
constexpr uint8_t NumConfiguredCans=2;
constexpr uint8_t CanEncryptionKeySize = 16;
constexpr float CanFrequency[NumConfiguredCans] = {433.1, 433.7};
constexpr bool CanUseEncryption[NumConfiguredCans] = { true,true };
constexpr const char * CanName[NumConfiguredCans] = {	"Can with ID 0",
														"Can with ID 1"};
constexpr unsigned char CanEncryptionKey[NumConfiguredCans][CanEncryptionKeySize] ={
                                                 { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                                                   0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08 } ,
                                                 { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                                                   0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08 }
                                             };
constexpr ReferenceValue_t CanReferences[NumConfiguredCans] = {
								{100.0f,  -80.0f },
                {100.0f, -82.0f }  // If x0 = 0, the entry is ignored.
							};
// Additional information for use by our simulators
constexpr unsigned int TrackedCanPeriod[NumConfiguredCans] = {200, 200}; /*msec*/
constexpr const char * TrackedCanMessage[NumConfiguredCans] = {	"Message from tracked can 0",
														"Message from tracked can 1"};
constexpr int8_t TrackedCanPower[NumConfiguredCans] = {20, 20};  // in dBm. Range from 14-20 for RF69M



// Hardware configuration of RFM69
constexpr uint8_t RFM69_InterruptPin = 5;
constexpr uint8_t RFM69_ChipSelectPin = A3;
constexpr uint8_t RFM69_ResetPin = A4;
