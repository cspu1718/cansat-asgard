/**
 *  Test program for RSSI_Listener class.
 *
 */
 
 constexpr uint8_t BeaconID=0;  // The ID of the beacon to use for the tests (from EtheraConfig.h)

#include <RH_RF69.h>
#include "EtheraConfig.h"
#include "CSPU_Debug.h"
#include <RHReliableDatagram.h>
#include <math.h>
#include "RSSI_Listener.h"

RSSI_Listener listener;

// 2 constant needed
// set the beacon to 3 meter
// need to tell to enter manually data in
#if defined(ADAFRUIT_FEATHER_M0_EXPRESS) // FEATHER_M0_EXPRESS w/ radio
// Feather M0 w/Radio
#define RFM69_CS      A3
#define RFM69_INT     5
#define RFM69_RST     A4
#define LED           13
#else
#error "This program only works for FeatherM0 Express and Arduino Uno"
#endif

RH_RF69 rf69(RFM69_CS, RFM69_INT);



constexpr float RF69_Frequency = BeaconFrequency[0]; // Mhz

const int maxIrrelevantValue = 0;
const int minIrrelevantValue = -100;
float zeroConstants;
float rssiZero = 0;
int distanceZero;// meter
const int NumSamples = 5;


void zeroConstanstsConverter() {

  Serial << " Enter the current distance between the receiver and the beacon (an integer in meter)." << ENDL;
  Serial << " The program will then measure the corresponding RSSI (RSSI0)" << ENDL;
  while (distanceZero <= 0) {
    if (Serial.available()) {
      distanceZero = Serial.parseInt();
    }
  }
  Serial << " x0 = " << distanceZero << "m" << ENDL;
  if (!listener.measureRSSI0(distanceZero, 1000)) {
    Serial << "ERROR MEASURING RSSI0. Is the beacon switched on ? (Aborted)." << ENDL;
    exit(-1);
  }
  Serial << "  RSSI0/x0 configured. Now measuring distances..." << ENDL; 
}

void setup() {
  DINIT(115200);
  pinMode(LED, OUTPUT);
  pinMode(RFM69_RST, OUTPUT);
  digitalWrite(RFM69_RST, LOW);

  Serial << "RSSI Listener test program." << ENDL;
  Serial << "This program will measure RSSI from beacon " << BeaconID << " and convert it to distance";
  Serial.println();

  // manual reset
  digitalWrite(RFM69_RST, HIGH);
  delay(10);
  digitalWrite(RFM69_RST, LOW);
  delay(10);
  
  if (!rf69.init()) {
    Serial << "RFM69 radio init failed. Aborted." << ENDL;
    while (1);
  }
  Serial.println("RFM69 radio init OK!");
  Serial << "Current settings for beacon #" << BeaconID << ":" << ENDL;
  Serial << "   Freq = " << BeaconFrequency[BeaconID] << " MHz" << ENDL;
  if (BeaconUseEncryption[BeaconID]) {
    Serial << "   Encryption used" << ENDL;
  }
  else {
    Serial << "  NO encryption" << ENDL;
  }
  listener.begin(rf69, BeaconFrequency[BeaconID], BeaconUseEncryption[BeaconID], BeaconEncryptionKey[BeaconID], RSSI_MeasurementTimeout, 0) ;


  zeroConstanstsConverter();
  float testDistance;
  listener.convertRSSI_ToDistance(listener.getRSSI0(), testDistance) ;
  Serial << "Recomputing x0: " << testDistance << ENDL;
}



void loop() {
  float rssi;
  float distance;

  if (listener.measureDistance(rssi, distance)) {
    Serial << "you are at " << distance << "m of your beacon" << ENDL;
    Serial << "the rssi is " << rssi << ENDL;
    delay(2000);
  }

}
