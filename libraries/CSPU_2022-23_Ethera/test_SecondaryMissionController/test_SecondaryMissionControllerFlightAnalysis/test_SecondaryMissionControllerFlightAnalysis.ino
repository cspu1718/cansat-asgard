/*
    Test program for the flight analysis features of the SecondaryMissionController
    class (determination of the flight status and flight phase).

    To be adapted where marked $$$$ when moving the features to a generic class.

    Resulting records are stored on SD card.
    Define DBG_

    Requirements:
   	- SD Card reader (defaults to the on-board reader of Feather M0 Adalogger
   	- data file I2_10mDV.csv, located in folder
   	  testData must be in the SD-Card root directory.
*/

#define DEBUG_CSPU		  // Define before any include.
#include "CansatConfig.h"
#ifndef INCLUDE_REFERENCE_ALTITUDE
    #error "This test (and the tested features) requires INCLUDE_REFERENCE_ALTITUDE to be defined."
#endif
#include "CansatRecordTestFlow.h"
#include "EtheraSecondaryMissionController.h" // $$$$
#include "EtheraRecord.h"  // $$$$
#include "SD_Logger.h"
#include "DebugCSPU.h" // Include this as last to avoid undefined symbol issue
// on faiCSPU() during link.
#include "./TestUtilityFunctions.h"

bool stopAfterFailedTest = false;
bool stopAfterFirstFailedCheck = false;
constexpr bool UseCompleteDataFile = false; // If false a data file containing only the flight data will be used. 
constexpr bool ShowSlowCycles = false; // If true, a warning is issued everytime the processing of the record is slower than de delay between 2 records.  


byte SD_Pin = 4;

EtheraSecondaryMissionController controller; // $$$
CansatRecordTestFlow recordFlow;
EtheraRecord record;  // $$$
SD_Logger logger(SD_Pin);
uint32_t numErrors = 0;
uint32_t recordCounter;

void ProcessFile(const char* fileName, String& resultFile) {
  recordCounter = 0;
  logger.init("tst_");
  recordFlow.openInputFile(fileName, SD_Pin);
  Serial << ENDL << "Testing with file '" << fileName << "'" << ENDL;
  Serial << "Processing at the actual speed, according to record timestamps (be patient...)" << ENDL;
  resultFile = logger.fileName();
  Serial << "Saving records to '" << resultFile << "'" << ENDL;

  if (!recordFlow.openInputFile(fileName, SD_Pin)) {
    Serial << "Cannot read from file '" << fileName << "'. Test aborted" << ENDL;
    numErrors++;
    return;
  }
  waitEndOfStartup(controller, record);
  float lastAltitude = -10000;
  float lastDescentVelocity = -10000;
  FlightControlCode lastCtrlInfo = FlightControlCode::NoData;
  uint8_t lastPhase = 255;
  uint32_t localCounter = 0;
  elapsedMillis cycleDuration = 0;
  elapsedMillis processingDuration = 0;
  uint32_t previousRecTS = 0;
  uint32_t savedProcessingDuration;

  // record.printCSV(Serial, CansatRecord::DataSelector::All, CansatRecord::HeaderOrContent::Header);
  // Serial.println();
  while (recordFlow.getRecord(record)) {
    // record.printCSV(Serial);
    // Serial.println();
    cycleDuration=0;
    localCounter++;
    recordCounter++;
    processingDuration = 0;
    controller.run(record);
    savedProcessingDuration = processingDuration;
    if (ShowSlowCycles && savedProcessingDuration > 1) {
      Serial << "*** Processing duration suspiciously long:" << savedProcessingDuration << " msec)" << ENDL;
      Serial << "    recordCounter=" << recordCounter  << ENDL;
    }
    if ( (fabs(lastAltitude - record.altitude) > 10) ||
         (fabs(lastDescentVelocity - record.descentVelocity) > 1.0) ||
         (lastCtrlInfo != record.flightControlCode) ||
         (lastPhase != record.flightPhase) ||
         localCounter >= 20) {
      Serial << recordCounter << " (local=" << localCounter << "): ";
      printFlightInfo(record);
      lastAltitude = record.altitude;
      lastDescentVelocity = record.descentVelocity;
      lastCtrlInfo = record.flightControlCode;
      lastPhase = record.flightPhase;
      localCounter = 0;
    }
    logger.log(record);

    // We need the loop to actually run with the period found in the records, in order for the
    // controller to operate correctly.
    if (previousRecTS > 0) {
      uint32_t expectedDuration = record.timestamp - previousRecTS;
      if (expectedDuration > cycleDuration) {
        // Do not feed the records faster than the real-time: it would result in some of them being skipped. 
        delay(expectedDuration - cycleDuration + 1);
      }
      else if (ShowSlowCycles) {
        Serial << "Info: cycle too slow (due to SD-Card operations) !! " << ENDL;
        Serial << "recordCounter=" << recordCounter << " expectedDuration=" << expectedDuration
               << " cycleDur=" << cycleDuration
               << ", actualProcessing (display and SD-card operation excluded)=" << savedProcessingDuration << " msec" << ENDL;
      }
    }
    previousRecTS = record.timestamp;
  }
  Serial << "Test over. Check output file '" << resultFile << "' on SD-Card" << ENDL;
}

void TestStartupConditions() {
  bool tested;
  record.clear();
  Serial << ENDL << ENDL << "==== Testing detection of startup condition.. " << ENDL;
  do {
    record.timestamp = millis();
    controller.run(record);
    recordCounter++;
    if (record.timestamp < CansatMinDelayBeforeFlightControl )  {
      if (record.flightControlCode != FlightControlCode::StartingUp) {
        Serial << "*** Error: record with TS=" << record.timestamp << ", flightControlCode is NOT StartingUp: got " << (int) record.flightControlCode << ENDL;
        numErrors++;
      }
      if (record.flightPhase != CansatUndefinedFlightPhase) {
        Serial << "*** Error: record with TS=" << record.timestamp << ", flightPhase is NOT " << CansatUndefinedFlightPhase << " (CansatUndefinedFlightPhase): got " << record.flightPhase << ENDL;
        numErrors++;
      }
      tested = true;
    }
  } while ((millis() < CansatMinDelayBeforeFlightControl) && (!tested));
  if (!tested) {
    Serial << "*** Could not test detection of start-up condition" << ENDL;
    numErrors++;
  }
  printTestSummary();
}

void TestSafetyChecks() {
  /* This test is tricky: we want to test with many records, but records are
     not actually processed unless their timestamps are CansatMinDelayBetweenFlightControlUpdate
     apart. On the other end, keeping the altitude stable for that long
     cause the reference altitude to be updated, which is not the point.
     THIS IS NOT TRUE ANYMORE SINCE RefAltitude is computed by BMP.
     That's why we keep wobbling the altitude around.
  */

  Serial << ENDL << ENDL << "==== Testing safety checks ==== " << ENDL;
  record.refAltitude = 450;
  waitEndOfStartup(controller, record);

  // Ground condition detection:
  Serial << "1. Ground condition detection..." << ENDL;
  record.altitude = record.refAltitude - 10; // start with negative relative altitude.
  record.timestamp += CansatAcquisitionPeriod;
  delay(CansatAcquisitionPeriod);
  printFlightInfo(record);
  controller.run(record);
  recordCounter++;
  checkCtrlInfo(record, FlightControlCode::GroundConditions, "GroundConditions", 15);
  record.timestamp += CansatAcquisitionPeriod;
  delay(CansatAcquisitionPeriod);

  // Not descending
  // a) Ascent
  Serial << "2a. Ascending..." << ENDL;
  record.altitude = record.refAltitude + CansatMinAltitudeForFlightControl + 10;
  record.descentVelocity = -15;
  record.timestamp += CansatAcquisitionPeriod;
  delay(CansatAcquisitionPeriod);
  uint32_t startTS = record.timestamp;

  printFlightInfo(record);
  do {
    controller.run(record);
    recordCounter++;
    checkCtrlInfo(record, FlightControlCode::NotDescending, "NotDescending", 15);
    record.timestamp += CansatAcquisitionPeriod;
    if ((record.timestamp - startTS) > CansatMinDelayBetweenFlightPhaseChange) {
      record.descentVelocity += 0.5;
      record.altitude = record.altitude - record.descentVelocity * CansatMinDelayBetweenFlightPhaseChange / 1000.0;
      startTS = record.timestamp;
    }
  } while (record.descentVelocity < 0.0f);
  // b) ejection
  Serial << "2b Recently ejected..." << ENDL;
  uint32_t numRecordsForStatusChange = CansatMinDelayBetweenFlightPhaseChange / CansatAcquisitionPeriod + 1;
  if (numRecordsForStatusChange < controller.NumberOfVelocitySamples) {
    numRecordsForStatusChange = controller.NumberOfVelocitySamples;
  }

  Serial << "3. Velocity to low alert..." << ENDL;
  Serial << "*** TMP: ADD CHECK ON ALERT REACTIONS WHEN DEFINED" << ENDL;
  startTS = record.timestamp;
  record.descentVelocity = CansatVelocityLowerSafetyLimit - 0.1;
  for (int i = 0; i < numRecordsForStatusChange ; i++)  {
    record.timestamp += CansatAcquisitionPeriod;
    delay(CansatAcquisitionPeriod);
    controller.run(record);
    recordCounter++;
  }
  checkCtrlInfo(record, FlightControlCode::LowDescentVelocityAlert, "LowVelocityAlert", 15);

  // Descending in nominal speed range will be tested when testing flight phases

  // Velocity too high alert
  Serial << "4. Velocity to high alert..." << ENDL;
  record.descentVelocity = CansatVelocityUpperSafetyLimit + 0.1;
  for (int i = 0; i < numRecordsForStatusChange ; i++)  {
    record.timestamp += CansatAcquisitionPeriod;
    delay(CansatAcquisitionPeriod);
    controller.run(record);
    recordCounter++;
  }
  checkCtrlInfo(record, FlightControlCode::HighDescentVelocityAlert, "HighVelocityAlert", 15);
  printTestSummary();
}

void TestFlightPhaseDetection() {
  Serial << ENDL << ENDL << "==== Testing flight phase detection ==== " << ENDL;
  uint32_t numRecordsForStatusChange = CansatMinDelayBetweenFlightPhaseChange / CansatAcquisitionPeriod + 1;
  if (numRecordsForStatusChange < controller.NumberOfVelocitySamples) {
    numRecordsForStatusChange = controller.NumberOfVelocitySamples;
  }
  record.refAltitude = 300.0;
  record.descentVelocity = 10.0;
  record.altitude = record.refAltitude + 1000;
  for (unsigned int i = 0; i < numRecordsForStatusChange; i++) {
    record.timestamp += CansatAcquisitionPeriod;
    // do not change altitude
    delay(CansatAcquisitionPeriod);
    controller.run(record);
    recordCounter++;
  } ;
  checkCtrlInfo(record, FlightControlCode::NominalDescent, "NominalDescent", 0);
  Serial << "Starting descent from 1000m... " << ENDL;
  // flight from 1000 to 0 m., safe velocity.
  // phase detection can be delayed by at most CansatMinDelayBetweenFlightPhaseChange msec.
  record.altitude = record.refAltitude + 1000.0;
  record.descentVelocity = (CansatVelocityLowerSafetyLimit + CansatVelocityUpperSafetyLimit) / 2;
#ifdef REMOVED
  do  {
    checkCtrlInfo(record, FlightControlCode::NominalDescent, "NominalDescent", 15);
    record.timestamp += CansatAcquisitionPeriod;
    record.altitude -= record.descentVelocity * (CansatAcquisitionPeriod / 1000.0);
    delay(CansatAcquisitionPeriod);
    controller.run(record);
    recordCounter++;
    // Print at every phase change.
    if (record.flightPhase != currentPhase) {
      Serial << "   Flight phase=" << record.flightPhase
             << " (relative altitude=" << record.altitude - record.refAltitude << "m)" << ENDL;
      currentPhase = record.flightPhase;
    }

  } while (record.altitude > record.refAltitude);

  checkCtrlInfo(record, FlightControlCode::GroundConditions, "GroundCondition", 15);
#else
  uint8_t currentPhase = 255;
  uint8_t previousExpectedPhase = 255;
  uint8_t expectedPhase = 255;
  uint8_t tmp;
  int i;
  do {
    // printFlightInfo(record);
    controller.run(record);
    recordCounter++;
    // Print at every phase change.
    if (record.flightPhase != currentPhase) {
      Serial << "   --> Flight phase=" << record.flightPhase
             << " (relative altitude=" << record.altitude - record.refAltitude << "m)" << ENDL;
      currentPhase = record.flightPhase;
    }

    // Define expected phase and keep previouslyExpected phase to deal with detection delay.
    bool found = false;
    for (i = 0; i < CansatNumFlightPhases; i++) {
      if ((record.altitude - record.refAltitude) > CansatFlightPhaseAltitude[i]) {
        found = true;
        break;
      }
    }
    if (found) {
      tmp = (uint8_t) i;
    }
    else {
      tmp = 15;
    }
    if (tmp != expectedPhase) {
      previousExpectedPhase = expectedPhase;
      expectedPhase = tmp;
    }
    checkCtrlInfo(record, FlightControlCode::NominalDescent, "NominalDescent");
    checkPhase(record, expectedPhase, previousExpectedPhase);
    record.timestamp += CansatAcquisitionPeriod;
    delay(CansatAcquisitionPeriod);
    record.altitude -= record.descentVelocity * CansatAcquisitionPeriod / 1000.0f;
  } while (record.altitude > (record.refAltitude + CansatFlightPhaseAltitude[CansatNumFlightPhases - 1]));
  record.timestamp += (CansatMinDelayBetweenFlightPhaseChange + 1);
  printFlightInfo(record);
  controller.run(record);
  checkCtrlInfo(record, FlightControlCode::GroundConditions, "GroundConditions", 15);
#endif
  printTestSummary();
}

void setup() {
  DINIT(115200);
  Serial << "Init OK" << ENDL;
  Serial << "  Acquisition period (from CansatConfig.h): " << CansatAcquisitionPeriod << " msec" << ENDL;
  Serial << "This test assumes the following settings:" << ENDL;
  Serial << "  NoAltitudeChangeDurationForReset=1 min";
  if (NoAltitudeChangeDurationForReset == 60000) {
    Serial << " OK" << ENDL;
  } else {
    Serial << ENDL << " *** Current value=" << NoAltitudeChangeDurationForReset << " msec";
    Serial << ": test data must be adapted!" << ENDL;
  }
  Serial << "  NoAltitudeChangeTolerance=2m";
  if ((NoAltitudeChangeTolerance - 2.0) < 0.001) {
    Serial << " OK" << ENDL;
  } else {
    Serial << ENDL << " *** Current value=" << NoAltitudeChangeTolerance ;
    Serial << ": test data must be adapted!" << ENDL;
  }
  Serial << "REMOVE THIS ? This is a BMP issue now..." << ENDL;

  controller.begin();

  TestStartupConditions(); // This test must be the first one and happen as soon as possible after startup and init.

  // Testing safety checks
// RESTORE  TestSafetyChecks();

  // Testing flight phase detection
// RESTORE  TestFlightPhaseDetection();

  // Testing with actual flight data
  String resultFile;
  Serial << ENDL << "Testing with IsaTwo actual flight data (16850 lines)..." << ENDL;
  if (CansatSecondaryMissionPeriod > 70) {
    Serial << "WARNING: This test requires all records to be processed by the SecondaryMissionController. Therefore, the CansatSecondaryMissionPeriod " << ENDL;
    Serial << "         must be set to less than the actual duraction of the processing of a record by the test program. Since the acquisition period is 70 msec," << ENDL;
    Serial << "         less than 70 msec is required. The safest is to set CansatSecondaryMissionPeriod to 0 to make sure the secondary manager is used at each cycle.  " << ENDL;
    Serial << "         Current value: " << CansatSecondaryMissionPeriod << " msec (0 = always)" << ENDL;
  } else {
    if (UseCompleteDataFile) {
      Serial << ENDL << "Using complete file (16850 lines)... Test will take about 22 minutes." << ENDL;
      ProcessFile("I2_10mDV.csv", resultFile);
    } else {  
      Serial << ENDL << "Using file limited to flight)... Test will take about 2 minutes." << ENDL;
      ProcessFile("I2_fly_.csv", resultFile); 
    } 
    Serial << "Validated test results are in folder 'TestResults' and can be used to check the output of this test." << ENDL;
  }
  Serial << ENDL << "End of job. Number of errors: " << numErrors << ENDL;
  exit(0);
}

void loop() {
  // put your main code here, to run repeatedly
}
