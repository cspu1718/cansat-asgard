/**
 *  Test program for the generic flight analysis features of the secondary mission controller
 *  which are expected to be moved to a generic class in the future.
 *
 *  Lines tagged // GGG  should then be modified.
 */

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "EtheraSecondaryMissionController.h"  // GGG
EtheraRecord record;  // GGG
EtheraSecondaryMissionController ctrl; // GGG
constexpr bool detailDBG = false;
bool takeoffHappened = ctrl.takeoffDetected();
int numErrors = 0;

void printAvgVelocity() 
{
  if (fabs(ctrl.getAverageVelocity() - ctrl.InvalidVelocity) > 0.001) Serial <<  ctrl.getAverageVelocity() ;
      else Serial << "Invalid";
      Serial << ENDL;
}

void testConstantValue() {
  record.descentVelocity = (CansatVelocityThresholdForTakeoff / 2.0);

  Serial << "Testing constant velocity = " << record.descentVelocity << " , num samples = " << ctrl.NumberOfVelocitySamples  << ENDL;
  Serial << " sending " << ctrl.NumberOfVelocitySamples << " records" << ENDL;
  for (int i = 0  ; i < ctrl.NumberOfVelocitySamples ; i++) {
    if (detailDBG) {
      Serial << "Average Velocity before record #" << i << ":" ;
      printAvgVelocity();
    }
    ctrl.manageSecondaryMission(record);
    record.timestamp+=CansatAcquisitionPeriod;
  }
  float avg = ctrl.getAverageVelocity();
  if (fabs( ctrl.getAverageVelocity() - record.descentVelocity) > 0.001)   {
      Serial << "*** ERROR: Unexpected Value: avg velocity =" << ctrl.getAverageVelocity() << ENDL;
      numErrors++;
  }

  if ( fabs( ctrl.getAverageVelocity() - record.descentVelocity) > 0.001) {
    Serial << ctrl.getAverageVelocity() << ENDL;
    Serial << "Unexpected Value =" << record.descentVelocity << ENDL;
    numErrors++;
  }
  else {
    Serial << "Averaging succeeded --> Current Value =" << record.descentVelocity << ENDL;
    if (takeoffHappened == true) {
      Serial << "takeoff has been detected" << ENDL;
    }
  }
  Serial << "---------------" << ENDL;
}
//

void testAvg(float firstVelocityValue, float increment) {
  Serial << ENDL << "Test average with initial value " << firstVelocityValue << ", increment = " << increment << ", num. samples = " << ctrl.NumberOfVelocitySamples << ENDL;
  float avg = (2.0f * firstVelocityValue + (ctrl.NumberOfVelocitySamples-1) * increment) / 2.0f;
  record.descentVelocity = firstVelocityValue;
  for (int i = 0; i < ctrl.NumberOfVelocitySamples; i++) {
    ctrl.manageSecondaryMission(record);
    record.descentVelocity += increment;
    record.timestamp += CansatAcquisitionPeriod;
  }
  if ((fabs( ctrl.getAverageVelocity() - avg) > 0.001)) {
    Serial << "UnExpected Value =" << ctrl.getAverageVelocity() << ENDL;
    Serial << "Expected Value =" << avg << ENDL;
    numErrors++;
  }
  else {
    Serial << "Test Average is okay" << ENDL;
    if (takeoffHappened == true) {
      Serial << "takeoff has been detected" << ENDL;
    }
  }
  Serial << "------------------" << ENDL << ENDL;
}


void testavgAlternatingValues(float value1, float value2) {
  Serial << ENDL << "Test Alternating values with as first value: " << value1 << " and as second value: " << value2 << ", num. samples = " << ctrl.NumberOfVelocitySamples << ENDL;
  float avg = (value1*(ctrl.NumberOfVelocitySamples-(ctrl.NumberOfVelocitySamples/2)) + value2*(ctrl.NumberOfVelocitySamples/2)) / ctrl.NumberOfVelocitySamples;
  for (int i = 0; i < ctrl.NumberOfVelocitySamples; i++) {
    if ((i % 2) == 0) {
      record.descentVelocity = value1;
    }
    else {
      record.descentVelocity = value2;
    }
    ctrl.manageSecondaryMission(record);
  }
  
  if ((fabs( ctrl.getAverageVelocity() - avg) > 0.001)) {
    Serial << "UnExpected Value =" << ctrl.getAverageVelocity() << ENDL;
    Serial << "Expected Value =" << avg << ENDL;
    numErrors++;
  }
  else {
    Serial << "Test Alternating Value is okay" << ENDL;
    if (takeoffHappened == true) {
      Serial << "takeoff has been detected" << ENDL;
    }
  }
  Serial << "-------------------------------------------" << ENDL;
}

void testavgDelayedTimestamp() {
  record.timestamp = 11;
  record.descentVelocity = CansatVelocityThresholdForTakeoff / 2.0;
  for (int i = 1  ; i <= 2 * ctrl.NumberOfVelocitySamples ; i++) {
    if (detailDBG) {
      Serial << "Average Velocity before record #" << i << ":" ;
      printAvgVelocity();
    }
    ctrl.manageSecondaryMission(record);
    record.timestamp += CansatAcquisitionPeriod;
  }

  if (ctrl.getAverageVelocity() == ctrl.InvalidVelocity) {
    Serial << "*** Part 1: ERROR Velocity should not be invalid" << ENDL;
    numErrors++;
  }
  else {
    Serial << "Part 1 of Average test with a delayed timestamp is OK : INVALID AVERAGE VELOCITY RECEIVED" << ENDL;
    if (takeoffHappened == true) {
      Serial << "takeoff has been detected" << ENDL;
    }
  }
  record.timestamp += 4 * CansatAcquisitionPeriod;
  ctrl.manageSecondaryMission(record);
  if (ctrl.getAverageVelocity() != ctrl.InvalidVelocity) {
    Serial << "*** Part 2: ERROR Velocity should be invalid after too big gap between 2 timestamps " << ENDL;
    numErrors++;
  }
  else {
    Serial << "Part 2 of Average test with a delayed timestamp is OK : INVALID VELOCITY AVERAGE RECEIVED" << ENDL;

    if (takeoffHappened == true) {
      Serial << "takeoff has been detected" << ENDL;
    }
  }
  for (int i = 1  ; i <= 2 * ctrl.NumberOfVelocitySamples ; i++) {
    if (detailDBG) { 
      Serial << "Average Velocity before record #" << i << ":" ;
      printAvgVelocity();
    }
    ctrl.manageSecondaryMission(record);
    record.timestamp += CansatAcquisitionPeriod;
  }

  if (ctrl.getAverageVelocity() == ctrl.InvalidVelocity) {
    Serial << "*** Part 3: ERROR Velocity should not be invalid" << ENDL;
    numErrors++;
  }
  else {
    Serial << "Part 3 of Average test with a delayed timestamp is OK : VALID AVERAGE VELOCITY RECEIVED " << ":" << ctrl.getAverageVelocity() << ENDL;

    if (takeoffHappened == true) {
      Serial << "takeoff has been detected" << ENDL;
    }
  }
  ctrl.manageSecondaryMission(record);
  record.timestamp -= 71;
  ctrl.manageSecondaryMission(record);
  if (ctrl.getAverageVelocity() != ctrl.InvalidVelocity) {
    Serial << "*** Part 4: ERROR Velocity should be invalid after a too old timestamp" << ENDL;
    numErrors++;
  }
  else {
    Serial << "Part 4 of Average test with a delayed timestamp is OK : INVALID AVERAGE VELOCITY RECEIVED" << ENDL;
    if (takeoffHappened == true) {
      Serial << "takeoff has been detected" << ENDL;
    }
  }
}

void testEjection(EtheraRecord& record) {
  Serial << ENDL << "Ejection Test started" << ENDL;
  record.descentVelocity = 1;
  record.timestamp = 1000;
  uint32_t takeOffTS=0;
  bool takeoffAcknowledged = false;
  while (!ctrl.ejectionDetected()) {
    ctrl.manageSecondaryMission(record);
    record.descentVelocity -= 0.1;
    // Serial << "current Velocity: " << record.descentVelocity << ENDL;
    record.timestamp += 1;
    // Serial << "current timestamp: " << record.timestamp << ENDL;
    if (ctrl.takeoffDetected() && (!takeoffAcknowledged)) {
      takeOffTS = record.timestamp;
      Serial << "Takeoff at " << takeOffTS << ", avg: " << ctrl.getAverageVelocity() << ", Threshold: " << CansatVelocityThresholdForTakeoff << ENDL;
      takeoffAcknowledged = true;
    }
  }
  Serial << "Ejection detected at "  << record.timestamp << " msec" << ENDL;
  Serial << "Ejection delay = " << CansatMaxDelayFromTakeoffToEjection/1000.0 << " sec. Detected after " << (record.timestamp - takeOffTS)/1000.0 << " sec." << ENDL;
  if (fabs((record.timestamp-takeOffTS)-CansatMaxDelayFromTakeoffToEjection)  > 1000) {
    Serial << "*** ERROR: more than 1 sec inacurracy in ejection detection" << ENDL;
    numErrors++;
  }
  Serial << "-----------------" << ENDL;
}

void testInvalidVelocity(EtheraRecord& record) {
  Serial << ENDL << "Invalid Velocity Test started" << ENDL;
  record.descentVelocity = BMP_Client::InvalidDescentVelocity;
  ctrl.manageSecondaryMission(record);
  if (ctrl.getAverageVelocity() == ctrl.InvalidVelocity) {
    Serial << " TEST OK :Velocity got succesfully cleared." << ENDL;
  }
  else {
    Serial << " *** ERROR :Velocity isn't cleared current value= " << ctrl.getAverageVelocity() << ENDL;
    numErrors++;
  }
}

void testInitialConditions() {
  Serial << "Checking initial conditions..." << ENDL;
  record.descentVelocity = 0.01;
  ctrl.manageSecondaryMission(record);
  record.timestamp+=CansatAcquisitionPeriod;
  if (ctrl.getAverageVelocity() != EtheraSecondaryMissionController::InvalidVelocity) {
    Serial << "*** ERROR: descent velocity should be 'Invalid', got " << record.descentVelocity << ENDL;
    numErrors++;
  }
  if (record.flightControlCode != FlightControlCode::StartingUp) {
    Serial << "*** ERROR: flightControlCode should be 'StartingUp', got " << (int) record.flightControlCode << ENDL;
    numErrors++;
  }
  if (record.flightPhase != CansatUndefinedFlightPhase) {
    Serial << "*** ERROR: flight phase should be 'Undefined', got " << (int) record.flightPhase << ENDL;
    numErrors++;
  }
  // provide records which will be ignored anyway
  Serial << "Ignoring the first " << CansatMinDelayBeforeFlightControl/1000.0 << " seconds of records (i.e " << CansatMinDelayBeforeFlightControl/CansatAcquisitionPeriod << " records)" << ENDL;
  Serial << " + " << EtheraSecondaryMissionController::NumberOfVelocitySamples << " samples to get a valid average velocity." << ENDL;
  for (uint32_t i=0 ; i < CansatMinDelayBeforeFlightControl/CansatAcquisitionPeriod + EtheraSecondaryMissionController::NumberOfVelocitySamples + 1; i++)
  {
    ctrl.manageSecondaryMission(record);
    record.timestamp+=CansatAcquisitionPeriod;
  }
  if (ctrl.getAverageVelocity() == EtheraSecondaryMissionController::InvalidVelocity) {
    Serial << "*** ERROR: descent velocity should not be invalid!" << ENDL;
    numErrors++;
  }
  if (record.flightControlCode != FlightControlCode::GroundConditions) {
    Serial << "*** ERROR: flightControlCode should be 'GroundConditions', got " << (int) record.flightControlCode << ENDL;
    numErrors++;
  }
  if (record.flightPhase != CansatUndefinedFlightPhase) {
    Serial << "*** ERROR: flight phase should be 'Undefined', got " << (int) record.flightPhase << ENDL;
    numErrors++;
  }
  Serial << "----------------------" << ENDL;
}

void setup() {
  DINIT(115200);
  if (!ctrl.begin()) {
    Serial << "Unexpected error" << ENDL;
    exit (0);
  }
  record.clear();

  testInitialConditions();
  testConstantValue();
  testAvg(0, 1);
  testAvg(2, 10);
  testAvg(CansatVelocityThresholdForTakeoff / 2, 13);
  testAvg(CansatVelocityThresholdForTakeoff / 4, 0.13);
  testAvg(-CansatVelocityThresholdForTakeoff, -0.2); 
  testavgAlternatingValues(1,2);
  testavgAlternatingValues(CansatVelocityThresholdForTakeoff / 9, 2);
  testavgAlternatingValues(CansatVelocityThresholdForTakeoff / 3 , 11);
  testavgAlternatingValues(-3.14, 2.4);
  testavgAlternatingValues(CansatVelocityThresholdForTakeoff / 10, -2.94);
  testavgAlternatingValues(-CansatVelocityThresholdForTakeoff / 4 , -2.91);
  testavgDelayedTimestamp();
  testAvg(CansatVelocityThresholdForTakeoff + 7, 6);
  testEjection(record);
  testInvalidVelocity(record);
  Serial << "Number of errors: " << numErrors << ENDL;
  exit (0);
}

void loop() {


}
