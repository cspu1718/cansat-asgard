#include "EtheraBeacon.h"
#include "EtheraConfig.h"
#include "CSPU_Debug.h"

uint8_t beaconID = 0;

void setup() {

 EtheraBeacon::begin(id* uint8_t beaconID);
}

void loop() {
  EtheraBeacon::run();

}
