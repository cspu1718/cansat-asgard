/*
   Test program for class EtheraRecord
*/

#include "CansatConfig.h"
#include "EtheraRecord.h"
#include "StringStream.h"

#define DBG 1

class EtheraRecord_Test {
  public:
    void testClear(EtheraRecord &rec) {
      DPRINTLN(DBG, "Test: clearing record [BEGIN]");

      initValues(rec);
      DASSERT(rec.hasRSSI_Data() == true);
      clearValues(rec);
      DASSERT(rec.hasRSSI_Data() == false);
      checkClearedValues(rec);

      DPRINTLN(DBG, "Test successful");
      DPRINTLN(DBG, "Test: clearing record [END]");
    }
    void testCSVLen(EtheraRecord &rec) {
      DPRINTLN(DBG, "Test: CSV length [BEGIN]");
      initValues(rec);

      String s;
      StringStream ss(s);

      rec.printCSV_SecondaryMissionData(ss, true, true);
      DASSERT(rec.getSecondaryMissionMaxCSV_Size() >= s.length());

      s = "";

      rec.printCSV_SecondaryMissionHeader(ss, true, false);
      DASSERT(rec.getSecondaryMissionCSV_HeaderSize() == s.length());

      DPRINTLN(DBG, "Test successful");
      DPRINTLN(DBG, "Test: CSV length [END]");
    }
    void testReadWriteBinary(EtheraRecord &rec) {
      DPRINTLN(DBG, "Test: read/write binary [BEGIN]");

      initValues(rec);
      uint8_t binSize = rec.getBinarySizeSecondaryMissionData();
      byte bin[binSize];

      DASSERT(rec.writeBinarySecondaryMissionData(bin, binSize) == binSize);

      clearValues(rec);

      DASSERT(rec.readBinarySecondaryMissionData(bin, binSize) == binSize);

      checkValues(rec);

      DPRINTLN(DBG, "Test successful");
      DPRINTLN(DBG, "Test: read/write binary [END]");
    }
    void testCSVPrint(EtheraRecord &rec) {
      DPRINTLN(DBG, "Test: CSV print [BEGIN]");

      DPRINTLN(DBG, "Printing secondary mission CSV Header without additional separators")
      rec.printCSV_SecondaryMissionHeader(Serial, false, false);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Header without preceding separator, but with final separator")
      rec.printCSV_SecondaryMissionHeader(Serial, false, true);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Header without final separator, but with preceding separator")
      rec.printCSV_SecondaryMissionHeader(Serial, true, false);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Header with preceding and final separator")
      rec.printCSV_SecondaryMissionHeader(Serial, true, true);

      DPRINTLN(DBG, "\nPrinting secondary mission CSV Data without additional separators")
      rec.printCSV_SecondaryMissionData(Serial, false, false);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Data without preceding separator, but with final separator")
      rec.printCSV_SecondaryMissionData(Serial, false, true);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Data without final separator, but with preceding separator")
      rec.printCSV_SecondaryMissionData(Serial, true, false);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Data with preceding and final separator")
      rec.printCSV_SecondaryMissionData(Serial, true, true);


      DPRINTLN(DBG, "\nNote: Please check for yourself if the test was successful");
      DPRINTLN(DBG, "Test: CSV print [END]");
    }
    void testHumanReadablePrint(EtheraRecord &rec) {
      DPRINTLN(DBG, "Test: Human readable print [BEGIN]");
      rec.printSecondaryMissionData(Serial);
      DPRINTLN(DBG, "Note: Please check for yourself if the test was successful");
      DPRINTLN(DBG, "Test: Human readable print [END]");
    }

  private:
    void initValues(EtheraRecord &rec) {
      rec.brakePosition = 34;
      rec.flightControlCode = FlightControlCode::NominalDescent;
      rec.flightPhase = 35;
      rec.setFrequency(430.2) ;
      rec.setRSSI(-37.4);
      rec.distance = 10382;
    }
    void clearValues(EtheraRecord &rec) {
      rec.clearSecondaryMissionData();
    }
    void checkValues(EtheraRecord &rec) {
      DASSERT(rec.brakePosition == 34);
      DASSERT((uint8_t) rec.flightControlCode == (uint8_t) FlightControlCode::NominalDescent);
      DASSERT(rec.flightPhase == 35);
      DASSERT(fabs(rec.getFrequency() - 430.2) < 0.001);
      Serial.println(rec.getRSSI(), 10);
      DASSERT(fabs(rec.getRSSI() - -37.4) < 0.001);
      DASSERT(rec.distance == 10382);
    }
    void checkClearedValues(EtheraRecord &rec) {
      DASSERT(rec.brakePosition == 0);
      DASSERT((uint8_t) rec.flightControlCode == (uint8_t) FlightControlCode::NoData);
      DASSERT(rec.flightPhase == 0);
      DASSERT(rec.getFrequency() == 0);
      DASSERT(rec.getRSSI() == 0);
      DASSERT(rec.distance == 0);
    }
} ;

void setup() {
  DINIT(115200);

  EtheraRecord rec;
  EtheraRecord_Test test;

  Serial << "Binary size of record:" << rec.getBinarySize() << " bytes" << ENDL;
  test.testClear(rec);
  test.testCSVLen(rec);
  test.testReadWriteBinary(rec);
  test.testCSVPrint(rec);
  test.testHumanReadablePrint(rec);

  DPRINTLN(DBG, "Testing procedure complete");
}

void loop() {
  // put your main code here, to run repeatedly:

}
