/**
    A program just running the RSSI Cycle Manager with the period configured in the CansatConfig.h.
    It runs only the RF secondary mission of Ethera and displays the result on the Serial interface.
*/
#include "RSSI_CycleManager.h"
#include "elapsedMillis.h"
#include "EtheraRecord.h"

#undef RUN_GPS   // Define to include the GPS in order to check for interrupt conflicts.
#ifdef RUN_GPS
#include "Serial2.h"

// GPS is included to check for interrupt conflicts.
#include "GPS_Client.h"
GPS_Client gps(GPS_SerialPortNumber == 1 ? Serial1 : Serial2);
#endif

constexpr bool DisplayAllRecords = false; // If true ALL records are displayed, if false, only the record with an
// an actual RSSI measurement are displayed.
elapsedMillis idleDuration;

RSSI_CycleManager cycleManager;
EtheraRecord record;
elapsedMillis elapsed;

void setup() {
  DINIT(115200);
  Serial << "Test of RSSI_CycleManager with period=" << CansatAcquisitionPeriod << " msec." << ENDL;

  if (!cycleManager.begin()) {
    Serial << "*** Error initializing the RSSI_CycleManager" << ENDL;
    exit(-1);
  }
  record.clear();

#ifdef RUN_GSP
  // Initialise GPS to check for interrupt conflicts
  gps.begin((GPS_Client::Frequency) CansatGPS_Frequency, pin_GPS_Enable, true);
#endif

  Serial << "Initialisation OK." << ENDL;
  idleDuration = 0;
}

void loop() {
  if (elapsed > CansatAcquisitionPeriod) {
    elapsed = 0;
    record.clear();
    cycleManager.run(record);
    if (DisplayAllRecords || (record.hasRSSI_Data())) {
      Serial << "Freq=" << record.getFrequency() << " MHz, RSSI=" << record.getRSSI()
             << " dBm, distance=" << record.distance << "m" << ENDL;
      idleDuration = 0;
    }

#ifdef RUN_GPS
    gps.readData(record);
    record.printCSV(Serial);
    Serial.println();
#endif
  }

  if (idleDuration > 5000) {
    Serial << "Not receiving: is the source on ? " << ENDL;
    idleDuration = 0;
  }

}
