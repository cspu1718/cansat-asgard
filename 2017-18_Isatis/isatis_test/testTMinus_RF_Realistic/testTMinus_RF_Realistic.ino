
/* This sketch emits numbered cycles of integers on the RF link.

    If compiled for another board than the TMinus Serial1 is not used.
    (removed with cpp directives: Serial1 object is not defined.

*/

#include "IsatisDataRecord.h"
#include "StringStream.h"
#include "elapsedMillis.h"

const float PeriodCoefficient=0.1;
const int Serial1_BaudRate = 19200;
const int AcquisitionPeriod=IsatisAcquisitionPeriod * PeriodCoefficient;
String buffer;  // This is a string buffer to store a conversion of a record into CSV format.
elapsedMillis loopTimer;

void initBuffer() {
  IsatisDataRecord rec;
  rec.clear();
  rec.startTimestamp = 123456;
  rec.endTimestamp = 987654;
  rec.BMP_Temperature = 111.22222;
  rec.BMP_Pressure = 3333.44444;
  rec.BMP_Altitude = 55555.66666;
  rec.GPY_OutputV = 777.88888;
#ifndef USE_MINIMUM_DATARECORD
  rec.GPY_DustDensity = 999.12345;
  rec.GPY_AQI = 1234.56789;
#endif

  StringStream sstr(buffer);
  rec.printCSV(sstr);
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  while (!Serial);
  Serial.println("Init OK for serial link.");

  Serial << F("Size of Isatis data record is ") << sizeof(IsatisDataRecord) << F(" bytes") << ENDL;
  initBuffer();
  Serial << F("Size of CSV string representing Isatis data record is ") << buffer.length() << F(" bytes") << ENDL;
  Serial << F("IsatisAcquisitionPeriod: ") << IsatisAcquisitionPeriod << F(" msec") << ENDL;
  Serial << F("Coefficient used for test: ") << PeriodCoefficient << ENDL;
  Serial << F("Period used for test (IsatisAcquisitionPeriod*PeriodCoefficient): ") << AcquisitionPeriod << F(" msec") << ENDL;
 
#ifdef ARDUINO_TMinus
  Serial1.begin(Serial1_BaudRate);
  while (!Serial1) ;
  Serial.print("Baud rate for Serial1:");
  Serial.println(Serial1_BaudRate);
  while (!Serial1) ;

  Serial.println("Init OK for  RF transceiver.");
  Serial.println("The same data will be sent to Serial and Serial1..."); 
#else
  Serial.println(F("Not a TMinus card: this test is useless..."));
#endif

  Serial.println("End of Init");
  delay(100);
}


void loop() {
  static  unsigned long i = 0;
  if (loopTimer >= AcquisitionPeriod) {
    loopTimer = 0;
    Serial << millis() << F(": ") << buffer << F(" (") << i << F(")");
#ifdef ARDUINO_TMinus
    Serial << ENDL;
    Serial1 << millis() << F(": ") << buffer << F(" (") << i << F(")") << ENDL;
#else
    Serial << F(":  ***Not transmitted***") << ENDL;
#endif
    i++;
  }
}
