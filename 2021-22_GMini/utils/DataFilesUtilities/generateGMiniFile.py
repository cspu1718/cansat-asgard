#!/usr/local/bin/python3.7
# !/usr/bin/env python3

# import sys  # for argv
import os.path
import sys
import DataRecord
from itertools import repeat

MYSELF = os.path.basename(sys.argv[0])
TITLE = '\nCanSat data record file generator\n' \
          '---------------------------------'
USAGE = 'Usage: ' + MYSELF + ' (no option, no argument)'
OUTPUT_FILE_NAME = "GMiniTestData.csv"
FORCE_OUTPUT_FILE_OVERWRITE = True  # for development only.
START_LATITUDE = 50.80482
START_LONGITUDE = 4.34046
START_TEMP = 19.4
START_PRESSURE = 1024.3
START_ALTITUDE = 98.2
START_TS = [23542, 78456, 153288]
ADD_NOISE = True
DURATION_BEFORE_HANDOVER_IN_HOURS = 1
DURATION_BEFORE_TAKE_OFF_IN_HOURS = 2
DURATION_BEFORE_RETRIEVAL_IN_HOURS = 2
totalNumberOfRecords = 0

print(TITLE)
print()


def generate_records_downto_ground(ground_altitude, number_from_each_can):
    while number_from_each_can > 0:
        generate_records(5)
        for s in [0, 1, 2]:
            if record[s].altitude < ground_altitude:
                record[s].verticalVelocityVariation = None
                record[s].longitudeVariation = None
                record[s].latitudeVariation = None
                record[s].altitudeVariation = None
                record[s].thermistorVariation = None
                record[s].bmpTempVariation = None
        number_from_each_can -= 5


def generate_records(number):
    global totalNumberOfRecords
    global outputFile
    for _ in repeat(None, number):
        for s in [0, 1, 2]:
            record[s].modify_record()
            if ADD_NOISE:
                outputFile.write(record[s].noisy_version().csv_line()+'\n')
            else:
                outputFile.write(record[s].csv_line() + '\n')
    totalNumberOfRecords += number*3


if len(sys.argv) != 1:
    print(MYSELF, ': Missing or extraneous argument (Aborted)')
    print(USAGE)
    sys.exit(1)

if os.path.exists(OUTPUT_FILE_NAME):
    if FORCE_OUTPUT_FILE_OVERWRITE:
        print('Overwriting data file \'' + OUTPUT_FILE_NAME + '\'...')
    else:
        print(MYSELF + ': ERROR: Output file \'' + OUTPUT_FILE_NAME + '\' exists (Aborted)')
        exit(3)
else:
    print('Generating data file \'' + OUTPUT_FILE_NAME + '\'...')
outputFile = open(OUTPUT_FILE_NAME, 'w')
record = [DataRecord.DataRecord(0, START_TS[0]),
          DataRecord.DataRecord(1, START_TS[1]),
          DataRecord.DataRecord(2, START_TS[2])]
for i in range(0, 3):
    record[i].altitude = START_ALTITUDE
    record[i].pressure = START_PRESSURE
    record[i].refAltitude = START_ALTITUDE
    record[i].bmpTemperature = START_TEMP + 5.1
    record[i].therm1_Temperature = START_TEMP
    record[i].therm2_Temperature = START_TEMP + 0.7

# 1. Generate records before launch
print('  Cans startup...')
# 1.1 Without GPS data
# 1.1.1 Just main can
for i in range(1, 3 * 60 * 5):
    record[0].modify_record()
    outputFile.write(record[0].noisy_version().csv_line() + '\n')
    totalNumberOfRecords += 1
# 1.1.2 Main + sub1
for i in repeat(None, 4 * 60 * 5 + 11):
    for src in [0, 1]:
        record[src].modify_record()
        outputFile.write(record[src].noisy_version().csv_line() + '\n')
    totalNumberOfRecords += 2
# 1.1.2 Main + sub1 + sub2
generate_records(3 * 60 * 5 - 8)

# 1.2 With stable GPS data
print('  From startup to hand-over...')
for src in [0, 1, 2]:
    record[src].gpsAltitude = START_ALTITUDE
    record[src].gpsLatitude = START_LATITUDE
    record[src].gpsLongitude = START_LONGITUDE
generate_records(DURATION_BEFORE_HANDOVER_IN_HOURS * 60 * 60 * 5)

# 1.3 With varying GPS data (transport to launch site)
#     Only main can has an active GPS
print('  Transport to launch site...')
record[0].latitudeVariation = DataRecord.Variation(0.5, 40, False)
record[0].longitudeVariation = DataRecord.Variation(0.5, 50, False)
generate_records(300)

# 2. Take-off and ascent
print('  Take-off and ascent...')
#    Ascent on 120 record
#    Accelerate on 10 records, ascent on 90 more, decelerate on 20 last.
#    GPS activated for subs avec 50 records.
record[0].latitudeVariation = DataRecord.Variation(0.5, 40, repeat=True)
record[0].longitudeVariation = DataRecord.Variation(0.5, 50, repeat=True)
for src in [0, 1, 2]:
    record[src].altitudeVariation = DataRecord.Variation(995, 120, repeat=False)
    record[src].verticalVelocityVariation = DataRecord.Variation(-15, 10, repeat=False)
generate_records(50)
# resync and restart variations on lat/long
for src in [1, 2]:
    record[src].gpsLatitude = record[0].gpsLatitude
    record[src].gpsLongitude = record[0].gpsLongitude
    record[src].countSinceNewGPS = 2
record[0].countSinceNewGPS = 2
for src in [0, 1, 2]:
    record[src].latitudeVariation = DataRecord.Variation(0.5, 40, repeat=True)
    record[src].longitudeVariation = DataRecord.Variation(0.5, 50, repeat=True)
generate_records(50)
for src in [0, 1, 2]:
    record[src].verticalVelocityVariation = DataRecord.Variation(15, 20, False)
generate_records(20)
# We are at the apex. Assume ejection from rocket
# 3. Descent: acceleration to 10 m/s in 3 sec = 15 rec then stable velocity
#             variation of lat/long until the end
#              temperature drops by 15°C
#             After 4 seconds = 20 records ejection
print("  Descent...")
record[0].thermistorVariation = DataRecord.Variation(-15, 20, False)
record[0].bmpTempVariation = DataRecord.Variation(-15, 350, False)
for src in [0, 1, 2]:
    record[src].verticalVelocityVariation = DataRecord.Variation(10, 15, False)
generate_records(20)
print("  Ejection...")

# Ejection: main can slows down, subcans accelerate, variations in lat/long differ
#           temperature drops by 15°C
record[1].thermistorVariation = DataRecord.Variation(-14, 25, False)
record[1].bmpTempVariation = DataRecord.Variation(-14, 350, False)
record[2].thermistorVariation = DataRecord.Variation(-16, 25, False)
record[2].bmpTempVariation = DataRecord.Variation(-16, 350, False)
record[0].subcanEjected = True
record[0].verticalVelocityVariation = DataRecord.Variation(-2, 15, False)
record[0].altitudeVariation = DataRecord.Variation(-1600, 1000, False)  # = 8 m/s
record[1].verticalVelocityVariation = DataRecord.Variation(2.1, 15, False)
record[2].verticalVelocityVariation = DataRecord.Variation(1.9, 15, False)
record[1].altitudeVariation = DataRecord.Variation(-2400, 1000, False)  # = 12 m/s
record[2].altitudeVariation = DataRecord.Variation(-2400, 1000, False)  # = 12 m/s
record[1].latitudeVariation = DataRecord.Variation(0.7, 40, repeat=True)
record[1].longitudeVariation = DataRecord.Variation(-0.5, 50, repeat=True)
record[2].latitudeVariation = DataRecord.Variation(0.7, 50, repeat=True)
record[2].longitudeVariation = DataRecord.Variation(-0.5, 30, repeat=True)
generate_records(25)
for src in [0, 1, 2]:
    record[src].thermistorVariation = DataRecord.Variation(15, 500, False)

print('  Descent of all 3 cans down to 95 m...')
# When altitude reaches 95 m cans stop
generate_records_downto_ground(95, DURATION_BEFORE_RETRIEVAL_IN_HOURS * 60 * 60 * 5)

outputFile.close()
print(MYSELF + ': Generated', totalNumberOfRecords, 'records. End of job.')
exit(0)
