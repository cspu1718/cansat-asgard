import copy
import math
import random


class Variation:
    def __init__(self, max_delta, num_steps, repeat=False):
        """Initialise the requested variation
        Parameters
        ----------
        max_delta: integer or float, positive or negative
            The maximum variation to be applied.
        num_steps: integer
            The number of steps to use to vary from 0 to max_delta
        repeat: boolean
            If true, the variation is applied repetitively, alternately increasing and decreasing the value.
            If false, the variation stops after the max_delta is reached.
        """
        assert max_delta != 0
        assert num_steps > 0
        self.repeat = repeat
        self.current = 0
        self.maxDelta = max_delta
        self.increment = math.fabs(max_delta / num_steps)
        self.increasing = (max_delta > 0)
        self.done = False

    def __str__(self):
        return "max_delta={}, increment={}, increasing={}, repeat={}, done={}".format(self.maxDelta,
                                                                           self.increment,
                                                                           self.increasing,
                                                                           self.repeat,
                                                                           self.done)

    def get_increment(self):
        """Obtain the variation to apply in the current record"""
        if self.done:
            return 0

        if self.increasing:
            self.current += self.increment
            if ((self.maxDelta > 0) and (self.current > self.maxDelta)) or ((self.maxDelta < 0) and (self.current > 0)):
                if self.repeat:
                    self.current -= 2 * self.increment
                    self.increasing = False
                else:
                    self.done = True
        else:
            self.current -= self.increment
            if ((self.maxDelta < 0) and (self.current < self.maxDelta)) or ((self.maxDelta > 0) and (self.current < 0)):
                if self.repeat:
                    self.current += 2 * self.increment
                    self.increasing = True
                else:
                    self.done = True
        if self.done:
            return 0
        else:
            if self.increasing:
                return self.increment
            else:
                return -self.increment


class DataRecord:
    subcanEjected: bool
    random = random.Random()
    ACQUISITION_PERIOD = 200  # msec

    def __init__(self, src, start_ts):
        self.timestamp = start_ts
        self.newGPS_Data = 0
        self.gpsLatitude = 0.0
        self.gpsLongitude = 0.0
        self.gpsAltitude = 0.0
        self.pressure = 0.0
        self.altitude = 0.0
        self.refAltitude = 0.0
        self.verticalVelocity = 0.0
        self.bmpTemperature = 0.0
        self.therm1_Temperature = 0.0
        self.therm2_Temperature = 0.0
        self.subcanEjected = False
        self.sourceID = src
        self.countSinceNewGPS = 0
        self.latitudeVariation = None
        self.longitudeVariation = None
        self.altitudeVariation = None
        self.verticalVelocityVariation = None
        self.thermistorVariation = None
        self.bmpTempVariation = None

    def csv_line(self):
        return str(self.timestamp) + ',' \
               + str(self.newGPS_Data) + ',' \
               + '{:.5f},{:.5f},{:.1f},'.format(self.gpsLatitude, self.gpsLongitude, self.gpsAltitude) \
               + '{:.1f},{:.1f},{:.1f},{:.1f},'.format(self.bmpTemperature, self.pressure, self.altitude,
                                                       self.refAltitude) \
               + '{:.2f},{:.1f},{:.1f},'.format(self.verticalVelocity, self.therm1_Temperature,
                                                self.therm2_Temperature) \
               + str(self.sourceID) + ',' + str(self.subcanEjected)

    def noisy_version(self):
        noisy = copy.deepcopy(self)
        noisy.add_noise()
        return noisy

    def add_noise(self):
        if self.newGPS_Data == 1:
            if self.gpsLatitude != 0:
                self.gpsLatitude += self.random.normalvariate(0, 0.00005)
            if self.gpsLongitude != 0:
                self.gpsLongitude += self.random.normalvariate(0, 0.00005)
            if self.gpsAltitude != 0:
                self.gpsAltitude += self.random.normalvariate(0, 0.6)
        self.bmpTemperature += self.random.normalvariate(0, 0.1)
        self.pressure += self.random.normalvariate(0, 0.5)
        self.altitude += self.random.normalvariate(0, 0.5)
        self.verticalVelocity += self.random.normalvariate(0, 0.3)
        self.therm1_Temperature += self.random.normalvariate(0, 0.3)
        self.therm2_Temperature += self.random.normalvariate(0, 0.4)

    def modify_record(self):
        """Modify the record according to the rules defined with """
        self.timestamp += DataRecord.ACQUISITION_PERIOD
        self.countSinceNewGPS = (self.countSinceNewGPS + 1) % 3
        if self.countSinceNewGPS == 0:
            self.newGPS_Data = 1
            if self.latitudeVariation is not None:
                self.gpsLatitude += self.latitudeVariation.get_increment()
            if self.longitudeVariation is not None:
                self.gpsLongitude += self.longitudeVariation.get_increment()
                if self.gpsLongitude > 180:
                    print("ERROR: longitude={}".format(self.gpsLongitude))
                    exit(3)
        else:
            self.newGPS_Data = 0
        if self.altitudeVariation is not None:
            altitude_increment = self.altitudeVariation.get_increment()
            self.gpsAltitude += altitude_increment
            self.altitude += altitude_increment
            self.pressure -= altitude_increment / 10.0
        if self.verticalVelocityVariation is not None:
            self.verticalVelocity += self.verticalVelocityVariation.get_increment();
        if self.thermistorVariation is not None:
            temp_increment = self.thermistorVariation.get_increment()
            self.therm1_Temperature += temp_increment
            if self.sourceID == 0:
                self.therm2_Temperature += temp_increment
        if self.bmpTempVariation is not None:
            self.bmpTemperature += self.bmpTempVariation.get_increment()
