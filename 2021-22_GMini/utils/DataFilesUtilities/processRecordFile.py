#!/usr/local/bin/python3.7
# !/usr/bin/env python3

""" Script to process GMini data file:
    Split in 1 file per source, and modify file as follows, in all files:
        - Add header line
        - Add  and complete column CorrectedTS in all files
        - Add and complete column relativeTS in all files
        - Add and complete column relativeAltitude in all files
        - Add and complete columns tempTherm1corr and tempTherm2corr
    Save key information in file MYSELF.summary.txt in the current directory. If the file exists
    the program stops.
    Read required information for processing from file MYSELF.input.txt in current directory. If the
    file does not exist, an empty version of it, properly formatted is generated.
    To run the script at command prompt:
        make sure the directory containing the script is in PYTHON_PATH
        cd to directory containing data file
        python3.7 <thisScript> InputFile
"""
# import sys  # for argv
import os.path
import csv
import sys
from typing import List
from CommentStripper import comment_stripper
from datetime import datetime

MYSELF = os.path.basename(sys.argv[0])
MYSELF_NO_EXT = os.path.splitext(MYSELF)[0]
TITLE = '\nG-Mini record file processing utility\n' \
        '-------------------------------------'
PARAM_FILE_NAME = MYSELF_NO_EXT + ".input.txt"
SUMMARY_FILE_NAME = MYSELF_NO_EXT + ".summary.txt"
USAGE = 'Usage: ' + MYSELF + ' (file)'
NUM_RECORD_FIELDS = 14
TIMESTAMP_FIELD = 0
ALTITUDE_FIELD = 7
REF_ALTITUDE_FIELD = 8
THERM1_TEMP_INDEX = 10
THERM2_TEMP_INDEX = 11
SOURCE_ID_FIELD = 12  # idx of source ID field
GPS_ALTITUDE_FIELD = 4  # idx of source ID field
FORCE_OUTPUT_FILE_OVERWRITE = True  # for development only.
CSV_HEADER = 'TS,corrTS, relTS, relTS (s),GPS_Measures,GPS_LatDegrees,GPS_LongDegrees,GPS_Alt,tempBMP,pressure,alt.,' \
             'ref.alt.,rel.alt.,descVelocity,therm1, therm1corr, therm2, therm2corr, ' \
             'sourceID,subCanEjected'
# parameters
inputFileName = ''
relevantDataStartTS = 0
relevantDataStopTS = 0
originTS0 = 0
simultaneousTS = [0, 0, 0]
offsetTS = [0, 0, 0]
offsetTherm1 = [0.0, 0.0, 0.0]
offsetTherm2 = [0.0, 0.0, 0.0]
print(TITLE)
print()

if len(sys.argv) != 1:
    print(MYSELF_NO_EXT, ': Extraneous argument(s) detected (Aborted)')
    print(USAGE)
    sys.exit(1)


def read_integer_input_param(the_reader):
    local_row = the_reader.__next__()
    if len(local_row) == 1:
        value = int(local_row[0])
        if value == 0:
            print("Error: found null value in parameters file (line {}).".format(the_reader.line_num))
        return value
    else:
        print("Error reading '{}': line {} should contain 1 value.".format(PARAM_FILE_NAME, reader.line_num))
        print("      Got '{}'".format(",".join(local_row)))
        return 0


def read_float_input_param(the_reader):
    local_row = the_reader.__next__()
    if len(local_row) == 1:
        value = float(local_row[0])
        return value
    else:
        print("Error reading '{}': line {} should contain 1 value.".format(PARAM_FILE_NAME, reader.line_num))
        print("      Got '{}'".format(",".join(local_row)))
        return 0.0


# Find parameters file
if os.path.exists(PARAM_FILE_NAME):
    paramFile = open(PARAM_FILE_NAME, "r")
    reader = csv.reader(comment_stripper(paramFile))
    inputFileName = reader.__next__()[0]
    relevantDataStartTS = read_integer_input_param(reader)
    relevantDataStopTS = read_integer_input_param(reader)
    originTS0 = read_integer_input_param(reader)
    simultaneousTS[0] = read_integer_input_param(reader)
    simultaneousTS[1] = read_integer_input_param(reader)
    simultaneousTS[2] = read_integer_input_param(reader)
    offsetTS[1] = simultaneousTS[1] - simultaneousTS[0]
    offsetTS[2] = simultaneousTS[2] - simultaneousTS[0]
    offsetTherm1[0] = read_float_input_param(reader)
    offsetTherm2[0] = read_float_input_param(reader)
    offsetTherm1[1] = read_float_input_param(reader)
    offsetTherm2[1] = read_float_input_param(reader)
    offsetTherm1[2] = read_float_input_param(reader)
    offsetTherm2[2] = read_float_input_param(reader)

    if not (relevantDataStartTS and relevantDataStopTS and originTS0 and simultaneousTS[0]
            and simultaneousTS[1] and simultaneousTS[2]):
        print("{} aborted.".format(MYSELF))
        exit(2)
else:
    print("{}: Could not find parameters file '{}' in current directory".format(MYSELF_NO_EXT, PARAM_FILE_NAME))
    paramFile = open(PARAM_FILE_NAME, "w")
    paramFile.write("# Parameter file for utility {} (Comma-separated values, CSV). \n"
                    "# Complete nnnnn values, do not add or remove any line or column\n"
                    "#\n".format(MYSELF))
    paramFile.write(
        "# Input file (plain file from can or RF-Transceiver, records must by {} columns)\nxxxxx.csv\n".format(
            NUM_RECORD_FIELDS))
    paramFile.write("# Relevant data starts from (TS main can)\n00000\n")
    paramFile.write("# Relevant data stops at (TS main can)\n00000\n")
    paramFile.write("# Origin for relative timestamps (TS main can, will become ts 0)\n00000\n")
    paramFile.write("# TS of records from main can and subcans assumed to be simultaneous "
                    "(used to compute timestamp offsets)\n")
    paramFile.write("# TS main can record\n00000\n")
    paramFile.write("# TS subcan 1 record\n00000\n")
    paramFile.write("# TS subcan 2 record\n00000\n")
    paramFile.write("# Offset for temperature thermistor 1 in main can\n0.0\n")
    paramFile.write("# Offset for temperature thermistor 2 in main can\n0.0\n")
    paramFile.write("# Offset for temperature thermistor 1 in sub can 1\n0.0\n")
    paramFile.write("# Offset for temperature thermistor 2 in sub can 1\n0.0\n")
    paramFile.write("# Offset for temperature thermistor 1 in sub can 2\n0.0\n")
    paramFile.write("# Offset for temperature thermistor 2 in sub can 2\n0.0\n")
    paramFile.close()
    print("Created a template in current directory, please complete it and run the script again (Aborted).")
    exit(1)

# Check summary file can be created.
if (not FORCE_OUTPUT_FILE_OVERWRITE) and os.path.exists(SUMMARY_FILE_NAME):
    print("{}: ERROR: Output summary file '{}' exists. Remove it and launch again (Aborted).".format(MYSELF,
                                                                                                     SUMMARY_FILE_NAME))
    exit(3)

# Check input file can be found.
if not os.path.exists(inputFileName):
    print("{}: Cannot find input file '{}' (Aborted).".format(MYSELF, inputFileName))
    exit(2)

# check output files can be generated.
fileNameNoExt = os.path.splitext(inputFileName)[0]
outputFileNames: List[str] = [fileNameNoExt + '_0.csv', fileNameNoExt + '_1.csv', fileNameNoExt + '_2.csv']
for name in outputFileNames:
    if (not FORCE_OUTPUT_FILE_OVERWRITE) and os.path.exists(name):
        print(MYSELF + ': ERROR: Output file \'' + name + '\' exists (Aborted)')
        exit(3)

print("Processing file '{}'...".format(inputFileName))
summaryFile = open(SUMMARY_FILE_NAME, 'w')

# Let's go...
summaryFile.write("{}, {}\n\n".format(MYSELF, datetime.now()))
summaryFile.write("Input parameters:\n")
summaryFile.write("  Input file: {}\n".format(inputFileName))
summaryFile.write("  Relevant data starts at TS main = {}\n".format(relevantDataStartTS))
summaryFile.write("  Relevant data stops at TS main  = {}\n".format(relevantDataStopTS))
summaryFile.write(
    "  Duration of relevant data       = {} sec.\n".format((relevantDataStopTS - relevantDataStartTS) / 1000.0))
summaryFile.write("  t=0 at TS main                  = {}\n".format(originTS0))
summaryFile.write("  Simultaneous TS (0,1,2)         = {},{},{}\n".format(simultaneousTS[0],
                                                                          simultaneousTS[1],
                                                                          simultaneousTS[2]))
summaryFile.write("  Offset from TS main to TS sub1  = {}\n".format(offsetTS[1]))
summaryFile.write("  Offset from TS main to TS sub2  = {}\n".format(offsetTS[2]))
summaryFile.write("  Offset applied to temp. thermistor 1 (0,1,2)  = {}, {}, {} °C \n".format(offsetTherm1[0],
                                                                                              offsetTherm1[1],
                                                                                              offsetTherm1[2]))
summaryFile.write("  Offset applied to temp. thermistor 2 (0,1,2)  = {}, {}, {} °C\n".format(offsetTherm2[0],
                                                                                             offsetTherm2[1],
                                                                                             offsetTherm2[2]))

inputFile = open(inputFileName, 'r')
reader = csv.reader(inputFile, delimiter=',')
outputFiles = []
for i in [0, 1, 2]:
    outputFiles.append(open(outputFileNames[i], 'w'))
    outputFiles[i].write(CSV_HEADER + '\n')

# ------------ Real work starts here ..................................
# ---------------------------------------------------------------------

numSkippedRows = 0
numRecords = 0
numRecordsWritten = 0
numRecordsPerSource = [0, 0, 0]
numRecordsWrittenPerSource = [0, 0, 0]
minTS: List[int] = [1E20, 1E20, 1E20]
maxTS = [0, 0, 0]
minTS_Corrected: List[int] = [1E20, 1E20, 1E20]
maxTS_Corrected = [0, 0, 0]
outputMinTS_Corrected: List[int] = [1E20, 1E20, 1E20]
outputMaxTS_Corrected = [0, 0, 0]
outputMinTS_Relative: List[int] = [1E20, 1E20, 1E20]
outputMaxTS_Relative = [0, 0, 0]
maxRelativeAltitude = [-1000.0, -1000.0, -1000.0]
maxRelativeAltitudeTS = [0, 0, 0]
maxRelativeAltitudeTS_Corrected = [0, 0, 0]
maxRelativeAltitudeTS_Relative = [0, 0, 0]
maxTemperature = [-1000.0, -1000.0, -1000.0]
maxTemperatureTS = [0, 0, 0]
maxTemperatureTS_Corrected = [0, 0, 0]
maxTemperatureTS_Relative = [0, 0, 0]
minTemperature = [1000.0, 1000.0, 1000.0]
minTemperatureTS = [0, 0, 0]
minTemperatureTS_Corrected = [0, 0, 0]
minTemperatureTS_Relative = [0, 0, 0]
maxRelevantRelativeAltitude = [-1000.0, -1000.0, -1000.0]
maxRelevantRelativeAltitudeTS = [0, 0, 0]
maxRelevantRelativeAltitudeTS_Corrected = [0, 0, 0]
maxRelevantRelativeAltitudeTS_Relative = [0, 0, 0]
maxRelevantTemperature = [-1000.0, -1000.0, -1000.0]
maxRelevantTemperatureTS = [0, 0, 0]
maxRelevantTemperatureTS_Corrected = [0, 0, 0]
maxRelevantTemperatureTS_Relative = [0, 0, 0]
minRelevantTemperature = [1000.0, 1000.0, 1000.0]
minRelevantTemperatureTS = [0, 0, 0]
minRelevantTemperatureTS_Corrected = [0, 0, 0]
minRelevantTemperatureTS_Relative = [0, 0, 0]
withinRelevantData = False

for row in reader:
    if (len(row) != NUM_RECORD_FIELDS) or (not row[0].isdigit()) or (not row[SOURCE_ID_FIELD].isdigit()):
        numSkippedRows += 1
        continue
    else:
        sourceID = int(row[SOURCE_ID_FIELD])
        numRecords += 1
        numRecordsPerSource[sourceID] += 1
        ts = int(row[0])
        tsCorrected = ts - offsetTS[sourceID]
        tsRelative = tsCorrected - originTS0
        if ts > maxTS[sourceID]:
            maxTS[sourceID] = ts
            maxTS_Corrected[sourceID] = tsCorrected
        if ts < minTS[sourceID]:
            minTS[sourceID] = ts
            minTS_Corrected[sourceID] = tsCorrected
        temp = float(row[THERM1_TEMP_INDEX])
        if temp > maxTemperature[sourceID]:
            maxTemperature[sourceID] = temp
            maxTemperatureTS[sourceID] = ts
            maxTemperatureTS_Corrected[sourceID] = tsCorrected
            maxTemperatureTS_Relative[sourceID] = tsRelative
        if temp < minTemperature[sourceID]:
            minTemperature[sourceID] = temp
            minTemperatureTS[sourceID] = ts
            minTemperatureTS_Corrected[sourceID] = tsCorrected
            minTemperatureTS_Relative[sourceID] = tsRelative

        # calculate BEFORE adding column (changes indexes!)
        relative_altitude = float(row[ALTITUDE_FIELD]) - float(row[REF_ALTITUDE_FIELD])
        relative_altitude_str = '{:.1f}'.format(relative_altitude)
        therm1corr = float(row[THERM1_TEMP_INDEX]) + offsetTherm1[sourceID]
        therm2corr = float(row[THERM2_TEMP_INDEX]) + offsetTherm2[sourceID]

        if relative_altitude > maxRelativeAltitude[sourceID]:
            maxRelativeAltitude[sourceID] = relative_altitude
            maxRelativeAltitudeTS[sourceID] = ts
            maxRelativeAltitudeTS_Corrected[sourceID] = tsCorrected
            maxRelativeAltitudeTS_Relative[sourceID] = tsRelative
        if sourceID == 0:  # update isRelevantData is required.
            withinRelevantData = (ts >= relevantDataStartTS) and (ts <= relevantDataStopTS)

        if withinRelevantData:
            # collect relevant statistics
            if temp > maxRelevantTemperature[sourceID]:
                maxRelevantTemperature[sourceID] = temp
                maxRelevantTemperatureTS[sourceID] = ts
                maxRelevantTemperatureTS_Corrected[sourceID] = tsCorrected
                maxRelevantTemperatureTS_Relative[sourceID] = tsRelative
            if temp < minRelevantTemperature[sourceID]:
                minRelevantTemperature[sourceID] = temp
                minRelevantTemperatureTS[sourceID] = ts
                minRelevantTemperatureTS_Corrected[sourceID] = tsCorrected
                minRelevantTemperatureTS_Relative[sourceID] = tsRelative
            if relative_altitude > maxRelevantRelativeAltitude[sourceID]:
                maxRelevantRelativeAltitude[sourceID] = relative_altitude
                maxRelevantRelativeAltitudeTS[sourceID] = ts
                maxRelevantRelativeAltitudeTS_Corrected[sourceID] = tsCorrected
                maxRelevantRelativeAltitudeTS_Relative[sourceID] = tsRelative
            # insert and fill new columns if relevant
            if tsCorrected > outputMaxTS_Corrected[sourceID]:
                outputMaxTS_Corrected[sourceID] = tsCorrected
                outputMaxTS_Relative[sourceID] = tsRelative
            if tsCorrected < outputMinTS_Corrected[sourceID]:
                outputMinTS_Corrected[sourceID] = tsCorrected
                outputMinTS_Relative[sourceID] = tsRelative
            numRecordsWritten += 1
            numRecordsWrittenPerSource[sourceID] += 1
            row.insert(TIMESTAMP_FIELD + 1, str(tsCorrected))
            row.insert(TIMESTAMP_FIELD + 2, str(tsRelative))
            row.insert(TIMESTAMP_FIELD + 3, str(tsRelative/1000.0))
            row.insert(REF_ALTITUDE_FIELD + 4, relative_altitude_str)
            row.insert(THERM1_TEMP_INDEX + 5, "{:.1f}".format(therm1corr))
            row.insert(THERM2_TEMP_INDEX + 6, "{:.1f}".format(therm2corr))
            line = ','.join(str(x) for x in row)
            outputFiles[sourceID].write(line + '\n')

# Output statistics
summaryFile.write("\nStatistics\n")
summaryFile.write("  Number of records processed          : {}\n".format(numRecords))
summaryFile.write("  Number of non-record lines (skipped) : {}\n".format(numSkippedRows))

check = numRecords
for i in [0, 1, 2]:
    sourceNames = ["Main can", "Subcan 1", "Subcan 2"]
    summaryFile.write("  {} (source {}) :\n".format(sourceNames[i], i))
    # 1. General data
    summaryFile.write("      Processed {} record, local ts from {} to {} (corr. from {} to {})\n".format(
        numRecordsPerSource[i],
        minTS[i],
        maxTS[i],
        minTS_Corrected[i],
        maxTS_Corrected[i]))
    summaryFile.write("        Max. relative altitude: {:.1f}m at timestamp {} (corr. {}, relative {})\n".
                      format(maxRelativeAltitude[i],
                             maxRelativeAltitudeTS[i],
                             maxRelativeAltitudeTS_Corrected[i],
                             maxRelativeAltitudeTS_Relative[i]))
    summaryFile.write("        Max. temperature: {:.1f}°C at timestamp {} (corr. {}, rel. {})\n".format(
        maxTemperature[i],
        maxTemperatureTS[i],
        maxTemperatureTS_Corrected[i],
        maxTemperatureTS_Relative[i]))
    summaryFile.write("        Min. temperature: {:.1f}°C at timestamp {} (corr. {}, rel. {})\n".format(
        minTemperature[i],
        minTemperatureTS[i],
        minTemperatureTS_Corrected[i],
        minTemperatureTS_Relative[i]))
    summaryFile.write("      Output {} relevant record in file '{}', corr. ts from {} to {} (rel. from {} to {})\n".format(
        numRecordsWrittenPerSource[i],
        outputFileNames[i],
        outputMinTS_Corrected[i],
        outputMaxTS_Corrected[i],
        outputMinTS_Relative[i],
        outputMaxTS_Relative[i]))
    # 2. relevant data
    summaryFile.write("        Max. relevant relative altitude: {:.1f}m at timestamp {} (corr. {}, relative {})\n".
                      format(maxRelevantRelativeAltitude[i],
                             maxRelevantRelativeAltitudeTS[i],
                             maxRelevantRelativeAltitudeTS_Corrected[i],
                             maxRelevantRelativeAltitudeTS_Relative[i]))
    summaryFile.write("        Max. relevant temperature: {:.1f}°C at timestamp {} (corr. {}, rel. {})\n".format(
        maxRelevantTemperature[i],
        maxRelevantTemperatureTS[i],
        maxRelevantTemperatureTS_Corrected[i],
        maxRelevantTemperatureTS_Relative[i]))
    summaryFile.write("        Min. relevant temperature: {:.1f}°C at timestamp {} (corr. {}, rel. {})\n".format(
        minRelevantTemperature[i],
        minRelevantTemperatureTS[i],
        minRelevantTemperatureTS_Corrected[i],
        minRelevantTemperatureTS_Relative[i]))
    check -= numRecordsPerSource[i]
if check != 0:
    print(MYSELF + ': **** Inconsistency issue: numRecords is not sum of records from all sources.')

summaryFile.close()

print("Done.")
print("Results in files '" + "', ".join(outputFileNames) + "'.")
print("See details in file '{}'".format(SUMMARY_FILE_NAME))

exit(0)
