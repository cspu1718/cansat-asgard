"""
Comment stripper for csv files (ignores lines starting with '#')
Usage: reader = csv.reader (comment_stripper(open (csv_file)))
"""


def comment_stripper(iterator):
    for line in iterator:
        if line[:1] == '#':
            continue
        if not line.strip():
            continue
        yield line
