/*
   This an example for the main sketch running on-board the can for an hypothetical project named xxx,
   performed during year nnnn-nn)
   It makes use of all the reusable code from previous projects. It includes:

   - This main sketch (for a project, it should be located in cansat-asgard/nnnn-nn_xxx)
   - Subclasses and specialisations of the generic classes (for a project they should be located in cansat_asgard/libraries/nnnn_nn_xxx/src)
   	   - An example subclass of CansatRecord, xxxCansatRecord (based on CansatRecordExample located in library CansatData
     	   to be available to other test/example programs)
   	   - An example subclass of CansatAcquisitionProcess: xxxAcquisitionProcess
   	   - An example subclass of SecondaryMissionController: xxxMissionController
   	   - An example subclass of HardwareScanner: xxxHW_Scanner

   When creating a program for an actual Cansat project, be sure:
   1. To start from sketch.CansatMain, and make a copy as yourProjectNameMain in
      folder 20mm-nn_xxx.
      Possibly customize it for the project specific classes (see "$$$" tags)
   2. To create the required project-specific classes:
      - xxxAcquisitionProcess (almost always),
      - xxxHW_Scanner (if you have any specific hardware to detect or to provide accerss to)
      - xxxMissionController (if you need to control your secondary mission, i.e. do anything else than collecting data)
      - xxxCanCommander (if you need to support project-specific commands).
      Create those classes in libraries/CSPU_20nn-mm_xxx/src, together with their test programs.

   This structure has been used since the Torus project (except for the SecondaryMissionController which was integrated in the
   architecture based on Torus experience).

   NB: Would you need a can software including only the primary mission, please use the CansatMain sketch.
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CanSetupAndLoop.h"
#include "xxxAcquisitionProcess.h"
#include "xxxCanCommander.h"

class Can : public CanSetupAndLoop<xxxAcquisitionProcess, xxxCanCommander> {
    virtual const char* getProjectName() const {
      return "xxx";
    };
    virtual void printBanner()  const{
      Serial << "#     #  #     # #     # "  << ENDL;
      Serial << " #   #    #   #   #   #  "  << ENDL;
      Serial << "  # #      # #     # #   "  << ENDL;
      Serial << "   #        #       #    "  << ENDL;
      Serial << "  # #      # #     # #   "  << ENDL;
      Serial << " #   #    #   #   #   #  "  << ENDL;
      Serial << "#     #  #     # #     # "  << ENDL;
    };
};

Can can;

void setup() {
  can.setup();
}

void loop() {
  can.loop();
}
