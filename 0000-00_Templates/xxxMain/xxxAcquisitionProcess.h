/*
 * xxxAcquisitionProcess.h
 */

#pragma once
#include "CansatAcquisitionProcess.h"
#include "xxxMissionController.h"
#include "xxxHW_Scanner.h"
#include "xxxRecord.h"

/** @brief A CansatAcquisitionProcess subclass example for an hypothetical project
 *  named xxx. This dummy class demonstrate the architecture of a Cansat project
 *  in conjunction with xxxMainWithRT-CanCommander sketch. 
 *  For a real project:
 *  - this class would be located in the project's specific library
 *  - the methods implementation would be located in a cpp file, if it is
 *    more than a few lines.
 */
class xxxAcquisitionProcess: public CansatAcquisitionProcess<xxxRecord, xxxMissionController, xxxHW_Scanner> {
public:
	xxxAcquisitionProcess() {};
	virtual ~xxxAcquisitionProcess() {};
protected:
    /** @name Protected methods overridden from base class.
     * 	See detailed description in base classes.
     *  @{   */

    virtual void doIdle() override  {
    	CansatAcquisitionProcess::doIdle();
    	// do our own background tasks here, if any
    };

private:
	/** @name Private methods overridden from base class.
	 *  See detailed description in base classes.
	 *  @{   */
	virtual void initSpecificProject () override {
		// Perform specific project initialisation here
	};

	virtual void acquireSecondaryMissionData (CansatRecord &) override {
	  // Read secondary mission sensors and populate record here
	} ;
	/** @} */

	xxxMissionController xxxController; // Our secondaryMissionController.
};
