/*
 * xxxMissionController.h
 */

#pragma once

#include "SecondaryMissionController.h"
#include "xxxRecord.h"

int counter=0;

/** @brief The SecondaryMissionController of the hypothetical xxx project.
 *  This dummy class demonstrate the architecture of a Cansat project
 *  in conjunction with xxxMainWithRT-CanCommander sketch.
 *  @param record The data record populated with primary mission data,
 *                to populate with secondary mission data. */
#ifndef INCLUDE_FLIGHT_ANALYSIS_DATA
#error "xxxMain.ino is expected to be used with INCLUDE_FLIGHT_ANALYSIS_DATA defined in CansatConfig.h"
#endif
class xxxMissionController : public CansatFlightAnalyser<xxxRecord> {
  protected:
    virtual void manageProjectSpecificMission(xxxRecord& record) override {
         Serial << millis() << ": xxxMissionController::manageSecondaryMission() called with CansatRecord" << ENDL;
         // cast the record to xxxRecord and work with it: something like doSomething((xxxRecord&) record);
         // NB: Proper dynamic casting is not available with Arduino (RTTI is disabled to save memory,
         // so you cannot write xxxRecord& myXxxRecord = dynamic_cast<xxxRecord&>(record);
         xxxRecord& myXxxRecord = (xxxRecord&) record;
         // Use myXxxRecord here
         myXxxRecord.myXxxSpecificData =5;
         Serial << "record.myXxxSpecificData=" << myXxxRecord.myXxxSpecificData << ENDL;
    } ;
};
