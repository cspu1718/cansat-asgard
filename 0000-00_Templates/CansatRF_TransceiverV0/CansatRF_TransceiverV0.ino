/**
 *  This is a generic RF-Transceiver station for managing plain CansatRecord,
 *  sent by a can which only implements the Primary mission. It can be
 *  used together with sketch CansatMain loaded in the can.

    Implementation is provided by base class CansatRF_Transceiver,

    For actual projects, define a specific subclasses of CansatRecord to suit
    your needs and adapt the template parameters below.
    See example in xxxRF_Transceiver.
*/

// Disable warnings caused during the Arduino includes.
#define RF_TSCV_VERSION 0
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatRF_Transceiver.h"
#include "CansatRecord.h"

class Transceiver : public CansatRF_Transceiver<CansatRecord> {
  virtual const char* getProjectName() override {
    return "Cansat";
  };
};

Transceiver theTransceiver;

void setup() {
  if (!theTransceiver.begin()) {
    // Every required diagnostic output is provided by the transceiver.
    exit(1);
  }
}

void loop() {
  theTransceiver.run();
}
