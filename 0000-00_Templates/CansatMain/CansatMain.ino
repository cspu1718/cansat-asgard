/**
    This is a generic main for the can, using the CanSetupAndLoop template.

    This main managed plain CansatRecords, and primary mission only (including
    extensions added by CSPU in any project.   This can should be used together
    with the CansatRF_Transceiver.

    For actual projects, define specific
    specialisation and subclasses of CansatAcquisitionProcess<> and RT_CanCommander<> to suit your needs
    and adapt the template parameters below.
    See example in xxxMain.

    More info in the documentation of class CanSetupAndMain.
*/

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CanSetupAndLoop.h"

CanSetupAndLoop<> can;

void setup() {
  can.setup();
}

void loop() {
  can.loop();
}
