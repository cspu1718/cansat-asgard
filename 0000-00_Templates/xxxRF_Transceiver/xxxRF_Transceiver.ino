/** Example program for RF-Transceiver station for project xxx.
    Implementation is provided by base class CansatRF_Transceiver,
    and a project-specific subclass of CansatRecord.

    In this template, project is "xxx" and data is stored in "xxxRecord" instances.
*/

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatRF_Transceiver.h"
#include "xxxRecord.h"

class xxxTransceiver : public CansatRF_Transceiver<xxxRecord> {
	virtual const char* getProjectName() override {
		return "xxx";
	};
};

xxxTransceiver theTransceiver;

void setup() {
  if (!theTransceiver.begin()) {
	  // Every required diagnostic output is provided by the transceiver.
	  exit(1);
  }
}

void loop() {
  theTransceiver.run();
}
