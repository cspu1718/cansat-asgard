/**  \mainpage CSPU Cansat software documentation
 *
 *
 * This is the documentation of the software written by successive Cansat projects in Collège Saint-Pierre, Uccle.
 * 
 * It is extracted from the source code by the Doxygen® tool. 
 *
 * \section srcOrg Organisation of source code
 *
 * Our on-board source code is written in *C++* and abides by a strict organisation of folders
 * and files which is described in
 * document <em>Arduino Development Organisation</em> located in folder 2400 (Can Software) of our 
 * project folder. Be sure to read if before contributing. 
 *
 * - Any re-usable code is placed in Arduino libraries, which can be found under the "Modules" entry,
 *   and extensively documented using Doxygen block comments.
 * - Main programs are usually limited to a bare skeleton, and not documented through Doxygen.
 *
 * Some utility programs are written in *Python*, and documented using standard python DocStrings (complemented
 * with a some Doxygen mark-up) which are also collected by
 * Doxygen. The documentation is available under the "Namespaces", "classes" and "file" entries.
 *
 * \section info More info about code documentation
 *
 * See <em>Doxygen and other documentation tools: installation & use</em> located in folder 7300.
 *
 * \section step1 Using the on-board software
 *
 * Before contributing or using the on-board software, a number of products must be installed and
 * configured on your computer (ArduinoIDE, SourceTree, Doxygen...). Please refer to document 
 * <em>Getting Started with Cansat</em> in the root folder of Cansat0000_CSPU.
 */
