#include "BlinkingLED.h"

bool BlinkingLED::begin(uint8_t pinNbr, uint16_t periodInMsec) {
  this->pinNbr = pinNbr;
  this->periodInMsec = periodInMsec;
  this->lastChangeTimestamp = 0;
  pinMode(pinNbr, OUTPUT);
}
void BlinkingLED::run() {
  uint32_t now = millis();
  uint32_t elapsed = now - this->lastChangeTimestamp;

  if (elapsed > this->periodInMsec) {
    bool state = digitalRead(this->pinNbr);
    digitalWrite(this->pinNbr, !state);
    this->lastChangeTimestamp = now;
  }
}
