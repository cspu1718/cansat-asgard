#pragma once
#include "Arduino.h"

class BlinkingLED {


  public:
    bool begin(uint8_t pinNbr, uint16_t periodInMsec);
    void run();

  private:
    uint8_t pinNbr;
    uint16_t periodInMsec;
    uint32_t lastChangeTimestamp;
};
