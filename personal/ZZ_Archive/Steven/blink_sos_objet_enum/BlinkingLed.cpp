#include "BlinkingLed.h" //Include the headers that contain the objects

//Definition of the constructor and the method
BlinkingLed::BlinkingLed(byte thePinNumber, uint8_t theDuration ){
  pinNumber = thePinNumber;
  duration = theDuration;
  pinMode(pinNumber, OUTPUT);
}

void BlinkingLed::run(){    //this method is used to blink the led
  if(millis() - ts >= duration){
        digitalWrite(pinNumber, !digitalRead(pinNumber));
        ts = millis();
  }
}
