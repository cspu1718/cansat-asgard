#define DEBUG
#include "CSPU_Debug.h"

void setup() {
  while (!Serial) ;
  Serial << "Init OK, A0=" << A0 << ENDL;
  pinMode(A0, INPUT_PULLUP);   
  pinMode(A1, INPUT_PULLUP); 
  pinMode(A2, INPUT_PULLUP); 
  pinMode(LED_BUILTIN, OUTPUT);
  //pinMode(INPUT_PULLUP, 11); 
  // put your setup code here, to run once:

}

void loop() {
  Serial << "A0-1-2 (" << A0 << "-" << A1 << "-" << A2 << ")=" << digitalRead(A0) <<digitalRead(A1) <<digitalRead(A2) << ENDL;
  //Serial << "11=" << digitalRead(11) << ENDL;
  delay(200);
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
}
