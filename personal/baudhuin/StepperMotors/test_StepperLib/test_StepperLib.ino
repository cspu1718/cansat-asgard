// Test using the Stepper library
// Original source: https://docs.arduino.cc/learn/electronics/stepper-motors

#include <Stepper.h>
#include "CSPU_Debug.h"

const int stepsPerRevolution = 20;  // change this to fit the number of steps per revolution
// for your motor

// initialize the stepper library on pins 8 through 11:
Stepper myStepper(stepsPerRevolution, 8, 9, 10, 11);

int stepCount = 0;         // number of steps the motor has taken

void setup() {
  // initialize the serial port:
  DINIT(115200);
}

void loop() {
  // step one step:
  myStepper.step(1);
  Serial.print("steps:");
  Serial.println(stepCount);
  stepCount++;
  delay(500);
}
