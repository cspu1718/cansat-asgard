/*
   Simple demo, should work with any driver board

   Adapted for CSPU from BaseStepperDrive example in library StepperDriver.

   Connect STEP, DIR as indicated

   Copyright (C)2015-2017 Laurentiu Badea

   This file may be redistributed under the terms of the MIT license.
   A copy of this license has been included with this distribution in the file LICENSE.
*/
#include <Arduino.h>
#include "BasicStepperDriver.h"
#include "DebugCSPU.h"
#include "elapsedMillis.h"

// Motor steps per revolution. Most steppers are 200 steps or 1.8 degrees/step
#define MOTOR_STEPS 20
#define RPM 120

// Since microstepping is set externally, make sure this matches the selected mode
// If it doesn't, the motor will move at a different RPM than chosen
// 1=full step, 2=half step etc.
#define MICROSTEPS 4

// All the wires needed for full functionality
#define DIR 12
#define STEP 13
// define/undefine SLEEP to use enable/disable functionality
#define SLEEP 11

#ifdef SLEEP
BasicStepperDriver stepper(MOTOR_STEPS, DIR, STEP, SLEEP);
#else
// 2-wire basic config, microstepping is hardwired on the driver
BasicStepperDriver stepper(MOTOR_STEPS, DIR, STEP);
#endif

void setup() {
  DINIT(115200)
  Serial << "Initializing stepper library..." << ENDL;
  stepper.begin(RPM, MICROSTEPS);
  // if using enable/disable on ENABLE pin (active LOW) instead of SLEEP uncomment next line
  // stepper.setEnableActiveState(LOW);
  Serial << "  Done." << ENDL;
  Serial << "    Configuration: motor steps: " << MOTOR_STEPS << ", microsteps: " << MICROSTEPS << ENDL;
#ifdef SLEEP
  Serial << "                   pins: DIR=" << DIR << ", STEP=" << STEP << ", SLEEP=" << SLEEP << ENDL;
#else
  Serial << "                   pins: DIR=" << DIR << ", STEP=" << STEP << ", SLEEP not used" << ENDL;
#endif
  delay(1000);
}

void testUpAndDown() {
  Serial << "*** Starting 'full rotation' test" << ENDL;
  elapsedMillis timer;
  long elapsed;
  // energize coils - the motor will hold position
  //stepper.enable();

  for (int i = 1; i <= 5; i++) {

    Serial << "Moving motor " << i << " full revolution using the degree notation..." << ENDL;
    stepper.enable();
    timer = 0;
    stepper.rotate(360 * i);
    elapsed = timer;
    Serial << "   duration: " << elapsed << " msec." << ENDL;
    stepper.disable();



    delay(1000);
    Serial << "Moving motor to original position using " << -i*MOTOR_STEPS*MICROSTEPS << " steps.." << ENDL;
    stepper.enable();
    timer = 0;
    stepper.move(-i * MOTOR_STEPS * MICROSTEPS);
    elapsed = timer;
    Serial << "   duration: " << elapsed << " msec." << ENDL;
    stepper.disable();


    // pause and allow the motor to be moved by hand
    // stepper.disable();

    Serial << "pause 5 sec..." << ENDL;
    delay(5000);
  }
}

void testIncreasingRotation() {
  int turn = MOTOR_STEPS * MICROSTEPS;
  int steps = turn / 2;
  elapsedMillis timer;
  Serial << "*** Starting 'Increasing rotation test'" << ENDL;
  while (steps <= 3 * turn) {
    Serial << "Turning " << steps << " in positive direction..." << ENDL;
    timer = 0;
    stepper.move(steps);
    long numMillis = timer;
    Serial << "   (" <<  numMillis << ")" << ENDL;
    delay(1000);
    Serial << "Turning " << steps << " in negative direction..." << ENDL;
    timer = 0;
    stepper.move(-steps);
    numMillis = timer;
    Serial << "   (" <<  numMillis << ")" << ENDL;
    delay(1000);

    Serial << "Pausing 3 secs..." << ENDL;
    delay(3000);
    steps += turn / 2;

  }

}

void loop() {
  testUpAndDown();
  //testIncreasingRotation();
}
