/*
   test program for the use of I2C LED Display CN0296D

  Status:
  - Apparently I2C address is 0x3F, not 0x27 (cf. HardwareScanner)
  - Test with Uno and 5V: (SDA=A4, SCL=A5) idem. I2C slave properly detected.
       Solved with: 1) fix bad contact between controller & display
                    2) Adjest potentiometer!
  - Test with Feather (and VCC = 3.3V):
        works, with another potentiometer setting
        but readibility is insufficient.
        Using the Feather to drive it, but powering the display with 5V from Uno board
        (and connecting grounds) is OK.
        Powering the display from the USB bin is OK.
    Conclusion: We need a real 5V power supply for the backlight.

   Effect of commands:
     print: write from current cursor position. If exceed the current line, wrap to the next.
        if autoScroll, text is right-aligned, and I DO NOT UNDERSTAND.
     backlight/noBacklight: Nothing can be read without backlight
     display/noDisplay
     autoscroll/NoAutoscroll:
        in auto scroll mode:
            - the position of the cursor is not used.
            - ISSUES: cf. testAutoscrollMode().
        out of auto scroll mode: 
            - prints from current cursor position, from left to right.
            - characters not fitting on line wraps to another (logic is strange):
              0 to 2, 1 to 3, 2 to 1, 3 to 0
            WRAPPING MUST BE AVOIDED. 
         
     blink/noBlink: blink the character at current cursor position.
     cursor/noCursor: show/hide the cursor (line below character).


   Full tutorial: https://wiki.dfrobot.com/I2C_TWI_LCD1602_Module__Gadgeteer_Compatible___SKU__DFR0063_#target_4

   Notes:
    - LiquidCrystal_I2C library must be installed (done in CansatAsgard).
    - There is an Adafruit LiquidCrystal, which is a fork with compatibilty with additional
      boards.
*/
#include <Wire.h>
#include "LiquidCrystal_I2C.h"
#include "DebugCSPU.h"

LiquidCrystal_I2C lcd(0x3F, 20, 4); // set the LCD address to 0x20 for a 16 chars and 2 line display

void setup()
{
  DINIT(115200)
  Serial << "Initializing lcd...";
  lcd.init();                      // initialize the lcd
  Serial << "OK" << ENDL;


  // Print a message to the LCD.
  lcd.backlight();
  lcd.noAutoscroll();
  lcd.print("Hello, world!");

  // Move to the second row and print the string
  lcd.setCursor(0, 1);
  lcd.print(" IIC/I2C LCD2004 ");
  // Move to the third row and print the string
  lcd.setCursor(0, 2);
  lcd.print(" 20 cols, 4 rows ");
  // Move to the fourth row and print the string
  lcd.setCursor(0, 3);
  lcd.print(" www.sunfounder.com ");
  Serial << "Init over" << ENDL;
}

void blinking() {
  // This has no effect without backlight on.
  Serial << "    Blinking the character at current cursor position" << ENDL;
  lcd.cursor();
  lcd.blink();
  delay(2000);
  Serial << "Hiding cursor..." << ENDL;
  lcd.noCursor();
  delay(2000);
  lcd.noBlink();
  Serial << "    Not blinking" << ENDL;
  delay(1000);
}

void displayOnOff() {
  Serial << "  Display on" << ENDL;
  lcd.display();
  delay(1000);
  blinking();
  Serial << "  Display off" << ENDL;
  lcd.noDisplay();
  delay(1000);
}

void testNoAutoscrollMode() {
  lcd.backlight();
  lcd.noAutoscroll();
  lcd.display();
  delay(1000);
  lcd.clear();
  delay(1000);
  lcd.home();
  lcd.print("Hello, world!");
  // Move to the second row and print the string
  delay(1000);
  lcd.setCursor(0, 1);
  delay(1000);
  lcd.print("Second line");
  delay(1000);
  // Move to the third row and print the string
  lcd.setCursor(0, 2);
  delay(1000);
  lcd.print("third line");
  delay(1000);
  // Move to the fourth row and print the string
  lcd.setCursor(0, 3);
  delay(1000);
  lcd  <<   "Fourth line234567890";
  delay(5000);

  // Testing line wrap
  lcd.clear();
  lcd.print("Hello, world!12345678901234567890");
  // Move to the second row and print the string
  lcd.setCursor(0, 1);
  delay(2000);
  lcd.print("Second line12345678901234567890");
  delay(2000);
  // Move to the third row and print the string
  lcd.setCursor(0, 2);
  lcd.print("third line12345678901234567890");
  delay(2000);
  // Move to the fourth row and print the string
  lcd.setCursor(0, 3);
  lcd  <<   "Fourth line2345678901234567890";

  /* delay(2000);
    Serial << "Backlight off" << ENDL;
    lcd.noBacklight();
    displayOnOff(); */
}

void testAutoscrollMode() {
  lcd.clear();
  lcd.home();
  lcd.setCursor(10,4);
  lcd.blink();
  lcd.autoscroll();
  // Etrange: 1) le charactere qui clignote est en 0,0, tjs ?
  //          2) les caractères apparaissent à la droite de la ligne du cursor,
  //             indépendamment de la colonne fournie.
  //          3) les lignes sont croisées?? 3-1-2-0 et le texte est interrompu?? 
  for (int j = 0; j < 3 ; j++ ) {
    for (char c = 'a' ; c <= 'z'; c++) {
      lcd << c;
      delay(500);
    }
    lcd << "-";
    delay(1000);
  }
  /*
    for (int i = 1; i < 20; i++) {
    lcd << "Autoscroll" << i << ' ';
    Serial << i << ENDL;
    delay(2000);
    }
  */
  delay(4000);
  exit(0);
  lcd.clear();
  lcd.home();
  lcd << "Autoscrol + setCursor(0,1)";
  lcd.setCursor(0, 1);
  lcd << "AAA";
  delay(2000);
}

void displayCounter() {
  static int n=0;
  static int p=0;
  lcd.noAutoscroll();
  lcd.noCursor();
  lcd.noBlink();
  lcd.setCursor(15, 0);
  if (n < 10) lcd << '0';
  lcd << n++ << ':';
  if (p < 10) lcd << '0';
  lcd << p;
  p+=2;
  n = n %100;
  p = p%100; 
  
}
void loop()
{
  //testNoAutoscrollMode();
  //delay(3000);
  //testAutoscrollMode();
  //delay(3000);
  displayCounter();
  delay(500);
}
