/*
    test_SAMD_TimerLibrary
    See: https://avdweb.nl/arduino/timing/samd21-timer

    **** THIS DOES NOT WORK! The initialisation of the SAMDtimer blocks and the loop is never executed....
    **** WHY IS THAT ???

*/

#ifndef __SAMD21G18A__
#error "This sketch is only for SAMD boards"
#endif

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "avdweb_SAMDtimer.h"

uint32_t sampleRate = 1; //sample rate of the sine wave in Hertz, how many times per second the TC5_Handler() function gets called per second basically
#define DEBUG_CSPU
#include "DebugCSPU.h"

uint32_t periodInMicroSec = 100000; // expected delay between two successive calls of the interrupt handler.

#define LED_PIN 13 //just for an example
bool state = 0; //just for an example
uint32_t counter = 0;
unsigned long refMillis;

void myISR (struct tc_module *const module_inst)
{
  counter++;
 /* if (state == true) {
    digitalWrite(LED_PIN, HIGH);
  } else {
    digitalWrite(LED_PIN, LOW);
  }
  state = !state;
  */
}

void setup() {
  DINIT(115200);
  pinMode(LED_PIN, OUTPUT); //this configures the LED pin, you can remove this it's just example code
  digitalWrite(LED_PIN, LOW);
  delay(300);
  digitalWrite(LED_PIN, HIGH);
  delay(500);
  digitalWrite(LED_PIN, LOW);
  delay(300);

  SAMDtimer myTimer=SAMDtimer(4, myISR, periodInMicroSec);
  // 4 is the number of the timer (choose any free one).
  // periodInMicroSec: from 2 us to 1398080us
  // myISR is the inyterrupt handler.
}

void loop() {
  Serial << millis() << ": counter=" << counter << " freq=" 
         << ((float) counter) / (millis() - refMillis) * 1000.0 << " Hz" << ENDL;
  delay(1000);
}
