/*  Test program compatiblity of the DotStar class to control the RBG LED
    ItsyBitsy M4 (Adafruit_DotStar library has issues..
        
*/


#define CSPU_DEBUG
#include "CansatConfig.h"
#include "DebugCSPU.h"
#include "CSPU_Test.h"
#include "Serial2.h"
#include "GPS_Client.h"
#include "DotStar.h"

void setup() {
  DINIT(115200);
  Serial2.begin(115200);

  Serial << "Initializing GPS_Client" ;
  Serial << " on Serial2" << ENDL;
  GPS_Client gps(Serial2);
  gps.begin();  
  
  // initialize here
  CSPU_Test::pressAnyKey();

  Serial << "Turning off RGB LED..." << ENDL;
  DotStar::shutdownOnBoard();
  Serial << "Dotstar now shut down" << ENDL;

  CSPU_Test::pressAnyKey();
  Serial << "End of job " << ENDL;
  exit(0);
}

void loop() {
  // put your main code here, to run repeatedly:

}
