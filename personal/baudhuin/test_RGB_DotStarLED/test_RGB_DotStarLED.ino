/*  Test program to demonstrate the configuration of the RGB_DotStar LED on the
    ItsyBitsy M4.
 *    * Is there an incompatibility with CansatAcquisitionProcess: YES! Just declaring an
        object causes the accès to the DotStar to fail. Because of GPS_Client
 *    * Is there an incompatibility with GPS_Client? YES! Just declaring an
        object causes the accès to the DotStar to fail. Happens with Serial1 and Serial2,
        so not because of a Sercom incompatibility.
 *    * Is there an incompatibility with Adafruit_GPS? No.
 *    * Is there an incompatibility with SAMD_InterruptTimer used by the GPS client: YES.
        Just declaring an object causes the access to the DotStar to fail.
        WHERE IS THE ROOT CAUSE??? Not clear.... The Adafruit_DotStar lib communicates using
        and Adafruit_SPI_Device (a software one) which apparently uses the same timer as
        the SAMD_InterruptTimer.

        Other test shows that timer and RGB_DotStar are not incompatible. 

*/
#ifndef ARDUINO_ITSYBITSY_M4
#error "Test currently only supports ItsyBitsy M4"
#endif

#undef  TEST_WITH_CANSAT_ACQ_PROCESS
#undef TEST_WITH_GPS_CLIENT_ON_SERIAL1
#undef  TEST_WITH_GPS_CLIENT_ON_SERIAL2
#undef  TEST_WITH_ADAFRUIT_GPS
#undef TEST_WITH_SAMD_TIMER // ne fonctionne pas
#undef TEST_WITH_SAMD_TIMER2 // ne fonctionne pas
#define TEST_WITH_SAMD_TIMER_INTERRUPT //  fonctionne
#undef TEST_WITH_SAMD_ISR_TIMER // fonctionne

#define CSPU_DEBUG
#include "CansatConfig.h"
#include "DebugCSPU.h"
#include "Adafruit_DotStar.h"
#include "CSPU_Test.h"
#include "CansatHW_Scanner.h"
#include "CansatAcquisitionProcess.h"
#include "Adafruit_GPS.h"
#include "Serial2.h"

#if (defined(TEST_WITH_SAMD_TIMER) || defined(TEST_WITH_SAMD_TIMER2))
#  include "SAMD_InterruptTimer.h"
#endif
#ifdef TEST_WITH_SAMD_TIMER_INTERRUPT
#  include "SAMDTimerInterrupt.h"
#  include "SAMD_ISR_Timer.h"
#endif
#ifdef TEST_WITH_ISR_TIMER
#  include "SAMD_ISR_Timer.h"
#endif


#ifdef TEST_WITH_CANSAT_ACQ_PROCESS
CansatAcquisitionProcess acqProcess;
#endif

#ifdef TEST_WITH_SAMD_TIMER_INTERRUPT
  SAMD_ISR_Timer theISR_Timer;
  void myHandler() { theISR_Timer.run(); }

  void coucou() { Serial << "Coucou at " << millis()  << ENDL;}
#endif

void setup() {
  DINIT(115200);
  Serial2.begin(115200);
  Serial2 << "Test" << ENDL;
  if (0) {
    Serial << "Turning off RGB LED..." << ENDL;
    Adafruit_DotStar theDotStar(1, /*DATAPIN*/ 8, /*CLOCKPIN*/ 6, DOTSTAR_RGB);
    theDotStar.begin();
    theDotStar.show(); // Shutdown
    Serial << "DotStar now shut down" << ENDL;
    CSPU_Test::pressAnyKey();
    uint32_t magenta = theDotStar.Color(255, 0, 255);
    theDotStar.setPixelColor(0, magenta);
    theDotStar.show();
    Serial << "DotStar now magenta" << ENDL;
    CSPU_Test::pressAnyKey();
  }

  if (0) {
    Serial << "Initializing CansatHW_Scanner" << ENDL;
    CansatHW_Scanner hw;
    hw.init();
    Serial << "Turning off RGB LED..." << ENDL;
    Adafruit_DotStar theDotStar(1, /*DATAPIN*/ 8, /*CLOCKPIN*/ 6);
    theDotStar.begin();
    theDotStar.show(); // Shutdown
    Serial << "Dotstar now shut down" << ENDL;
    CSPU_Test::pressAnyKey();
  }

#ifdef TEST_WITH_CANSAT_ACQ_PROCESS
  {
    Serial << "Initializing CansatAcquisitionProcess" << ENDL;
    acqProcess.init();
    Serial << "Turning off RGB LED..." << ENDL;
    Adafruit_DotStar theDotStar(1, /*DATAPIN*/ 8, /*CLOCKPIN*/ 6);
    theDotStar.begin();
    theDotStar.show(); // Shutdown
    Serial << "Dotstar now shut down" << ENDL;
    CSPU_Test::pressAnyKey();
  }
#endif

#if (defined(TEST_WITH_GPS_CLIENT_ON_SERIAL1) || defined(TEST_WITH_GPS_CLIENT_ON_SERIAL2))
  {
    Serial << "Initializing GPS_Client" ;
#ifdef TEST_WITH_GPS_CLIENT_ON_SERIAL1
    Serial << " on Serial1" << ENDL;
    GPS_Client gps(Serial1);
#else
    Serial << " on Serial2" << ENDL;
    GPS_Client gps(Serial2);
#endif
    Serial << "Turning off RGB LED..." << ENDL;
    Adafruit_DotStar theDotStar(1, /*DATAPIN*/ 8, /*CLOCKPIN*/ 6);
    theDotStar.begin();
    theDotStar.show(); // Shutdown
    Serial << "Dotstar now shut down" << ENDL;
    CSPU_Test::pressAnyKey();
  }
#endif

#ifdef TEST_WITH_ADAFRUIT_GPS
  {
    Serial << "Initializing Adafruit_GPS" << ENDL;
    Adafruit_GPS gps(&Serial2);
    //gps.begin();
    Serial << "Turning off RGB LED..." << ENDL;
    Adafruit_DotStar theDotStar(1, /*DATAPIN*/ 8, /*CLOCKPIN*/ 6);
    Serial << "a" << ENDL;
    theDotStar.begin();
    Serial << "b" << ENDL;
    theDotStar.show(); // Shutdown
    Serial << "Dotstar now shut down" << ENDL;
    CSPU_Test::pressAnyKey();
  }
#endif

#ifdef TEST_WITH_SAMD_TIMER
  {
    Serial << "Creating SAMD timer" << ENDL;
    SAMD_InterruptTimer gpsTimer;
    Serial << "Turning off RGB LED..." << ENDL;
    Adafruit_DotStar theDotStar(1, /*DATAPIN*/ 8, /*CLOCKPIN*/ 6);
    Serial << "a" << ENDL;
    theDotStar.begin();
    Serial << "b" << ENDL;
    theDotStar.show(); // Shutdown
    Serial << "Dotstar now shut down" << ENDL;
    CSPU_Test::pressAnyKey();
  }
#endif

#ifdef TEST_WITH_SAMD_TIMER2
  {
    Serial << "Creating SAMD timer" << ENDL;
    SAMD_InterruptTimer gpsTimer;
    Serial << "Turning off RGB LED in own block..." << ENDL;
    {
      Adafruit_DotStar theDotStar(1, /*DATAPIN*/ 8, /*CLOCKPIN*/ 6);
      Serial << "a" << ENDL;
      theDotStar.begin();
      Serial << "b" << ENDL;
      theDotStar.show(); // Shutdown
      Serial << "Dotstar now shut down" << ENDL;
    }
    CSPU_Test::pressAnyKey();
  }
#endif

#ifdef TEST_WITH_SAMD_TIMER_INTERRUPT

  {
    Serial << "Included SAMD_TimerInterrupt (OK) & global ISR_Timer on TC3 (ok)" << ENDL;
    SAMDTimer myTimer(TIMER_TC3); // Only TC3 is possible on both SAMD21 and SAMD51

    bool ok = myTimer.attachInterruptInterval(1000UL, myHandler);
    if (ok) {
      Serial << "HW Timer set" << ENDL;
    } else {
      Serial << "Cannot set HW Timer" << ENDL;
    }
    constexpr unsigned long TimerPeriod=5000000UL; // NE DONNE PAS 5 SEC ????
    auto result = myTimer.setInterval(TimerPeriod, coucou);
    if (result == -1) {
      Serial << "Error setting interval & callback" << ENDL;
    } else {
      Serial << "Timer started (ID=" << result << ", period=" << TimerPeriod << " msec)" << ENDL;
    }
    delay(10000);
    Serial << "Turning off RGB LED..." << ENDL;
    Adafruit_DotStar theDotStar(1, /*DATAPIN*/ 8, /*CLOCKPIN*/ 6);
    Serial << "a" << ENDL;
    theDotStar.begin();
    Serial << "b" << ENDL;
    theDotStar.show(); // Shutdown
    Serial << "Dotstar now shut down" << ENDL;
    CSPU_Test::pressAnyKey();
  }
#endif //TEST_WITH_SAMD_TIMER_INTERRUPT

#ifdef TEST_WITH_SAMD_ISR_TIMER
  {
    Serial << "Included SAMD_ISR_Timer.h" << ENDL;
    Serial << "Turning off RGB LED..." << ENDL;
    Adafruit_DotStar theDotStar(1, /*DATAPIN*/ 8, /*CLOCKPIN*/ 6);
    Serial << "a" << ENDL;
    theDotStar.begin();
    Serial << "b" << ENDL;
    theDotStar.show(); // Shutdown
    Serial << "Dotstar now shut down" << ENDL;
    CSPU_Test::pressAnyKey();
  }
#endif

  Serial << "End of job " << ENDL;
  exit(0);
}

void loop() {
  // put your main code here, to run repeatedly:

}
