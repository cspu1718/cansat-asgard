/*
    Test a square wave generatede with standard PWM pin
    capability to drive a Piezo buzzer.

    Conclusion: 
      - not loud enough
      - value has is not relevant, some values to be avoided (10, 170, 210, 250, 255): 
        they are even softer).
*/
#define DEBUG
#include "CSPU_Debug.h"

constexpr unsigned long ToneDuration = 1500; // msec
constexpr uint8_t TonePin = 11;

uint8_t pwmValue = 0;
void setup() {
  DINIT(115200);
  pinMode(TonePin, OUTPUT);
  pwmValue = 10;
}

void loop() {
  Serial << "Starting PWM (value =" << pwmValue << ") for " << ToneDuration / 1000.0 << " s..." << ENDL;
  analogWrite(TonePin, pwmValue);
  delay(ToneDuration);
  analogWrite(TonePin, 0);
  switch (pwmValue) {
    case 250:
      pwmValue = 255;
      break;
    case 255:
      pwmValue = 10;
      break;
    default:
      pwmValue += 10;
  }
  delay(ToneDuration);
}
