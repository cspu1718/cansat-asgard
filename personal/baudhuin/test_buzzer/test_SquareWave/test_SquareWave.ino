/* 
 *  Test a square wave generatede with tone function to drive a Piezo buzzer.
 *  
 *  Conclusion: works, but not loud enough with small piezzo buzzer. Optimal frequency = 2000 Hz. 
 */
#define DEBUG
#include "CSPU_Debug.h"

int toneFrequency; // Hz
constexpr unsigned long ToneDuration=1000; // msec
constexpr uint8_t TonePin=11;

void setup() {
  DINIT(115200);
  pinMode(TonePin, OUTPUT);
  toneFrequency = 1500;
}

void loop() {
  Serial << "Starting tone (" << toneFrequency << " Hz) for " << ToneDuration/1000.0 << " s..." << ENDL;
  tone(TonePin, toneFrequency, ToneDuration); 
  toneFrequency = toneFrequency * 1.1;
  if (toneFrequency > 5000) {
    toneFrequency = 1500;
  }
  delay(2*ToneDuration);
}
