/*
 * Test portability of SAMD_InterruptTimer library for SAMD21 and SAMD51
 */

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

// These define's must be placed at the beginning before #include "SAMDTimerInterrupt.h"
// _TIMERINTERRUPT_LOGLEVEL_ from 0 to 4
// Don't define _TIMERINTERRUPT_LOGLEVEL_ > 0. Only for special ISR debugging only. Can hang the system.
// Don't define TIMER_INTERRUPT_DEBUG > 2. Only for special ISR debugging only. Can hang the system.
#define TIMER_INTERRUPT_DEBUG         2
#define _TIMERINTERRUPT_LOGLEVEL_     0
#include "SAMDTimerInterrupt.h"
#include "SAMD_ISR_Timer.h"

#define DEBUG_CSPU
#include "DebugCSPU.h"

// Init SAMD timer TIMER_TC3
// Depending on the board, you can select SAMD21 Hardware Timer from TC3-TCC
// SAMD21 Hardware Timer from TC3 or TCC
// SAMD51 Hardware Timer only TC3
SAMDTimer ITimer(TIMER_TC3);
// Init SAMD_ISR_Timer
// Each SAMD_ISR_Timer can service 16 different ISR-based timers
SAMD_ISR_Timer ISR_Timer;
uint32_t startTS;

void TimerHandler()
{
  static bool toggle  = false;
  static bool started = false;
  static int timeRun  = 0;

  ISR_Timer.run();  // could be empty except for this.

  // Toggle LED every 2000 call to the handler, ie. 2000ms = 2s
  if (++timeRun == (2000) )
  {
    timeRun = 0;

    if (!started)
    {
      started = true;
      pinMode(LED_BUILTIN, OUTPUT);
    }

    //timer interrupt toggles pin LED_BUILTIN
    digitalWrite(LED_BUILTIN, toggle);
    toggle = !toggle;
  }
}

const uint32_t TargetPeriod1 = 570;
const uint32_t TargetPeriod2 = 1213;

volatile unsigned long counter1 = 0;
volatile unsigned long counter2 = 0;

void IRQ_CallBack1() { 
  static uint32_t lastTS;
  uint32_t now=millis();
  uint32_t delta= now-lastTS;
  Serial <<now << ": ";
  if (delta != TargetPeriod1) {
    Serial << "Error: delta=" << delta << " ";
  }
  Serial << TargetPeriod1  << ENDL;
  counter1++;
  lastTS=now;
}

void IRQ_CallBack2() { 
  static uint32_t lastTS;
  uint32_t now=millis();
  uint32_t delta= now-lastTS;
  Serial <<now << ": ";
  if (delta != TargetPeriod2) {
    Serial << "Error: delta=" << delta << " ";
  }
  Serial << TargetPeriod2  << ENDL;
  counter2++;
  lastTS=now;
}

void setup() {
   Serial.begin(115200);
  while (!Serial);

  delay(100);

  Serial.print(F("\nStarting ISR_16_Timers_Array on ")); Serial.println(BOARD_NAME);
  Serial.println(SAMD_TIMER_INTERRUPT_VERSION);
  Serial.print(F("CPU Frequency = ")); Serial.print(F_CPU / 1000000); Serial.println(F(" MHz"));

  // Interval= 1 msec, in microsecs
  if (ITimer.attachInterruptInterval(1000, TimerHandler))
  {
    Serial.print(F("Starting ITimer OK, millis() = ")); Serial.println(millis());
  }
  else
    Serial.println(F("Can't set ITimer. Select another freq. or timer"));

  // Just to demonstrate, don't use too many ISR Timers if not absolutely necessary
  // You can use up to 16 timer for each SAMD_ISR_Timer
  startTS=millis();
  ISR_Timer.setInterval(TargetPeriod1, IRQ_CallBack1); 
  ISR_Timer.setInterval(TargetPeriod2, IRQ_CallBack2); 
  Serial << "Configuration over at " << startTS << " msec." << ENDL; 
  
}

void loop() {
  delay(1000);
}
