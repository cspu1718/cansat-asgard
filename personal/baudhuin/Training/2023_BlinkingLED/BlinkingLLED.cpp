
#define CSPU_DEBUG true
#include "CSPU_Debug.h"
#include "BlinkingLED.h"

#define DBG_INIT true
#define DBG_TOGGLE true

bool BlinkingLED::begin(uint8_t pinNbr, uint16_t period) {
  DPRINTSLN(DBG_INIT, "Entering begin method")
  this->pinNbr = pinNbr;
  this->period = period;
  this->lastToggleTS = 0;
  return true;
}

bool BlinkingLED::actionRequired() {
  return millis() - lastToggleTS > period;
}

void BlinkingLED::toggleLED(){
  DPRINTSLN(DBG_TOGGLE, "changing LED state");
  digitalWrite(pinNbr, !digitalRead(pinNbr));
}
  
void BlinkingLED::run() {
  if (actionRequired()) {
    toggleLED();
  }
 
}
