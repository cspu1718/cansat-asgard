
#pragma once

#include "Arduino.h"

class BlinkingLED {

public:
  bool begin(uint8_t pinNbr, uint16_t period);

  void run();

private:
  bool actionRequired();
  void toggleLED();
  uint8_t pinNbr;
  uint16_t period;
  uint32_t lastToggleTS;
  
};
