/*
 * Demo program for Opbject-oriented blinking LED Implementation
 * Usage:
 *    ddfdd
 */
#define CSPU_DEBUG true
#include "CSPU_Debug.h"
#include "BlinkingLED.h"

constexpr uint8_t PinNumberForLED1=13;
constexpr uint16_t PeriodForLED1=1000; // msec

BlinkingLED led1;

void setup() {
  DINIT(115200);
  if (!led1.begin(PinNumberForLED1, PeriodForLED1)) {
    Serial.println("Error in led 1 initialization");
    while(true) delay(1000);
  }
  else {
    Serial.println("Init OK.");
  }
}

void loop() {
  led1.run();
}
