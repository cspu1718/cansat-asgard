/*
  Program to send a SOS (Version 1)
*/

constexpr unsigned int DotDuration=200; // msec
constexpr unsigned int DashDuration=600; // msec
constexpr unsigned int InterSignDelay=200; // msec. 

constexpr byte ledSOS=13; 

enum MorseSign_t {
  dot,
  dash 
};

void setup()
{
  pinMode(ledSOS, OUTPUT);
}

void emitMorseSign(byte ledNbr, MorseSign_t sign, byte numRepetitions ) {
 for (int i = 0 ; i< numRepetitions ; i++) 
 {
   digitalWrite(ledNbr, HIGH);
   if (sign==dot) delay(DotDuration);
   else delay(DashDuration);
   digitalWrite(ledNbr,LOW);
   delay(InterSignDelay);
 }
}

void emitSOS() {
  emitMorseSign(ledSOS, dot, 3);
  emitMorseSign(ledSOS, dash, 3);
  emitMorseSign(ledSOS, dot, 3);
}

void loop()
{
  emitSOS();
  delay(2000); // Wait for 2 second(s)
}
