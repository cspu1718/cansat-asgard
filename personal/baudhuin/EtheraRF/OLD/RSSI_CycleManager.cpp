/*
 * RSSI_CycleManager.cpp
 */

#include "RSSI_CycleManager.h"
#include "DebugCSPU.h"

#define DBG_BEGIN 1
#define DBG_RUN 1
#define DBG_MOVE_TO_NEXT 1

RSSI_CycleManager::RSSI_CycleManager(uint8_t CS_Pin, uint8_t interruptPin) :
	maxNumRetries(1), numRetries(0),
	rf69(CS_Pin, interruptPin), currentListener(0)
{}

bool RSSI_CycleManager::begin(uint8_t numRetries) {
	this->numRetries = numRetries;
	this->currentListener = -1;

	// Initialize the radio module
	pinMode(RFM69_ResetPin, OUTPUT);
	digitalWrite(RFM69_ResetPin, LOW);
	// manual reset
	digitalWrite(RFM69_ResetPin, HIGH);
	delay(10);
	digitalWrite(RFM69_ResetPin, LOW);
	delay(10);

	if (!rf69.init()) {
		DPRINTSLN(DBG_DIAGNOSTIC, "*** RFM69 radio init failed.");
		return false;
	}
	DPRINTSLN(DBG_BEGIN, "RFM69 radio init OK.");

	// Initialize all RSSI listeners
	for (int bID = 0; bID < NumBeacons; bID++) {
		DPRINTS(DBG_BEGIN, "Initializing listener to beacon ");
		DPRINTLN(DBG_BEGIN, bID);
		if (!listeners[bID].begin(	rf69, BeaconFrequency[bID], BeaconUseEncryption[bID],
								BeaconEncryptionKey[bID], RSSI_MeasurementTimeout, bID)) {
			DPRINTS(DBG_DIAGNOSTIC, "** Failure while initializing listener to beacon ");
			DPRINTLN(DBG_DIAGNOSTIC, bID);
			return false;
		}
	}
	for (int canID = 0; canID < NumTrackedCans; canID++) {
		DPRINTS(DBG_BEGIN, "Initializing listener to can ");
		DPRINTLN(DBG_BEGIN, CanName[canID]);
		if (!listeners[NumBeacons+canID].begin(rf69, CanFrequency[canID], CanUseEncryption[canID],
				CanEncryptionKey[canID], RSSI_MeasurementTimeout, -1))
		{
			DPRINTS(DBG_DIAGNOSTIC, "** Failure while initializing listener to can ");
			DPRINTLN(DBG_DIAGNOSTIC, CanName[canID]);
			return false;
		}
	}
	moveToNextListener();
	return true;
}

void RSSI_CycleManager::moveToNextListener() {
	// We'll explore NumBeacons+NumTrackedCans possibilities
	// We'll explore from the currentListener+1 or from 0 if currentListener is -1. In both
	// cases, this is (currentListener + 1) % (NumBeacons+NumTrackedCans);
	// Whatever the outcome (new listener, same listener or no listener, numRetries must be reset.
	DPRINTS(DBG_MOVE_TO_NEXT, "Moving to next from ");
	DPRINTLN(DBG_MOVE_TO_NEXT, currentListener);
	uint8_t counter = 0;
	int8_t candidate=currentListener;
	numRetries=0;
	do {
		counter++;
		candidate = (candidate + 1) % (NumBeacons+NumTrackedCans);
		DPRINTS(DBG_MOVE_TO_NEXT, "  Counter= ");
		DPRINT(DBG_MOVE_TO_NEXT, counter);
		DPRINTS(DBG_MOVE_TO_NEXT, ",candidate= ");
		DPRINT(DBG_MOVE_TO_NEXT, candidate);
		if (listeners[candidate].getStatus() >= RSSI_Listener::ListenerStatus::ReadyForRSSI) {
			// Found an active one.
			DPRINTSLN(DBG_MOVE_TO_NEXT, ". Active. Set as next.");
			currentListener=candidate;
			// Anticipate the RF configuration so the reception can occur immediately
			listeners[candidate].configureRF();
			return;
		}
	} while (counter <= NumBeacons+NumTrackedCans);
    // We didn't find any listener, and exhausted the number of candidates
	DPRINTSLN(DBG_MOVE_TO_NEXT, ". None found. Next=-1");
	currentListener=-1;
}

void RSSI_CycleManager::run(EtheraRecord &record) {
	if (currentListener == -1) {
		// If no active listener exists, check again, in case one was initialized since the
		// previous cycle.
		moveToNextListener();
		if (currentListener == -1) {
			DPRINTSLN(DBG_RUN, "run: no active listener");
			// No really, there is nothing to work with....
			return;
		}
	}

	float rssi, distance;
	DPRINTS(DBG_RUN, "run: currentListener=");
	DPRINT(DBG_RUN, currentListener);
	bool measureOK;
	if (listeners[currentListener].getStatus() == RSSI_Listener::ListenerStatus::ReadyForDistance) {
		DPRINTSLN(DBG_RUN, " Ready for distance");
		measureOK=listeners[currentListener].measureDistance(rssi, distance);
	} else {
		DPRINTSLN(DBG_RUN, " Ready for rssi only");
		measureOK=listeners[currentListener].getRSSI(rssi);
		distance = 0;
	}

	if (measureOK) {
		DPRINTS(DBG_RUN, "run: measure OK. rssi=");
		DPRINT(DBG_RUN,rssi);
		DPRINTS(DBG_RUN, ", distance=");
		DPRINTLN(DBG_RUN, distance);
		// Store results
		record.setFrequency(listeners[currentListener].getFrequency());
		record.setRSSI(rssi);
		record.distance=distance;

		moveToNextListener();
	} else {
		if (numRetries < maxNumRetries) {
			DPRINTSLN(DBG_RUN, "run: measure KO. planning retry.");
			numRetries++;
		} else {
			DPRINTSLN(DBG_RUN, "run: measure KO, retries exhausted.");
			numRetries=0;
			moveToNextListener();
		}
	}


}
