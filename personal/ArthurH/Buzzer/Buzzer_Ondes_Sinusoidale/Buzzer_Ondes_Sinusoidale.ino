int buzzer = 9;
int i = 100;
int k = 1200;
 
void setup() {
  // put your setup code here, to run once:
  pinMode(buzzer,OUTPUT);
}
 
void beep(){
  for(i = 100; i <= 1200; i++){
    digitalWrite(buzzer,HIGH);
    delayMicroseconds(i);
    digitalWrite(buzzer,LOW);
    delayMicroseconds(i);
  }
}
 
void ContreBeep(){
  for(k = 1200; k >= 100; k -= 1){
    digitalWrite(buzzer,HIGH);
    delayMicroseconds(k);
    digitalWrite(buzzer,LOW);
    delayMicroseconds(k);
  }
}
 
void loop() {
  // put your main code here, to run repeatedly:
  beep();
  ContreBeep();
}
 