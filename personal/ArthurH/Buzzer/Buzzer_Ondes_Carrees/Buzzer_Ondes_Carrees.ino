int buzzer = 9;
int i = 0;
 
void setup() {
  // put your setup code here, to run once:
  pinMode(buzzer,OUTPUT);
}
 
void beep(){
  for(i = 0; i <= 1200; i++){
    digitalWrite(buzzer,HIGH);
    delayMicroseconds(200);
    digitalWrite(buzzer,LOW);
    delayMicroseconds(200);
  }
}
 
void loop() {
  // put your main code here, to run repeatedly:
  beep();
  delay(200);
  beep();
  delay(200);
}
 