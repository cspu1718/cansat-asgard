/**
  Sketch simulating RT transmission from subcan. Run this sketch with the
  appropriate XBee module (according to GMiniConfig.h). It will detect which
  subcan it must emulate.

  Test with Feather M0 or ItsyBitsy M4 board.

  The contents of the records are defined in GMiniRecordExample class .h
  (because it is shared by the various sketches.

  Wiring: µC to XBee module.
       Feather/ItsyBitsy
       3.3V to VCC
       RX   to DOUT  CONFIRMED although it is the opposite to connect to XCTU!
       TX   to DIN   CONFIRMED although it is the opposite to connect to XCTU!
       GND  to GND
*/

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#ifndef ARDUINO_ARCH_SAMD
#  error "This program only works on SAMD boards (Feather MO_Express, ItsyBitsy M4 Express, etc.)"
#endif

#include "GMiniConfig.h"
#include "GMiniXBeeClient.h"
#include "GMiniRecordExample.h"
#include "CSPU_Test.h"

constexpr bool RegularlyDisplayChannel = false; // if true, the current channel is displayed
//                                                 every ChannelPrintPeriod ms (debug: disrupts
//                                                 RF link)
constexpr unsigned int ChannelPrintPeriod = 20000; // ms
constexpr unsigned int StatsPrintPeriod = 30000; // ms
constexpr unsigned int MaxLineLength = 75;
constexpr bool DetailedErrorMsgOnEmission = false; // if true, detailed error message when failing
//                                                    to send to ground.  (debug: disrupts
//                                                    RF link)
constexpr uint16_t RF_AckDelay = 0; // ms. 0 = don't wait for ack.

#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

// --------------------- Globals --------------------

CansatCanID MyCanID;             // Detected from XBee module.
HardwareSerial &RF = Serial1;
GMiniXBeeClient xbc(GM_MainCan_XBeeAddressSH, GM_MainCan_XBeeAddressSL); // Address here is irrelevant
GMiniRecordExample myRecord;
char myString[xbc.MaxStringSize];
CansatFrameType stringType;
bool interrupted = false;
char myself[25];
unsigned long numRecordsSentOK = 0; // Count sent OK
unsigned long numRecordsSentKO = 0; // Count sent KO
unsigned long charCounter = 0;  // character counter to split lines.

elapsedMillis elapsed = 1000, elapsedSinceChannelPrint = 0, statsElapsed = 0;
unsigned long numErrors = 0;

// --------------------------------------------------

void  printStats() {
  if (statsElapsed > StatsPrintPeriod) {
    statsElapsed = 0;
    Serial << ENDL << myself << " stats: (acq. period=" << CansatAcquisitionPeriod << " msec)" << ENDL;
    Serial << "  Rec. sent: " << numRecordsSentKO << "/" << numRecordsSentKO + numRecordsSentOK
           << " = " << (float) (100.0 * numRecordsSentKO) / (numRecordsSentOK + numRecordsSentKO) << "% errors" << ENDL;
  }
}


void checkSerialInput() {
  const char* ResumeInstructions = "'r'=resume, 'd'=detect other nodes, 'x'=XBee config...";
  char c;
  if ((c = CSPU_Test::keyPressed()) != 0) {
    if (!interrupted) {
      Serial << ENDL << myself << ": emission suspended. " << ResumeInstructions << ENDL;
      interrupted = !interrupted;
    }
    else {
      switch (c) {
        case 'd':
          xbc.printDiscoveredNodes();
          Serial << myself << ": " << ResumeInstructions << ENDL;
          break;
        case 'r':
          interrupted = !interrupted;
          Serial << myself << ": Resuming..." << ENDL;
          break;
        case 'x':
          xbc.printConfigurationSummary(Serial);
          Serial << myself << ": " << ResumeInstructions << ENDL;
          break;
        default:
          Serial << myself << ": ignored '" << c << "'. " << ResumeInstructions << ENDL;
      } // switch
    } // interrupted
  } // key pressed
} // checkSerial

void printCurrentChannel() {
  uint8_t channel;
  if (xbc.queryParameter("CH", channel) )
  {
    Serial << "Current channel: " << channel << ENDL;
  } else {
    Serial << "*** Error querying channel" << ENDL;
  }
}

void sentRecordTo(uint32_t sh, uint32_t sl, char c) {
  if (xbc.send(myRecord, RF_AckDelay, sh, sl)) {
    Serial << c ;
    charCounter++;
    numRecordsSentOK++;
  } else {
    numRecordsSentKO++;
    if (DetailedErrorMsgOnEmission) {
      Serial << "*** " << myself  << ": Error sending record (" << ++numErrors << ")" << ENDL;
      Serial << "    Destination: "; PrintHexWithLeadingZeroes(Serial, sh);
      Serial << " - "; PrintHexWithLeadingZeroes(Serial, sl);
      Serial << ENDL;
    } else Serial << '*';
  }
  if (charCounter > MaxLineLength) {
    Serial << ENDL;
    charCounter = 0;
  }
}

void sendRecord() {
  myRecord.timestamp++;
  switch (GMiniSelectedRF_Strategy) {
    case GMiniRF_Strategy::MainCanAsRepeater:
      sentRecordTo(GM_MainCan_XBeeAddressSH, GM_MainCan_XBeeAddressSL, 'M');
      break;
    case GMiniRF_Strategy::DoubleTransmissionFromSubcans:
      sentRecordTo(GM_MainCan_XBeeAddressSH, GM_MainCan_XBeeAddressSL, 'M');
      sentRecordTo(GM_Ground_XBeeAddressSH, GM_Ground_XBeeAddressSL, 'G');
      break;
    default:
      Serial << "*** " << myself << ": Error: unsupported RF_Strategy: '" << xbc.getLabel(GMiniSelectedRF_Strategy) << "'" << ENDL;
      Serial << "Aborted" << ENDL;
      Serial.flush();
      exit(-1);
  } // switch
}

void setup() {
  DINIT(115200);

  pinMode(LED_BUILTIN, OUTPUT);
  Serial << ENDL << ENDL;
  Serial << "***** SUB-CAN SKETCH *****" << ENDL;
  Serial << "Initializing Serials and communications..." << ENDL;
  RF.begin(115200);
  xbc.begin(RF); // no return value
// TMP pour test:
if (xbc.setParameter("NH", (uint8_t) 1))
{
  Serial << "AAA Set NH to 1"<< ENDL;
}
else Serial << "AAA Error setting NH" << ENDL;

  xbc.printConfigurationSummary(Serial);
  auto component = xbc.getXBeeSystemComponent();
  strcpy(myself, xbc.getLabel(component));


  // Check we have an appropriate RF module connected.
  switch (component) {
    case GMiniXBeeClient::GMiniComponent::SubCan1:
    case GMiniXBeeClient::GMiniComponent::SubCan2:
    case GMiniXBeeClient::GMiniComponent::SubCan3:
    case GMiniXBeeClient::GMiniComponent::SubCan4:
      Serial << "Simulating " << myself << "...." << ENDL;
      break;
    default:
      Serial << "*** Error: wrong XBee module detected: " << myself
             << " instead of one of the subcan modules" << ENDL;
      Serial << "Aborted." << ENDL;
      Serial.flush();
      exit(-1);
  }

  // Check the module is properly configured
  if (!xbc.checkXBeeModuleConfiguration()) {
    Serial << "*** Error: fix module configuration with XBee_ConfigureGMiniModule.ino  before running the test!" << ENDL;
    Serial << "Aborted." << ENDL;
    Serial.flush();
    exit(-1);
  }

  // At this stage, the RF module is ok!
  MyCanID = (CansatCanID) component;
  myRecord.initValues();
  myRecord.sourceID = MyCanID;

  Serial << "Initialisation over. Test record content (size = " << sizeof(myRecord) << " bytes):" << ENDL;
  myRecord.print(Serial);
  Serial << ENDL << "Test string : " << ENDL;
  Serial << "'" << GMiniRecordExample::testString << "' (length=" << strlen(GMiniRecordExample::testString) << ")" << ENDL;
  Serial << "-------------------" << ENDL << ENDL;

  Serial << "RF_Strategy: '" << xbc.getLabel(GMiniSelectedRF_Strategy) << "'" << ENDL;
  switch (GMiniSelectedRF_Strategy) {
    case GMiniRF_Strategy::MainCanAsRepeater:
      Serial << "Sending record to main can (ID=" << xbc.getXBeeModuleID(GM_MainCan_XBeeAddressSH, GM_MainCan_XBeeAddressSL) << ", ";
      break;
    case GMiniRF_Strategy::DoubleTransmissionFromSubcans:
      Serial << "Sending record to main can (ID=" << xbc.getXBeeModuleID(GM_MainCan_XBeeAddressSH, GM_MainCan_XBeeAddressSL) << ", ";
      PrintHexWithLeadingZeroes(Serial, GM_MainCan_XBeeAddressSH);
      Serial << " - ";
      PrintHexWithLeadingZeroes(Serial, GM_MainCan_XBeeAddressSL);
      Serial << ")" << ENDL;
      Serial << "AND to Ground (ID=" << xbc.getXBeeModuleID(GM_Ground_XBeeAddressSH, GM_Ground_XBeeAddressSL) << ", ";
      PrintHexWithLeadingZeroes(Serial, GM_Ground_XBeeAddressSH);
      Serial << " - ";
      PrintHexWithLeadingZeroes(Serial, GM_Ground_XBeeAddressSL);
      Serial << ")" << ENDL;
      break;
    default:
      Serial << "*** " << myself << ": Error: unsupported RF_Strategy: '" << xbc.getLabel(GMiniSelectedRF_Strategy) << "'" << ENDL;
      Serial << "Aborted" << ENDL;
      Serial.flush();
      exit(-1);
  } // switch
  Serial << ENDL << "---------------------------------------" << ENDL;
  Serial << " - Sending records every " << CansatAcquisitionPeriod << " ms" << ENDL;
  Serial << "Starting simulation of " << myself << ".... (type any character+<return> to interrupt)" << ENDL << ENDL;
}

void loop() {
  if (!interrupted) {
    if (RegularlyDisplayChannel && (elapsedSinceChannelPrint > ChannelPrintPeriod)) {
      printCurrentChannel();
      elapsedSinceChannelPrint = 0;
    }
    if (elapsed > CansatAcquisitionPeriod) {
      elapsed = 0;
      sendRecord();
    }
  } // ! interrupted
  checkSerialInput();
  CSPU_Test::heartBeat();
  printStats();
}
