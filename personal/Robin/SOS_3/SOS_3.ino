#include "SOS.h"
const uint8_t ledPin = 11;
const uint8_t ledPin2 = 12;

SOS mySOS;
SOS mySOS2;

void setup() {
  Serial.begin(115200);
  if (!mySOS.begin(ledPin) || !mySOS2.begin(ledPin2)) {
    Serial.println("ERROR");
    exit(2);
  }
  
}

void loop() {
  mySOS.blink();
  mySOS2.blink();
  delay(3000);
}
