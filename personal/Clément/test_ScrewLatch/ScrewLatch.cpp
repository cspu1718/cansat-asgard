#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "SrewLatch.h"

bool ScrewLatch::begin() {
  pinMode(MotorForward1, OUTPUT);
  pinMode(MotorReverse1, OUTPUT);
  pinMode(enableBridge1, OUTPUT);
  return true;
}

void ScrewLatch::lock () {
  myScrew.digitalWrite(enableBridge1, HIGH); // Active pont en H
  myScrew.analogWrite(MotorReverse1, 0);
  myScrew.analogWrite(MotorForward1, Power);
  delay(DelayForwardLong);
  myScrew.analogWrite(MotorForward1, 0);
  myScrew.analogWrite(MotorReverse1, 0);
}

void ScrewLatch::unlock () {
  myScrew.digitalWrite(enableBridge1, HIGH); // Active pont en H
  myScrew.analogWrite(MotorReverse1, 0);
  myScrew.analogWrite(MotorForward1, Power);
  delay(DelayForwardShort);
  myScrew.analogWrite(MotorForward1, 0);
  myScrew.analogWrite(MotorReverse1, 0);
}

void ScrewLatch::stop () {
  analogWrite(MotorForward1, 0);
  analogWrite(MotorReverse1, 0);
  digitalWrite(enableBridge1, LOW); // désactvie le pont en H
};
