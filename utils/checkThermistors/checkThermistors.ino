/*
    This program tests the thermistors (1, 2 or 3 depending on configuration) 
    on any Cansat using a dedicated voltage regulator.
    It is assumed that the regulator output is used as external analog reference.

    This program only support Feather M0 Express and Genuino Uno.

*/

// (Constants describing wiring and thermistor models are now in their classes).

#define DEBUG
#include "DebugCSPU.h"
#include "CansatConfig.h"

// Include header files for the right thermistor classes defined in
// CansatConfig.h. This requires some preprocessor tricks...
// See https://stackoverflow.com/questions/32066204/construct-path-for-include-directive-with-macro
// for details.
#define IDENT(x) x
#define XSTR(x) #x
#define STR(x) XSTR(x)
#define PATH(x,y) STR(IDENT(x)IDENT(y))
#define H_EXTENSION .h

#include PATH(THERMISTOR1_CLASS,H_EXTENSION)
#ifdef INCLUDE_THERMISTOR2
#include PATH(THERMISTOR2_CLASS,H_EXTENSION)
#endif
#ifdef INCLUDE_THERMISTOR3
#include PATH(THERMISTOR3_CLASS,H_EXTENSION)
#endif


#ifdef ARDUINO_AVR_UNO
constexpr float Vcc=5.0;
#elif defined(ARDUINO_ARCH_SAMD)
constexpr float Vcc=3.3;
#else
#error "Board not recognized"
#endif

THERMISTOR1_CLASS therm1(	ThermistorTension,
							Thermistor1_AnalogInPinNbr, Thermistor1_Resistor);
#ifdef INCLUDE_THERMISTOR2
THERMISTOR2_CLASS therm2(ThermistorTension,
						 Thermistor2_AnalogInPinNbr, Thermistor2_Resistor);
#endif
#ifdef INCLUDE_THERMISTOR3
THERMISTOR3_CLASS therm3(ThermistorTension,
					     Thermistor3_AnalogInPinNbr, Thermistor3_Resistor);
#endif

void setup() {
  DINIT(115200);
  Serial << "  Vcc = "<< Vcc << "V" <<ENDL;
  
  analogReference(AR_EXTERNAL); // AR_EXTERNAL, no external for Feather M0 Express
  Serial << "Set reference tension to AR_EXTERNAL" << ENDL;

  Serial << "Thermistor configuration: Vcc=" << ThermistorTension << "V" << ENDL;
  Serial << "  1: " << STR(THERMISTOR1_CLASS) << ", pin=A" << Thermistor1_AnalogInPinNbr - A0 << ", R=" << Thermistor1_Resistor << ENDL;
#ifdef INCLUDE_THERMISTOR2
  Serial << "  2: " << STR(THERMISTOR2_CLASS) << ", pin=A" << Thermistor2_AnalogInPinNbr - A0 << ", R=" << Thermistor2_Resistor << ENDL;
#endif
#ifdef INCLUDE_THERMISTOR3
  Serial << "  3: " << STR(THERMISTOR3_CLASS) << ", pin=A" << Thermistor3_AnalogInPinNbr - A0 << ", R=" << Thermistor3_Resistor << ENDL;
#endif
}

void loop() {
  Serial <<  millis() << ": 1) " << therm1.readResistance() << " ohms, " << therm1.readTemperature() << "°C";
#ifdef INCLUDE_THERMISTOR2
  Serial <<  "     2) " << therm2.readResistance() << " ohms, " << therm2.readTemperature() << "°C";
#endif    
#ifdef INCLUDE_THERMISTOR3
  Serial <<  "     3) " << therm3.readResistance() << " ohms, " << therm3.readTemperature() << "°C";
#endif
  Serial << ENDL;

  delay(1000);
}
