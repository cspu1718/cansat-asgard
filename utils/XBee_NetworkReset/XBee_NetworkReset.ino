/**
  Utility sktech to trigger a network reset on a XBee module 
  Tested with Feather M0 board (should work with any other SAMD board...)

  Wiring: µC to XBee module
   3.3V to VCC
   RX   to DOUT  CONFIRMED although it is the opposite to connect to XCTU!
   TX   to DIN   CONFIRMED although it is the opposite to connect to XCTU!
   GND  to GND
*/

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#ifndef ARDUINO_ARCH_SAMD
#error "This program only works on SAMD boards"
#endif

#include "CansatConfig.h"
#include "XBeeClient.h"
#include "XBee.h"
#include "CSPU_Test.h"

#ifndef RF_ACTIVATE_API_MODE
#error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

HardwareSerial &RF = Serial1;
XBeeClient xbc(0x1234, 0x5678);  // adresses are irrelevant here

void setup() {
  DINIT(115200);
  Serial << "***** XBee Network Reset sketch *****" << ENDL;
  Serial << "Initialising Serials and communications..." << ENDL;
  RF.begin(115200);
  xbc.begin(RF);  // no return value

  xbc.printConfigurationSummary(Serial);

  Serial << ENDL << "Initialisation OK." << ENDL;

  auto deviceType = xbc.getDeviceType();
  Serial << "Ready to perform a Network Reset." << ENDL;
  Serial << "This will preserve the configuration except for the Operating 16-bits PAN Id." << ENDL;
  if (deviceType == XBeeTypes::DeviceType::Coordinator) {
    Serial << "WARNING: This node is a Coordinator: performing a network reset" << ENDL;
    Serial << "         will prevent any previously connected node to connect" << ENDL;
    Serial << "         again, without a Network Reset on each node. " << ENDL;
  } else {
    Serial << "This node is a Router or an End-device. A Network Reset will allow the module" << ENDL;
    Serial << "to connect to a network with the same PAN ID but managed by another Coordinator" << ENDL;
    Serial << "than the one that managed the first network the node joined." << ENDL;
  }
  Serial << "This could require power-cycling the module (disconnect/reconnect) after the procedure." << ENDL;
  uint16_t iValue;
  if (!xbc.queryParameter("OI", iValue, 0, 0)) {
    Serial << "*** Error retrieving current 'OI' value. ***" << ENDL;
  }
  Serial << "Current Operating 16-bits PAN Id (OI): 0x";
  Serial.println(iValue, HEX);
  if (iValue == 0xFFFF) {
    Serial << "INFO: Value OxFFFF means that this node never joined a network." << ENDL;
    Serial << "      Network Reset will have no effect." << ENDL;
  }
  if (CSPU_Test::askYN("*** Do you want to proceed ?")) {
    if (xbc.setParameter("NR", (uint8_t)0)) {
      Serial << "Network reset performed. " << ENDL;
      if (!xbc.queryParameter("OI", iValue, 0, 0)) {
        Serial << "*** Error retrieving new 'OI' value. ***" << ENDL;
      }
      Serial << "New Operating 16-bits PAN Id (OI): 0x";
      Serial.println(iValue, HEX);
    } else {
      Serial << "*** Error: Network reset failed." << ENDL;
    }

  } else {
    Serial << "Aborted." << ENDL;
  }
}

void loop() {
  delay(3000);
}
